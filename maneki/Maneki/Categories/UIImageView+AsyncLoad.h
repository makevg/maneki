//
// Created by Buravlev Mikhail on 20.12.15.
// Copyright (c) 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (AsyncLoad)

- (void)loadAsyncFromUrl:(NSString *)photoUrl;

- (void)loadAsyncFromUrl:(NSString *)photoUrl placeHolder:(NSString*) placeholder;

@end