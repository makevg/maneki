//
// Created by Buravlev Mikhail on 20.12.15.
// Copyright (c) 2015 Maneki. All rights reserved.
//

#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+AsyncLoad.h"


@implementation UIImageView (AsyncLoad)

- (void)loadAsyncFromUrl:(NSString *)photoUrl {
    [self loadAsyncFromUrl:photoUrl placeHolder:@"Placeholder"];
}

- (void)loadAsyncFromUrl:(NSString *)photoUrl placeHolder:(NSString *)placeholder {
//    dispatch_async(dispatch_get_main_queue(), ^() {
        [self sd_setImageWithURL:[NSURL URLWithString:photoUrl]
                placeholderImage:[UIImage imageNamed:placeholder]
                         options:(SDWebImageOptions) (SDWebImageLowPriority|SDWebImageRetryFailed)
                        progress:nil
                       completed:^(UIImage *image_, NSError *error, SDImageCacheType cacheType_, NSURL *url) {
                           if (!image_)
                               return;

//                                if (cacheType_ == SDImageCacheTypeNone) {
//                                    self.alpha = 0.0;
//                                }
//
//                                if (cacheType_ == SDImageCacheTypeNone) {
//                                    [UIView animateWithDuration:1.0
//                                                     animations:^{
//                                                         self.alpha = 1.0;
//                                                     }];
//                                }
                       }];
//    });
}


@end