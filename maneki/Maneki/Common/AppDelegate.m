//
//  AppDelegate.m
//  maneki
//
//  Created by Buravlev Mikhail on 15.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "AppDelegate.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MADB.h"
#import "TAGContainerOpener.h"
#import "TAGManager.h"
#import "AppsFlyerTracker.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <VK-ios-sdk/VKSdk.h>

@interface AppDelegate () <TAGContainerOpenerNotifier>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Fabric with:@[[Crashlytics class]]];
    self.tagManager = [TAGManager instance];
    [self.tagManager.logger setLogLevel:kTAGLoggerLogLevelVerbose];
    [TAGContainerOpener openContainerWithId:@"GTM-MX53F5"
                                 tagManager:self.tagManager
                                   openType:kTAGOpenTypePreferFresh
                                    timeout:nil
                                   notifier:self];

    [self configureNavBar];

    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"gZkbJiRjbXmYmgLR4V88rd";
    [AppsFlyerTracker sharedTracker].appleAppID = @"1084665597";
    [AppsFlyerTracker sharedTracker].currencyCode = @"RUB";

    return YES;
}

- (void)configureNavBar {
    [[UINavigationBar appearance] setBarTintColor:[MAColorPalette backgroundColor]];
    [[UINavigationBar appearance] setTintColor:[MAColorPalette whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [MAColorPalette whiteColor],
            NSFontAttributeName : [MAFontSet navBatTitleFont]}];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UINavigationBar appearance] setTranslucent:NO];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL res = [VKSdk processOpenURL:url
                     fromApplication:sourceApplication];

    if (!res) {
        res = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                             openURL:url
                                                   sourceApplication:sourceApplication
                                                          annotation:annotation];

    }

    return res;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [MANEKI_DB saveContext];
}

- (void)containerAvailable:(TAGContainer *)container {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.container = container;
    });
}


@end
