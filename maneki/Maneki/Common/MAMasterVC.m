//
//  MAMasterVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 05.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAMasterVC.h"
#import "MAColorPalette.h"
#import "MACityChooseVC.h"
#import "MACommonService.h"
#import "MAWelcomeVC.h"

@interface MAMasterVC ()

@end

@implementation MAMasterVC

#pragma mark - Private

- (void)configureController {
    [[UIApplication sharedApplication].keyWindow setBackgroundColor:[MAColorPalette backgroundColor]];
    [MACommonService isLoggedIn] ? [self showWelcomeScreen] : [self showCityChooseScreen];
}

- (void)showWelcomeScreen {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[MAWelcomeVC storyboardName] bundle:nil];
    MAWelcomeVC *vc = [sb instantiateInitialViewController];
    vc.checkUser = YES;
    [[UIApplication sharedApplication] keyWindow].rootViewController = vc;
}

- (void)showCityChooseScreen {
    [self showFeatureByName:[MACityChooseVC storyboardName]];
}

- (void)showFeatureByName:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *vc = [sb instantiateInitialViewController];
    [[UIApplication sharedApplication] keyWindow].rootViewController = vc;
}

@end
