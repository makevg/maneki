//
//  MAProfileTextField.h
//  Maneki
//
//  Created by Максимычев Е.О. on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ProfileFieldType) {
    eRequiredType = 0,
    eOptionalType
};

@interface MAProfileTextField : UITextField

@property (nonatomic) ProfileFieldType type;

@end
