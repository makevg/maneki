//
//  MAProfileTextField.m
//  Maneki
//
//  Created by Максимычев Е.О. on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProfileTextField.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@implementation MAProfileTextField

#pragma mark - Init

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self configure];
    }
    return self;
}

#pragma mark - Public

- (void)setPlaceholder:(NSString *)placeholder {
    [super setPlaceholder:placeholder];
    UIColor *color = [self colorByType:self.type];
    
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder
                                                                 attributes:@{NSForegroundColorAttributeName:color,
                                                                              NSFontAttributeName:[MAFontSet contactFieldFont]}];
}

#pragma mark - Private

- (void)configure {
    [self setTextColor:[MAColorPalette blackColor]];
    [self setFont:[MAFontSet contactFieldFont]];
    [self setClearButtonMode:UITextFieldViewModeWhileEditing];
}

- (UIColor *)colorByType:(ProfileFieldType)type {
    UIColor *color;
    switch (type) {
        case eRequiredType:
            color = [MAColorPalette contactFieldPlaceholderRequiredTextColor];
            break;
        case eOptionalType:
            color = [MAColorPalette contactFieldPlaceholderTextColor];
            break;
        default:
            break;
    }
    return color;
}

@end
