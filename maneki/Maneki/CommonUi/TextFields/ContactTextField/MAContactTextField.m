//
//  MAContactTextField.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAContactTextField.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@implementation MAContactTextField

#pragma mark - Init

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self configure];
    }
    return self;
}

#pragma mark - Private

- (void)configure {
    [self setBackgroundColor:[MAColorPalette contactFieldBackgroundColor]];
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder
                                                                 attributes:@{NSForegroundColorAttributeName:[MAColorPalette contactFieldPlaceholderTextColor],
                                                                              NSFontAttributeName:[MAFontSet contactFieldFont]}];
    [self setTextColor:[MAColorPalette whiteColor]];
    [self setFont:[MAFontSet contactFieldFont]];
    [self setClearButtonMode:UITextFieldViewModeWhileEditing];
    [self prepareLeftPaddingView];
}

- (void)prepareLeftPaddingView {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 20)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

@end
