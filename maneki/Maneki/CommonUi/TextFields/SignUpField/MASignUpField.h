//
//  MASignUpField.h
//  Maneki
//
//  Created by Maximychev Evgeny on 23.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SignUpFieldType) {
    ePhoneType = 0,
    eCodeType
};

@interface MASignUpField : UITextField

@property (nonatomic) SignUpFieldType type;

@end
