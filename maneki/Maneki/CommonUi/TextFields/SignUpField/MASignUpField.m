//
//  MASignUpField.m
//  Maneki
//
//  Created by Maximychev Evgeny on 23.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASignUpField.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cSignUpFieldPaddingViewText = @"+7";

@implementation MASignUpField

#pragma mark - Init

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self configure];
    }
    return self;
}

- (void)setType:(SignUpFieldType)type {
    _type = type;
    [self configureFieldByType:type];
}

#pragma mark - Private

- (void)configure {
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder
                                                                 attributes:@{NSForegroundColorAttributeName:[MAColorPalette grayColor],
                                                                              NSFontAttributeName:[MAFontSet signUpFieldFont]}];
    [self setTextColor:[UIColor blackColor]];
    [self setFont:[MAFontSet signUpFieldFont]];
    [self setClearButtonMode:UITextFieldViewModeWhileEditing];
    [self setBottomBorder];
}

- (void)configureFieldByType:(SignUpFieldType)type {
    if (self.type == ePhoneType) {
        [self setLeftPaddingView];
    }
}

- (void)setLeftPaddingView {
    UILabel *label = [[UILabel alloc] init];
    [label setFrame:CGRectMake(0, 0, 25, CGRectGetHeight(self.frame))];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor blackColor]];
    label.text = cSignUpFieldPaddingViewText;
    label.font = [MAFontSet signUpFieldFont];
    self.leftView = label;
    self.leftViewMode = UITextFieldViewModeAlways;
}

- (void)setBottomBorder {
    CALayer *bottomBorder = [CALayer layer];
    CGFloat borderWidth = 1.0f;
    bottomBorder.borderColor = [[MAColorPalette grayColor] CGColor];
    bottomBorder.frame = CGRectMake(0, self.frame.size.height - borderWidth,
                                    self.frame.size.width,
                                    self.frame.size.height);
    bottomBorder.borderWidth = borderWidth;
    
    [self.layer addSublayer:bottomBorder];
    self.layer.masksToBounds = YES;
}

@end
