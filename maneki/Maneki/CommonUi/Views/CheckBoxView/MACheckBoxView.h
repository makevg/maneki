//
//  MACheckBoxView.h
//  Maneki
//
//  Created by Максимычев Е.О. on 12.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MACheckBoxView : UIImageView

@property (nonatomic) BOOL isChecked;
@property (nonatomic) UIImage *uncheckedImage;
@property (nonatomic) UIImage *checkedImage;

@end
