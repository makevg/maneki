//
//  MACheckBoxView.m
//  Maneki
//
//  Created by Максимычев Е.О. on 12.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MACheckBoxView.h"

@implementation MACheckBoxView

- (void)awakeFromNib {
    [self configure];
}

#pragma mark - Private

- (void)configure {
    self.isChecked = NO;
    self.userInteractionEnabled = YES;
    [self prepareImages];
    [self prepareImageByState:self.isChecked];
    [self prepareTapRecognizer];
}

- (void)prepareTapRecognizer {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(performAction)];
    [self addGestureRecognizer:tapGestureRecognizer];
}

- (void)prepareImages {
    self.uncheckedImage = [UIImage imageNamed:@"checkbox_unchecked"];
    self.checkedImage = [UIImage imageNamed:@"checkbox_checked"];
}

- (void)prepareImageByState:(BOOL)state {
    UIImage *image = state ? self.checkedImage : self.uncheckedImage;
    self.image = image;
}

- (void)performAction {
    self.isChecked = !self.isChecked;
    [self prepareImageByState:self.isChecked];
}

@end
