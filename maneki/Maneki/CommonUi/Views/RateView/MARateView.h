//
//  MABaseView.h
//  Maneki
//
//  Created by Maximychev Evgeny on 28.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    RateViewAlignmentLeft,
    RateViewAlignmentCenter,
    RateViewAlignmentRight
} RateViewAlignment;

@protocol MARateViewDelegate;

@interface MARateView : UIView {
    UIImage *_fullStarImage;
    UIImage *_emptyStarImage;
    CGPoint _origin;
    NSInteger _numOfStars;
}

@property(nonatomic) RateViewAlignment alignment;
@property(nonatomic) CGFloat rate;
@property(nonatomic) CGFloat padding;
@property(nonatomic) BOOL editable;
@property(nonatomic) UIImage *fullStarImage;
@property(nonatomic) UIImage *emptyStarImage;
@property(weak, nonatomic) id<MARateViewDelegate> delegate;

- (MARateView *)initWithFrame:(CGRect)frame;
- (MARateView *)initWithFrame:(CGRect)rect fullStar:(UIImage *)fullStarImage emptyStar:(UIImage *)emptyStarImage;

@end

@protocol MARateViewDelegate

- (void)rateView:(MARateView *)rateView changedToNewRate:(NSNumber *)rate;

@end
