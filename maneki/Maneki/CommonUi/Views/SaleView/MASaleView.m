//
//  MASaleView.m
//  Maneki
//
//  Created by Максимычев Е.О. on 02.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASaleView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@implementation MASaleView

#pragma mark - Init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.mainView];
        [self configureView];
    }
    return self;
}

#pragma mark - Private

- (void)configureView {
    [self prepareConstraints];
    [self configureSubviews];
}

- (void)prepareConstraints {
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
}

- (void)configureSubviews {
    [self setBackgroundColor:[UIColor clearColor]];
    [self.mainView setBackgroundColor:[MAColorPalette orangeColor]];
    self.mainView.layer.cornerRadius = self.frame.size.width/2;
    [self.saleLabel setTextColor:[MAColorPalette whiteColor]];
    UIFont *saleLabelFont = [self saleLabelTextFont];
    [self.saleLabel setFont:saleLabelFont];
    self.saleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
}

- (UIFont *)saleLabelTextFont {
    CGFloat fontSize = self.frame.size.width/2.2;
    return [UIFont fontWithName:cBoldFontName size:fontSize];
}

#pragma mark - Public

- (void)prepareWithSale:(NSUInteger)sale {
    NSString *saleString = [NSString stringWithFormat:@"%lu%%", (unsigned long)sale];
    [self.saleLabel setText:saleString];
}

@end
