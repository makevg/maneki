//
//  MASaleView.h
//  Maneki
//
//  Created by Максимычев Е.О. on 02.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MASaleView : MABaseView

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *saleLabel;

- (void)prepareWithSale:(NSUInteger)sale;

@end
