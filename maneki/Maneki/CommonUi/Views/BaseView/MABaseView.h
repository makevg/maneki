//
//  MABaseView.h
//  Maneki
//
//  Created by Maximychev Evgeny on 19.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MABaseView : UIView

+ (UIView *)loadViewByNibName:(NSString *)nibName;

- (void)setup;
- (void)setModel:(id)model;

@end
