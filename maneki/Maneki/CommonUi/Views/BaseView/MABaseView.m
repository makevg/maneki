//
//  MABaseView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 19.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABaseView.h"

@implementation MABaseView

#pragma mark - Init

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setup];
}

#pragma mark - Public

+ (UIView *)loadViewByNibName:(NSString *)nibName {
    NSArray *viewNib = [[NSBundle mainBundle] loadNibNamed:nibName
                                                     owner:self
                                                   options:nil];
    return (UIView *)[viewNib firstObject];
}

- (void)setup {
    // Abstract method.
}

- (void)setModel:(id)model {
    // Abstract method.
}

- (void)startAnimation {
}

- (void)stopAnimation {
}

@end
