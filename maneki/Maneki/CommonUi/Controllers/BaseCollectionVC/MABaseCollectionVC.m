//
// Created by Buravlev Mikhail on 20.12.15.
// Copyright (c) 2015 Maneki. All rights reserved.
//

#import "MABaseCollectionVC.h"
#import "MAFontSet.h"
#import "MABaseTableVC.h"
#import "UIScrollView+EmptyDataSet.h"
#import "MADB.h"

@implementation MABaseCollectionVC {
    BOOL forceHideLoading;
}

#pragma mark - Private

- (void)setFetchResult:(NSFetchedResultsController *)fetchResult {
    _fetchResult = fetchResult;
    _fetchResult.delegate = self;
    forceHideLoading = [[self.fetchResult fetchedObjects] count] != 0;

    if (forceHideLoading)
        [self hideLoadingState];
}

- (void)viewDidLoad {
    forceHideLoading = NO;
    [self prepareLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                   forView:[self getCollectionView]];

    [super viewDidLoad];
}

- (void)showLoadingState {
    if (![[self.fetchResult fetchedObjects] count])
        [super showLoadingState];
}

- (void)hideLoadingState {
    if (forceHideLoading || ![[MANEKI_DB produceContextForRead] hasChanges])
        [super hideLoadingState];
};

#pragma mark - UICollectionViewDatasource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [[self.fetchResult sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchResult sections][(NSUInteger) section];
    return [sectionInfo numberOfObjects];
}

#pragma mark - Public

- (UICollectionView *)getCollectionView {
    return nil;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return ![self isLoading];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    if ([self isLoading])
        return nil;

    NSString *error = [self requestErrorDescription];
    return error ? [UIImage imageNamed:cSadKittyImageName] : nil;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    if ([self isLoading])
        return nil;

    NSString *error = [self requestErrorDescription];
    if (error) {
        NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
        paragraph.lineBreakMode = NSLineBreakByWordWrapping;
        paragraph.alignment = NSTextAlignmentCenter;

        NSDictionary *attributes = @{NSFontAttributeName : [MAFontSet navBatTitleFont],
                NSForegroundColorAttributeName : [self colorForErrorTitle],
                NSParagraphStyleAttributeName : paragraph};

        return [[NSAttributedString alloc] initWithString:error attributes:attributes];
    }
    return nil;
}

- (UIColor *)colorForErrorTitle {
    return [UIColor blackColor];
}

#pragma mark - NSFetchResultControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    forceHideLoading = YES;
    [self hideLoadingState];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [[self getCollectionView] reloadEmptyDataSet];
    [[self getCollectionView] reloadData];
}

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];
}

- (void)serverError:(NSError *)error forService:(Class)service {
    forceHideLoading = YES;
    [super serverError:error forService:service];

    [[self getCollectionView] reloadData];
}

- (void)networkError:(NSError *)error forService:(Class)service {
    forceHideLoading = YES;
    [super networkError:error forService:service];

    [[self getCollectionView] reloadData];
}

@end