//
// Created by Buravlev Mikhail on 20.12.15.
// Copyright (c) 2015 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MABaseVC.h"
#import <CoreData/CoreData.h>
#import "MABaseView.h"


@interface MABaseCollectionVC : MABaseVC <UICollectionViewDataSource, NSFetchedResultsControllerDelegate>

@property(nonatomic, strong) NSFetchedResultsController *fetchResult;

- (UICollectionView *)getCollectionView;

- (UIColor *)colorForErrorTitle;

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView;
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView;

@end