//
//  MABaseTableVC.m
//  Maneki
//
//  Created by Максимычев Е.О. on 10.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "MABaseTableVC.h"
#import "MAFontSet.h"

NSString *cSadKittyImageName = @"SadKitty";

@interface MABaseTableVC () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@end

@implementation MABaseTableVC

- (void)viewDidLoad {
    [self prepareLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                   forView:[self getTableView]];

    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)showLoadingState {
    if (![[self.fetchResult fetchedObjects] count])
        [super showLoadingState];
}

#pragma mark - Public

- (UITableView *)getTableView {
    return nil;
}

- (void)setFetchResult:(NSFetchedResultsController *)fetchResult {
    _fetchResult = fetchResult;
    _fetchResult.delegate = self;

    if ([[self.fetchResult fetchedObjects] count])
        [self hideLoadingState];
}

- (void)prepareTableView {
    [self setDelegatesAndDataSource];
}

- (void)setDelegatesAndDataSource {
    [self getTableView].dataSource = self;
    [self getTableView].delegate = self;
    [self getTableView].emptyDataSetSource = self;
    [self getTableView].emptyDataSetDelegate = self;
}

- (void)scrollToTop {
    NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:0
                                                        inSection:0];
    [[self getTableView] scrollToRowAtIndexPath:indexPathToScroll
                               atScrollPosition:UITableViewScrollPositionTop
                                       animated:YES];
}

- (void)requestData {
    [super requestData];

    [[self getTableView] reloadEmptyDataSet];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchResult sections][(NSUInteger) section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }

    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - NSFetchResultControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self hideLoadingState];

    [[self getTableView] beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type {
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [[self getTableView] insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                               withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [[self getTableView] deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                               withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [[self getTableView] insertRowsAtIndexPaths:@[newIndexPath]
                                       withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [[self getTableView] deleteRowsAtIndexPaths:@[indexPath]
                                       withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            //[self configureCell:[[self tableView] cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [[self getTableView] deleteRowsAtIndexPaths:@[indexPath]
                                       withRowAnimation:UITableViewRowAnimationFade];
            [[self getTableView] insertRowsAtIndexPaths:@[newIndexPath]
                                       withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [[self getTableView] endUpdates];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    if ([self isLoading])
        return nil;

    NSString *error = [self requestErrorDescription];
    return error ? [UIImage imageNamed:cSadKittyImageName] : nil;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    if ([self isLoading])
        return nil;

    NSString *error = [self requestErrorDescription];
    if (error) {
        NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
        paragraph.lineBreakMode = NSLineBreakByWordWrapping;
        paragraph.alignment = NSTextAlignmentCenter;

        NSDictionary *attributes = @{NSFontAttributeName : [MAFontSet navBatTitleFont],
                NSForegroundColorAttributeName : [self colorForErrorTitle],
                NSParagraphStyleAttributeName : paragraph};

        return [[NSAttributedString alloc] initWithString:error attributes:attributes];
    }
    return nil;
}

- (UIColor *)colorForErrorTitle {
    return [UIColor blackColor];
}

#pragma mark - ServiceResultDelegate

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];

    [[self getTableView] reloadData];
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];

    [[self getTableView] reloadData];
}

@end
