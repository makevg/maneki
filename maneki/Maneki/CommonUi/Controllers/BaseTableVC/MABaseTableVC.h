//
//  MABaseTableVC.h
//  Maneki
//
//  Created by Максимычев Е.О. on 10.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "MABaseVC.h"

extern NSString *cSadKittyImageName;

@class MABaseView;

@interface MABaseTableVC : MABaseVC <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property(nonatomic) NSFetchedResultsController *fetchResult;

- (UITableView *)getTableView;

- (void)prepareTableView;

- (void)scrollToTop;

- (UIColor *)colorForErrorTitle;

@end
