//
//  MABaseVC.h
//  Maneki
//
//  Created by Maximychev Evgeny on 21.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RESideMenu/RESideMenu.h>
#import "MAServiceLayer.h"
#import "PCAngularActivityIndicatorView.h"

extern NSString *const MABaseVCUpdateContentNotification;

@interface MABaseVC : UIViewController <ServiceResultDelegate>

+ (NSString *)storyboardName;

+ (NSString *)identifier;

- (void)configureController;

- (void)showFeatureByName:(NSString *)storyboardName;

- (void)presentModalFeatureByName:(NSString *)storyboardName animated:(BOOL)animated;

- (void)pushFeatureByName:(NSString *)storyboardName animated:(BOOL)animated;

- (void)setRootFeatureByName:(NSString *)storyboardName;

- (void)hideMenuViewController;

- (void)cancelsTouches:(BOOL)cancel;

- (void)showMessage:(NSString *)message;

- (void)networkStateDidChanged:(NSNotification *)notification;

- (BOOL)showCartButton;
- (void)updateCartButton;

- (void)requestData;
- (BOOL)isDataNonLoaded;
- (void)setIsDataNotLoaded:(BOOL)isDataNotLoaded;
- (NSString *)requestErrorDescription;

- (void)prepareLoadingIndicatorWithStyle:(PCAngularActivityIndicatorViewStyle)style
                                 forView:(UIView *)view;
- (PCAngularActivityIndicatorView *)getLoadingIndicatorWithStyle:(PCAngularActivityIndicatorViewStyle)style
                                                         forView:(UIView *)view;

- (BOOL)isLoading;
- (void)showLoadingState;
- (void)showLoadingStateForce;
- (void)hideLoadingState;

- (void)addContentUpdateObserver:(id)observer selector:(SEL)selector;
- (void)removeObserver:(id)observer;

@end
