//
//  MABaseVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABaseVC.h"
#import "MAAPI.h"
#import "MACartButtonItem.h"
#import "MACartVC.h"
#import "MACartService.h"
#import "TSMessage.h"
#import "MAColorPalette.h"

NSString *const MABaseVCUpdateContentNotification = @"MABaseVCUpdateContentNotification";
NSString *const cBaseVCStoryboardName = @"MABaseVC";
NSString *const cBaseVCIdentifier = @"BaseVCIdentifier";
NSString *const cCartMessage = @"Корзина пуста";

@interface MABaseVC ()
@end

@implementation MABaseVC {
    PCAngularActivityIndicatorView *_loadingIndicator;
    UITapGestureRecognizer *_tapGestureRecognizer;
    MACartButtonItem *_cartButtonItem;
    BOOL _dataNotLoaded;
    NSError *_error;
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self localConfigure];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkStateDidChanged:)
                                                 name:kNetworkChangeStateNotification
                                               object:nil];
    if ([self showCartButton]) {
        [self prepareCartButton];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNetworkChangeStateNotification
                                                  object:nil];
}

#pragma mark - Private

- (void)localConfigure {
    _dataNotLoaded = FALSE;

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self prepareTapRecognizer];
    [self configureController];
}

- (void)prepareTapRecognizer {
    _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                    action:@selector(performAction)];
    [self.view addGestureRecognizer:_tapGestureRecognizer];
}

- (void)openCart {
    if (![CART_SERVICE cartIsEmpty]) {
        [self pushFeatureByName:[MACartVC storyboardName] animated:YES];
    } else {
        [self showMessage:cCartMessage];
    }
}

- (void)performAction {
    [self.view endEditing:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cBaseVCStoryboardName;
}

+ (NSString *)identifier {
    return cBaseVCIdentifier;
}

- (void)configureController {
    // Abstract method.
}

- (void)showFeatureByName:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *vc = [sb instantiateInitialViewController];
    self.sideMenuViewController.contentViewController = vc;
    [self.sideMenuViewController hideMenuViewController];
}

- (void)presentModalFeatureByName:(NSString *)storyboardName animated:(BOOL)animated {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *vc = [sb instantiateInitialViewController];
    [self presentViewController:vc animated:animated completion:nil];
}

- (void)pushFeatureByName:(NSString *)storyboardName animated:(BOOL)animated {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *vc = [sb instantiateInitialViewController];
    [self.navigationController pushViewController:vc animated:animated];
}

- (void)setRootFeatureByName:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *vc = [sb instantiateInitialViewController];
    [[UIApplication sharedApplication] keyWindow].rootViewController = vc;
}

- (void)hideMenuViewController {
    [self.sideMenuViewController hideMenuViewController];
}

- (void)cancelsTouches:(BOOL)cancel {
    _tapGestureRecognizer.cancelsTouchesInView = cancel;
}

- (void)showMessage:(NSString *)message {
    [TSMessage showNotificationWithMessage:message];
}

- (BOOL)showCartButton {
    return NO;
}

- (void)prepareCartButton {
    _cartButtonItem = [[MACartButtonItem alloc] initWithTarget:self
                                                        action:@selector(openCart)];
    [_cartButtonItem updateByCost:(NSUInteger) [CART_SERVICE totalAmountByDeliveryType:CART_SERVICE.currentDeliveryMethod]];
    self.navigationItem.rightBarButtonItem = _cartButtonItem;
}

- (void)updateCartButton {
    [_cartButtonItem updateByCost:(NSUInteger) [CART_SERVICE totalAmountByDeliveryType:CART_SERVICE.currentDeliveryMethod]];
}

- (void)networkStateDidChanged:(NSNotification *)notification {
    if ([self isDataNonLoaded] && [notification.object boolValue]) {
        _error = nil;

        [self setIsDataNotLoaded:FALSE];
        [self requestData];
    }
}

- (BOOL)isDataNonLoaded {
    return _dataNotLoaded;
}

- (void)setIsDataNotLoaded:(BOOL)isDataNotLoaded {
    _dataNotLoaded = isDataNotLoaded;
}

- (NSString *)requestErrorDescription {
    return [MANEKI_API isOnline] ? _error.localizedDescription : @"Проблемы с подключением к интернету";
}

- (void)prepareLoadingIndicatorWithStyle:(PCAngularActivityIndicatorViewStyle)style
                                 forView:(UIView *)view {
    _loadingIndicator = [self getLoadingIndicatorWithStyle:style forView:view];
}

- (PCAngularActivityIndicatorView *)getLoadingIndicatorWithStyle:(PCAngularActivityIndicatorViewStyle)style forView:(UIView *)view {
    PCAngularActivityIndicatorView *loadingIndicator = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
    loadingIndicator.color = [MAColorPalette orangeColor];
    loadingIndicator.translatesAutoresizingMaskIntoConstraints = NO;

    [view addSubview:loadingIndicator];
    [view addConstraints:@[
            [NSLayoutConstraint constraintWithItem:loadingIndicator
                                         attribute:NSLayoutAttributeCenterX
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:view
                                         attribute:NSLayoutAttributeCenterX
                                        multiplier:1
                                          constant:0],
            [NSLayoutConstraint constraintWithItem:loadingIndicator
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:view
                                         attribute:NSLayoutAttributeCenterY
                                        multiplier:1
                                          constant:0]
    ]];

    return loadingIndicator;
}

- (BOOL)isLoading {
    return [_loadingIndicator isAnimating];
}

- (void)showLoadingState {
    [_loadingIndicator startAnimating];
}

- (void)showLoadingStateForce {
    [_loadingIndicator startAnimating];
}

- (void)hideLoadingState {
    [_loadingIndicator stopAnimating];
}

- (void)requestData {
    [self showLoadingState];
}

#pragma mark - ServiceResultDelegate

- (void)successResult:(id)result forService:(Class) service {
    [self hideLoadingState];
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [self hideLoadingState];
    [self setIsDataNotLoaded:TRUE];

    _error = error;
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [self hideLoadingState];
    [self setIsDataNotLoaded:TRUE];

    _error = error;
}

#pragma mark - Observers

- (void)addContentUpdateObserver:(id)observer selector:(SEL)selector {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:selector
                                                 name:MABaseVCUpdateContentNotification
                                               object:nil];
}

- (void)removeObserver:(id)observer {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
