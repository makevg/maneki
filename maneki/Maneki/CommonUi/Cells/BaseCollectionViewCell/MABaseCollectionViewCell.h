//
//  MABaseCollectionViewCell.h
//  Maneki
//
//  Created by Maximychev Evgeny on 19.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MABaseCollectionViewCell : UICollectionViewCell

+ (NSString *)cellIdentifier;

- (void)setModel:(id)model;
- (void)configureCell;

@end
