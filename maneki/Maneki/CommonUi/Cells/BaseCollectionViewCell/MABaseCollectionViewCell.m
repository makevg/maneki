//
//  MABaseCollectionViewCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 19.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABaseCollectionViewCell.h"

NSString *const cBaseCollectionViewCellIdentifier = @"BaseCollectionViewCell";

@implementation MABaseCollectionViewCell

- (void)awakeFromNib {
    [self configureCell];
}

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cBaseCollectionViewCellIdentifier;
}

- (void)setModel:(id)model {
    // Abstract method.
}

- (void)configureCell {
    // Abstract method.
}

@end
