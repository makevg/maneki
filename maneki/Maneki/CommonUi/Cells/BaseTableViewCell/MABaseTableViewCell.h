//
//  MABaseTableViewCell.h
//  Maneki
//
//  Created by Maximychev Evgeny on 19.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MABaseTableViewCell : UITableViewCell

+ (NSString *)cellIdentifier;
+ (CGFloat)cellHeight;

- (void)setModel:(id)model;
- (void)configureCell;

@end
