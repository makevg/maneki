//
//  MABaseTableViewCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 19.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABaseTableViewCell.h"

NSString *const cBaseTableViewCellIdentifier = @"BaseTableViewCell";

@implementation MABaseTableViewCell

- (void)awakeFromNib {
    [self configureCell];
}

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cBaseTableViewCellIdentifier;
}

+ (CGFloat)cellHeight {
    return 44.f;
}

- (void)setModel:(id)model {
    // Abstract method.
}

- (void)configureCell {
    // Abstract method.
}

@end
