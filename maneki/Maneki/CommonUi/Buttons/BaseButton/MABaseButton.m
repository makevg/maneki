//
//  MABaseButton.m
//  Maneki
//
//  Created by Maximychev Evgeny on 07.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABaseButton.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@implementation MABaseButton

#pragma mark - Init

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self localInit];
    }
    return self;
}

#pragma mark - Private

- (void)localInit {
    [self setTitleColor:[MAColorPalette whiteColor] forState:UIControlStateNormal];
    self.titleLabel.font = [MAFontSet buttonFont];
    [self configure];
}

#pragma mark - Public

- (void)configure {
    // Abstract method.
}

@end
