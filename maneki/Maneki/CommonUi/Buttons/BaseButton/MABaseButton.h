//
//  MABaseButton.h
//  Maneki
//
//  Created by Maximychev Evgeny on 07.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MABaseButton : UIButton

- (void)configure;

@end
