//
//  MABottomButton.h
//  Maneki
//
//  Created by Maximychev Evgeny on 21.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseButton.h"

typedef NS_ENUM(NSInteger, MABottomButtonStyle) {
    MABottomButtonGreenStyle = 0,
    MABottomButtonOrangeStyle
};

@interface MABottomButton : MABaseButton

@property (nonatomic) MABottomButtonStyle style;

@end
