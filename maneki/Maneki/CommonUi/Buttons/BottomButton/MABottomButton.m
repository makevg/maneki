//
//  MABottomButton.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABottomButton.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@implementation MABottomButton

#pragma mark - Configure

- (void)configure {
    [self setEnabled:YES];
    [self setStyle:MABottomButtonGreenStyle];
    [self.titleLabel setFont:[MAFontSet buttonFont]];
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    self.backgroundColor = enabled ? [self getBackgroundColorByStyle:self.style] : [MAColorPalette grayColor];
    UIColor *textColor = enabled ? [MAColorPalette whiteColor] : [MAColorPalette lightGrayColor];
    [self setTitleColor:textColor
               forState:UIControlStateNormal];
}

- (void)setStyle:(MABottomButtonStyle)style {
    _style = style;
    self.backgroundColor = [self getBackgroundColorByStyle:_style];
}

#pragma mark - Private

- (UIColor *)getBackgroundColorByStyle:(MABottomButtonStyle)style {
    switch (style) {
        case MABottomButtonGreenStyle:
            return [MAColorPalette buttonColor];
        case MABottomButtonOrangeStyle:
            return [MAColorPalette orangeColor];
    }
}

@end
