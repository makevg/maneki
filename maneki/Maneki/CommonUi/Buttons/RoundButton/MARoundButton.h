//
//  MARoundButton.h
//  Maneki
//
//  Created by Maximychev Evgeny on 07.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseButton.h"

@interface MARoundButton : MABaseButton

@end
