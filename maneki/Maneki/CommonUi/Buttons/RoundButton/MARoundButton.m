//
//  MARoundButton.m
//  Maneki
//
//  Created by Maximychev Evgeny on 07.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MARoundButton.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@implementation MARoundButton

#pragma mark - Configure

- (void)configure {
    self.layer.cornerRadius = CGRectGetWidth(self.frame)/2;
    [self setTitleColor:[MAColorPalette whiteColor]
               forState:UIControlStateNormal];
    [self.titleLabel setFont:[self titleLabelFont]];
}

#pragma mark - Private

- (UIFont *)titleLabelFont {
    CGFloat fontSize = self.frame.size.width/1.1f;
    return [UIFont fontWithName:cRegularFontName size:fontSize];
}

@end
