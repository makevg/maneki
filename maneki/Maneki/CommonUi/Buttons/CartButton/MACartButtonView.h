//
//  MACartButtonView.h
//  Maneki
//
//  Created by Максимычев Е.О. on 25.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MACartButtonView : MABaseView

- (void)setCost:(NSUInteger)cost;

@end
