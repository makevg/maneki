//
//  MACartButtonItem.m
//  Maneki
//
//  Created by Максимычев Е.О. on 25.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MACartButtonItem.h"
#import "MACartButtonView.h"

@interface MACartButtonItem ()
@property (nonatomic) MACartButtonView *cartButtonView;
@property (nonatomic) UIButton *button;
@end

@implementation MACartButtonItem

#pragma mark - Init

- (MACartButtonItem *)initWithTarget:(id)target action:(SEL)action {
    if (self = [super init]) {
        self.cartButtonView = (MACartButtonView *)[MABaseView loadViewByNibName:NSStringFromClass([MACartButtonView class])];
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.frame = CGRectMake(0, 0, self.cartButtonView.frame.size.width, self.cartButtonView.frame.size.height);
        [self.button addSubview:self.cartButtonView];
        [self.button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
        self = [super initWithCustomView:self.button];
    }
    return self;
}

#pragma mark - Public

- (void)updateByCost:(NSUInteger)cost {
    [self.cartButtonView setCost:cost];
    self.button.frame = CGRectMake(self.button.frame.origin.x, self.button.frame.origin.y, self.cartButtonView.frame.size.width, self.cartButtonView.frame.size.height);
}

@end
