//
//  MACartButtonItem.h
//  Maneki
//
//  Created by Максимычев Е.О. on 25.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MACartButtonItem : UIBarButtonItem

- (MACartButtonItem *)initWithTarget:(id)target action:(SEL)action;
- (void)updateByCost:(NSUInteger)cost;

@end
