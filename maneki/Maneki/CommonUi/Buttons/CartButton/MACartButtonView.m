//
//  MACartButtonView.m
//  Maneki
//
//  Created by Максимычев Е.О. on 25.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MACartButtonView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@interface MACartButtonView ()
@property (weak, nonatomic) IBOutlet UIView *costView;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cartImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *costViewWidthConstraint;
@end

@implementation MACartButtonView

#pragma mark - Setup

- (void)setup {
    self.userInteractionEnabled = NO;
    self.backgroundColor = [UIColor clearColor];
    self.costView.backgroundColor = [MAColorPalette lightGreenColor];
    self.costView.layer.cornerRadius = 2.5f;
    self.costView.clipsToBounds = YES;
    self.costLabel.textColor = [MAColorPalette whiteColor];
    self.costLabel.font = [MAFontSet cartButtonFont];
}

#pragma mark - Private

- (void)prepareShadowForView:(UIView *)view {
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    view.layer.shadowOpacity = 0.2f;
    view.layer.shadowPath = shadowPath.CGPath;
}

#pragma mark - Public

- (void)setCost:(NSUInteger)cost {
    [self.costView setHidden:!cost];

    NSString *costText = [NSString stringWithFormat:@"%lu р.", (unsigned long)cost];
    NSDictionary *attributes = @{NSFontAttributeName:[MAFontSet cartButtonFont]};
    CGRect rect = [costText boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:attributes
                                         context:nil];
    CGFloat padding = 8.f;
//    self.costLabel.frame = CGRectMake(0, 0, rect.size.width, rect.size.height);
    self.costLabel.text = costText;
    self.costViewWidthConstraint.constant = rect.size.width + padding;

    [self prepareShadowForView:self.costView];
}

@end
