//
//  MAColorPalette.h
//  Maneki
//
//  Created by Maximychev Evgeny on 18.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MAColorPalette : NSObject

+ (UIColor *)backgroundColor;
+ (UIColor *)cityCellBackgroundColor;
+ (UIColor *)buttonColor;
+ (UIColor *)whiteColor;
+ (UIColor *)lightGreenColor;
+ (UIColor *)grayColor;
+ (UIColor *)mediumGrayColor;
+ (UIColor *)lightGrayColor;
+ (UIColor *)orangeColor;
+ (UIColor *)blackColor;
+ (UIColor *)thinLineViewBackgroundColor;
+ (UIColor *)contactFieldBackgroundColor;
+ (UIColor *)contactFieldPlaceholderTextColor;
+ (UIColor *)contactFieldPlaceholderRequiredTextColor;
+ (UIColor *)backMenuCellSelectedStateColor;
+ (UIColor *)newsCellDescriptionLabelColor;

@end
