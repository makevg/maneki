//
//  MAColorPalette.m
//  Maneki
//
//  Created by Maximychev Evgeny on 18.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAColorPalette.h"

@implementation MAColorPalette

+ (UIColor *)backgroundColor {
    return [UIColor colorWithRed:55/255.0f green:177/255.0f blue:0/255.f alpha:1.0f];
}

+ (UIColor *)cityCellBackgroundColor {
    return [UIColor colorWithRed:95/255.0f green:194/255.0f blue:51/255.0f alpha:1.0f];
}

+ (UIColor *)buttonColor {
    return [UIColor colorWithRed:42/255.0f green:165/255.0f blue:0/255.0f alpha:1.0f];
}

+ (UIColor *)whiteColor {
    return [UIColor whiteColor];
}

+ (UIColor *)lightGreenColor {
    return [UIColor colorWithRed:121/255.0f green:210/255.0f blue:85/255.0f alpha:1.0f];
}

+ (UIColor *)grayColor {
    return [UIColor colorWithRed:149/255.0f green:149/255.0f blue:149/255.0f alpha:1.0f];
}

+ (UIColor *)mediumGrayColor {
    return [UIColor colorWithRed:193/255.0f green:193/255.0f blue:193/255.0f alpha:1.0f];
}

+ (UIColor *)lightGrayColor {
    return [UIColor colorWithRed:233/255.0f green:233/255.0f blue:233/255.0f alpha:1.0f];
}

+ (UIColor *)orangeColor {
   return [UIColor colorWithRed:231/255.0f green:132/255.0f blue:0/255.0f alpha:1.0f];
}

+ (UIColor *)blackColor {
    return [UIColor blackColor];
}

+ (UIColor *)thinLineViewBackgroundColor {
    return [UIColor colorWithRed:80/255.0f green:188/255.0f blue:30/255.0f alpha:1.0f];
}

+ (UIColor *)contactFieldBackgroundColor {
    return [UIColor colorWithRed:95/255.0f green:194/255.0f blue:51/255.0f alpha:1.0f];
}

+ (UIColor *)contactFieldPlaceholderTextColor {
    return [UIColor colorWithRed:157/255.0f green:218/255.0f blue:137/255.0f alpha:1.0f];
}

+ (UIColor *)contactFieldPlaceholderRequiredTextColor {
    return [UIColor colorWithRed:238/255.0f green:106/255.0f blue:87/255.0f alpha:1.0f];
}

+ (UIColor *)backMenuCellSelectedStateColor {
    return [UIColor colorWithRed:51/255.0f green:159/255.0f blue:0/255.0f alpha:1.0f];
}

+ (UIColor *)newsCellDescriptionLabelColor {
    return [UIColor colorWithRed:136/255.0f green:136/255.0f blue:136/255.0f alpha:1.0f];
}

@end
