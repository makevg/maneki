//
//  MADataFormatter.h
//  Maneki
//
//  Created by Максимычев Е.О. on 11.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface MADataFormatter : NSObject

+ (NSDate *)dateByString:(NSString *)dateString;
+ (NSString *)stringByDate:(NSDate *)date;
+ (NSString *)stringByUnixTimeStamp:(NSNumber *)unixTimeStamp;
+ (NSString *)fullDateTimeStringByUnixTimeStamp:(NSNumber *)unixTimeStamp;
+ (NSNumber *)numberByString:(NSString *)string;
+ (NSString *)stringByObject:(NSObject *)object;
+ (NSString *)stringByHTML:(NSString *)htmlString;

@end
