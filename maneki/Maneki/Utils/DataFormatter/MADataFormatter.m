//
//  MADataFormatter.m
//  Maneki
//
//  Created by Максимычев Е.О. on 11.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MADataFormatter.h"

NSString *const cDateFormatDefault  = @"dd.MM.yyyy";
NSString *const cDateFormatWithTime = @"dd.MM.yyyy HH:mm:ss";
NSString *const cTimeFormat         = @"HH:mm";

@implementation MADataFormatter

+ (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"]];
    return dateFormatter;
}

+ (NSDate *)dateByString:(NSString *)dateString {
    NSDateFormatter *dateFormatter = [self dateFormatterWithFormat:cDateFormatWithTime];
    return [dateFormatter dateFromString:dateString];
}

+ (NSString *)stringByDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [self dateFormatterWithFormat:cDateFormatDefault];
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)stringByUnixTimeStamp:(NSNumber *)unixTimeStamp {
    long long int timeStamp = [unixTimeStamp longLongValue];
    NSTimeInterval interval = timeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [self stringByDate:date];
}

+ (NSString *)fullDateTimeStringByUnixTimeStamp:(NSNumber *)unixTimeStamp {
    long long int timeStamp = [unixTimeStamp longLongValue];
    NSTimeInterval interval = timeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDate *today = [NSDate date];
    NSDate *yesterday = [today dateByAddingTimeInterval:-86400.0];
    if ([self isSameDayWithFirstDate:date secondDate:today]) {
        NSDateFormatter *dateFormatter = [self dateFormatterWithFormat:cTimeFormat];
        return [NSString stringWithFormat:@"Сегодня, %@", [dateFormatter stringFromDate:date]];
    } else if ([self isSameDayWithFirstDate:date secondDate:yesterday]) {
        NSDateFormatter *dateFormatter = [self dateFormatterWithFormat:cTimeFormat];
        return [NSString stringWithFormat:@"Вчера, %@", [dateFormatter stringFromDate:date]];
    } else {
        NSDateFormatter *dateFormatter = [self dateFormatterWithFormat:@"dd MMM HH:mm"];
        return [dateFormatter stringFromDate:date];
    }
}

+ (BOOL)isSameDayWithFirstDate:(NSDate *)firstDate secondDate:(NSDate *)secondDate {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSCalendarUnitDay;
    NSDateComponents *comp1 = [calendar components:unitFlags fromDate:firstDate];
    NSDateComponents *comp2 = [calendar components:unitFlags fromDate:secondDate];
    
    return [comp1 day] == [comp2 day];
}

+ (NSNumber *)numberByString:(NSString *)string {
    if ([string isKindOfClass:[NSNull class]] || [string isEqualToString:@""]) string = @"0";
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    return [numberFormatter numberFromString:string];
}

+ (NSString *)stringByObject:(NSObject *)object {
    return [object isKindOfClass:[NSNull class]] ? @"" : (NSString *)object;
}

+ (NSString *)stringByHTML:(NSString *)htmlString {
    htmlString = [self stringByObject:htmlString];
    NSRange r;
    while ((r = [htmlString rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        htmlString = [htmlString stringByReplacingCharactersInRange:r withString:@""];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&quot;" withString:@""];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@":&#41;" withString:@""];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&#41;" withString:@""];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&#40;" withString:@""];
    return htmlString;
}

@end
