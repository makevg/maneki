//
//  MADataValidator.m
//  Maneki
//
//  Created by Maximychev Evgeny on 05.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MADataValidator.h"

@implementation MADataValidator

#pragma mark - Private

+ (BOOL)validateValue:(NSString *)value byRegex:(NSString *)regex {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:value];
}

#pragma mark - Public

+ (BOOL)validatePhone:(NSString *)phone {
    NSString *phoneRegex = @"((8|\\+7)-?)?\\s?\\(?\\d{3}\\)?(\\s|-)?\\d{1}-?\\d{1}-?\\d{1}-?\\d{1}-?\\d{1}-?\\d{1}-?\\d{1}";
    return [MADataValidator validateValue:phone byRegex:phoneRegex];
}

+ (BOOL)validateEmail:(NSString *)email {
    NSString *emailRegex = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    return  [MADataValidator validateValue:email byRegex:emailRegex];
}

@end
