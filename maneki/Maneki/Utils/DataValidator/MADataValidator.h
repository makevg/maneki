//
//  MADataValidator.h
//  Maneki
//
//  Created by Maximychev Evgeny on 05.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MADataValidator : NSObject

+ (BOOL)validatePhone:(NSString *)phone;
+ (BOOL)validateEmail:(NSString *)email;

@end
