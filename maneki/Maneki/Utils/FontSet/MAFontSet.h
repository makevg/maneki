//
//  MAFontSet.h
//  Maneki
//
//  Created by Maximychev Evgeny on 26.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString *const cBoldFontName;
extern NSString *const cRegularFontName;

@interface MAFontSet : NSObject

+ (UIFont *)navBatTitleFont;
+ (UIFont *)cartButtonFont;
+ (UIFont *)cityCellFont;
+ (UIFont *)buttonFont;
+ (UIFont *)signUpFieldFont;
+ (UIFont *)contactFieldFont;
+ (UIFont *)categoryItemFont;
+ (UIFont *)cityChooseLabelFont;
+ (UIFont *)authLabelFont;
+ (UIFont *)forgotPasswordLabel;
+ (UIFont *)backMenuCellFont;
+ (UIFont *)backMenuCityButtonFont;
+ (UIFont *)saleLabelFont;
+ (UIFont *)welcomeLabel;
+ (UIFont *)productCategoryCellFont;
+ (UIFont *)registrationViewLabelFont;
+ (UIFont *)notificationFont;
+ (UIFont *)productNameLabelFont;
+ (UIFont *)productPriceLabelFont;
+ (UIFont *)productCountLabelFont;
+ (UIFont *)newsCellCaptionLabelFont;
+ (UIFont *)newsCellPublishLabelFont;
+ (UIFont *)newsCellDescriptionLabelFont;
+ (UIFont *)newsViewCaptionLabelFont;
+ (UIFont *)newsViewPublishLabelFont;
+ (UIFont *)deliveryHeaderFont;
+ (UIFont *)deliveryPriceFont;
+ (UIFont *)deliveryTitleFont;
+ (UIFont *)deliveryDescriptionFont;
+ (UIFont *)deliveryInfoLabelFont;
+ (UIFont *)deliveryStepLabelFont;
+ (UIFont *)productScreenTitleFont;
+ (UIFont *)productScreenDescriptionFont;
+ (UIFont *)productScreenPriceFont;
+ (UIFont *)ordersInfoScreenLabelFont;
+ (UIFont *)profileLabelFont;
+ (UIFont *)profileLogoutButtonFont;
+ (UIFont *)ordersCellInfoLabelFont;
+ (UIFont *)ordersCellDateLabelFont;
+ (UIFont *)orderCellDefaultFont;
+ (UIFont *)orderProductCellCountFont;
+ (UIFont *)orderInfoCellFont;
+ (UIFont *)orderInfoCellPriceFont;
+ (UIFont *)orderInfoCellPropertiesFont;
+ (UIFont *)wokProductCellFont;
+ (UIFont *)wokBasisCellInfoFont;
+ (UIFont *)wokBasisCellDescrFont;
+ (UIFont *)wokBasisCellCostFont;
+ (UIFont *)wokSousCellFont;
+ (UIFont *)wokChoosesCellInfoFont;
+ (UIFont *)wokChoosesCellCounterFont;
+ (UIFont *)wokChoosesCellDescrFont;
+ (UIFont *)wokChoosesCellCostInfoFont;
+ (UIFont *)wokChoosesCellCostValueFont;
+ (UIFont *)wokHeaderTitleFont;
+ (UIFont *)wokHeaderPositionFont;
+ (UIFont *)reviewUserNameFont;
+ (UIFont *)reviewDateTimeFont;
+ (UIFont *)reviewDescriptionFont;
+ (UIFont *)checkoutInfoLabelFont;
+ (UIFont *)checkoutSaveAddressLabelFont;
+ (UIFont *)cartViewTitleLabelFont;
+ (UIFont *)cartViewDescrLabelFont;

@end
