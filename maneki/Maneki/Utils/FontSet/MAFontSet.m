//
//  MAFontSet.m
//  Maneki
//
//  Created by Maximychev Evgeny on 26.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAFontSet.h"

NSString *const cBoldFontName    = @"RotondaC-Bold";
NSString *const cRegularFontName = @"RotondaC";

@implementation MAFontSet

+ (UIFont *)navBatTitleFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:19];
}

+ (UIFont *)cartButtonFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:12];
}

+ (UIFont *)cityCellFont {
    return [UIFont fontWithName:@"RotondaC" size:16];
}

+ (UIFont *)buttonFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:16];
}

+ (UIFont *)signUpFieldFont {
    return [UIFont fontWithName:@"RotondaC" size:18];
}

+ (UIFont *)contactFieldFont {
    return [UIFont fontWithName:@"RotondaC" size:16];
}

+ (UIFont *)categoryItemFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:22];
}

+ (UIFont *)cityChooseLabelFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:20];
}

+ (UIFont *)authLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:20];
}

+ (UIFont *)forgotPasswordLabel {
    return [UIFont fontWithName:@"RotondaC" size:16];
}

+ (UIFont *)backMenuCellFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:18];
}

+ (UIFont *)backMenuCityButtonFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:16];
}

+ (UIFont *)saleLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:15];
}

+ (UIFont *)welcomeLabel {
    return [UIFont fontWithName:@"RotondaC-Bold" size:20];
}

+ (UIFont *)productCategoryCellFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:18];
}

+ (UIFont *)registrationViewLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:15];
}

+ (UIFont *)notificationFont {
    return [UIFont fontWithName:@"RotondaC" size:18];
}

+ (UIFont *)productNameLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:16];
}

+ (UIFont *)productPriceLabelFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:18];
}

+ (UIFont *)productCountLabelFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:15];
}

+ (UIFont *)newsCellCaptionLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:18];
}

+ (UIFont *)newsCellPublishLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:14];
}

+ (UIFont *)newsCellDescriptionLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:16];
}

+ (UIFont *)newsViewCaptionLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:20];
}

+ (UIFont *)newsViewPublishLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:14];
}

+ (UIFont *)deliveryHeaderFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:18];
}

+ (UIFont *)deliveryPriceFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:25];
}

+ (UIFont *)deliveryTitleFont {
    return [UIFont fontWithName:@"RotondaC" size:20];
}

+ (UIFont *)deliveryDescriptionFont {
    return [UIFont fontWithName:@"RotondaC" size:14];
}

+ (UIFont *)deliveryInfoLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:18];
}

+ (UIFont *)deliveryStepLabelFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:22];
}

+ (UIFont *)productScreenTitleFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:18];
}

+ (UIFont *)productScreenDescriptionFont {
    return [UIFont fontWithName:@"RotondaC" size:14];
}

+ (UIFont *)productScreenPriceFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:22];
}

+ (UIFont *)ordersInfoScreenLabelFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:20];
}

+ (UIFont *)profileLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:16];
}

+ (UIFont *)profileLogoutButtonFont {
    return [UIFont fontWithName:@"RotondaC" size:16];
}

+ (UIFont *)ordersCellInfoLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:18];
}

+ (UIFont *)ordersCellDateLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:14];
}

+ (UIFont *)orderCellDefaultFont {
    return [UIFont fontWithName:@"RotondaC" size:16];
}

+ (UIFont *)orderProductCellCountFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:20];
}

+ (UIFont *)orderInfoCellFont {
    return [UIFont fontWithName:@"RotondaC" size:14];
}

+ (UIFont *)orderInfoCellPriceFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:24];
}

+ (UIFont *)orderInfoCellPropertiesFont {
    return [UIFont fontWithName:@"RotondaC" size:15];
}

+ (UIFont *)wokProductCellFont {
    return [UIFont fontWithName:@"RotondaC" size:16];
}

+ (UIFont *)wokBasisCellInfoFont {
    return [UIFont fontWithName:@"RotondaC" size:18];
}

+ (UIFont *)wokBasisCellDescrFont {
    return [UIFont fontWithName:@"RotondaC" size:14];
}

+ (UIFont *)wokBasisCellCostFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:20];
}

+ (UIFont *)wokSousCellFont {
    return [UIFont fontWithName:@"RotondaC" size:16];
}

+ (UIFont *)wokChoosesCellInfoFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:18];
}

+ (UIFont *)wokChoosesCellCounterFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:14];
}

+ (UIFont *)wokChoosesCellDescrFont {
    return [UIFont fontWithName:@"RotondaC" size:14];
}

+ (UIFont *)wokChoosesCellCostInfoFont {
    return [UIFont fontWithName:@"RotondaC" size:14];
}

+ (UIFont *)wokChoosesCellCostValueFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:24];
}

+ (UIFont *)wokHeaderTitleFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:17];
}

+ (UIFont *)wokHeaderPositionFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:22];
}

+ (UIFont *)reviewUserNameFont {
    return [UIFont fontWithName:@"RotondaC" size:18];
}

+ (UIFont *)reviewDateTimeFont {
    return [UIFont fontWithName:@"RotondaC" size:14];
}

+ (UIFont *)reviewDescriptionFont {
    return [UIFont fontWithName:@"RotondaC" size:15];
}

+ (UIFont *)checkoutInfoLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:13];
}

+ (UIFont *)checkoutSaveAddressLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:16];
}

+ (UIFont *)cartViewTitleLabelFont {
    return [UIFont fontWithName:@"RotondaC-Bold" size:22];
}

+ (UIFont *)cartViewDescrLabelFont {
    return [UIFont fontWithName:@"RotondaC" size:13];
}

@end
