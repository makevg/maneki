//
//  MACommonService.m
//  Maneki
//
//  Created by Maximychev Evgeny on 19.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "VKSdk.h"
#import "MACommonService.h"
#import "City.h"

NSString *const cCurrentCityId = @"currentCityId";
NSString *const cLoggedIn      = @"loggedIn";

@implementation MACommonService

+ (void)saveCurrentCity:(City *)city {
    [[NSUserDefaults standardUserDefaults] setObject:city.string_id
                                              forKey:cCurrentCityId];
}

+ (NSString *)getCurrentCityId {
    return [[NSUserDefaults standardUserDefaults] objectForKey:cCurrentCityId];
}

+ (void)setLoggedIn:(BOOL)loggedIn {
    [[NSUserDefaults standardUserDefaults] setBool:loggedIn
                                            forKey:cLoggedIn];

    if( !loggedIn ) {
        [VKSdk forceLogout];
        [[FBSDKLoginManager new] logOut];
    }

}

+ (BOOL)isLoggedIn {
    return [[NSUserDefaults standardUserDefaults] boolForKey:cLoggedIn];
}

+ (void)callToPhone:(NSString *)phone {
    NSString *cleanedString = [[phone componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

+ (NSString *)getVkAppId {
    NSDictionary *data = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]
                                                                     pathForResource:@"Info"
                                                                     ofType:@"plist"]];
    return data[@"VkAppID"];
}

@end
