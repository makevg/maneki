//
//  MACommonService.h
//  Maneki
//
//  Created by Maximychev Evgeny on 19.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class City;

@interface MACommonService : NSObject

+ (void)saveCurrentCity:(City *)city;
+ (NSString *)getCurrentCityId;
+ (void)setLoggedIn:(BOOL)loggedIn;
+ (BOOL)isLoggedIn;
+ (void)callToPhone:(NSString *)phone;
+ (NSString *)getVkAppId;

@end
