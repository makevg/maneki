//
// Created by Buravlev Mikhail on 05.01.16.
// Copyright (c) 2016 Maneki. All rights reserved.
//

#import "MAProductService.h"

@interface MAProductService (DB)

+ (void)updateProducts:(NSDictionary *)dictionary inCity:(City *)city inContext:(NSManagedObjectContext *)localContext;

+ (void)updateWokProperties:(NSDictionary *)dictionary inCity:(City *)city inContext:(NSManagedObjectContext *)localContext;

+ (void)updateCategories:(NSDictionary *)dictionary inCity:(City *)city inContext:(NSManagedObjectContext *)localContext;

@end