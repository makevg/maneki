#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalDataImport.h>
#import "Product.h"

#import "MAProductService.h"
#import "ProductCategory.h"
#import "City.h"
#import "MAProductService+DB.h"
#import "WokProperty.h"

NSString *const cProductsUrl = @"http://api.manekicafe.ru/menu/";
NSString *const cProductsVersionKey = @"ProductVersion";
NSString *const cCategoriesVersionKey = @"CategoryVersion";

@implementation MAProductService

+ (Product *)getProductByPk:(NSNumber *)pk {
    return [Product MR_findFirstByAttribute:@"id"
                                  withValue:pk
                                  inContext:[MANEKI_DB produceContextForRead]];
}

+ (void)getWokProductsByCategoryCode:(NSString *)categoryCode inCity:(City *)city receiver:(ServiceHandlerArray)receiver withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [self getProductsByCategoryCode:categoryCode inCity:city receiver:^(NSFetchedResultsController *controller) {
                NSFetchedResultsController *wokPropsFRC = [WokProperty MR_fetchAllSortedBy:@"code"
                                                                                 ascending:YES
                                                                             withPredicate:[NSPredicate predicateWithFormat:@"city = %@", city]
                                                                                   groupBy:nil
                                                                                  delegate:nil
                                                                                 inContext:[MANEKI_DB produceContextForRead]];

                NSArray <NSFetchedResultsController *> *result = @[controller, wokPropsFRC];

                if (receiver)
                    receiver(result);
            }
                       withDelegate:delegate];
}

+ (void)getProductsByCategoryCode:(NSString *)categoryCode inCity:(City *)city receiver:(ServiceHandler)receiver withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [self updateCategoriesInCity:city receiver:^(BOOL contextDidSave, NSError *error) {
        NSFetchedResultsController *fetchedResultsController = [Product MR_fetchAllSortedBy:@"position"
                                                                                  ascending:YES
                                                                              withPredicate:[NSPredicate predicateWithFormat:@"parent = %@", [self getProductCategoryByCode:categoryCode inCity:city]]
                                                                                    groupBy:NULL
                                                                                   delegate:NULL
                                                                                  inContext:[MANEKI_DB produceContextForRead]];

        [self updateProductsInCity:city withDelegate:nil];

        if (receiver) {
            if ([NSThread isMainThread]) {
                receiver(fetchedResultsController);
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    receiver(fetchedResultsController);
                });
            }
        }
    }               withDelegate:delegate];
}

+ (void)getCategoriesByCategoryCode:(NSString *)categoryCode inCity:(City *)city receiver:(ServiceHandler)receiver withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    ProductCategory *productCategory = [self getProductCategoryByCode:categoryCode inCity:city];

    if (productCategory) {
        NSFetchedResultsController *fetchedResultsController = [ProductCategory MR_fetchAllSortedBy:@"position"
                                                                                          ascending:YES
                                                                                      withPredicate:[NSPredicate predicateWithFormat:@"parent = %@", productCategory]
                                                                                            groupBy:NULL
                                                                                           delegate:NULL
                                                                                          inContext:[MANEKI_DB produceContextForRead]];

        if (receiver) {
            if ([NSThread isMainThread]) {
                receiver(fetchedResultsController);
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    receiver(fetchedResultsController);
                });
            }
        }

        [self updateCategoriesInCity:city
                            receiver:nil
                        withDelegate:delegate];
    } else {
        [self updateCategoriesInCity:city receiver:^(BOOL contextDidSave, NSError *error) {
                    ProductCategory *localProductCategory = [self getProductCategoryByCode:categoryCode inCity:city];

                    NSFetchedResultsController *fetchedResultsController = [ProductCategory MR_fetchAllSortedBy:@"position"
                                                                                                      ascending:YES
                                                                                                  withPredicate:[NSPredicate predicateWithFormat:@"parent = %@", localProductCategory]
                                                                                                        groupBy:NULL
                                                                                                       delegate:NULL
                                                                                                      inContext:[MANEKI_DB produceContextForRead]];

                    if (receiver) {
                        if ([NSThread isMainThread]) {
                            receiver(fetchedResultsController);
                        } else {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                receiver(fetchedResultsController);
                            });
                        }
                    }
                }
                        withDelegate:delegate];
    }
}

+ (ProductCategory *)getProductCategoryByCode:(NSString *)code
                                       inCity:(City *)city {
    return [ProductCategory MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"string_id = %@ AND city = %@", code, city]
                                            inContext:[MANEKI_DB produceContextForRead]];
}

+ (void)updateProductsInCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        [MANEKI_API requestGetToUrlString:cProductsUrl
                                andParams:@{
                                        @"last_update" : @YES,
                                        @"city" : city.string_id
                                } andHandler:^(NSURLResponse *responseCheck, id responseObjectCheck, NSError *errorCheck) {
                    if ([self handleResponse:responseCheck
                          withResponseObject:responseObjectCheck
                                   withError:errorCheck
                                withDelegate:delegate]) {
                        return;
                    }

                    NSString *online_version = responseObjectCheck[@"last_update"][@"products"];
                    NSString *userDefaultsKey = [NSString stringWithFormat:@"%@%@", city.string_id, cProductsVersionKey];
                    NSString *current_version = [[NSUserDefaults standardUserDefaults] stringForKey:userDefaultsKey];

                    if ([online_version isEqualToString:current_version]) {
                        [self handleSuccessWithDelegate:delegate result:nil];
                        return;
                    }

                    [MANEKI_API requestGetToUrlString:cProductsUrl
                                            andParams:@{
                                                    @"city" : city.string_id
                                            }
                                           andHandler:^(NSURLResponse *responseProduct, id responseObjectProduct, NSError *errorProduct) {
                                               if ([self handleResponse:responseProduct
                                                     withResponseObject:responseObjectProduct
                                                              withError:errorProduct
                                                           withDelegate:delegate]) {
                                                   return;
                                               }

                                               [MANEKI_API requestGetToUrlString:cProductsUrl
                                                                       andParams:@{
                                                                               @"city" : city.string_id,
                                                                               @"property" : @YES
                                                                       }
                                                                      andHandler:^(NSURLResponse *responseProp, id responseObjectProp, NSError *errorProp) {
                                                                          if ([self handleResponse:responseProp
                                                                                withResponseObject:responseObjectProp
                                                                                         withError:errorProp
                                                                                      withDelegate:delegate]) {
                                                                              return;
                                                                          }

                                                                          [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                                                                      NSDictionary *responseProdDict = responseObjectProduct;
                                                                                      [self updateProducts:responseProdDict
                                                                                                    inCity:city
                                                                                                 inContext:localContext];

                                                                                      NSDictionary *responsePropDict = responseObjectProp;
                                                                                      [self updateWokProperties:responsePropDict
                                                                                                         inCity:city
                                                                                                      inContext:localContext];
                                                                                  }
                                                                                        completion:^(BOOL contextDidSave, NSError *saveError) {
                                                                                            [[NSUserDefaults standardUserDefaults] setObject:online_version
                                                                                                                                      forKey:userDefaultsKey];
                                                                                            [[NSUserDefaults standardUserDefaults] synchronize];

                                                                                            [self handleSuccessWithDelegate:delegate result:nil];
                                                                                        }];
                                                                      }];
                                           }];
                }];
    });
}

+ (void)updateCategoriesInCity:(City *)city receiver:(MASaveCompletionHandler)receiver withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        [MANEKI_API requestGetToUrlString:cProductsUrl
                                andParams:@{
                                        @"last_update" : @YES,
                                        @"city" : city.string_id
                                } andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                    if ([self handleResponse:response
                          withResponseObject:responseObject
                                   withError:error
                                withDelegate:delegate]) {
                        return;
                    }

                    NSString *online_version = responseObject[@"last_update"][@"category"];
                    NSString *userDefaultsKey = [NSString stringWithFormat:@"%@%@", city.string_id, cCategoriesVersionKey];
                    NSString *current_version = [[NSUserDefaults standardUserDefaults] stringForKey:userDefaultsKey];

                    if ([online_version isEqualToString:current_version]) {
                        if (receiver)
                            receiver(NO, nil);

                        [self handleSuccessWithDelegate:delegate result:nil];
                        return;
                    }

                    [MANEKI_API requestGetToUrlString:cProductsUrl
                                            andParams:@{
                                                    @"city" : city.string_id,
                                                    @"groups" : @YES
                                            }
                                           andHandler:^(NSURLResponse *response2, id responseObject2, NSError *error2) {
                                               if ([self handleResponse:response
                                                     withResponseObject:responseObject
                                                              withError:error
                                                           withDelegate:delegate]) {
                                                   return;
                                               }

                                               [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                                           NSDictionary *responseDict = responseObject2;
                                                           [self updateCategories:responseDict
                                                                           inCity:city
                                                                        inContext:localContext];
                                                       }
                                                             completion:^(BOOL contextDidSave, NSError *saveError) {
                                                                 [[NSUserDefaults standardUserDefaults] setObject:online_version
                                                                                                           forKey:userDefaultsKey];
                                                                 [[NSUserDefaults standardUserDefaults] synchronize];

                                                                 if (receiver)
                                                                     receiver(contextDidSave, saveError);

                                                                 [self handleSuccessWithDelegate:delegate result:nil];
                                                             }];
                                           }];
                }];
    });
}

@end