//
// Created by Buravlev Mikhail on 05.01.16.
// Copyright (c) 2016 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import "MAProductService+DB.h"
#import "City.h"
#import "MADataFormatter.h"
#import "Product.h"
#import "ProductCategory.h"
#import "WokProperty.h"
#import "WokPropertyValue.h"


@implementation MAProductService (DB)

+ (void)updateProducts:(NSDictionary *)dictionary
                inCity:(City *)city
             inContext:(NSManagedObjectContext *)localContext {
    City *localCity = [city MR_inContext:localContext];

    NSArray *products = dictionary[@"product"];
    NSMutableArray *existsProducts = [@[] mutableCopy];
    NSInteger position = 0;

    for (NSDictionary *dictProduct in products) {
        Product *product = [Product MR_findFirstOrCreateByAttribute:@"id"
                                                          withValue:dictProduct[@"id"]
                                                          inContext:localContext];

        [existsProducts addObject:dictProduct[@"id"]];

        product.title = dictProduct[@"title"];
        product.descr = dictProduct[@"descr"];
        product.position = @(++position);
        product.parent = [ProductCategory MR_findFirstOrCreateByAttribute:@"id"
                                                                withValue:dictProduct[@"parent_id"]
                                                                inContext:localContext];
        product.image_url = [dictProduct[@"image_url"] isKindOfClass:[NSNull class]] ? @"" : dictProduct[@"image_url"];
        product.preview_image_url = [dictProduct[@"preview_image_url"] isKindOfClass:[NSNull class]] ? @"" : dictProduct[@"preview_image_url"];
        product.cost = dictProduct[@"cost"];

        NSDictionary *props = dictProduct[@"props"];

        for (NSDictionary *prop in props) {
            NSString *code = props[prop];

            if ([code isEqualToString:@"hot"]) {
                product.prop_hot = @YES;
                continue;
            }

            if ([code isEqualToString:@"hot2"]) {
                product.prop_hot2 = @YES;
                continue;
            }

            if ([code isEqualToString:@"veg"]) {
                product.prop_veg = @YES;
                continue;
            }

            if ([code isEqualToString:@"new"]) {
                product.prop_new = @YES;
                continue;
            }

            if ([code isEqualToString:@"hit"]) {
                product.prop_hit = @YES;
                continue;
            }
        }
    }

    [Product MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@) and parent.city = %@", existsProducts, localCity]
                                 inContext:localContext];
}

+ (void)updateWokProperties:(NSDictionary *)dictionary inCity:(City *)city inContext:(NSManagedObjectContext *)context {
    City *localCity = [city MR_inContext:context];
    NSDictionary *wok_properties = dictionary[@"wok_property"];
    NSMutableArray *woks = [@[] mutableCopy];

    for (NSDictionary *wok_property in wok_properties) {
        NSDictionary *wokPropertyDict = wok_properties[wok_property];

        WokProperty *wokProperty = [WokProperty MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"code = %@ AND city = %@", wokPropertyDict[@"code"], localCity]
                                                                inContext:context];

        if (!wokProperty) {
            wokProperty = [WokProperty MR_createEntityInContext:context];
        }

        if (![wokProperty.title isEqualToString:wokPropertyDict[@"name"]])
            wokProperty.title = wokPropertyDict[@"name"];

        if (![wokProperty.code isEqualToString:wokPropertyDict[@"code"]])
            wokProperty.code = wokPropertyDict[@"code"];

        if (![wokProperty.city isEqual:localCity])
            wokProperty.city = localCity;

        [woks addObject:wokPropertyDict[@"code"]];
        NSMutableArray *codes = [@[] mutableCopy];
        NSDictionary *values = wokPropertyDict[@"values"];

        NSInteger i = 0;
        for (NSString *wokValueCode in values) {
            NSDictionary *wokValues = values[wokValueCode];
            NSString *wokValueTitle = wokValues[@"name"];
            NSString *wokValueImage = wokValues[@"picture"];
            NSString *strId = wokValues[@"id"];
            NSNumber *id = @([strId integerValue]);
            NSNumber *hot = @([wokValueCode containsString:@"_hot"]);

            [codes addObject:id];

            WokPropertyValue *wokPropertyValue = [WokPropertyValue MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"id = %@ and wok_property = %@", id, wokProperty]
                                                                                   inContext:context];

            if (!wokPropertyValue) {
                wokPropertyValue = [WokPropertyValue MR_createEntityInContext:context];
            }

            if ([wokPropertyValue.title isEqualToString:wokValueTitle] &&
                    [wokPropertyValue.id isEqualToNumber:id] &&
                    [wokPropertyValue.image isEqualToString:wokValueImage] &&
                    [wokPropertyValue.code isEqualToString:wokValueCode] &&
                    [wokPropertyValue.hot isEqualToNumber:hot] &&
                    [wokPropertyValue.position isEqualToNumber:@(i)]
                    )
                continue;

            if ([wokPropertyValue.wok_property isEqual:wokProperty])
                continue;

            wokPropertyValue.title = wokValueTitle;
            wokPropertyValue.code = wokValueCode;
            wokPropertyValue.id = id;
            wokPropertyValue.position = @(i++);
            wokPropertyValue.image = wokValueImage;
            wokPropertyValue.hot = hot;

            [wokProperty addValuesObject:wokPropertyValue];
        }

        [WokPropertyValue MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@) AND wok_property = %@", codes, wokProperty]
                                              inContext:context];
    }

    [WokProperty MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (code IN %@) AND city = %@", woks, localCity]
                                     inContext:context];
}

+ (void)updateCategories:(NSDictionary *)dictionary
                  inCity:(City *)city
               inContext:(NSManagedObjectContext *)localContext {
    City *localCity = [city MR_inContext:localContext];

    NSArray *categories = dictionary[@"groups"];

    NSMutableArray *existsCategories = [@[] mutableCopy];

    for (NSDictionary *dictProduct in categories) {
        ProductCategory *category = [ProductCategory MR_findFirstOrCreateByAttribute:@"id"
                                                                           withValue:[MADataFormatter numberByString:dictProduct[@"id"]]
                                                                           inContext:localContext];

        [existsCategories addObject:[MADataFormatter numberByString:dictProduct[@"id"]]];

        if (![category.title isEqualToString:dictProduct[@"name"]])
            category.title = dictProduct[@"name"];

        if (![category.position isEqualToNumber:[MADataFormatter numberByString:dictProduct[@"sort"]]])
            category.position = [MADataFormatter numberByString:dictProduct[@"sort"]];

        if (![category.string_id isEqualToString:dictProduct[@"code"]])
            category.string_id = dictProduct[@"code"];

        NSString *image_url = [dictProduct[@"picture"] isKindOfClass:[NSNull class]] ? @"" : dictProduct[@"picture"];
        if (![category.image_url isEqualToString:image_url])
            category.image_url = image_url;

        if (!category.city || ![category.city.objectID isEqual:localCity.objectID])
            category.city = localCity;

        if (![dictProduct[@"parent_id"] isKindOfClass:[NSNull class]]) {
            ProductCategory *parent = [ProductCategory MR_findFirstOrCreateByAttribute:@"id"
                                                                             withValue:[MADataFormatter numberByString:dictProduct[@"parent_id"]]
                                                                             inContext:localContext];

            if (![category.parent.objectID isEqual:parent.objectID])
                category.parent = parent;
        }
    }

    [ProductCategory MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@) AND city = %@", existsCategories, localCity]
                                         inContext:localContext];
}

@end