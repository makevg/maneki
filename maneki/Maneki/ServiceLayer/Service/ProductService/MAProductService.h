#import <Foundation/Foundation.h>
#import "MABaseService.h"
#import "MAServiceLayer.h"

@class ProductCategory;
@class City;
@class Product;

@interface MAProductService : MABaseService

+ (Product *)getProductByPk:(NSNumber *)pk;

+ (void)getWokProductsByCategoryCode:(NSString *)categoryCode inCity:(City *)city receiver:(ServiceHandlerArray)receiver withDelegate:(__weak id<ServiceResultDelegate>) delegate;

+ (void)getProductsByCategoryCode:(NSString *)categoryCode inCity:(City *)city receiver:(ServiceHandler)receiver withDelegate:(__weak id<ServiceResultDelegate>) delegate;

+ (void)getCategoriesByCategoryCode:(NSString *)categoryCode inCity:(City *)city receiver:(ServiceHandler)receiver withDelegate:(__weak id<ServiceResultDelegate>) delegate;

@end