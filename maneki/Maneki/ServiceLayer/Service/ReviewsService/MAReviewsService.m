//
//  MAReviewsService.m
//  Maneki
//
//  Created by Максимычев Е.О. on 29.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/NSManagedObject+MagicalDataImport.h>
#import "MAReviewsService.h"
#import "Review.h"
#import "City.h"
#import "MADataFormatter.h"

NSString *const reviewsUrl = @"http://api.manekicafe.ru/reviews/";

//http://api.manekicafe.ru/reviews/?add&city=s1&rate=4&review=text

@implementation MAReviewsService

+ (void)addReviewWithText:(NSString *)text rate:(NSNumber *)rate city:(City *)city delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:reviewsUrl
                            andParams:@{
                                    @"city" : city.string_id,
                                    @"rate" : rate,
                                    @"review" : text,
                                    @"add": @YES
                            }
                           andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                               if ([[self class] handleResponse:response
                                             withResponseObject:responseObject
                                                      withError:error
                                                   withDelegate:delegate]) {
                                   return;
                               }

                               [[self class] handleSuccessWithDelegate:delegate result:nil];
                           }];
}

+ (NSFetchedResultsController *)getReviewsListForCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    NSFetchedResultsController *fetchedResultsController = [Review MR_fetchAllSortedBy:@"published_at"
                                                                             ascending:NO
                                                                         withPredicate:[NSPredicate predicateWithFormat:@"city = %@", city]
                                                                               groupBy:NULL
                                                                              delegate:NULL
                                                                             inContext:[MANEKI_DB produceContextForRead]];
    [self updateReviewsInCity:city withDelegate:delegate];
    return fetchedResultsController;
}

+ (NSFetchedResultsController *)getMyReviewsListForCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    NSFetchedResultsController *fetchedResultsController = [Review MR_fetchAllSortedBy:@"published_at"
                                                                             ascending:NO
                                                                         withPredicate:[NSPredicate predicateWithFormat:@"city = %@ AND is_my == TRUE", city]
                                                                               groupBy:NULL
                                                                              delegate:NULL
                                                                             inContext:[MANEKI_DB produceContextForRead]];
    [self updateReviewsInCity:city withDelegate:delegate];
    return fetchedResultsController;
}

+ (void)updateReviewsInCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:reviewsUrl
                            andParams:@{@"city" : city.string_id}
                           andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                               if ([self handleResponse:response
                                     withResponseObject:responseObject
                                              withError:error
                                           withDelegate:delegate]) {
                                   return;
                               }

                               [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                           City *localCity = [city MR_inContext:localContext];
                                           NSDictionary *responseDict = responseObject;
                                           NSArray *reviewsList = responseDict[@"reviews"];
                                           NSMutableArray *existsArrayIds = [@[] mutableCopy];

                                           for (NSDictionary *dictReviews in reviewsList) {
                                               Review *review = [Review MR_findFirstOrCreateByAttribute:@"id"
                                                                                              withValue:dictReviews[@"id"]
                                                                                              inContext:localContext];
                                               [existsArrayIds addObject:dictReviews[@"id"]];
                                               if(![review.city isEqual:localCity])
                                               review.city = localCity;

                                               [self fillReview:review
                                                   byDictionary:dictReviews];
                                           }

                                           [Review MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@) and city = %@", existsArrayIds, localCity]
                                                                       inContext:localContext];
                                       }
                                             completion:^(BOOL contextDidSave, NSError *saveError) {
                                                 [self handleSuccessWithDelegate:delegate result:nil];
                                             }];
                           }];
}

+ (void)fillReview:(Review *)review byDictionary:(NSDictionary *)dictReviews {
    if (![review.id isEqualToNumber:dictReviews[@"id"]])
        review.id = dictReviews[@"id"];

    if (![review.user_name isEqualToString:[MADataFormatter stringByObject:dictReviews[@"user_name"]]])
        review.user_name = [MADataFormatter stringByObject:dictReviews[@"user_name"]];

    if (![review.published_at isEqualToNumber:dictReviews[@"published_at"]])
        review.published_at = dictReviews[@"published_at"];

    if (![review.body isEqualToString:[MADataFormatter stringByHTML:dictReviews[@"descr"]]])
        review.body = [MADataFormatter stringByHTML:dictReviews[@"descr"]];

    if (![review.rate isEqualToNumber:[MADataFormatter numberByString:dictReviews[@"rate"]]])
        review.rate = [MADataFormatter numberByString:dictReviews[@"rate"]];
    
    if (![review.is_my isEqualToNumber:dictReviews[@"is_my"]])
        review.is_my = dictReviews[@"is_my"];
}

@end
