//
//  MAReviewsService.h
//  Maneki
//
//  Created by Максимычев Е.О. on 29.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MABaseService.h"

@class City;

@interface MAReviewsService : MABaseService

+ (NSFetchedResultsController*)getReviewsListForCity:(City *)city withDelegate:(__weak id<ServiceResultDelegate>) delegate;
+ (NSFetchedResultsController *)getMyReviewsListForCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate;
+ (void)addReviewWithText:(NSString *)text rate:(NSNumber *)rate city:(City *)city delegate:(__weak id <ServiceResultDelegate>)delegate;

@end
