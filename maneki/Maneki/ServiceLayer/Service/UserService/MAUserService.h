//
// Created by Buravlev Mikhail on 11.01.16.
// Copyright (c) 2016 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MABaseService.h"

typedef NS_ENUM(NSUInteger, SocialRegisterType) {
    eSRTVK = 0,
    eSRTFB
};

@interface MAUserService : MABaseService

- (void)loginUserWithEmail:(NSString *)email password:(NSString *)password delegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)registerUserWithEmail:(NSString *)login password:(NSString *)password password2:(NSString *)password2 delegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)loginUserWithSocialType:(SocialRegisterType)type token:(NSString *)token delegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)bindSocialType:(SocialRegisterType)type token:(NSString *)token delegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)unBindSocialType:(SocialRegisterType)type delegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)validatePhoneFirstStepWithPhone:(NSString *)phone hash:(NSString *)hash delegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)validatePhoneSecondStepWithPhone:(NSString *)phone code:(NSString *)code hash:(NSString *)hash delegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)getForgotPasswordWithEmail:(NSString *)email delegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)changeUserName:(NSString *)userName delegate:(__weak id <ServiceResultDelegate>)delegate;

- (NSFetchedResultsController *)getUserInfoWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

+ (void)deleteUserWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

@end