//
// Created by Buravlev Mikhail on 11.01.16.
// Copyright (c) 2016 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import "MAUserService.h"
#import "MACityService.h"
#import "City.h"
#import "MAServiceLayer.h"
#import "User.h"

NSString *const infoUrl = @"http://api.manekicafe.ru/personal/";
NSString *const regUrl = @"http://api.manekicafe.ru/register/";
NSString *const socialUrl = @"http://api.manekicafe.ru/auth/social/";
//http://api.manekicafe.ru/register/?email=testmobapi4@laboratori.ru&pass=123456&pass_confirm=123456&city=s1
//http://api.manekicafe.ru/register/?phone_validate&phone=9023322247&user_hash=mY9PBwRz51879f78d2f56143ded03e34c51be9a2
//http://api.manekicafe.ru/register/?phone_validate&phone=9023322247&code=6776&user_hash=mY9PBwRz51879f78d2f56143ded03e34c51be9a2


NSString *const loginUrl = @"http://api.manekicafe.ru/auth/";
//http://api.manekicafe.ru/auth/?login=aaa&pass=bbb

NSString *addressUrl = @"http://private-f27e-maneki.apiary-mock.com/user/address";

//http://api.manekicafe.ru/auth/password/?login=testmobapi4@laboratori.ru
NSString *forgotPasswordUrl = @"http://api.manekicafe.ru/auth/password/";

NSString *changeUserInfoUrl = @"http://api.manekicafe.ru/personal/edit/";

@implementation MAUserService

- (void)handleAuthorization:(NSURLResponse *)response withResponseObject:(id)responseObject withError:(NSError *)error withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    NSDictionary *responseDict = responseObject;

    if (response && responseObject) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSError *serverError = nil;
        if (httpResponse.statusCode == 403) {
            serverError = [NSError errorWithDomain:kErrorDomain
                                              code:httpResponse.statusCode
                                          userInfo:@{
                                                  NSLocalizedDescriptionKey : @"Требуется подтверждение телефона",
                                                  @"result" : @{
                                                          @"user_hash" : responseObject[@"user_hash"]
                                                  }
                                          }];
        } else if (responseDict[@"error"]) {
            serverError = [NSError errorWithDomain:kErrorDomain
                                              code:httpResponse.statusCode
                                          userInfo:@{
                                                  NSLocalizedDescriptionKey : responseDict[@"error"]
                                          }];
        }

        if (serverError && [delegate respondsToSelector:@selector(serverError:forService:)]) {
            dispatch_safe_main_async(^{
                [delegate serverError:serverError forService:[self class]];
            });

            return;
        }
    }

    if (error) {
        if ([delegate respondsToSelector:@selector(networkError:forService:)]) {
            NSError *networkError = [NSError errorWithDomain:error.domain
                                                        code:error.code
                                                    userInfo:@{
                                                            NSLocalizedDescriptionKey : @"Проблемы с подключением к интернету"
                                                    }];

            dispatch_safe_main_async(^{
                [delegate networkError:networkError forService:[self class]];
            });
        }

        return;
    }

    if ([delegate respondsToSelector:@selector(successResult:forService:)]) {
        dispatch_safe_main_async(^{
            [delegate successResult:nil forService:[self class]];
        });
    }
}

- (void)loginUserWithEmail:(NSString *)email password:(NSString *)password delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:loginUrl
                            andParams:@{
                                    @"login" : email,
                                    @"pass" : password
                            } andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                [self handleAuthorization:response
                       withResponseObject:responseObject
                                withError:error
                             withDelegate:delegate];
            }];
}

- (void)registerUserWithEmail:(NSString *)email password:(NSString *)password password2:(NSString *)password2 delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:regUrl
                            andParams:@{
                                    @"email" : email,
                                    @"pass" : password,
                                    @"pass_confirm" : password2,
                                    @"city" : [MACityService getCurrentCity].string_id
                            } andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                [self handleAuthorization:response
                       withResponseObject:responseObject
                                withError:error
                             withDelegate:delegate];
            }];
}

- (void)loginUserWithSocialType:(SocialRegisterType)type token:(NSString *)token delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:socialUrl
                            andParams:@{
                                    @"social" : @(type),
                                    @"token" : token,
                                    @"city" : [MACityService getCurrentCity].string_id,
                            } andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                [self handleAuthorization:response
                       withResponseObject:responseObject
                                withError:error
                             withDelegate:delegate];
            }];
}

- (void)bindSocialType:(SocialRegisterType)type token:(NSString *)token delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:changeUserInfoUrl
                            andParams:@{
                                    @"add_social" : @(type),
                                    @"token" : token
                            } andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                if ([[self class] handleResponse:response
                              withResponseObject:responseObject
                                       withError:error
                                    withDelegate:delegate]) {
                    return;
                }

                [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                            User *user = [User MR_findFirstInContext:localContext];
                            switch (type) {
                                case eSRTFB:
                                    user.fb = @(YES);
                                    break;
                                case eSRTVK:
                                    user.vk = @(YES);
                                    break;
                            }
                        }
                              completion:^(BOOL contextDidSave, NSError *saveError) {
                                  [[self class] handleSuccessWithDelegate:delegate result:nil];
                              }];
            }];
}

- (void)unBindSocialType:(SocialRegisterType)type delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:changeUserInfoUrl
                            andParams:@{
                                    @"delete_social" : @(type)
                            } andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                if ([[self class] handleResponse:response
                              withResponseObject:responseObject
                                       withError:error
                                    withDelegate:delegate]) {
                    return;
                }

                [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                            User *user = [User MR_findFirstInContext:localContext];
                            switch (type) {
                                case eSRTFB:
                                    user.fb = @(NO);
                                    break;
                                case eSRTVK:
                                    user.vk = @(NO);
                                    break;
                            }
                        }
                              completion:^(BOOL contextDidSave, NSError *saveError) {
                                  [[self class] handleSuccessWithDelegate:delegate result:nil];
                              }];
            }];
}


- (void)validatePhoneFirstStepWithPhone:(NSString *)phone hash:(NSString *)hash delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:regUrl
                            andParams:@{
                                    @"phone_validate" : @TRUE,
                                    @"phone" : phone,
                                    @"user_hash" : hash,
                            } andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                if ([[self class] handleResponse:response
                              withResponseObject:responseObject
                                       withError:error
                                    withDelegate:delegate]) {
                    return;
                }

                [[self class] handleSuccessWithDelegate:delegate result:nil];
            }];
}

- (void)validatePhoneSecondStepWithPhone:(NSString *)phone code:(NSString *)code hash:(NSString *)hash delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:regUrl
                            andParams:@{
                                    @"phone_validate" : @TRUE,
                                    @"phone" : phone,
                                    @"code" : code,
                                    @"user_hash" : hash
                            } andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                if ([[self class] handleResponse:response
                              withResponseObject:responseObject
                                       withError:error
                                    withDelegate:delegate]) {
                    return;
                }

                [[self class] handleSuccessWithDelegate:delegate result:nil];
            }];
}

- (void)getForgotPasswordWithEmail:(NSString *)email delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:forgotPasswordUrl
                            andParams:@{@"login" : email}
                           andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                               if ([[self class] handleResponse:response
                                             withResponseObject:responseObject
                                                      withError:error
                                                   withDelegate:delegate]) {
                                   return;
                               }

                               [[self class] handleSuccessWithDelegate:delegate result:nil];
                           }];
}

- (void)changeUserName:(NSString *)userName delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestPostToUrlString:changeUserInfoUrl
                             andParams:@{@"name" : userName}
                            andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                if ([[self class] handleResponse:response
                                              withResponseObject:responseObject
                                                       withError:error
                                                    withDelegate:delegate]) {
                                    return;
                                }

                                [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                            User *user = [User MR_findFirstInContext:localContext];
                                            user.name = userName;
                                        }
                                              completion:^(BOOL contextDidSave, NSError *saveError) {
                                                  [[self class] handleSuccessWithDelegate:delegate result:nil];
                                              }];
                            }];
}

- (NSFetchedResultsController *)getUserInfoFromDB {
    NSFetchedResultsController *fetchedResultsController = [User MR_fetchAllSortedBy:@"name"
                                                                           ascending:NO
                                                                       withPredicate:[NSPredicate predicateWithValue:YES]
                                                                             groupBy:NULL
                                                                            delegate:NULL
                                                                           inContext:[MANEKI_DB produceContextForRead]];

    return fetchedResultsController;
}

- (NSFetchedResultsController *)getUserInfoWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    NSFetchedResultsController *fetchedResultsController = [self getUserInfoFromDB];

    [MANEKI_API requestGetToUrlString:infoUrl
                            andParams:@{
                            } andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                if ([[self class] handleResponse:response
                              withResponseObject:responseObject
                                       withError:error
                                    withDelegate:delegate]) {
                    return;
                }

                [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                            NSDictionary *responseDict = responseObject;
                            NSDictionary *info = responseDict[@"info"];

                            User *user = [User MR_findFirstInContext:localContext];
                            if (!user)
                                user = [User MR_createEntityInContext:localContext];

                            if (![user.name isEqualToString:info[@"name"]])
                                user.name = info[@"name"];

                            if (![user.phone isEqualToString:info[@"phone"]])
                                user.phone = info[@"phone"];

                            if (![user.vk isEqualToNumber:info[@"vk"]])
                                user.vk = info[@"vk"];

                            if (![user.fb isEqualToNumber:info[@"fb"]])
                                user.fb = info[@"fb"];
                        }
                              completion:^(BOOL contextDidSave, NSError *saveError) {
                                  [[self class] handleSuccessWithDelegate:delegate result:nil];
                              }];
            }];

    return fetchedResultsController;
}

+ (void)deleteUserWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                [User MR_deleteAllMatchingPredicate:[NSPredicate predicateWithValue:YES]
                                          inContext:localContext];
            }
                  completion:^(BOOL contextDidSave, NSError *error) {
                      [[self class] handleSuccessWithDelegate:delegate result:nil];
                  }];
}

@end