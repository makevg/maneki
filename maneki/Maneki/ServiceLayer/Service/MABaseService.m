#import "MABaseService.h"
#import "MAServiceLayer.h"

NSString *const kErrorDomain = @"ru.maneki.api.error.domain";

@implementation MABaseService

+ (NSError *)handleResponse:(NSURLResponse *)response withResponseObject:(id)responseObject withError:(NSError *)error withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    NSDictionary *responseDict = responseObject;

    if (response && responseObject) {
        if (responseDict[@"error"]) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;

            NSError *serverError = [NSError errorWithDomain:kErrorDomain
                                                       code:httpResponse.statusCode
                                                   userInfo:@{
                                                           NSLocalizedDescriptionKey : responseDict[@"error"]
                                                   }];

            if ([delegate respondsToSelector:@selector(serverError:forService:)]) {
                dispatch_safe_main_async(^{
                    [delegate serverError:serverError forService:self];
                });
            }

            return serverError;
        }
    }

    if (error) {
        if ([delegate respondsToSelector:@selector(networkError:forService:)]) {
            NSString* errorString = response ? @"Внутренняя ошибка сервера" : @"Проблемы с подключением к интернету";
            NSError *networkError = [NSError errorWithDomain:error.domain
                                                        code:error.code
                                                    userInfo:@{
                                                            NSLocalizedDescriptionKey : errorString
                                                    }];

            dispatch_safe_main_async(^{
                [delegate networkError:networkError forService:self];
            });
        }

        return error;
    }

    return nil;
}

+ (void)handleSuccessWithDelegate:(__weak id <ServiceResultDelegate>)delegate result:(id)result {
    if ([delegate respondsToSelector:@selector(successResult:forService:)]) {
        dispatch_safe_main_async(^{
            [delegate successResult:result forService:self];
        });
    }
}


@end
