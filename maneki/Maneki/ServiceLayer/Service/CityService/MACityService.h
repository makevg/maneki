#import <Foundation/Foundation.h>
#import "MABaseService.h"

@class City;

@interface MACityService : MABaseService

+ (City*)getCurrentCity;
+ (NSFetchedResultsController*)getCitiesListWithDelegate:(__weak id<ServiceResultDelegate>) delegate;

@end