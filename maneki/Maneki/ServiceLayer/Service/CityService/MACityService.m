#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalRecord.h>
#import "City.h"
#import "MAProductService.h"
#import "MACityService.h"
#import "MACommonService.h"

NSString *const cCitiesUrl = @"http://api.manekicafe.ru/cities/";

@implementation MACityService

+ (City *)getCurrentCity {
    City *city = [City MR_findFirstByAttribute:@"string_id"
                                     withValue:[MACommonService getCurrentCityId]
                                     inContext:[MANEKI_DB produceContextForRead]];

    if (!city) {
        city = [City MR_createEntityInContext:[NSManagedObjectContext MR_rootSavingContext]];
        city.id = @1;
        city.string_id = @"s1";
        city.name = @"Ярославль";
        city.position = @0;

        NSError *error;
        [[NSManagedObjectContext MR_rootSavingContext] save:&error];
    }
    return city;
}

+ (NSFetchedResultsController *)getCitiesListWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    NSFetchedResultsController *fetchedResultsController = [City MR_fetchAllSortedBy:@"position"
                                                                           ascending:NO
                                                                       withPredicate:[NSPredicate predicateWithValue:YES]
                                                                             groupBy:NULL
                                                                            delegate:NULL
                                                                           inContext:[MANEKI_DB produceContextForRead]];
    [self updateCitiesWithDelegate:delegate];
    return fetchedResultsController;
}

+ (void)updateCitiesWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:cCitiesUrl
                            andParams:nil
                           andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                               if ([self handleResponse:response
                                     withResponseObject:responseObject
                                              withError:error
                                           withDelegate:delegate]) {
                                   return;
                               }

                               [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                           NSDictionary * responseDict = responseObject;
                                           NSMutableArray *existsArrayIds = [@[] mutableCopy];
                                           NSArray *citiesList = responseDict[@"cities"];
                                           NSUInteger position = 0;
                                           for (NSDictionary *dictCities in citiesList) {
                                               City *city = [City MR_findFirstOrCreateByAttribute:@"string_id"
                                                                                        withValue:dictCities[@"id"]
                                                                                        inContext:localContext];

                                               position++;
                                               [self fillCity:city
                                                 byDictionary:dictCities
                                                     position:position];

                                               [existsArrayIds addObject:dictCities[@"id"]];
                                           }

                                           [City MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (string_id IN %@)", existsArrayIds]
                                                                     inContext:localContext];
                                       }
                                             completion:^(BOOL contextDidSave, NSError *saveError) {
                                                 [self handleSuccessWithDelegate:delegate result:nil];
                                             }];
                           }];
}

+ (void)fillCity:(City *)city byDictionary:(NSDictionary *)dictCities position:(NSUInteger)position {
    if (![city.string_id isEqualToString:dictCities[@"id"]])
        city.string_id = dictCities[@"id"];

    if (![city.name isEqualToString:dictCities[@"title"]])
        city.name = dictCities[@"title"];

    if (![city.position isEqualToNumber:@(position)])
        city.position = @(position);
}

@end