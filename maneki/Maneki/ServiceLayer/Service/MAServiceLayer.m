#import "MAServiceLayer.h"
#import "ProductCategory.h"
#import "MAProductService.h"
#import "MACityService.h"
#import "MANewsService.h"
#import "MASaleService.h"
#import "MAReviewsService.h"
#import "MARestaurantService.h"
#import "Product.h"
#import "Restaurant.h"
#import "MACommonService.h"
#import "MAOrdersService.h"

@implementation MAServiceLayer

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if( self = [super init] ) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(unauthorizedHandler:)
                                                     name:kUnauthorizedNotification
                                                   object:nil];
    }

    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
};

- (void)unauthorizedHandler:(NSNotification *)notification {
    [MACommonService setLoggedIn:NO];
}

- (Product *)getProductByPk:(NSNumber *)pk {
    return [MAProductService getProductByPk:pk];
}

- (NSFetchedResultsController *)getNewsListWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    return [MANewsService getNewsListForCity:[MACityService getCurrentCity] withDelegate:delegate];
}

- (NSFetchedResultsController *)getSalesListWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    return [MASaleService getSalesListForCity:[MACityService getCurrentCity] withDelegate:delegate];
}

- (NSFetchedResultsController *)getCitiesListWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    return [MACityService getCitiesListWithDelegate:delegate];
}

- (NSFetchedResultsController *)getReviewsListWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    return [MAReviewsService getReviewsListForCity:[MACityService getCurrentCity] withDelegate:delegate];
}

- (NSFetchedResultsController *)getMyReviewsListWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    return [MAReviewsService getMyReviewsListForCity:[MACityService getCurrentCity] withDelegate:delegate];
}

- (NSFetchedResultsController *)getRestaurantInfoWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    return [MARestaurantService getRestaurantInfoForCity:[MACityService getCurrentCity] withDelegate:delegate];
}

- (NSFetchedResultsController *)getOrdersListWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    return [MAOrdersService getOrdersListWithDelegate:delegate];
}

- (Restaurant *)getOnlyRestaurantInfo {
    return [MARestaurantService getOnlyRestaurantInfoForCity:[MACityService getCurrentCity]];
}

- (void)getProductsByCategoryCode:(NSString *)categoryCode receiver:(ServiceHandler)receiver delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MAProductService getProductsByCategoryCode:categoryCode
                                         inCity:[MACityService getCurrentCity]
                                       receiver:receiver
                                   withDelegate:delegate];
}

- (void)getWokProductsByCategoryCode:(NSString *)category receiver:(ServiceHandlerArray)receiver delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MAProductService getWokProductsByCategoryCode:category
                                            inCity:[MACityService getCurrentCity]
                                          receiver:receiver
                                      withDelegate:delegate];
}

- (void)getCategoriesListByCategoryCode:(NSString *)categoryCode receiver:(ServiceHandler)receiver delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MAProductService getCategoriesByCategoryCode:categoryCode
                                           inCity:[MACityService getCurrentCity]
                                         receiver:receiver
                                     withDelegate:delegate];
}

- (void)addReviewWithText:(NSString *)text rate:(NSNumber *)rate delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MAReviewsService addReviewWithText:text
                                   rate:rate
                                   city:[MACityService getCurrentCity]
                               delegate:delegate];
}

@end
