//
//  MANewsService.m
//  Maneki
//
//  Created by Maximychev Evgeny on 10.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/NSManagedObject+MagicalDataImport.h>
#import "MANewsService.h"
#import "News.h"
#import "City.h"
#import "MADataFormatter.h"

NSString *const newsUrl = @"http://api.manekicafe.ru/news/list.php";

@implementation MANewsService

+ (NSFetchedResultsController *)getNewsListForCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    NSFetchedResultsController *fetchedResultsController = [News MR_fetchAllSortedBy:@"published_at"
                                                                           ascending:NO
                                                                       withPredicate:[NSPredicate predicateWithFormat:@"city = %@", city]
                                                                             groupBy:NULL
                                                                            delegate:NULL
                                                                           inContext:[MANEKI_DB produceContextForRead]];
    [self updateNewsInCity:city withDelegate:delegate];
    return fetchedResultsController;
}

+ (void)updateNewsInCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:newsUrl
                            andParams:@{@"city" : city.string_id}
                           andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                               if ([self handleResponse:response
                                     withResponseObject:responseObject
                                              withError:error
                                           withDelegate:delegate]) {
                                   return;
                               }

                               [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                           City *localCity = [city MR_inContext:localContext];
                                           NSDictionary *responseDict = responseObject;
                                           NSArray *newsList = responseDict[@"news"];
                                           NSMutableArray *existsArrayIds = [@[] mutableCopy];

                                           for (NSDictionary *dictNews in newsList) {
                                               News *news = [News MR_findFirstOrCreateByAttribute:@"id"
                                                                                        withValue:[MADataFormatter numberByString:dictNews[@"id"]]
                                                                                        inContext:localContext];
                                               [existsArrayIds addObject:[MADataFormatter numberByString:dictNews[@"id"]]];

                                               if (![news.city isEqual:localCity])
                                                   news.city = localCity;

                                               [self fillNews:news
                                                 byDictionary:dictNews];
                                           }

                                           [News MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@) and city = %@", existsArrayIds, localCity]
                                                                     inContext:localContext];
                                       }
                                             completion:^(BOOL contextDidSave, NSError *saveError) {
                                                 [self handleSuccessWithDelegate:delegate result:nil];
                                             }];
                           }];
}

+ (void)fillNews:(News *)news byDictionary:(NSDictionary *)dictNews {
    if (![news.id isEqualToNumber:[MADataFormatter numberByString:dictNews[@"id"]]])
        news.id = [MADataFormatter numberByString:dictNews[@"id"]];

    if (![news.title isEqualToString:[MADataFormatter stringByObject:dictNews[@"caption"]]])
        news.title = [MADataFormatter stringByObject:dictNews[@"caption"]];

    if (![news.published_at isEqualToNumber:dictNews[@"published_at"]])
        news.published_at = dictNews[@"published_at"];

    if (![news.short_descr isEqualToString:[MADataFormatter stringByObject:dictNews[@"short_descr"]]])
        news.short_descr = [MADataFormatter stringByObject:dictNews[@"short_descr"]];

    NSString *long_descr = [MADataFormatter stringByObject:dictNews[@"long_descr"]];
    long_descr = [long_descr length] > 0 ? long_descr : news.short_descr;
    long_descr = [long_descr stringByReplacingOccurrencesOfString:@"src=\""
                                                       withString:[NSString stringWithFormat:@"%@%@", @"src=\"", @"https://manekicafe.ru"]];

    long_descr = [long_descr stringByReplacingOccurrencesOfString:@"<img"
                                                       withString:@"<img style=\"height:auto;max-width:100%\""];

    long_descr = [NSString stringWithFormat:@"<body style=\"font-family:'RotondaC';line-height: 16pt;\">%@</body>", long_descr];

    if (![news.long_descr isEqualToString:long_descr])
        news.long_descr = long_descr;

    if (![news.image_url isEqualToString:[MADataFormatter stringByObject:dictNews[@"image_url"]]])
        news.image_url = [MADataFormatter stringByObject:dictNews[@"image_url"]];

    if (![news.preview_image_url isEqualToString:[MADataFormatter stringByObject:dictNews[@"preview_image_url"]]])
        news.preview_image_url = [MADataFormatter stringByObject:dictNews[@"preview_image_url"]];
}

@end
