//
//  MANewsService.h
//  Maneki
//
//  Created by Maximychev Evgeny on 10.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MABaseService.h"

@class City;

@interface MANewsService : MABaseService

+ (NSFetchedResultsController*)getNewsListForCity:(City *)city withDelegate:(__weak id<ServiceResultDelegate>) delegate;

@end
