//
//  MARestaurantService.m
//  Maneki
//
//  Created by Максимычев Е.О. on 17.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import "Restaurant.h"
#import <MagicalRecord/NSManagedObject+MagicalDataImport.h>
#import "MARestaurantService.h"
#import "ResraurantDeliveryInfo.h"
#import "RestaurantDeliveryCost.h"
#import "RestaurantPayMethod.h"
#import "City.h"

@implementation MARestaurantService

NSString *const cRestaurantInfoUrl = @"http://api.manekicafe.ru/restoran/";

+ (Restaurant *)getOnlyRestaurantInfoForCity:(City *)city {
    return [Restaurant MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"city = %@", city]
                                       inContext:[MANEKI_DB produceContextForRead]];
}

+ (NSFetchedResultsController *)getRestaurantInfoForCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    NSFetchedResultsController *fetchedResultsController = [Restaurant MR_fetchAllSortedBy:@"id"
                                                                                 ascending:NO
                                                                             withPredicate:[NSPredicate predicateWithFormat:@"city = %@", city]
                                                                                   groupBy:NULL
                                                                                  delegate:NULL
                                                                                 inContext:[MANEKI_DB produceContextForRead]];
    [self updateRestaurantInfoForCity:city withDelegate:delegate];
    return fetchedResultsController;
}

+ (void)updateRestaurantInfoForCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:cRestaurantInfoUrl
                            andParams:@{@"city" : city.string_id}
                           andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                               if ([self handleResponse:response
                                     withResponseObject:responseObject
                                              withError:error
                                           withDelegate:delegate]) {
                                   return;
                               }

                               [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                           City *localCity = [city MR_inContext:localContext];
                                           NSDictionary *responseDict = responseObject;
                                           NSDictionary *infoDict = responseDict[@"info"];
                                           Restaurant *restaurant = [Restaurant MR_findFirstOrCreateByAttribute:@"city"
                                                                                                      withValue:localCity
                                                                                                      inContext:localContext];

                                           restaurant.city = localCity;

                                           [self fillRestaurant:restaurant
                                               byInfoDictionary:infoDict
                                                        context:localContext];
                                       }
                                             completion:^(BOOL contextDidSave, NSError *saveError) {
                                                 [self handleSuccessWithDelegate:delegate result:nil];
                                             }];
                           }];
}

+ (void)fillRestaurant:(Restaurant *)restaurant
      byInfoDictionary:(NSDictionary *)infoDict
               context:(NSManagedObjectContext *)context {
    if (![restaurant.phone isEqualToString:infoDict[@"phone"]])
        restaurant.phone = infoDict[@"phone"];

    [self fillDeliveryInfoByDictionary:infoDict[@"delivery_info"] restaurant:restaurant context:context];
    [self fillDeliveryCostsByDictionary:infoDict[@"payment_info"] restaurant:restaurant context:context];
    [self fillDeliveryPayMethodsByDictionary:infoDict[@"payment_method"] restaurant:restaurant context:context];
}

+ (void)fillDeliveryInfoByDictionary:(NSArray *)delivery_info
                          restaurant:(Restaurant *)restaurant
                             context:(NSManagedObjectContext *)context {
    NSArray *deliveryCostFromDB = [restaurant.delivety_infos allObjects];
    NSUInteger fromDBCount = [deliveryCostFromDB count];
    NSUInteger fromServerCount = [delivery_info count];
    NSMutableArray *mutableArray = [@[] mutableCopy];

    for (size_t i = 0, size = MIN(fromDBCount, fromServerCount); i != size; ++i) {
        ResraurantDeliveryInfo *entity = deliveryCostFromDB[i];
        NSDictionary *dict = delivery_info[i];

        entity.id = dict[@"id"];
        entity.delivery_type = dict[@"delivery_type"];
        entity.title = dict[@"title"];
        entity.body = dict[@"description"];
        entity.image_url = [dict[@"image_url"] isKindOfClass:[NSNull class]] ? @"" : dict[@"image_url"];
        entity.position = @(i);
        entity.restaurant = restaurant;

        [mutableArray addObject:dict[@"id"]];
    }

    if (fromDBCount == fromServerCount)
        return;

    if (fromServerCount > fromDBCount) {
        for (size_t i = fromDBCount, size = fromServerCount; i != size; ++i) {
            NSDictionary *dict = delivery_info[i];
            ResraurantDeliveryInfo *entity = [ResraurantDeliveryInfo MR_createEntityInContext:context];

            entity.id = dict[@"id"];
            entity.delivery_type = dict[@"delivery_type"];
            entity.title = dict[@"title"];
            entity.body = dict[@"description"];
            entity.image_url = [dict[@"image_url"] isKindOfClass:[NSNull class]] ? @"" : dict[@"image_url"];
            entity.position = @(i);
            entity.restaurant = restaurant;

            [mutableArray addObject:dict[@"id"]];
        }
    }

    [ResraurantDeliveryInfo MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"restaurant = %@ AND NOT (id IN %@)", restaurant, mutableArray]
                                                inContext:context];
}

+ (void)fillDeliveryCostsByDictionary:(NSDictionary *)delivery_costs
                           restaurant:(Restaurant *)restaurant
                              context:(NSManagedObjectContext *)context {
    restaurant.min_payment_value = delivery_costs[@"min_payment_value"];

    NSArray *deliveryRuleInfo = delivery_costs[@"information"];
    NSArray *deliveryRuleFromDB = [restaurant.delivery_costs allObjects];
    NSUInteger fromDBCount = [deliveryRuleFromDB count];
    NSUInteger fromServerCount = [deliveryRuleInfo count];
    NSMutableArray *mutableArray = [@[] mutableCopy];

    for (size_t i = 0, size = MIN(fromDBCount, fromServerCount); i != size; ++i) {
        NSDictionary *dict = deliveryRuleInfo[i];
        RestaurantDeliveryCost *entity = deliveryRuleFromDB[i];

        if ([entity.cost isEqualToNumber:dict[@"value"]] && [entity.body isEqualToString:dict[@"description"]])
            continue;

        entity.id = @(i);
        entity.body = dict[@"regions"];
        entity.cost = dict[@"value"];
        entity.position = @(i);
        entity.restaurant = restaurant;

        [mutableArray addObject:@(i)];
    }

    if (fromDBCount == fromServerCount)
        return;

    if (fromServerCount > fromDBCount) {
        for (size_t i = fromDBCount, size = fromServerCount; i != size; ++i) {
            NSDictionary *dict = deliveryRuleInfo[i];
            RestaurantDeliveryCost *entity = [RestaurantDeliveryCost MR_createEntityInContext:context];

            entity.id = @(i);
            entity.body = dict[@"regions"];
            entity.cost = dict[@"value"];
            entity.position = @(i);
            entity.restaurant = restaurant;

            [mutableArray addObject:@(i)];
        }
    }

    [RestaurantDeliveryCost MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"restaurant = %@ AND NOT (id IN %@)", restaurant, mutableArray]
                                                inContext:context];
}

+ (void)fillDeliveryPayMethodsByDictionary:(NSDictionary *)payment_method
                                restaurant:(Restaurant *)restaurant
                                   context:(NSManagedObjectContext *)context {
    NSDictionary *pay_permissions = payment_method[@"allowed_methods"];
    restaurant.money = pay_permissions[@"money"];
    restaurant.online_card = pay_permissions[@"online_card"];
    restaurant.onboard_card = pay_permissions[@"manual_card"];

    NSArray *deliveryPayMethodsInfo = payment_method[@"information"];
    NSArray *payMethodsFromDB = [restaurant.pay_methods allObjects];
    NSUInteger fromDBCount = [payMethodsFromDB count];
    NSUInteger fromServerCount = [deliveryPayMethodsInfo count];
    NSMutableArray *mutableArray = [@[] mutableCopy];

    for (size_t i = 0, size = MIN(fromDBCount, fromServerCount); i != size; ++i) {
        NSDictionary *dict = deliveryPayMethodsInfo[i];
        RestaurantPayMethod *entity = payMethodsFromDB[i];

        if ([entity.title isEqualToString:dict[@"title"]] && [entity.desctiption isEqualToString:dict[@"description"]])
            continue;

        entity.id = @(i);
        entity.title = dict[@"title"];
        entity.desctiption = dict[@"description"];
        entity.position = @(i);
        entity.restaurant = restaurant;
        [mutableArray addObject:@(i)];
    }

    if (fromDBCount == fromServerCount)
        return;

    if (fromServerCount > fromDBCount) {
        for (size_t i = fromDBCount, size = fromServerCount; i != size; ++i) {
            NSDictionary *dict = deliveryPayMethodsInfo[i];
            RestaurantPayMethod *entity = [RestaurantPayMethod MR_createEntityInContext:context];

            entity.id = @(i);
            entity.title = dict[@"title"];
            entity.desctiption = dict[@"description"];
            entity.position = @(i);
            entity.restaurant = restaurant;
            [mutableArray addObject:@(i)];
        }
    }

    [RestaurantPayMethod MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"restaurant = %@ AND NOT (id IN %@)", restaurant, mutableArray]
                                             inContext:context];
}

@end
