//
//  MARestaurantService.h
//  Maneki
//
//  Created by Максимычев Е.О. on 17.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABaseService.h"

@class City;
@class Restaurant;

@interface MARestaurantService : MABaseService

+ (Restaurant *)getOnlyRestaurantInfoForCity:(City *)city;

+ (NSFetchedResultsController*)getRestaurantInfoForCity:(City *)city withDelegate:(__weak id<ServiceResultDelegate>) delegate;

@end
