//
//  MAOrdersService.m
//  Maneki
//
//  Created by Maximychev Evgeny on 19.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import "MAOrdersService.h"
#import "Order.h"
#import "OrderProduct.h"
#import "MADataFormatter.h"

NSString *const cOrdersUrl = @"http://api.manekicafe.ru/personal/orders/";

@implementation MAOrdersService

+ (NSFetchedResultsController *)getOrdersListWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    NSFetchedResultsController *fetchedResultsController = [Order MR_fetchAllSortedBy:@"received_at"
                                                                            ascending:NO
                                                                        withPredicate:[NSPredicate predicateWithValue:YES]
                                                                              groupBy:NULL
                                                                             delegate:NULL
                                                                            inContext:[MANEKI_DB produceContextForRead]];
    [self updateOrdersWithDelegate:delegate];
    return fetchedResultsController;
}

+ (void)updateOrdersWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:cOrdersUrl
                            andParams:nil
                           andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                               if ([self handleResponse:response
                                     withResponseObject:responseObject
                                              withError:error
                                           withDelegate:delegate]) {
                                   return;
                               }

                               [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                           NSDictionary *responseDict = responseObject;
                                           NSArray *ordersList = responseDict[@"orders"];
                                           NSMutableArray *existsArrayIds = [@[] mutableCopy];

                                           for (NSDictionary *dictOrders in ordersList) {
                                               Order *order = [Order MR_findFirstOrCreateByAttribute:@"id"
                                                                                           withValue:dictOrders[@"id"]
                                                                                           inContext:localContext];
                                               [existsArrayIds addObject:dictOrders[@"id"]];

                                               [self fillOrder:order
                                                  byDictionary:dictOrders
                                                       context:localContext];
                                           }

                                           [Order MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@)", existsArrayIds]
                                                                      inContext:localContext];
                                       }
                                             completion:^(BOOL contextDidSave, NSError *saveError) {
                                                 [self handleSuccessWithDelegate:delegate result:nil];
                                             }];
                           }];
}

+ (void)fillOrder:(Order *)order byDictionary:(NSDictionary *)dictOrders context:(NSManagedObjectContext *)context {
    if (![order.id isEqualToNumber:dictOrders[@"id"]])
        order.id = dictOrders[@"id"];
    
    if (![order.received_at isEqualToNumber:dictOrders[@"received_at"]])
        order.received_at = dictOrders[@"received_at"];
    
    if (![order.summ isEqualToNumber:dictOrders[@"summ"]])
        order.summ = dictOrders[@"summ"];
    
    if (![order.delivery_type isEqualToNumber:dictOrders[@"delivery_type"]])
        order.delivery_type = dictOrders[@"delivery_type"];
    
    if (![order.phone isEqualToString:dictOrders[@"phone"]])
        order.phone = dictOrders[@"phone"];
    
    NSDictionary *addressDict = dictOrders[@"address"];
    if (![order.street isEqualToString:addressDict[@"street"]])
        order.street = addressDict[@"street"];
    
    if (![order.house isEqualToString:addressDict[@"house"]])
        order.house = addressDict[@"house"];
    
    if (![order.flat isEqualToString:addressDict[@"flat"]])
        order.flat = addressDict[@"flat"];
    
    NSArray *productsList = dictOrders[@"products"];
    NSInteger i = 0;
    for (NSDictionary *dictProducts in productsList) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"prod_id == %@ AND order == %@", dictProducts[@"product_id"], order];
        OrderProduct *product = [OrderProduct MR_findFirstWithPredicate:predicate
                                                              inContext:context];
        if (!product)
            product = [OrderProduct MR_createEntityInContext:context];
        
        product.prod_id = dictProducts[@"product_id"];
        product.position = @(i++);
        product.title = dictProducts[@"name"];
        product.price = dictProducts[@"price"];
        product.amount = dictProducts[@"amount"];
        product.order = order;
        NSArray *props = dictProducts[@"props"];
        if ([props count] > 0) {
            for (NSDictionary *dictProp in props) {
                if ([dictProp[@"key"] isEqualToString:@"noodles_rice"]) {
                    product.noodles_rice_id = [MADataFormatter numberByString:dictProp[@"id"]];
                    product.noodles_rice = dictProp[@"description"];
                    product.noodles_rice_value = dictProp[@"key"];
                } else {
                    product.sauce_id = [MADataFormatter numberByString:dictProp[@"id"]];
                    product.sauce = dictProp[@"description"];
                    product.sauce_value = dictProp[@"key"];
                }
            }
        }
    }
}

@end
