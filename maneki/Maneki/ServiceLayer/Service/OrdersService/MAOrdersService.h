//
//  MAOrdersService.h
//  Maneki
//
//  Created by Maximychev Evgeny on 19.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MABaseService.h"

@interface MAOrdersService : MABaseService

+ (NSFetchedResultsController *)getOrdersListWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

@end
