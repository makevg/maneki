//
//  MASaleService.m
//  Maneki
//
//  Created by Максимычев Е.О. on 14.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/NSManagedObject+MagicalDataImport.h>
#import "Sale.h"
#import "City.h"
#import "MADataFormatter.h"

NSString *const salesUrl = @"http://api.manekicafe.ru/actions/";

#import "MASaleService.h"

@implementation MASaleService

+ (NSFetchedResultsController *)getSalesListForCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    NSFetchedResultsController *fetchedResultsController = [Sale MR_fetchAllSortedBy:@"id"
                                                                           ascending:NO
                                                                       withPredicate:[NSPredicate predicateWithFormat:@"city = %@", city]
                                                                             groupBy:NULL
                                                                            delegate:NULL
                                                                           inContext:[MANEKI_DB produceContextForRead]];
    [self updateSalesInCity:city withDelegate:delegate];
    return fetchedResultsController;
}

+ (void)updateSalesInCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:salesUrl
                            andParams:@{@"city" : city.string_id}
                           andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                               if ([self handleResponse:response
                                     withResponseObject:responseObject
                                              withError:error
                                           withDelegate:delegate]) {
                                   return;
                               }

                               [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                           City *localCity = [city MR_inContext:localContext];
                                           NSDictionary *responseDict = responseObject;
                                           NSArray *salesList = responseDict[@"actions"];
                                           NSMutableArray *existsArrayIds = [@[] mutableCopy];

                                           for (NSDictionary *dictSales in salesList) {
                                               Sale *sale = [Sale MR_findFirstOrCreateByAttribute:@"id"
                                                                                        withValue:[MADataFormatter numberByString:dictSales[@"id"]]
                                                                                        inContext:localContext];
                                               [existsArrayIds addObject:[MADataFormatter numberByString:dictSales[@"id"]]];
                                               if (!    [sale.city isEqual:localCity])
                                                   sale.city = localCity;

                                               [self fillSale:sale
                                                 byDictionary:dictSales];
                                           }

                                           [Sale MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@) and city = %@", existsArrayIds, localCity]
                                                                     inContext:localContext];
                                       }
                                             completion:^(BOOL contextDidSave, NSError *saveError) {
                                                 [self handleSuccessWithDelegate:delegate result:nil];
                                             }];
                           }];
}

+ (void)fillSale:(Sale *)sale byDictionary:(NSDictionary *)dictSales {
    if (![sale.id isEqualToNumber:[MADataFormatter numberByString:dictSales[@"id"]]])
        sale.id = [MADataFormatter numberByString:dictSales[@"id"]];

    if (![sale.title isEqualToString:[MADataFormatter stringByObject:dictSales[@"caption"]]])
        sale.title = [MADataFormatter stringByObject:dictSales[@"caption"]];

    if (![sale.short_descr isEqualToString:[MADataFormatter stringByObject:dictSales[@"short_descr"]]])
        sale.short_descr = [MADataFormatter stringByObject:dictSales[@"short_descr"]];

    NSString *long_descr = [MADataFormatter stringByObject:dictSales[@"long_descr"]];
    long_descr = [long_descr length] > 0 ? long_descr : sale.short_descr;
    long_descr = [long_descr stringByReplacingOccurrencesOfString:@"src=\""
                                                       withString:[NSString stringWithFormat:@"%@%@", @"src=\"", @"https://manekicafe.ru"]];

    long_descr = [long_descr stringByReplacingOccurrencesOfString:@"<img"
                                                       withString:@"<img style=\"height:auto;max-width:100%\""];

    long_descr = [NSString stringWithFormat:@"<body style=\"font-family:'RotondaC';line-height: 16pt;\">%@</body>", long_descr];

    if (![sale.long_descr isEqualToString:long_descr])
        sale.long_descr = long_descr;

    if (![sale.image_url isEqualToString:[MADataFormatter stringByObject:dictSales[@"image_url"]]])
        sale.image_url = [MADataFormatter stringByObject:dictSales[@"image_url"]];

    if (![sale.preview_image_url isEqualToString:[MADataFormatter stringByObject:dictSales[@"preview_image_url"]]])
        sale.preview_image_url = [MADataFormatter stringByObject:dictSales[@"preview_image_url"]];
}

@end
