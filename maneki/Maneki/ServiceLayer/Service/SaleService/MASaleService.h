//
//  MASaleService.h
//  Maneki
//
//  Created by Максимычев Е.О. on 14.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MABaseService.h"

@class City;

@interface MASaleService : MABaseService

+ (NSFetchedResultsController*)getSalesListForCity:(City *)city withDelegate:(__weak id <ServiceResultDelegate>)delegate;

@end
