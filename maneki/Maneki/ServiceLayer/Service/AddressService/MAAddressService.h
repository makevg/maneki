//
//  MAAddressService.h
//  Maneki
//
//  Created by Maximychev Evgeny on 02.02.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MABaseService.h"

@class Address;

@interface MAAddressService : MABaseService

+ (Address *)addressById:(NSManagedObjectID *)addressId;

+ (NSFetchedResultsController *)getAddressListWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

+ (void)addAddressesWithStreet:(NSString *)street
                         house:(NSString *)house
                          flat:(NSString *)flat
                         porch:(NSString *)porch
                         floor:(NSString *)floor
                    porch_code:(NSString *)porch_code
                      delegate:(__weak id <ServiceResultDelegate>)delegate;

+ (void)updateAddressesById:(NSNumber *)addressId
                     street:(NSString *)street
                      house:(NSString *)house
                       flat:(NSString *)flat
                      porch:(NSString *)porch
                      floor:(NSString *)floor
                 porch_code:(NSString *)porch_code
                   delegate:(__weak id <ServiceResultDelegate>)delegate;

+ (void)deleteAddress:(Address *)address delegate:(__weak id <ServiceResultDelegate>)delegate;

+ (void)deleteAddressesWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

+ (void)setIsMainAddress:(Address *)address delegate:(__weak id <ServiceResultDelegate>)delegate;

@end
