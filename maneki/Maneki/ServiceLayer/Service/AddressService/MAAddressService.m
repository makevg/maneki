//
//  MAAddressService.m
//  Maneki
//
//  Created by Maximychev Evgeny on 02.02.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import "MAAddressService.h"
#import "MADataFormatter.h"
#import "Address.h"

NSString *const cGetAdressesUrl = @"http://api.manekicafe.ru/personal/address/?list";
NSString *const cAddAdressUrl = @"http://api.manekicafe.ru/personal/address/?create";
NSString *const cAdressUrl = @"http://api.manekicafe.ru/personal/address/";

//Работа с адресами пользователя:
//  -------------------------------
//  /personal/address/?list - список адресов
//
//  /personal/address/?set_main=<address_id>
//
//  /personal/address/?create - создать адрес
//      POST:
//      - street
//      - house
//      - room
//      - floor
//      - porch_code
//      - porch
//
//  /personal/address/?update=<address_id>
//какие поля переданы те и обновятся.
//      POST:
//      - street
//      - house
//      - room
//      - floor
//      - porch_code
//      - porch
//
//  /personal/address/?delete=<address_id>

@implementation MAAddressService

#pragma mark - Public API

+ (Address *)addressById:(NSManagedObjectID *)addressId {
    return [[MANEKI_DB produceContextForRead] objectRegisteredForID:addressId];
}

+ (NSFetchedResultsController *)getAddressListWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    NSFetchedResultsController *fetchedResultsController = [Address MR_fetchAllSortedBy:@"street"
                                                                              ascending:YES
                                                                          withPredicate:[NSPredicate predicateWithValue:YES]
                                                                                groupBy:NULL
                                                                               delegate:NULL
                                                                              inContext:[MANEKI_DB produceContextForRead]];
    [self updateAddressesListWithDelegate:delegate];
    return fetchedResultsController;
}

+ (void)updateAddressesListWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:cGetAdressesUrl
                            andParams:nil
                           andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                               if ([[self class] handleResponse:response
                                             withResponseObject:responseObject
                                                      withError:error
                                                   withDelegate:delegate]) {
                                   return;
                               }
                               
                               [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                   NSDictionary *responseDict = responseObject;
                                   NSArray *addressesList = responseDict[@"address"];
                                   NSMutableArray *existsArrayIds = [@[] mutableCopy];
                                   
                                   for (NSDictionary *dictAddress in addressesList) {
                                       Address *address = [Address MR_findFirstOrCreateByAttribute:@"id"
                                                                                         withValue:[MADataFormatter numberByString:dictAddress[@"id"]]
                                                                                         inContext:localContext];
                                       [existsArrayIds addObject:[MADataFormatter numberByString:dictAddress[@"id"]]];
                                       
                                       [MAAddressService fillAddress:address
                                                        byDictionary:dictAddress];
                                   }
                                   
                                   [Address MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@)", existsArrayIds]
                                                                inContext:localContext];
                               }
                                             completion:^(BOOL contextDidSave, NSError *error) {
                                                 [[self class] handleSuccessWithDelegate:delegate result:nil];
                                             }];
                               
                           }];
}

+ (void)fillAddress:(Address *)address byDictionary:(NSDictionary *)dictAddress {
    if (![address.id isEqualToNumber:[MADataFormatter numberByString:dictAddress[@"id"]]])
        address.id = [MADataFormatter numberByString:dictAddress[@"id"]];
    
    if (![address.street isEqualToString:[MADataFormatter stringByObject:dictAddress[@"street"]]])
        address.street = [MADataFormatter stringByObject:dictAddress[@"street"]];
    
    if (![address.house isEqualToString:[MADataFormatter stringByObject:dictAddress[@"house"]]])
        address.house = [MADataFormatter stringByObject:dictAddress[@"house"]];
    
    if (![address.flat isEqualToNumber:[MADataFormatter numberByString:dictAddress[@"room"]]])
        address.flat = [MADataFormatter numberByString:dictAddress[@"room"]];
    
    if (![address.floor isEqualToNumber:[MADataFormatter numberByString:dictAddress[@"floor"]]])
        address.floor = [MADataFormatter numberByString:dictAddress[@"floor"]];
    
    if (![address.porch_code isEqualToString:[MADataFormatter stringByObject:dictAddress[@"porch_code"]]])
        address.porch_code = [MADataFormatter stringByObject:dictAddress[@"porch_code"]];
    
    if (![address.porch isEqualToNumber:[MADataFormatter numberByString:dictAddress[@"porch"]]])
        address.porch = [MADataFormatter numberByString:dictAddress[@"porch"]];
    
    if (![address.is_main isEqualToNumber:dictAddress[@"is_main"]])
        address.is_main = dictAddress[@"is_main"];
}

+ (void)addAddressesWithStreet:(NSString *)street
                         house:(NSString *)house
                          flat:(NSString *)flat
                         porch:(NSString *)porch
                         floor:(NSString *)floor
                    porch_code:(NSString *)porch_code
                      delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestPostToUrlString:cAddAdressUrl
                             andParams:@{@"street" : street,
                                         @"house" : house,
                                         @"room" : flat,
                                         @"floor" : floor,
                                         @"porch_code" : porch_code,
                                         @"porch" : porch}
                            andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                if ([[self class] handleResponse:response
                                              withResponseObject:responseObject
                                                       withError:error
                                                    withDelegate:delegate]) {
                                    return;
                                }
                                
                                [[self class] handleSuccessWithDelegate:delegate result:nil];
                            }];
}

+ (void)updateAddressesById:(NSNumber *)addressId
                     street:(NSString *)street
                      house:(NSString *)house
                       flat:(NSString *)flat
                      porch:(NSString *)porch
                      floor:(NSString *)floor
                 porch_code:(NSString *)porch_code
                   delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestPostToUrlString:cAdressUrl
                             andParams:@{@"update" : addressId,
                                         @"street" : street,
                                         @"house" : house,
                                         @"room" : flat,
                                         @"floor" : floor,
                                         @"porch_code" : porch_code,
                                         @"porch" : porch}
                            andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                if ([[self class] handleResponse:response
                                              withResponseObject:responseObject
                                                       withError:error
                                                    withDelegate:delegate]) {
                                    return;
                                }
                                
                                [[self class] handleSuccessWithDelegate:delegate result:nil];
                            }];
}

+ (void)deleteAddress:(Address *)address delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:cAdressUrl
                            andParams:@{@"delete" : address.id}
                           andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                               if ([[self class] handleResponse:response
                                             withResponseObject:responseObject
                                                      withError:error
                                                   withDelegate:delegate]) {
                                   return;
                               }
                               
                               [[self class] handleSuccessWithDelegate:delegate result:nil];
                           }];
}

+ (void)deleteAddressesWithDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
        [Address MR_deleteAllMatchingPredicate:[NSPredicate predicateWithValue:YES]
                                     inContext:localContext];
    }
                  completion:^(BOOL contextDidSave, NSError *error) {
                      [[self class] handleSuccessWithDelegate:delegate result:nil];
                  }];
}

+ (void)setIsMainAddress:(Address *)address delegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestGetToUrlString:cAdressUrl
                            andParams:@{@"set_main" : address.id}
                           andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                               if ([[self class] handleResponse:response
                                             withResponseObject:responseObject
                                                      withError:error
                                                   withDelegate:delegate]) {
                                   return;
                               }
                               
                               [[self class] handleSuccessWithDelegate:delegate result:nil];
                           }];
}

@end
