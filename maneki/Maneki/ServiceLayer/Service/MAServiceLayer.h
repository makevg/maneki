#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define dispatch_safe_main_sync(block)\
    if ([NSThread isMainThread]) {\
        block();\
    } else {\
        dispatch_sync(dispatch_get_main_queue(), block);\
    }

#define dispatch_safe_main_async(block)\
    if ([NSThread isMainThread]) {\
        block();\
    } else {\
        dispatch_async(dispatch_get_main_queue(), block);\
    }

@class Product;
@class MABaseService;
@class Restaurant;

@protocol ServiceResultDelegate <NSObject>

- (void)successResult:(id)object forService:(Class)service;

- (void)networkError:(NSError *)error forService:(Class)service;

- (void)serverError:(NSError *)error forService:(Class)service;

@end

#define MANEKI_SERVICE [MAServiceLayer sharedInstance]

typedef void(^ServiceHandler)(NSFetchedResultsController *);

typedef void(^ServiceHandlerArray)(NSArray<NSFetchedResultsController *> *);

@interface MAServiceLayer : NSObject

+ (instancetype)sharedInstance;

- (Product *)getProductByPk:(NSNumber *)pk;

- (NSFetchedResultsController *)getNewsListWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

- (NSFetchedResultsController *)getSalesListWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

- (NSFetchedResultsController *)getCitiesListWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

- (NSFetchedResultsController *)getReviewsListWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

- (NSFetchedResultsController *)getMyReviewsListWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

- (NSFetchedResultsController *)getRestaurantInfoWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

- (NSFetchedResultsController *)getOrdersListWithDelegate:(__weak id <ServiceResultDelegate>)delegate;

- (Restaurant *)getOnlyRestaurantInfo;

- (void)getProductsByCategoryCode:(NSString *)category receiver:(ServiceHandler)receiver delegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)getWokProductsByCategoryCode:(NSString *)category receiver:(ServiceHandlerArray)receiver delegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)getCategoriesListByCategoryCode:(NSString *)categoryCode receiver:(ServiceHandler)receiver delegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)addReviewWithText:(NSString *)text rate:(NSNumber *)rate delegate:(__weak id <ServiceResultDelegate>)delegate;

@end
