//
//  MACartService.h
//  Maneki
//
//  Created by Maximychev Evgeny on 11.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MABaseService.h"

@class MACartItem;
@protocol ServiceResultDelegate;
@class Product;
@class WokProperty;
@class WokPropertyValue;

typedef void(^MACartChangeSuccessHandler)(NSUInteger productId, NSUInteger amount);

typedef void(^MACartChangeFailureHandler)(NSUInteger productId, NSUInteger amount, NSError *error);

typedef void(^MACartSuccessHandler)();

typedef void(^MACartFailureHandler)(NSError *error);

typedef void(^MAOnlinePaySuccessHandler)(BOOL);

typedef void(^MAOnlinePayFailureHandler)(NSError *error);

#define CART_SERVICE [MACartService sharedInstance]

typedef NS_OPTIONS(NSUInteger, MAPayMethod) {
    MAPayMethodMoney = 0,
    MAPayMethodOnlineCard,
    MAPayMethodManualCard
};

typedef NS_OPTIONS(NSUInteger, MADeliveryMethod) {
    MADeliveryMethodDelivery = 0,
    MADeliveryMethodManual,
    MADeliveryMethodUnknown
};

@interface MACartService : MABaseService

@property(nonatomic, assign) MADeliveryMethod currentDeliveryMethod;

+ (instancetype)sharedInstance;

#pragma mark - API

- (void)updateProductId:(NSNumber *)productId
                 amount:(NSUInteger)amount
         successHandler:(MACartChangeSuccessHandler)successHandler
         failureHandler:(MACartChangeFailureHandler)failureHandler;

- (void)updateRiceId:(NSNumber *)riceId
           riceTitle:(NSString *)riceTitle
            riceCode:(NSString *)riceCode
           productId:(NSNumber *)productId
             sauceId:(NSNumber *)sauceId
          sauceTitle:(NSString *)sauceTitle
           sauceCode:(NSString *)sauceCode
              amount:(NSUInteger)amount
      successHandler:(MACartChangeSuccessHandler)successHandler
      failureHandler:(MACartChangeFailureHandler)failureHandler;

- (void)updateCartItem:(MACartItem *)item
                amount:(NSUInteger)amount
        successHandler:(MACartChangeSuccessHandler)successHandler
        failureHandler:(MACartChangeFailureHandler)failureHandler;

- (void)removeItemById:(NSUInteger)cartItemId
             productId:(NSUInteger)productId
        successHandler:(MACartChangeSuccessHandler)successHandler
        failureHandler:(MACartChangeFailureHandler)failureHandler;

- (void)updateCartInfoWithSuccessHandler:(MACartSuccessHandler)successHandler
                          failureHandler:(MACartFailureHandler)failureHandler;

- (void)sendOrderWithPayMethod:(MAPayMethod)payMethod
                  deliveryType:(MADeliveryMethod)deliveryMethod
                        street:(NSString *)street
                         house:(NSString *)house
                          room:(NSString *)room
                          name:(NSString *)name
                         phone:(NSString *)phone
                         porch:(NSString *)porch
                         floor:(NSString *)floor
                     porchCode:(NSString *)porchCode
                   description:(NSString *)description
                   saveAddress:(BOOL)saveAddress
                  withDelegate:(__weak id <ServiceResultDelegate>)delegate;

- (void)clearCartWithSuccessHandler:(MACartSuccessHandler)successHandler
                     failureHandler:(MACartFailureHandler)failureHandler;

- (void)checkPayOrderId:(NSNumber *)orderId
     withSuccessHandler:(MAOnlinePaySuccessHandler)successHandler
         failureHandler:(MAOnlinePayFailureHandler)failureHandler;

#pragma mark - For CartVC

- (BOOL)isMethodAvailable:(MADeliveryMethod)deliveryMethod;

- (NSInteger)itemPositionById:(NSUInteger)cartItemId;

- (NSNumber *)getMinOrderCostByDeliveryType:(MADeliveryMethod)deliveryMethod;

- (NSInteger)getSaleValueByDeliveryType:(MADeliveryMethod)deliveryMethod;

- (BOOL)canMoneyMethodByDeliveryType:(MADeliveryMethod)deliveryMethod;

- (BOOL)canOnlineCardMethodByDeliveryType:(MADeliveryMethod)deliveryMethod;

- (BOOL)canManualCardMethodByDeliveryType:(MADeliveryMethod)deliveryMethod;

- (BOOL)checkOrderAbilityByDeliveryType:(MADeliveryMethod)deliveryMethod;

- (NSArray <MACartItem *> *)getItems;

- (MACartItem *)getItemByProductId:(NSNumber *)productId;

- (BOOL)cartIsEmpty;

- (double)totalAmountByDeliveryType:(MADeliveryMethod)deliveryMethod;

@end
