//
// Created by Buravlev Mikhail on 18.01.16.
// Copyright (c) 2016 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MACartService.h"

@class MACartItem;


@interface MACartSource : NSObject

@property (nonatomic, strong) NSMutableArray <MACartItem *> *items;
//@property (nonatomic, retNSInteger itemId;

@property (nonatomic, strong) NSNumber *minOrderCost;
@property (nonatomic, strong) NSNumber *deliveryId;
@property (nonatomic, strong) NSNumber *saleValue;

@property (nonatomic, assign) MADeliveryMethod deliveryType;

@property (nonatomic, assign) BOOL canMoney;
@property (nonatomic, assign) BOOL canOnlineCard;
@property (nonatomic, assign) BOOL canManualCard;

@end