//
//  MACartService.m
//  Maneki
//
//  Created by Maximychev Evgeny on 11.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <Crashlytics/Crashlytics/CLSLogging.h>
#import "MACartService.h"
#import "MACartItem.h"
#import "City+CoreDataProperties.h"
#import "MAServiceLayer.h"
#import "Restaurant.h"
#import "ResraurantDeliveryInfo.h"
#import "MACityService.h"
#import "MACartSource.h"

NSString *const cartUrl = @"http://api.manekicafe.ru/cart/";
NSString *const orderUrl = @"http://api.manekicafe.ru/cart/order/";
NSString *const orderOnlineUrl = @"http://api.manekicafe.ru/cart/order/onlinepay/";

@implementation MACartService {
    MACartSource *m_deliverySource;
    MACartSource *m_selfSource;
}

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] initInstance];
    });
    return sharedInstance;
}

#pragma mark - Private

- (instancetype)initInstance {
    self = [super init];
    if (self) {
        m_deliverySource = [[MACartSource alloc] init];
        m_selfSource = [[MACartSource alloc] init];

        m_deliverySource.deliveryType = MADeliveryMethodDelivery;
        m_selfSource.deliveryType = MADeliveryMethodManual;

        self.currentDeliveryMethod = MADeliveryMethodUnknown;
    }
    return self;
}

- (MACartItem *)existItemByProductId:(NSNumber *)productId {
    MACartItem *existItem;
    for (MACartItem *item in [self getSource:self.currentDeliveryMethod].items) {
        if ([productId isEqualToNumber:item.productId]) {
            existItem = item;
            break;
        }
    }
    return existItem;
}

- (MACartItem *)existItemByRiceTitle:(NSString *)riceTitle
                           productId:(NSNumber *)productId
                           sousTitle:(NSString *)sousTitle {
    MACartItem *existItem;
    for (MACartItem *item in [self getSource:self.currentDeliveryMethod].items) {
        if ([riceTitle isEqualToString:item.riceTitle]
                && [productId isEqualToNumber:item.productId]
                && [sousTitle isEqualToString:item.sousTitle]) {
            existItem = item;
            break;
        }
    }
    return existItem;
}

- (MACartSource *)getSource:(MADeliveryMethod)deliveryMethod {
    switch (deliveryMethod) {
        case MADeliveryMethodDelivery:
        default:
            return m_deliverySource;
        case MADeliveryMethodManual:
            return m_selfSource;
    }
}

+ (NSError *)errorWithDescription:(NSString *)descr {
    return [NSError errorWithDomain:@"ru.maneki.domain"
                               code:1000
                           userInfo:@{NSLocalizedDescriptionKey : descr}];
}


+ (NSError *)unavailableDeliveryError {
    return [NSError errorWithDomain:@"ru.maneki.domain"
                               code:1000
                           userInfo:@{
                                   NSLocalizedDescriptionKey : @"Доставка не осуществляется, приносим извинения."
                           }];
}

#pragma mark - Public

- (void)updateProductId:(NSNumber *)productId
                 amount:(NSUInteger)amount
         successHandler:(MACartChangeSuccessHandler)successHandler
         failureHandler:(MACartChangeFailureHandler)failureHandler {
    if (self.currentDeliveryMethod == MADeliveryMethodUnknown) {
        failureHandler(0, 0, [[self class] unavailableDeliveryError]);
    }

    MACartItem *existItem = [self existItemByProductId:productId];

    if (existItem) {
        if (amount)
            [self updateCartItem:existItem
                          amount:amount
                  successHandler:successHandler
                  failureHandler:failureHandler];
        else
            [self removeItemById:existItem.itemId
                       productId:[existItem.productId unsignedIntegerValue]
                  successHandler:successHandler
                  failureHandler:failureHandler];
    } else {
        [MANEKI_API requestGetToUrlString:cartUrl
                                andParams:@{@"add" : productId,
                                            @"quantity" : @(amount),
                                            @"city" : [MACityService getCurrentCity].string_id}
                               andHandler:^(NSURLResponse *_, id __, NSError *___) {
                                   NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) _;
                                   NSDictionary *responseDict = __;
                                   NSString *errorDescr = responseDict[@"error"];
                                   if (httpResponse.statusCode == 500) {
                                       dispatch_safe_main_async(^{
                                           failureHandler([productId integerValue], 0, [[self class] errorWithDescription:errorDescr]);
                                       });
                                   } else {
                                       [self updateCartInfoWithSuccessHandler:^{
                                           MACartItem *item = [self getItemByProductId:productId];
                                           dispatch_safe_main_async(^{
                                               successHandler([item.productId unsignedIntegerValue], item.quantity);
                                           });
                                       }
                                                               failureHandler:^(NSError *_error) {
                                                                   MACartItem *item = [self getItemByProductId:productId];
                                                                   dispatch_safe_main_async(^{
                                                                       failureHandler([item.productId unsignedIntegerValue], item.quantity, _error);
                                                                   });
                                                               }];
                                   }
                               }];
    }
}

- (void)updateRiceId:(NSNumber *)riceId
           riceTitle:(NSString *)riceTitle
            riceCode:(NSString *)riceCode
           productId:(NSNumber *)productId
             sauceId:(NSNumber *)sauceId
          sauceTitle:(NSString *)sauceTitle
           sauceCode:(NSString *)sauceCode
              amount:(NSUInteger)amount
      successHandler:(MACartChangeSuccessHandler)successHandler
      failureHandler:(MACartChangeFailureHandler)failureHandler {
    if (self.currentDeliveryMethod == MADeliveryMethodUnknown) {
        failureHandler(0, 0, [[self class] unavailableDeliveryError]);
    }

    MACartItem *existItem = [self existItemByRiceTitle:riceTitle
                                             productId:productId
                                             sousTitle:sauceTitle];
    
    if (existItem) {
        [self updateCartItem:existItem
                      amount:amount
              successHandler:successHandler
              failureHandler:failureHandler];
    } else {
        [MANEKI_API requestGetToUrlString:cartUrl
                                andParams:@{@"add" : productId,
                                            @"quantity" : @(amount),
                                            @"prop" : @{riceCode : riceId,
                                                        sauceCode : sauceId},
                                            @"city" : [MACityService getCurrentCity].string_id}
                               andHandler:^(NSURLResponse *_, id __, NSError *___) {
                                   NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) _;
                                   NSDictionary *responseDict = __;
                                   NSString *errorDescr = responseDict[@"error"];
                                   if (httpResponse.statusCode == 500) {
                                       dispatch_safe_main_async(^{
                                           failureHandler([productId integerValue], 0, [[self class] errorWithDescription:errorDescr]);
                                       });
                                   } else {
                                       [self updateCartInfoWithSuccessHandler:^{
                                           MACartItem *item = [self getItemByProductId:productId];
                                           dispatch_safe_main_async(^{
                                               successHandler([item.productId unsignedIntegerValue], item.quantity);
                                           });
                                       }
                                                               failureHandler:^(NSError *_error) {
                                                                   MACartItem *item = [self getItemByProductId:productId];
                                                                   dispatch_safe_main_async(^{
                                                                       failureHandler([item.productId unsignedIntegerValue], item.quantity, _error);
                                                                   });
                                                               }];
                                   }
                               }];
    }
}

- (void)updateCartItem:(MACartItem *)item
                amount:(NSUInteger)amount
        successHandler:(MACartChangeSuccessHandler)successHandler
        failureHandler:(MACartChangeFailureHandler)failureHandler {
    if (self.currentDeliveryMethod == MADeliveryMethodUnknown) {
        failureHandler(0, 0, [[self class] unavailableDeliveryError]);
    }

    if (amount == 0) {
        return [self removeItemById:item.itemId
                          productId:[item.productId unsignedIntegerValue]
                     successHandler:successHandler
                     failureHandler:failureHandler];
    }

    NSNumber *productId = item.productId;

    [MANEKI_API requestGetToUrlString:cartUrl
                            andParams:@{
                                    @"update" : @(item.itemId),
                                    @"quantity" : @(amount),
                                    @"city" : [MACityService getCurrentCity].string_id
                            } andHandler:^(NSURLResponse *_, id __, NSError *___) {
                [self updateCartInfoWithSuccessHandler:^{
                            MACartItem *_item = [self getItemByProductId:productId];

                            successHandler([_item.productId unsignedIntegerValue], _item.quantity);
                        }
                                        failureHandler:^(NSError *_error) {
                                            MACartItem *_item = [self getItemByProductId:productId];

                                            failureHandler([_item.productId unsignedIntegerValue], _item.quantity, _error);
                                        }];
            }

    ];
}

- (void)removeItemById:(NSUInteger)cartItemId
             productId:(NSUInteger)productId
        successHandler:(MACartChangeSuccessHandler)successHandler
        failureHandler:(MACartChangeFailureHandler)failureHandler {
    if (self.currentDeliveryMethod == MADeliveryMethodUnknown) {
        failureHandler(0, 0, [[self class] unavailableDeliveryError]);
    }

    [MANEKI_API requestGetToUrlString:cartUrl
                            andParams:@{
                                    @"delete" : @(cartItemId),
                                    @"city" : [MACityService getCurrentCity].string_id
                            } andHandler:^(NSURLResponse *_, id __, NSError *___) {
                [self updateCartInfoWithSuccessHandler:^{
                            successHandler(productId, 0);
                        }
                                        failureHandler:^(NSError *_error) {
                                            NSMutableArray *items = [self getSource:self.currentDeliveryMethod].items;

                                            @synchronized (items) {
                                                for (MACartItem *item in items) {
                                                    if (cartItemId == item.itemId) {
                                                        failureHandler(productId, item.quantity, _error);
                                                        return;
                                                    }
                                                }
                                            }

                                            failureHandler(productId, 0, _error);
                                        }];
            }];
}

- (BOOL)isMethodAvailable:(MADeliveryMethod)deliveryMethod {
    switch (deliveryMethod) {
        case MADeliveryMethodDelivery:
            return m_deliverySource.deliveryId != nil;
        case MADeliveryMethodManual:
            return m_selfSource.deliveryId != nil;
        default:
            return NO;
    }
}

- (NSInteger)itemPositionById:(NSUInteger)cartItemId {
    NSInteger position = 0;
    NSArray *items = [self getSource:self.currentDeliveryMethod].items;
    for (NSUInteger i = 0; i < [items count]; i++) {
        if (cartItemId == [items[i] itemId]) {
            position = i;
            break;
        }
    }
    return position;
}

- (MACartItem *)getItemByProductId:(NSNumber *)productId {
    MACartItem *item;
    for (MACartItem *cartItem in [self getSource:self.currentDeliveryMethod].items) {
        if ([productId isEqualToNumber:cartItem.productId]) {
            item = cartItem;
            break;
        }
    }

    return item;
}

- (NSArray <MACartItem *> *)getItems {
    return [self getSource:self.currentDeliveryMethod].items;
}

- (void)clearCartWithSuccessHandler:(MACartSuccessHandler)successHandler
                     failureHandler:(MACartFailureHandler)failureHandler {
    [MANEKI_API requestGetToUrlString:cartUrl
                            andParams:@{
                                    @"clear" : @YES
                            } andHandler:^(NSURLResponse *_, id __, NSError *___) {
                [self updateCartInfoWithSuccessHandler:^{
                            successHandler();
                        }
                                        failureHandler:^(NSError *_error) {
                                            [m_selfSource.items removeAllObjects];
                                            [m_deliverySource.items removeAllObjects];

                                            m_selfSource.saleValue = @0;
                                            m_deliverySource.saleValue = @0;

                                            failureHandler(_error);
                                        }];
            }

    ];
}

- (void)checkPayOrderId:(NSNumber *)orderId
     withSuccessHandler:(MAOnlinePaySuccessHandler)successHandler
         failureHandler:(MAOnlinePayFailureHandler)failureHandler {
    [MANEKI_API requestGetToUrlString:orderOnlineUrl
                            andParams:@{
                                    @"check_payment" : orderId
                            } andHandler:^(NSURLResponse *_, id responseObject, NSError *_error) {
                if (_error) {
                    dispatch_safe_main_async(^{
                        failureHandler(_error);
                    });

                    return;
                }

                dispatch_safe_main_async(^{
                    successHandler([responseObject[@"result"] boolValue]);
                });
            }

    ];
}

- (BOOL)cartIsEmpty {
    if (self.currentDeliveryMethod == MADeliveryMethodUnknown)
        return YES;

    return [[self getSource:self.currentDeliveryMethod].items count] == 0;
}

- (double)totalAmountByDeliveryType:(MADeliveryMethod)deliveryMethod {
    double total = 0;
    for (MACartItem *item in [self getSource:deliveryMethod].items) {
        total += ([item.cost doubleValue] * item.quantity);
    }

//    total = total - total * [[self getSource:deliveryMethod].saleValue integerValue] / 100;
    return total;
}

- (NSNumber *)getMinOrderCostByDeliveryType:(MADeliveryMethod)deliveryMethod {
    return [self getSource:deliveryMethod].minOrderCost;
}

- (NSInteger)getSaleValueByDeliveryType:(MADeliveryMethod)deliveryMethod {
    return [[self getSource:deliveryMethod].saleValue integerValue];
}

- (BOOL)canMoneyMethodByDeliveryType:(MADeliveryMethod)deliveryMethod {
    return [self getSource:deliveryMethod].canMoney;
}

- (BOOL)canOnlineCardMethodByDeliveryType:(MADeliveryMethod)deliveryMethod {
    return [self getSource:deliveryMethod].canOnlineCard;
}

- (BOOL)canManualCardMethodByDeliveryType:(MADeliveryMethod)deliveryMethod {
    return [self getSource:deliveryMethod].canManualCard;
}

- (BOOL)checkOrderAbilityByDeliveryType:(MADeliveryMethod)deliveryMethod {
    return [self totalAmountByDeliveryType:deliveryMethod] >= [[self getSource:deliveryMethod].minOrderCost doubleValue];
}

#pragma mark - online

- (void)fillSource:(MACartSource *)source
    byDeliveryType:(MADeliveryMethod)deliveryMethod
    successHandler:(MACartSuccessHandler)successHandler
    failureHandler:(MACartFailureHandler)failureHandler {
    Restaurant *restaurant = [MANEKI_SERVICE getOnlyRestaurantInfo];
    source.deliveryId = nil;
    source.deliveryType = deliveryMethod;

    for (ResraurantDeliveryInfo *payMethod in restaurant.delivety_infos) {
        if ([payMethod.delivery_type isEqualToNumber:@(deliveryMethod)]) {
            source.deliveryId = payMethod.id;
            break;
        }
    }

    if (!source.deliveryId) {
        @synchronized (source.items) {
            source.minOrderCost = @0;
            source.saleValue = @0;
            [source.items removeAllObjects];
        }

        successHandler();
        return;
    }

    [MANEKI_API requestGetToUrlString:cartUrl
                            andParams:@{
                                    @"city" : restaurant.city.string_id,
                                    @"delivery_id" : source.deliveryId
                            } andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {

                if (!response) {
                    NSError *networkError = [NSError errorWithDomain:error.domain
                                                                code:error.code
                                                            userInfo:@{
                                                                    NSLocalizedDescriptionKey : @"Проблемы с подключением к интернету"
                                                            }];

                    dispatch_safe_main_async(^{
                        failureHandler(networkError);
                    });

                    return;
                }

                if (error) {
                    dispatch_safe_main_async(^{
                        failureHandler(error);
                    });

                    return;
                }

                NSDictionary *cartInfo = responseObject[@"info"];
                NSDictionary *cartProducts = responseObject[@"cart"];
                NSDictionary *allowedMethods = cartInfo[@"allowed_methods"];

                for (NSString *key in allowedMethods) {
                    if ([key isEqualToString:@"money"]) {
                        source.canMoney = [allowedMethods[key] boolValue];
                    }

                    if ([key isEqualToString:@"online_card"]) {
                        source.canOnlineCard = [allowedMethods[key] boolValue];
                    }

                    if ([key isEqualToString:@"manual_card"]) {
                        source.canManualCard = [allowedMethods[key] boolValue];
                    }
                }

                source.minOrderCost = cartInfo[@"min_payment_value"];
                source.saleValue = @0;

                @synchronized (source.items) {
                    [source.items removeAllObjects];
                    if (![cartProducts isKindOfClass:[NSNull class]]) {
                        for (NSDictionary *product in cartProducts) {
                            NSDictionary *wokProperties = product[@"wok_property"];
                            NSNumber* saleValue = product[@"discount_price_percent"];
                            
                            if( [saleValue integerValue] > [source.saleValue integerValue] )
                                source.saleValue = saleValue;

                            NSString *riceTitle = nil;
                            NSString *sousTitle = nil;

                            if (wokProperties) {
                                for (NSString *key in wokProperties) {
                                    if ([key isEqualToString:@"noodles_rice"]) {
                                        riceTitle = wokProperties[key][@"value"];
                                    }

                                    if ([key isEqualToString:@"sauce"]) {
                                        sousTitle = wokProperties[key][@"value"];
                                    }
                                }
                            }

                            NSNumber *price = product[@"price"];
//                            NSNumber *discountPrice = product[@"discount_price"];

                            if (!riceTitle && !sousTitle) {
                                MACartItem *item = [[MACartItem alloc] initWithItemId:[product[@"id"] unsignedIntegerValue]
                                                                            productId:product[@"product_id"]
                                                                                title:product[@"name"]
                                                                                descr:product[@"preview_text"]
                                                                                 cost:price
                                                                             quantity:[product[@"quantity"] unsignedIntegerValue]];
                                [source.items addObject:item];
                            } else {
                                MACartItem *item = [[MACartItem alloc] initWithItemId:[product[@"id"] unsignedIntegerValue]
                                                                            riceTitle:riceTitle
                                                                            productId:product[@"product_id"]
                                                                         productTitle:product[@"name"]
                                                                            sousTitle:sousTitle
                                                                                 cost:price
                                                                             quantity:[product[@"quantity"] unsignedIntegerValue]];
                                [source.items addObject:item];
                            }
                        }
                    }
                }

                dispatch_safe_main_async(^{
                    successHandler();
                });
            }];
}

- (void)updateCartInfoWithSuccessHandler:(MACartSuccessHandler)successHandler
                          failureHandler:(MACartFailureHandler)failureHandler {
    [self fillSource:m_selfSource
      byDeliveryType:MADeliveryMethodManual
      successHandler:^{
          [self fillSource:m_deliverySource
            byDeliveryType:MADeliveryMethodDelivery
            successHandler:^{
                dispatch_safe_main_async(^{
                    if (m_deliverySource.deliveryId) {
                        self.currentDeliveryMethod = m_deliverySource.deliveryType;
                    }
                    else if (m_selfSource.deliveryId) {
                        self.currentDeliveryMethod = m_selfSource.deliveryType;
                    } else {
                        self.currentDeliveryMethod = MADeliveryMethodUnknown;
                    }

                    successHandler();
                });
            }
            failureHandler:^(NSError *error) {
                dispatch_safe_main_async(^{
                    failureHandler(error);
                });
            }];
      }
      failureHandler:^(NSError *error) {
          dispatch_safe_main_async(^{
              failureHandler(error);
          });
      }];
}

- (void)sendOrderWithPayMethod:(MAPayMethod)payMethod
                  deliveryType:(MADeliveryMethod)deliveryMethod
                        street:(NSString *)street
                         house:(NSString *)house
                          room:(NSString *)room
                          name:(NSString *)name
                         phone:(NSString *)phone
                         porch:(NSString *)porch
                         floor:(NSString *)floor
                     porchCode:(NSString *)porchCode
                   description:(NSString *)description
                   saveAddress:(BOOL)saveAddress
                  withDelegate:(__weak id <ServiceResultDelegate>)delegate {
    [MANEKI_API requestPostToUrlString:orderUrl
                             andParams:@{
                                     @"city" : [MACityService getCurrentCity].string_id,
                                     @"delivery_id" : [self getSource:deliveryMethod].deliveryId,
                                     @"pay" : [MACartService payConverter:payMethod],
                                     @"street" : street,
                                     @"house" : house,
                                     @"room" : room,
                                     @"name" : name,
                                     @"phone" : phone,
                                     @"floor" : floor,
                                     @"porch_code" : porchCode,
                                     @"porch" : porch,
                                     @"description" : description,
                                     @"save_address" : saveAddress ? @"y" : @"n"
                             } andHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                if ([[self class] handleResponse:response
                              withResponseObject:responseObject
                                       withError:error
                                    withDelegate:delegate]) {
                    return;
                }

                CLS_LOG(@"%@", responseObject);
                [[self class] handleSuccessWithDelegate:delegate result:responseObject[@"order"]];
            }];
}

+ (NSString *)payConverter:(MAPayMethod)payMethod {
    switch (payMethod) {
        case MAPayMethodMoney:
        default:
            return @"money";
        case MAPayMethodOnlineCard:
            return @"online_card";
        case MAPayMethodManualCard:
            return @"manual_card";
    }
}


@end