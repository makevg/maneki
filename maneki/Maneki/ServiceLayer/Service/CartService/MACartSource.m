//
// Created by Buravlev Mikhail on 18.01.16.
// Copyright (c) 2016 Maneki. All rights reserved.
//

#import "MACartSource.h"
#import "MACartItem.h"


@implementation MACartSource

- (instancetype)init {
    if( self = [super init] ) {
        self.items = [@[] mutableCopy];
        self.saleValue = @0;
    }

    return self;
}

@end