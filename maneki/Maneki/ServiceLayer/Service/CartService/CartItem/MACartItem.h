//
//  MACartItem.h
//  Maneki
//
//  Created by Maximychev Evgeny on 11.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MACartItem : NSObject

@property(nonatomic) NSUInteger itemId;
@property(nonatomic) NSNumber *productId;
@property(nonatomic) NSString *productTitle;
@property(nonatomic) NSString *productDescr;
@property(nonatomic) NSString *riceTitle;
@property(nonatomic) NSString *sousTitle;
@property(nonatomic) NSNumber *cost;
@property(nonatomic) NSUInteger quantity;

- (instancetype)initWithItemId:(NSUInteger)itemId
                     productId:(NSNumber *)productId
                         title:(NSString *)title
                         descr:(NSString *)descr
                          cost:(NSNumber *)cost
                      quantity:(NSUInteger)quantity;

- (instancetype)initWithItemId:(NSUInteger)itemId
                     riceTitle:(NSString *)riceTitle
                     productId:(NSNumber *)productId
                  productTitle:(NSString *)prodTitle
                     sousTitle:(NSString *)sousTitle
                          cost:(NSNumber *)cost
                      quantity:(NSUInteger)quantity;

@end
