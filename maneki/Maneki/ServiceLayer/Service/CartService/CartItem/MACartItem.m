//
//  MACartItem.m
//  Maneki
//
//  Created by Maximychev Evgeny on 11.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MACartItem.h"

@implementation MACartItem

#pragma mark - Init

- (instancetype)initWithItemId:(NSUInteger)itemId
                     productId:(NSNumber *)productId
                         title:(NSString *)title
                         descr:(NSString *)descr
                          cost:(NSNumber *)cost
                      quantity:(NSUInteger)quantity {
    self = [super init];
    if (self) {
        _itemId = itemId;
        _productId = productId;
        _productTitle = title;
        _productDescr = descr;
        _cost = cost;
        _quantity = quantity;
    }
    return self;
}

- (instancetype)initWithItemId:(NSUInteger)itemId
                     riceTitle:(NSString *)riceTitle
                     productId:(NSNumber *)productId
                  productTitle:(NSString *)prodTitle
                     sousTitle:(NSString *)sousTitle
                          cost:(NSNumber *)cost
                      quantity:(NSUInteger)quantity {
    self = [super init];
    if (self) {
        _itemId = itemId;
        _riceTitle = riceTitle;
        _sousTitle = sousTitle;
        _productId = productId;
        _productTitle = @"Вок-конструктор";
        _productDescr = [MACartItem wokDescrByRiceTitle:riceTitle
                                           productTitle:prodTitle
                                              sousTitle:sousTitle];
        _cost = cost;
        _quantity = quantity;
    }
    return self;
}

#pragma mark - Private

+ (NSString *)wokDescrByRiceTitle:(NSString *)riceTitle
                     productTitle:(NSString *)prodTitle
                        sousTitle:(NSString *)sousTitle {
    return [NSString stringWithFormat:@"1. %@\n2. %@\n3. %@", riceTitle, prodTitle, sousTitle];
}

@end
