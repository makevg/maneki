#import <Foundation/Foundation.h>
#import "MAAPI.h"
#import "MADB.h"

extern NSString *const kErrorDomain;
extern NSInteger const kUnknownErrorCode;

@protocol ServiceResultDelegate;

@interface MABaseService : NSObject

+ (NSError *)handleResponse:(NSURLResponse *)response withResponseObject:(id)responseObject withError:(NSError *)error withDelegate:(__weak id <ServiceResultDelegate>)delegate;

+ (void)handleSuccessWithDelegate:(__weak id <ServiceResultDelegate>)delegate result:(id)result;

@end
