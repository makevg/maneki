//
//  User+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 18.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"
#import "Address.h"

@implementation User (CoreDataProperties)

@dynamic fb;
@dynamic id;
@dynamic name;
@dynamic phone;
@dynamic vk;
@dynamic my_city;
@dynamic adresses;

@end
