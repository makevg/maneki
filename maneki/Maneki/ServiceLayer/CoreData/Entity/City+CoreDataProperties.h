//
//  City+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 17.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "City.h"

@class Review;

NS_ASSUME_NONNULL_BEGIN

@interface City (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *position;
@property (nullable, nonatomic, retain) NSString *string_id;
@property (nullable, nonatomic, retain) NSSet<ProductCategory *> *categories;
@property (nullable, nonatomic, retain) NSSet<News *> *news;
@property (nullable, nonatomic, retain) Restaurant *restaurant;
@property (nullable, nonatomic, retain) NSSet<Sale *> *sales;
@property (nullable, nonatomic, retain) User *user;
@property (nullable, nonatomic, retain) NSSet<Review *> *reviews;
@property (nullable, nonatomic, retain) NSSet<WokProperty *> *wokProperty;

@end

@interface City (CoreDataGeneratedAccessors)

- (void)addCategoriesObject:(ProductCategory *)value;
- (void)removeCategoriesObject:(ProductCategory *)value;
- (void)addCategories:(NSSet<ProductCategory *> *)values;
- (void)removeCategories:(NSSet<ProductCategory *> *)values;

- (void)addNewsObject:(News *)value;
- (void)removeNewsObject:(News *)value;
- (void)addNews:(NSSet<News *> *)values;
- (void)removeNews:(NSSet<News *> *)values;

- (void)addSalesObject:(Sale *)value;
- (void)removeSalesObject:(Sale *)value;
- (void)addSales:(NSSet<Sale *> *)values;
- (void)removeSales:(NSSet<Sale *> *)values;

- (void)addReviewsObject:(Review *)value;
- (void)removeReviewsObject:(Review *)value;
- (void)addReviews:(NSSet<Review *> *)values;
- (void)removeReviews:(NSSet<Review *> *)values;

- (void)addWokPropertyObject:(WokProperty *)value;
- (void)removeWokPropertyObject:(WokProperty *)value;
- (void)addWokProperty:(NSSet<WokProperty *> *)values;
- (void)removeWokProperty:(NSSet<WokProperty *> *)values;

@end

NS_ASSUME_NONNULL_END
