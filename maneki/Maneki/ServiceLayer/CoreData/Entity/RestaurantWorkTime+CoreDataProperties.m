//
//  RestaurantWorkTime+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RestaurantWorkTime+CoreDataProperties.h"

@implementation RestaurantWorkTime (CoreDataProperties)

@dynamic dow;
@dynamic id;
@dynamic time_from;
@dynamic time_to;
@dynamic resraurant;

@end
