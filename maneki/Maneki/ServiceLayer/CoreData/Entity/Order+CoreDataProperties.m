//
//  Order+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Order+CoreDataProperties.h"

@implementation Order (CoreDataProperties)

@dynamic id;
@dynamic received_at;
@dynamic sale_value;
@dynamic self_receive;
@dynamic status;
@dynamic summ;
@dynamic delivery_type;
@dynamic user_name;
@dynamic phone;
@dynamic street;
@dynamic house;
@dynamic flat;
@dynamic set;

@end
