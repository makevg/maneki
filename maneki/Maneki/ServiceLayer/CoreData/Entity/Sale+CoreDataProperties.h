//
//  Sale+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Sale.h"

NS_ASSUME_NONNULL_BEGIN

@interface Sale (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *image_url;
@property (nullable, nonatomic, retain) NSString *long_descr;
@property (nullable, nonatomic, retain) NSString *preview_image_url;
@property (nullable, nonatomic, retain) NSNumber *published_at;
@property (nullable, nonatomic, retain) NSString *short_descr;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) City *city;

@end

NS_ASSUME_NONNULL_END
