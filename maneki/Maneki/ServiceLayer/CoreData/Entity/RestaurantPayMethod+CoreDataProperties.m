//
//  RestaurantPayMethod+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 17.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RestaurantPayMethod+CoreDataProperties.h"

@implementation RestaurantPayMethod (CoreDataProperties)

@dynamic desctiption;
@dynamic id;
@dynamic position;
@dynamic title;
@dynamic restaurant;

@end
