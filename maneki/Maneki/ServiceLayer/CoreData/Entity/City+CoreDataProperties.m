//
//  City+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 17.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "City+CoreDataProperties.h"
#import "Review.h"

@implementation City (CoreDataProperties)

@dynamic id;
@dynamic name;
@dynamic position;
@dynamic string_id;
@dynamic categories;
@dynamic news;
@dynamic restaurant;
@dynamic sales;
@dynamic user;
@dynamic reviews;
@dynamic wokProperty;

@end
