//
//  Address+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 18.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Address.h"

@class User;

NS_ASSUME_NONNULL_BEGIN

@interface Address (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *flat;
@property (nullable, nonatomic, retain) NSNumber *floor;
@property (nullable, nonatomic, retain) NSString *house;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSNumber *is_main;
@property (nullable, nonatomic, retain) NSNumber *porch;
@property (nullable, nonatomic, retain) NSString *porch_code;
@property (nullable, nonatomic, retain) NSNumber *position;
@property (nullable, nonatomic, retain) NSString *street;
@property (nullable, nonatomic, retain) User *user;

@end

NS_ASSUME_NONNULL_END
