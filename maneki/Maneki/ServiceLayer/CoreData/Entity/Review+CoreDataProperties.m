//
//  Review+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 17.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Review+CoreDataProperties.h"
#import "City.h"

@implementation Review (CoreDataProperties)

@dynamic body;
@dynamic id;
@dynamic published_at;
@dynamic rate;
@dynamic user_name;
@dynamic is_my;
@dynamic restaurant;
@dynamic city;

@end
