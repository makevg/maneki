//
//  Product+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Product+CoreDataProperties.h"

@implementation Product (CoreDataProperties)

@dynamic cost;
@dynamic descr;
@dynamic id;
@dynamic image_url;
@dynamic position;
@dynamic preview_image_url;
@dynamic prop_hit;
@dynamic prop_hot;
@dynamic prop_hot2;
@dynamic prop_new;
@dynamic prop_veg;
@dynamic title;
@dynamic in_order;
@dynamic parent;

@end
