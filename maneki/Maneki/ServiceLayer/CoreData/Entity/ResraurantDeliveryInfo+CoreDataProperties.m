//
//  ResraurantDeliveryInfo+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 17.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ResraurantDeliveryInfo+CoreDataProperties.h"

@implementation ResraurantDeliveryInfo (CoreDataProperties)

@dynamic body;
@dynamic id;
@dynamic image_url;
@dynamic position;
@dynamic title;
@dynamic delivery_type;
@dynamic restaurant;

@end
