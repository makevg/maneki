//
//  RestaurantWorkTime+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RestaurantWorkTime.h"

NS_ASSUME_NONNULL_BEGIN

@interface RestaurantWorkTime (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *dow;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSNumber *time_from;
@property (nullable, nonatomic, retain) NSNumber *time_to;
@property (nullable, nonatomic, retain) Restaurant *resraurant;

@end

NS_ASSUME_NONNULL_END
