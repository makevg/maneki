//
//  ProductCategory+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProductCategory.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductCategory (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *image_url;
@property (nullable, nonatomic, retain) NSNumber *position;
@property (nullable, nonatomic, retain) NSString *string_id;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSSet<ProductCategory *> *childs;
@property (nullable, nonatomic, retain) City *city;
@property (nullable, nonatomic, retain) ProductCategory *parent;
@property (nullable, nonatomic, retain) NSSet<Product *> *products;

@end

@interface ProductCategory (CoreDataGeneratedAccessors)

- (void)addChildsObject:(ProductCategory *)value;
- (void)removeChildsObject:(ProductCategory *)value;
- (void)addChilds:(NSSet<ProductCategory *> *)values;
- (void)removeChilds:(NSSet<ProductCategory *> *)values;

- (void)addProductsObject:(Product *)value;
- (void)removeProductsObject:(Product *)value;
- (void)addProducts:(NSSet<Product *> *)values;
- (void)removeProducts:(NSSet<Product *> *)values;

@end

NS_ASSUME_NONNULL_END
