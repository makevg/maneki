//
//  WokPropertyValue+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WokPropertyValue+CoreDataProperties.h"

@implementation WokPropertyValue (CoreDataProperties)

@dynamic code;
@dynamic hot;
@dynamic id;
@dynamic image;
@dynamic position;
@dynamic title;
@dynamic wok_property;

@end
