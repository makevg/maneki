//
//  ProductCategory+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProductCategory+CoreDataProperties.h"

@implementation ProductCategory (CoreDataProperties)

@dynamic id;
@dynamic image_url;
@dynamic position;
@dynamic string_id;
@dynamic title;
@dynamic childs;
@dynamic city;
@dynamic parent;
@dynamic products;

@end
