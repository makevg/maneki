//
//  Restaurant+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Restaurant.h"

NS_ASSUME_NONNULL_BEGIN

@interface Restaurant (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSNumber *min_payment_value;
@property (nullable, nonatomic, retain) NSNumber *money;
@property (nullable, nonatomic, retain) NSNumber *onboard_card;
@property (nullable, nonatomic, retain) NSNumber *online_card;
@property (nullable, nonatomic, retain) NSString *phone;
@property (nullable, nonatomic, retain) City *city;
@property (nullable, nonatomic, retain) NSSet<RestaurantDeliveryCost *> *delivery_costs;
@property (nullable, nonatomic, retain) NSSet<ResraurantDeliveryInfo *> *delivety_infos;
@property (nullable, nonatomic, retain) NSSet<RestaurantPayMethod *> *pay_methods;
@property (nullable, nonatomic, retain) NSSet<Review *> *reviews;
@property (nullable, nonatomic, retain) NSSet<RestaurantWorkTime *> *work_times;

@end

@interface Restaurant (CoreDataGeneratedAccessors)

- (void)addDelivery_costsObject:(RestaurantDeliveryCost *)value;
- (void)removeDelivery_costsObject:(RestaurantDeliveryCost *)value;
- (void)addDelivery_costs:(NSSet<RestaurantDeliveryCost *> *)values;
- (void)removeDelivery_costs:(NSSet<RestaurantDeliveryCost *> *)values;

- (void)addDelivety_infosObject:(ResraurantDeliveryInfo *)value;
- (void)removeDelivety_infosObject:(ResraurantDeliveryInfo *)value;
- (void)addDelivety_infos:(NSSet<ResraurantDeliveryInfo *> *)values;
- (void)removeDelivety_infos:(NSSet<ResraurantDeliveryInfo *> *)values;

- (void)addPay_methodsObject:(RestaurantPayMethod *)value;
- (void)removePay_methodsObject:(RestaurantPayMethod *)value;
- (void)addPay_methods:(NSSet<RestaurantPayMethod *> *)values;
- (void)removePay_methods:(NSSet<RestaurantPayMethod *> *)values;

- (void)addReviewsObject:(Review *)value;
- (void)removeReviewsObject:(Review *)value;
- (void)addReviews:(NSSet<Review *> *)values;
- (void)removeReviews:(NSSet<Review *> *)values;

- (void)addWork_timesObject:(RestaurantWorkTime *)value;
- (void)removeWork_timesObject:(RestaurantWorkTime *)value;
- (void)addWork_times:(NSSet<RestaurantWorkTime *> *)values;
- (void)removeWork_times:(NSSet<RestaurantWorkTime *> *)values;

@end

NS_ASSUME_NONNULL_END
