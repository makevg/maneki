//
//  Order+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Order.h"

NS_ASSUME_NONNULL_BEGIN

@interface Order (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSNumber *received_at;
@property (nullable, nonatomic, retain) NSNumber *sale_value;
@property (nullable, nonatomic, retain) NSNumber *self_receive;
@property (nullable, nonatomic, retain) NSNumber *status;
@property (nullable, nonatomic, retain) NSNumber *summ;
@property (nullable, nonatomic, retain) NSNumber *delivery_type;
@property (nullable, nonatomic, retain) NSString *user_name;
@property (nullable, nonatomic, retain) NSString *phone;
@property (nullable, nonatomic, retain) NSString *street;
@property (nullable, nonatomic, retain) NSString *house;
@property (nullable, nonatomic, retain) NSString *flat;
@property (nullable, nonatomic, retain) NSSet<OrderProduct *> *set;

@end

NS_ASSUME_NONNULL_END
