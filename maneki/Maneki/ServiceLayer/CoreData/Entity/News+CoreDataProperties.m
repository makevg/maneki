//
//  News+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "News+CoreDataProperties.h"

@implementation News (CoreDataProperties)

@dynamic id;
@dynamic image_url;
@dynamic long_descr;
@dynamic preview_image_url;
@dynamic published_at;
@dynamic short_descr;
@dynamic title;
@dynamic city;

@end
