//
//  News.h
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class City;

NS_ASSUME_NONNULL_BEGIN

@interface News : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "News+CoreDataProperties.h"
