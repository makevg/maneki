//
//  Product+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Product.h"

NS_ASSUME_NONNULL_BEGIN

@interface Product (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *cost;
@property (nullable, nonatomic, retain) NSString *descr;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *image_url;
@property (nullable, nonatomic, retain) NSNumber *position;
@property (nullable, nonatomic, retain) NSString *preview_image_url;
@property (nullable, nonatomic, retain) NSNumber *prop_hit;
@property (nullable, nonatomic, retain) NSNumber *prop_hot;
@property (nullable, nonatomic, retain) NSNumber *prop_hot2;
@property (nullable, nonatomic, retain) NSNumber *prop_new;
@property (nullable, nonatomic, retain) NSNumber *prop_veg;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSSet<OrderProduct *> *in_order;
@property (nullable, nonatomic, retain) ProductCategory *parent;

@end

@interface Product (CoreDataGeneratedAccessors)

- (void)addIn_orderObject:(OrderProduct *)value;
- (void)removeIn_orderObject:(OrderProduct *)value;
- (void)addIn_order:(NSSet<OrderProduct *> *)values;
- (void)removeIn_order:(NSSet<OrderProduct *> *)values;

@end

NS_ASSUME_NONNULL_END
