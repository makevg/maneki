//
//  RestaurantWorkTime.h
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Restaurant;

NS_ASSUME_NONNULL_BEGIN

@interface RestaurantWorkTime : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "RestaurantWorkTime+CoreDataProperties.h"
