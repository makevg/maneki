//
//  WokProperty+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WokProperty+CoreDataProperties.h"

@implementation WokProperty (CoreDataProperties)

@dynamic code;
@dynamic title;
@dynamic city;
@dynamic values;

@end
