//
//  WokPropertyValue+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WokPropertyValue.h"

NS_ASSUME_NONNULL_BEGIN

@interface WokPropertyValue (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *code;
@property (nullable, nonatomic, retain) NSNumber *hot;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *image;
@property (nullable, nonatomic, retain) NSNumber *position;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) WokProperty *wok_property;

@end

NS_ASSUME_NONNULL_END
