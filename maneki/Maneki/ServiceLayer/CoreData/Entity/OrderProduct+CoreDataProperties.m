//
//  OrderProduct+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OrderProduct+CoreDataProperties.h"

@implementation OrderProduct (CoreDataProperties)

@dynamic position;
@dynamic prod_id;
@dynamic amount;
@dynamic title;
@dynamic price;
@dynamic noodles_rice;
@dynamic noodles_rice_id;
@dynamic noodles_rice_value;
@dynamic sauce;
@dynamic sauce_id;
@dynamic sauce_value;
@dynamic product;
@dynamic order;

@end
