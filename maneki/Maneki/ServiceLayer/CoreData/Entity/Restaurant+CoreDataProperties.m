//
//  Restaurant+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Restaurant+CoreDataProperties.h"

@implementation Restaurant (CoreDataProperties)

@dynamic id;
@dynamic min_payment_value;
@dynamic money;
@dynamic onboard_card;
@dynamic online_card;
@dynamic phone;
@dynamic city;
@dynamic delivery_costs;
@dynamic delivety_infos;
@dynamic pay_methods;
@dynamic reviews;
@dynamic work_times;

@end
