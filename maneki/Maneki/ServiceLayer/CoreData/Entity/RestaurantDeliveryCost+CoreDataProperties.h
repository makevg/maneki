//
//  RestaurantDeliveryCost+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 17.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RestaurantDeliveryCost.h"

NS_ASSUME_NONNULL_BEGIN

@interface RestaurantDeliveryCost (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *body;
@property (nullable, nonatomic, retain) NSNumber *cost;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSNumber *position;
@property (nullable, nonatomic, retain) Restaurant *restaurant;

@end

NS_ASSUME_NONNULL_END
