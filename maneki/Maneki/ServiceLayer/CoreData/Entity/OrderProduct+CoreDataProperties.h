//
//  OrderProduct+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OrderProduct.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderProduct (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *position;
@property (nullable, nonatomic, retain) NSNumber *prod_id;
@property (nullable, nonatomic, retain) NSNumber *amount;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSString *noodles_rice;
@property (nullable, nonatomic, retain) NSNumber *noodles_rice_id;
@property (nullable, nonatomic, retain) NSString *noodles_rice_value;
@property (nullable, nonatomic, retain) NSString *sauce;
@property (nullable, nonatomic, retain) NSNumber *sauce_id;
@property (nullable, nonatomic, retain) NSString *sauce_value;
@property (nullable, nonatomic, retain) Product *product;
@property (nullable, nonatomic, retain) Order *order;

@end

NS_ASSUME_NONNULL_END
