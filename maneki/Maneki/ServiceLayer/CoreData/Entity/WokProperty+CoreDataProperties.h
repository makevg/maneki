//
//  WokProperty+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 12.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WokProperty.h"

NS_ASSUME_NONNULL_BEGIN

@interface WokProperty (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *code;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) City *city;
@property (nullable, nonatomic, retain) NSSet<WokPropertyValue *> *values;

@end

@interface WokProperty (CoreDataGeneratedAccessors)

- (void)addValuesObject:(WokPropertyValue *)value;
- (void)removeValuesObject:(WokPropertyValue *)value;
- (void)addValues:(NSSet<WokPropertyValue *> *)values;
- (void)removeValues:(NSSet<WokPropertyValue *> *)values;

@end

NS_ASSUME_NONNULL_END
