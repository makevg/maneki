//
//  User+CoreDataProperties.h
//  
//
//  Created by Buravlev Mikhail on 18.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User.h"

@class Address;

NS_ASSUME_NONNULL_BEGIN

@interface User (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *fb;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *phone;
@property (nullable, nonatomic, retain) NSNumber *vk;
@property (nullable, nonatomic, retain) City *my_city;
@property (nullable, nonatomic, retain) NSSet<Address *> *adresses;

@end

@interface User (CoreDataGeneratedAccessors)

- (void)addAdressesObject:(Address *)value;
- (void)removeAdressesObject:(Address *)value;
- (void)addAdresses:(NSSet<Address *> *)values;
- (void)removeAdresses:(NSSet<Address *> *)values;

@end

NS_ASSUME_NONNULL_END
