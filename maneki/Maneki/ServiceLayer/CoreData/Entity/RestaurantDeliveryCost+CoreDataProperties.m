//
//  RestaurantDeliveryCost+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 17.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RestaurantDeliveryCost+CoreDataProperties.h"

@implementation RestaurantDeliveryCost (CoreDataProperties)

@dynamic body;
@dynamic cost;
@dynamic id;
@dynamic position;
@dynamic restaurant;

@end
