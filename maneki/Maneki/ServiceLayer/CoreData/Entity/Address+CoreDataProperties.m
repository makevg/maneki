//
//  Address+CoreDataProperties.m
//  
//
//  Created by Buravlev Mikhail on 18.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Address+CoreDataProperties.h"
#import "User.h"

@implementation Address (CoreDataProperties)

@dynamic flat;
@dynamic floor;
@dynamic house;
@dynamic id;
@dynamic is_main;
@dynamic porch;
@dynamic porch_code;
@dynamic position;
@dynamic street;
@dynamic user;

@end
