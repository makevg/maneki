#import <MagicalRecord/MagicalRecordInternal.h>
#import <MagicalRecord/MagicalRecord+Setup.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+Options.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalSaves.h>
#import "MADB.h"

@interface MADB ()

@property(strong, nonatomic) NSManagedObjectContext *managedObjectContext; // for read

@end

@implementation MADB

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"maneki"];
#ifdef DEBUG
        [MagicalRecord setLoggingLevel:MagicalRecordLoggingLevelVerbose];
#else
        [MagicalRecord setLoggingLevel:MagicalRecordLoggingLevelOff];
#endif

    });
    return sharedInstance;
}

#pragma mark - Core Data stack


#pragma mark - Core Data Saving support

- (NSManagedObjectContext *)produceContextForRead {
    return [NSManagedObjectContext MR_defaultContext];
}

- (NSManagedObjectContext *)produceContextForChange {
    return [NSManagedObjectContext MR_rootSavingContext];
}

- (void)saveWithBlock:(void (^)(NSManagedObjectContext *localContext))block completion:(MASaveCompletionHandler)completion {
    NSManagedObjectContext *localContext = [self produceContextForChange];

    [localContext performBlock:^{
        if (block) {
            block(localContext);
        }

        [localContext MR_saveWithOptions:MRSaveParentContexts
                              completion:completion];
    }];
}

- (void)saveContext {
    if (self.managedObjectContext != nil) {
        NSError *error = nil;
        if ([self.managedObjectContext hasChanges] && ![self.managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
