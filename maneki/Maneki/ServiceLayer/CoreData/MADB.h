#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define MANEKI_DB [MADB sharedInstance]

typedef void (^MASaveCompletionHandler)(BOOL contextDidSave, NSError *error);

@interface MADB : NSObject

+ (instancetype)sharedInstance;

- (NSManagedObjectContext*)produceContextForRead;

- (void)saveWithBlock:(void (^)(NSManagedObjectContext *localContext))block completion:(MASaveCompletionHandler)completion;

- (void)saveContext;

@end
