#import <AFNetworking/AFHTTPSessionManager.h>
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>
#import <Crashlytics/Crashlytics/CLSLogging.h>
#import "MAAPI.h"

#define servers @[@"http://api.manekicafe.ru",@"http://private-389cc-maneki.apiary-mock.com"]

#ifdef DEBUG
#define NSDebugLog(log) NSLog(@"%s, %s", __PRETTY_FUNCTION__, log)
#else
#define NSDebugLog(log)
#endif

NSString *const kNetworkChangeStateNotification = @"ru.maneki.networking.reachibility.change";
NSString *const kUnauthorizedNotification = @"ru.maneki.networking.unauthorized";

const char *const kApiQueueName = "ru.maneki.app.apiQueue";

typedef enum MAAPIServers {
    kMAAPIServerProd,
    kMAAPIServerApiary,
} MAAPIServers;

@interface MAAPI () {
    AFURLSessionManager *m_session;
    AFHTTPSessionManager *m_httpSession;
    NSString *m_server;
    dispatch_queue_t m_completitionQueue;
    AFNetworkReachabilityStatus m_currentOnlineState;
}

@end

@implementation MAAPI

- (void)changeServerWthType:(MAAPIServers)type {
    m_server = servers[type];
}

- (BOOL)lazyLoad {
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    m_session = [[AFURLSessionManager alloc] initWithSessionConfiguration:config];
    m_httpSession = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:config];
    m_completitionQueue = dispatch_queue_create(kApiQueueName, NULL);
    m_session.completionQueue = m_completitionQueue;
    m_currentOnlineState = AFNetworkReachabilityStatusUnknown;

    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        BOOL reachable;
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
                NSDebugLog("No Internet Connection");
                reachable = NO;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSDebugLog("WIFI");
                reachable = YES;
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSDebugLog("3G");
                reachable = YES;
                break;
            default:
                NSDebugLog("Unknown network status");
                reachable = NO;
                break;
        }

        if (status != m_currentOnlineState && m_currentOnlineState != AFNetworkReachabilityStatusUnknown)
            [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkChangeStateNotification
                                                                object:@(reachable)];

        m_currentOnlineState = status;
    }];

    [[AFNetworkReachabilityManager sharedManager] startMonitoring];

    return true;
}

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance lazyLoad];
        [sharedInstance changeServerWthType:kMAAPIServerProd];
    });
    return sharedInstance;
}

- (NSString *)urlWithPart:(NSString *)part {
    return [NSString stringWithFormat:@"%@%@", m_server, part];
}


- (BOOL)isOnline {
    return m_currentOnlineState == AFNetworkReachabilityStatusUnknown || [[AFNetworkReachabilityManager sharedManager] isReachable];
}

- (BOOL)requestGetToUrlString:(NSString *)urlString
                    andParams:(NSDictionary *)params
                   andHandler:(MAAPIHandler)handler {
    return [self requestToUrlString:urlString andParams:params andHandler:handler andMethod:@"GET"];
}

- (BOOL)requestPostToUrlString:(NSString *)urlString
                     andParams:(NSDictionary *)params
                    andHandler:(MAAPIHandler)handler {
    return [self requestToUrlString:urlString andParams:params andHandler:handler andMethod:@"POST"];
}

#pragma mark - private

- (BOOL)requestToUrlString:(NSString *)urlString
                 andParams:(NSDictionary *)params
                andHandler:(MAAPIHandler)handler
                 andMethod:(NSString *)method {
    NSError *err;
    NSMutableURLRequest *req = [m_httpSession.requestSerializer requestWithMethod:method
                                                                        URLString:[NSString stringWithFormat:@"%@", urlString]
                                                                       parameters:params
                                                                            error:&err];
    if (err) {
        return FALSE;
    }

    NSURLSessionDataTask *dataTask = [m_session dataTaskWithRequest:req
                                                  completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                                      if (error) {
                                                          if( response ) {
                                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                              if( httpResponse.statusCode == 401 ) {
                                                                  [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizedNotification
                                                                                                                      object:nil];
                                                              }
                                                          }

                                                          CLS_LOG(@"Error: %@", error);
                                                      } else {
                                                          CLS_LOG(@"%@ %@", response, responseObject);
                                                      }

                                                      if (handler)
                                                          handler(response, responseObject, error);
                                                  }];

    if (!dataTask)
        return FALSE;

    [dataTask resume];

    return TRUE;
}


@end
