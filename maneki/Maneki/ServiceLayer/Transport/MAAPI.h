#import <Foundation/Foundation.h>

#define MANEKI_API [MAAPI sharedInstance]

extern NSString *const kNetworkChangeStateNotification;
extern NSString *const kUnauthorizedNotification;

typedef void (^MAAPIHandler)(NSURLResponse *response, id responseObject, NSError *error);

@interface MAAPI : NSObject

+ (instancetype)sharedInstance;

- (NSString *)urlWithPart:(NSString *)part;

- (BOOL)isOnline;

- (BOOL)requestGetToUrlString:(NSString *)urlString andParams:(NSDictionary *)params andHandler:(MAAPIHandler)handler;

- (BOOL)requestPostToUrlString:(NSString *)urlString andParams:(NSDictionary *)params andHandler:(MAAPIHandler)handler;

@end
