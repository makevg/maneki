//
//  MAOrdersListView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrdersListView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "PCAngularActivityIndicatorView.h"

@interface MAOrdersListView ()
@property (nonatomic) PCAngularActivityIndicatorView *activityIndicator;
@end

@implementation MAOrdersListView

#pragma mark - Setup

- (void)setup {
    [self.tableView setBackgroundColor:[MAColorPalette lightGrayColor]];
    self.tableView.tableFooterView = [UIView new];
    self.activityIndicator = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleDefault];
    self.activityIndicator.color = [MAColorPalette orangeColor];
    [self.tableView addSubview:self.activityIndicator];
    self.activityIndicator.color = [MAColorPalette orangeColor];
}

#pragma mark - Public

- (void)startAnimation {
    self.activityIndicator.center = CGPointMake(self.tableView.bounds.size.width/2, self.tableView.bounds.size.height/2);
    [self.activityIndicator startAnimating];
}

- (void)stopAnimation {
    [self.activityIndicator stopAnimating];
}

@end
