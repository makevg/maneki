//
//  MAOrdersListCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrdersListCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "Order.h"
#import "OrderProduct.h"
#import "MADataFormatter.h"

NSString *const cOrdersListCellIdentifier = @"OrdersListCell";

@interface MAOrdersListCell ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@end

@implementation MAOrdersListCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cOrdersListCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[Order class]]) {
        [self fillCellByOrder:model];
    }
}

- (void)configureCell {
    self.infoLabel.textColor = [MAColorPalette blackColor];
    self.infoLabel.font = [MAFontSet ordersCellInfoLabelFont];
    self.dateTimeLabel.textColor = [MAColorPalette grayColor];
    self.dateTimeLabel.font = [MAFontSet ordersCellDateLabelFont];
}

#pragma mark - Private

- (void)fillCellByOrder:(Order *)order {
    NSInteger count = [[order.set allObjects] count];
    self.infoLabel.text = [NSString stringWithFormat:@"%ld %@, %@ р.", (long)count, [self stringByProductsCount:count], order.summ];
    self.dateTimeLabel.text = [MADataFormatter fullDateTimeStringByUnixTimeStamp:order.received_at];
}

- (NSString *)stringByProductsCount:(NSInteger)count {
    NSString *str;
    NSInteger ost = count % 10;
    if (ost == 1) {
        str = @"Блюдо";
    } else if (ost == 2 || ost == 3 || ost == 4) {
        str = @"Блюда";
    } else if (ost == 5 || ost == 6 || ost == 7 || ost == 8 || ost == 9) {
        str = @"Блюд";
    }
    return str;
}

@end
