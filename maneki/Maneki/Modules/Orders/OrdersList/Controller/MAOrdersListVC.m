//
//  MAOrdersListVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrdersListVC.h"
#import "MAOrdersListView.h"
#import "MAOrdersListCell.h"
#import "MAOrderVC.h"
#import "MAServiceLayer.h"
#import "Order.h"
#import "MAFontSet.h"

NSString *const cOrdersListStoryboardName = @"MAOrdersList";
NSString *const cOrdersListTitle = @"Заказы";

@interface MAOrdersListVC () <ServiceResultDelegate>
@property (strong, nonatomic) IBOutlet MAOrdersListView *contentView;
@end

@implementation MAOrdersListVC

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTitle:cOrdersListTitle];
}

- (void)dealloc {
    [self removeObserver:self];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cOrdersListStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (UITableView *)getTableView {
    return self.contentView.tableView;
}

- (void)configureController {
    [self addContentUpdateObserver:self
                          selector:@selector(updateOrdersNotification)];
    [self prepareTableView];
    [self cancelsTouches:NO];
    [self requestData];
}

- (void)requestData {
    [super requestData];
    self.fetchResult = [MANEKI_SERVICE getOrdersListWithDelegate:self];
}

#pragma mark - UITableViewDelegate

- (void)showOrderScreenBy:(NSManagedObjectID *)orderId {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[MAOrderVC storyboardName] bundle:nil];
    MAOrderVC *vc = [sb instantiateInitialViewController];
    vc.orderId = orderId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.fetchResult.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [MAOrdersListCell cellIdentifier];
    MAOrdersListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                             forIndexPath:indexPath];
    [cell setModel:[self.fetchResult objectAtIndexPath:indexPath]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Order *order = [self.fetchResult objectAtIndexPath:indexPath];
    [self showOrderScreenBy:order.objectID];
}

#pragma mark - DZNEmptyDataSetSource

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:cSadKittyImageName];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"Нет заказов";
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName : [MAFontSet navBatTitleFont],
                                 NSForegroundColorAttributeName : [UIColor blackColor],
                                 NSParagraphStyleAttributeName : paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - Notifications

- (void)updateOrdersNotification {
    [self requestData];
    [self.contentView.tableView reloadData];
}

@end
