//
//  MAOrdersInfoView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 20.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrdersInfoView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@interface MAOrdersInfoView ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@end

@implementation MAOrdersInfoView

#pragma mark - Setup

- (void)setup {
    [self setBackgroundColor:[MAColorPalette backgroundColor]];
    self.separatorView.backgroundColor = [MAColorPalette thinLineViewBackgroundColor];
    self.infoLabel.textColor = [MAColorPalette whiteColor];
    self.infoLabel.font = [MAFontSet ordersInfoScreenLabelFont];
}

@end
