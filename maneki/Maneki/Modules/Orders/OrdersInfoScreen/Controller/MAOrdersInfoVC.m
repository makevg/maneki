//
//  MAOrdersInfoVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 20.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrdersInfoVC.h"
#import "MASignInVC.h"
#import "MARegistrationVC.h"

NSString *const cOrdersInfoStoryboardName = @"MAOrdersInfo";

@interface MAOrdersInfoVC ()
@end

@implementation MAOrdersInfoVC

#pragma mark - Public

+ (NSString *)storyboardName {
    return cOrdersInfoStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

#pragma mark - Actions

- (IBAction)tappedSignInButton:(id)sender {
    [self pushFeatureByName:[MASignInVC storyboardName] animated:YES];
}

- (IBAction)tappedSignUpButton:(id)sender {
    [self pushFeatureByName:[MARegistrationVC storyboardName] animated:YES];
}

@end
