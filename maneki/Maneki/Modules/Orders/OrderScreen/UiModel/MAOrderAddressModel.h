//
//  MAOrderAddressModel.h
//  Maneki
//
//  Created by Максимычев Е.О. on 21.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAOrderAddressModel : NSObject

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *data;

- (instancetype)initWithTitle:(NSString *)title data:(NSString *)data;

@end
