//
//  MAOrderAddressModel.m
//  Maneki
//
//  Created by Максимычев Е.О. on 21.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAOrderAddressModel.h"

@implementation MAOrderAddressModel

- (instancetype)initWithTitle:(NSString *)title data:(NSString *)data {
    self = [super init];
    if (self) {
        _title = title;
        _data = data;
    }
    return self;
}

@end
