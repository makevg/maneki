//
//  MAOrderHeader.h
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MAOrderHeader : MABaseView

- (void)configureWithTitle:(NSString *)title;

@end
