//
//  MAOrderHeader.m
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrderHeader.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@implementation MAOrderHeader {
    UILabel *label;
}

#pragma mark - Init

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configure];
    }
    return self;
}

#pragma mark - Private

- (void)configure {
    [self setBackgroundColor:[UIColor clearColor]];
}

- (void)configureWithTitle:(NSString *)title
                titleColor:(UIColor *)titleColor
                 titleFont:(UIFont *)titleFont
                     width:(CGFloat)width {
    NSDictionary *attributes = @{NSFontAttributeName:titleFont};
    CGRect rect = [title boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributes
                                      context:nil];
    label = [[UILabel alloc] initWithFrame:CGRectMake(16, 30, width, rect.size.height)];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.textAlignment = NSTextAlignmentLeft;
    [label setText:title];
    [label setTextColor:titleColor];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:titleFont];
    [self addSubview:label];
}

#pragma mark - Setup

- (void)configureWithTitle:(NSString *)title {
    [self configureWithTitle:title
                  titleColor:[MAColorPalette blackColor]
                   titleFont:[MAFontSet deliveryHeaderFont]
                       width:CGRectGetWidth(self.frame)];
}

@end
