//
//  MAOrderProductInfoCell.m
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrderProductInfoCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "OrderProduct.h"

NSString *const cOrderProductInfoCellIdentifier = @"OrderProductInfoCell";

@interface MAOrderProductInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *productLabel;
@property (weak, nonatomic) IBOutlet UILabel *productCountLabel;
@end

@implementation MAOrderProductInfoCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cOrderProductInfoCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[OrderProduct class]]) {
        [self fillCellByProduct:model];
    }
}

- (void)configureCell {
    self.productLabel.textColor = [MAColorPalette blackColor];
    self.productLabel.font = [MAFontSet orderCellDefaultFont];
    self.productCountLabel.textColor = [MAColorPalette orangeColor];
    self.productCountLabel.font = [MAFontSet orderProductCellCountFont];
}

#pragma mark - Private

- (void)fillCellByProduct:(OrderProduct *)product {
    if ([product.noodles_rice length] > 0 && [product.sauce length] > 0) {
        self.productLabel.numberOfLines = 0;
        self.productLabel.text = [NSString stringWithFormat:@"Вок-конструктор\n\n1. %@\n2. %@\n3. %@", product.noodles_rice, product.title, product.sauce];
    } else {
        self.productLabel.text = product.title;
    }
    self.productCountLabel.text = [NSString stringWithFormat:@"%@ x %@", product.price, product.amount];
}

@end
