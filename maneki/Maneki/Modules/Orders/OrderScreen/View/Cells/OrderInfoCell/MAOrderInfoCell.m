//
//  MAOrderInfoCell.m
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrderInfoCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cOrderInfoCellIdentifier = @"OrderInfoCell";

@interface MAOrderInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *priceKeyLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceValueLabel;
@end

@implementation MAOrderInfoCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cOrderInfoCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSNumber class]]) {
        self.priceValueLabel.text = [NSString stringWithFormat:@"%ld р.", (long)[model integerValue]];
    }
}

- (void)configureCell {
    self.priceKeyLabel.textColor = [MAColorPalette grayColor];
    self.priceKeyLabel.font = [MAFontSet orderInfoCellFont];
    self.priceValueLabel.textColor = [MAColorPalette orangeColor];
    self.priceValueLabel.font = [MAFontSet orderInfoCellPriceFont];
}

@end
