//
//  MAOrderAddressInfoCell.m
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrderAddressInfoCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MAOrderAddressModel.h"

NSString *const cOrderAddressInfoCellIdentifier = @"OrderAddressInfoCell";

@interface MAOrderAddressInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *addressInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressValueLabel;
@end

@implementation MAOrderAddressInfoCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cOrderAddressInfoCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[MAOrderAddressModel class]]) {
        [self fillCellByAddressModel:model];
    }
}

- (void)configureCell {
    self.addressInfoLabel.textColor = [MAColorPalette grayColor];
    self.addressInfoLabel.font = [MAFontSet orderCellDefaultFont];
    self.addressValueLabel.textColor = [MAColorPalette blackColor];
    self.addressValueLabel.font = [MAFontSet orderCellDefaultFont];
}

#pragma mark - Private

- (void)fillCellByAddressModel:(MAOrderAddressModel *)model {
    self.addressInfoLabel.text = model.title;
    self.addressValueLabel.text = model.data;
}

@end
