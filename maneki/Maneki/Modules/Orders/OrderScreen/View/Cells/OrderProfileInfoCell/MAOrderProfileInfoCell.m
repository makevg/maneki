//
//  MAOrderProfileInfoCell.m
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrderProfileInfoCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cOrderProfileInfoCellIdentifier = @"OrderProfileInfoCell";

@interface MAOrderProfileInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *profileInfoLabel;
@end

@implementation MAOrderProfileInfoCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cOrderProfileInfoCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSString class]]) {
        self.profileInfoLabel.text = model;
    }
}

- (void)configureCell {
    self.profileInfoLabel.textColor = [MAColorPalette blackColor];
    self.profileInfoLabel.font = [MAFontSet orderCellDefaultFont];
}

@end
