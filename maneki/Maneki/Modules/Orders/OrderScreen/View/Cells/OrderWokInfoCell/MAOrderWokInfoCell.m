//
//  MAOrderWokInfoCell.m
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrderWokInfoCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cOrderWokInfoCellIdentifier = @"OrderWokInfoCell";

@interface MAOrderWokInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *wokInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *wokCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *wokPropertiesLabel;
@end

@implementation MAOrderWokInfoCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cOrderWokInfoCellIdentifier;
}

- (void)setModel:(id)model {
    
}

- (void)configureCell {
    self.wokInfoLabel.textColor = [MAColorPalette blackColor];
    self.wokInfoLabel.font = [MAFontSet orderCellDefaultFont];
    self.wokCountLabel.textColor = [MAColorPalette orangeColor];
    self.wokCountLabel.font = [MAFontSet orderProductCellCountFont];
    self.wokPropertiesLabel.textColor = [MAColorPalette grayColor];
    self.wokPropertiesLabel.font = [MAFontSet orderInfoCellPropertiesFont];
}

@end
