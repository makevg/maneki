//
//  MAOrderView.h
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@class MABottomButton;

@interface MAOrderView : MABaseView

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet MABottomButton *repeatOrderButton;

@end
