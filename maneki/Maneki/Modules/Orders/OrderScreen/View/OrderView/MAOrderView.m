//
//  MAOrderView.m
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrderView.h"
#import "MABottomButton.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@implementation MAOrderView

#pragma mark - Setup

- (void)setup {
    self.tableView.backgroundColor = [MAColorPalette lightGrayColor];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 55.f;
    self.repeatOrderButton.backgroundColor = [MAColorPalette orangeColor];
}

@end
