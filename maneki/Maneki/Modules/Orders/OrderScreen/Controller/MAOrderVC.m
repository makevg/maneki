//
//  MAOrderVC.m
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAOrderVC.h"
#import "MAOrderView.h"
#import "MAOrderHeader.h"
#import "MABottomButton.h"
#import "MAOrderProductInfoCell.h"
#import "MAOrderProfileInfoCell.h"
#import "MAOrderAddressInfoCell.h"
#import "MAOrderInfoCell.h"
#import "MAOrderAddressModel.h"
#import "Order.h"
#import "OrderProduct.h"
#import "MADB.h"
#import "MACartService.h"
#import "SCLAlertView.h"
#import "MAFontSet.h"
#import "MAColorPalette.h"

NSString *const cOrderStoryboardName = @"MAOrder";
NSString *const cOrderTitle = @"Заказ";

CGFloat const cDefaultHeaderHeight = 20.f;

typedef NS_ENUM(NSInteger, MAOrderSectionsType) {
    eProductsSectionType = 0,
    eProfileInfoSectionsType,
    eAddressInfoSectionType,
    eOrderInfoSectionType
};

@interface MAOrderVC ()
@property (strong, nonatomic) IBOutlet MAOrderView *contentView;
@property (weak, nonatomic) IBOutlet MABottomButton *reorderButton;
@end

@implementation MAOrderVC {
    NSArray<OrderProduct *> *firstSectionData;
    NSArray<NSString *> *sectionsHeaders;
    NSArray<NSString *> *secondSectionData;
    NSArray<MAOrderAddressModel *> *thirdSectionData;
    NSArray<NSNumber *> *fourthSectionData;
    PCAngularActivityIndicatorView *indicatorView;
}

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTitle:cOrderTitle];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cOrderStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (UITableView *)getTableView {
    return self.contentView.tableView;
}

- (void)configureController {
    [self initData];
    [self prepareTableView];
  
    [self cancelsTouches:NO];
    self.reorderButton.style = MABottomButtonOrangeStyle;
    indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleSmall
                                               forView:self.reorderButton];
}

#pragma mark - Private

- (void)initData {
    Order *order = [[MANEKI_DB produceContextForRead] objectRegisteredForID:self.orderId];
    firstSectionData = [order.set allObjects];
    firstSectionData = [firstSectionData sortedArrayUsingComparator:^NSComparisonResult(OrderProduct *obj1, OrderProduct *obj2) {
        if ([obj1.position integerValue] > [obj2.position integerValue])
            return NSOrderedDescending;
        
        return NSOrderedAscending;
    }];
    secondSectionData = @[order.phone];
    thirdSectionData = [self prepareThirdSectionDataByOrder:order];
    fourthSectionData = @[order.summ];
    sectionsHeaders = [self prepareSectionsHeaders];
}

- (NSArray<MAOrderAddressModel *> *)prepareThirdSectionDataByOrder:(Order *)order {
    NSMutableArray<MAOrderAddressModel *> *array = [NSMutableArray new];
    if ([order.street length] > 0) {
        MAOrderAddressModel *streetModel = [[MAOrderAddressModel alloc] initWithTitle:@"Улица" data:order.street];
        [array addObject:streetModel];
    }
    if ([order.house length] > 0) {
        MAOrderAddressModel *houseModel = [[MAOrderAddressModel alloc] initWithTitle:@"Дом" data:order.house];
        [array addObject:houseModel];
    }
    if ([order.flat length] > 0) {
        MAOrderAddressModel *flatModel = [[MAOrderAddressModel alloc] initWithTitle:@"Квартира" data:order.flat];
        [array addObject:flatModel];
    }
    return array;
}

- (NSArray<NSString *> *)prepareSectionsHeaders {
    return @[@"Состав заказа", @"Контактные данные", @"Адрес доставки", @""];
}

- (NSString *)cellIdentifierByIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier;
    switch (indexPath.section) {
        case eProductsSectionType:
            identifier = [MAOrderProductInfoCell cellIdentifier];
            break;
        case eProfileInfoSectionsType:
            identifier = [MAOrderProfileInfoCell cellIdentifier];
            break;
        case eAddressInfoSectionType:
            identifier = [MAOrderAddressInfoCell cellIdentifier];
            break;
        case eOrderInfoSectionType:
            identifier = [MAOrderInfoCell cellIdentifier];
            break;
        default:
            break;
    }
    return identifier;
}

- (void)addProductAtIndex:(NSInteger)index {
    __typeof (self) __weak weakSelf = self;
    if (index < [firstSectionData count]) {
        OrderProduct *product = firstSectionData[(NSUInteger) index];
        if ([product.noodles_rice length] > 0 && [product.sauce length] > 0) {
            [CART_SERVICE updateRiceId:product.noodles_rice_id
                             riceTitle:product.noodles_rice
                              riceCode:product.noodles_rice_value
                             productId:product.prod_id
                               sauceId:product.sauce_id
                            sauceTitle:product.sauce
                             sauceCode:product.sauce_value
                                amount:(NSUInteger) [product.amount integerValue]
                        successHandler:^(NSUInteger productId, NSUInteger amount) {
                            __strong __typeof(&*weakSelf) strongSelf = weakSelf;
                            if( strongSelf ) {
                                [strongSelf successStateForProductAtIndex:index];
                            }
                        }
                        failureHandler:^(NSUInteger productId, NSUInteger amount, NSError *error) {
                            __strong __typeof(&*weakSelf) strongSelf = weakSelf;
                            if( strongSelf ) {
                                [strongSelf errorStateByError:error];
                            }
                        }];
        } else {
            [CART_SERVICE updateProductId:product.prod_id
                                   amount:(NSUInteger) [product.amount integerValue]
                           successHandler:^(NSUInteger productId, NSUInteger amount) {
                               __strong __typeof(&*weakSelf) strongSelf = weakSelf;
                               if( strongSelf ) {
                                   [strongSelf successStateForProductAtIndex:index];
                               }
                           }
                           failureHandler:^(NSUInteger productId, NSUInteger amount, NSError *error) {
                               __strong __typeof(&*weakSelf) strongSelf = weakSelf;
                               if( strongSelf ) {
                                   [strongSelf errorStateByError:error];
                               }
                           }];
        }
    }
}

- (void)successStateForProductAtIndex:(NSInteger)index {
    if (index == [firstSectionData count] - 1) {
        [self setEnabledReorderButton:YES];
        [self showSuccessAlert];
    } else {
        index++;
        [self addProductAtIndex:index];
    }
}

- (void)showSuccessAlert {
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.customViewColor = [MAColorPalette orangeColor];
    alert.iconTintColor = [UIColor whiteColor];

    [alert setTitleFontFamily:cRegularFontName withSize:20.f];
    [alert setBodyTextFontFamily:cRegularFontName withSize:14.f];
    [alert setButtonsTextFontFamily:cBoldFontName withSize:14.f];

    [alert addButton:@"Ок" actionBlock:^(void) {
        [self.navigationController popViewControllerAnimated:YES];
    }];

    [alert showInfo:@"Подзравляем"
           subTitle:[NSString stringWithFormat:@"Товары из заказа добавлены в корзину"]
   closeButtonTitle:nil
           duration:0.0f];
}

- (void)errorStateByError:(NSError *)error {
    [self setEnabledReorderButton:YES];
    [self showErrorAlertWithError:error];
}

- (void)showErrorAlertWithError:(NSError *)error {
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    [alert showError:@"Внимание"
            subTitle:error.localizedDescription
    closeButtonTitle:@"Ок"
            duration:0.0f];
}

- (void)setEnabledReorderButton:(BOOL)enabled {
    self.reorderButton.enabled = enabled;
    enabled ? [indicatorView stopAnimating] : [indicatorView startAnimating];
}

#pragma mark - Actions

- (IBAction)tappedRepeatOrderButton:(id)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self setEnabledReorderButton:NO];
    [self addProductAtIndex:0];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [sectionsHeaders count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case eProductsSectionType:
            return [firstSectionData count];
        case eProfileInfoSectionsType:
            return [secondSectionData count];
        case eAddressInfoSectionType:
            return [thirdSectionData count];
        case eOrderInfoSectionType:
            return [fourthSectionData count];
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [self cellIdentifierByIndexPath:indexPath];
    MABaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                                forIndexPath:indexPath];
    if ([identifier isEqualToString:[MAOrderProductInfoCell cellIdentifier]]) {
        [cell setModel:firstSectionData[(NSUInteger) indexPath.row]];
    } else if ([identifier isEqualToString:[MAOrderProfileInfoCell cellIdentifier]]) {
        [cell setModel:secondSectionData[(NSUInteger) indexPath.row]];
    } else if ([identifier isEqualToString:[MAOrderAddressInfoCell cellIdentifier]]) {
        [cell setModel:thirdSectionData[(NSUInteger) indexPath.row]];
    } else if ([identifier isEqualToString:[MAOrderInfoCell cellIdentifier]]) {
        [cell setModel:fourthSectionData[(NSUInteger) indexPath.row]];
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, 60);
    MAOrderHeader *view = [[MAOrderHeader alloc] initWithFrame:frame];
    [view configureWithTitle:sectionsHeaders[(NSUInteger) section]];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat headerHeight = section != eOrderInfoSectionType ? self.contentView.tableView.sectionHeaderHeight : cDefaultHeaderHeight;
    return headerHeight;
}

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];
    [self setEnabledReorderButton:YES];
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];
    
    [self showMessage:[self requestErrorDescription]];
    [self setEnabledReorderButton:YES];
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];
    
    [self showMessage:error.localizedDescription];
    [self setEnabledReorderButton:YES];
}

@end
