//
//  MAAboutDeliveryVC.m
//  Maneki
//
//  Created by Максимычев Е.О. on 14.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAAboutDeliveryVC.h"
#import "MAAboutDeliveryView.h"
#import "MAAboutDeliveryHeader.h"
#import "MAAboutDeliveryCellWithImage.h"
#import "MAAboutDeliveryCellWithPrice.h"
#import "MAAboutDeliveryCellWithStep.h"
#import "MAAboutDeliveryEmptyCell.h"
#import "Restaurant.h"
#import "RestaurantDeliveryCost.h"
#import "ResraurantDeliveryInfo.h"
#import "RestaurantPayMethod.h"

NSString *const cAboutDeliveryStoryboardName = @"MAAboutDelivery";
NSString *const cAboutDeliveryTitle = @"О доставке";

@interface MAAboutDeliveryVC () <ServiceResultDelegate>
@property(strong, nonatomic) IBOutlet MAAboutDeliveryView *contentView;
@end

@implementation MAAboutDeliveryVC {
    NSArray *sectionHeaders;
    NSArray *sectionsData;
}

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTitle:cAboutDeliveryTitle];
}

- (void)dealloc {
    [self removeObserver:self];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cAboutDeliveryStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (UITableView *)getTableView {
    return self.contentView.tableView;
}

- (void)configureController {
    [self addContentUpdateObserver:self
                          selector:@selector(updateDeliveryInfoNotification)];
    [self prepareTableView];
    [self cancelsTouches:NO];
    [self initData];
}

- (void)requestData {
    [super requestData];
    self.fetchResult = [MANEKI_SERVICE getRestaurantInfoWithDelegate:self];
}

#pragma mark - Private

- (void)initData {
    [self requestData];
    sectionHeaders = [self prepareSectionHeaders];
    sectionsData = [NSArray array];
}

- (NSArray *)prepareSectionHeaders {
    return @[@"Условия доставки", @"Минимальная сумма заказа", @"Способы оплаты"];
}

- (NSString *)cellIdentifierByIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier;
    switch (indexPath.section) {
        case 0:
            identifier = [MAAboutDeliveryCellWithImage cellIdentifier];
            break;
        case 1:
            identifier = indexPath.row == 0 ? [MAAboutDeliveryEmptyCell cellIdentifier] : [MAAboutDeliveryCellWithPrice cellIdentifier];
            break;
        case 2:
            identifier = [MAAboutDeliveryCellWithStep cellIdentifier];
            break;
        default:
            break;
    }
    return identifier;
}

- (void)prepareSectionsData {
    NSArray *array = self.fetchResult.fetchedObjects;
    if ([array count] > 0) {
        Restaurant *restaurant = array[0];
        
        NSArray *deliveryCosts = [[restaurant.delivery_costs allObjects] mutableCopy];
        deliveryCosts = [deliveryCosts sortedArrayUsingComparator:^NSComparisonResult(RestaurantDeliveryCost *obj1, RestaurantDeliveryCost *obj2) {
            return [obj1.cost compare:obj2.cost];
        }];
        
        NSMutableArray *deliveryCostsMutable = [deliveryCosts mutableCopy];
        [deliveryCostsMutable insertObject:@"" atIndex:0];
        
        NSArray *deliveryInfos = [restaurant.delivety_infos allObjects];
        deliveryInfos = [deliveryInfos sortedArrayUsingComparator:^NSComparisonResult(ResraurantDeliveryInfo *obj1, ResraurantDeliveryInfo *obj2) {
            return [obj1.position compare:obj2.position];
        }];
        
        NSArray *payMethods = [restaurant.pay_methods allObjects];
        payMethods = [payMethods sortedArrayUsingComparator:^NSComparisonResult(RestaurantPayMethod *obj1, RestaurantPayMethod *obj2) {
            return [obj1.position compare:obj2.position];
        }];
        
        sectionsData = @[deliveryInfos, deliveryCostsMutable, payMethods];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!self.fetchResult) {
        return 0;
    } else {
        return [sectionsData count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([sectionsData count] > 0) {
        switch (section) {
            case 0:
            case 1:
            case 2:
                return [sectionsData[section] count];
            default:
                return 0;
        }
    } else {
        return 0;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [self cellIdentifierByIndexPath:indexPath];
    MABaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                                forIndexPath:indexPath];
    if ([sectionsData count] > 0) {
        NSInteger section = indexPath.section;
        switch (indexPath.section) {
            case 0:
            case 1:
            case 2:
                [cell setModel:sectionsData[section][indexPath.row]];
                break;
                
            default:
                break;
        }
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }

    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, 60);
    MAAboutDeliveryHeader *view = [[MAAboutDeliveryHeader alloc] initWithFrame:frame];
    [view configureWithTitle:sectionHeaders[(NSUInteger) section]];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return self.contentView.tableView.sectionHeaderHeight;
}

#pragma mark - NSFetchResultControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type {
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self prepareSectionsData];
    [self.contentView.tableView reloadData];
}

#pragma mark - Notifications

- (void)updateDeliveryInfoNotification {
    [self requestData];
    [self.contentView.tableView reloadData];
}

@end
