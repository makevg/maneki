//
//  MAAboutDeliveryView.h
//  Maneki
//
//  Created by Максимычев Е.О. on 14.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MAAboutDeliveryView : MABaseView

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
