//
//  MAAboutDeliveryView.m
//  Maneki
//
//  Created by Максимычев Е.О. on 14.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAAboutDeliveryView.h"
#import "MAColorPalette.h"

@interface MAAboutDeliveryView ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation MAAboutDeliveryView

#pragma mark - Setup

- (void)setup {
    self.tableView.backgroundColor = [MAColorPalette lightGrayColor];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 95.f;
    self.activityIndicator.color = [MAColorPalette orangeColor];
}

@end
