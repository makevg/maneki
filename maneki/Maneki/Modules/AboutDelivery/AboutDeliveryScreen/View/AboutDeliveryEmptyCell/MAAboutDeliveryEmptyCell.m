//
//  MAAboutDeliveryEmptyCell.m
//  Maneki
//
//  Created by Максимычев Е.О. on 14.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAAboutDeliveryEmptyCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cAboutDeliveryEmptyCell = @"AboutDeliveryEmptyCell";

@interface MAAboutDeliveryEmptyCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation MAAboutDeliveryEmptyCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cAboutDeliveryEmptyCell;
}

- (void)setModel:(id)model {
}

#pragma mark - Private

- (void)configureCell {
    self.titleLabel.textColor = [MAColorPalette blackColor];
    self.titleLabel.font = [MAFontSet deliveryInfoLabelFont];
}

@end
