//
//  MAAboutDeliveryCellWithStep.m
//  Maneki
//
//  Created by Максимычев Е.О. on 16.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAAboutDeliveryCellWithStep.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "RestaurantPayMethod.h"

NSString *const cAboutDeliveryCellWithStep = @"AboutDeliveryCellWithStep";

@interface MAAboutDeliveryCellWithStep ()
@property (weak, nonatomic) IBOutlet UIView *stepView;
@property (weak, nonatomic) IBOutlet UILabel *stepLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@end

@implementation MAAboutDeliveryCellWithStep

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cAboutDeliveryCellWithStep;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[RestaurantPayMethod class]]) {
        [self fillCellByPayMethod:model];
    }
}

#pragma mark - Private

- (void)configureCell {
    self.stepView.layer.cornerRadius = CGRectGetWidth(self.stepView.frame) / 2;
    self.stepView.backgroundColor = [MAColorPalette orangeColor];
    self.stepView.clipsToBounds = YES;
    self.stepLabel.textColor = [MAColorPalette whiteColor];
    self.stepLabel.font = [MAFontSet deliveryStepLabelFont];
    self.titleLabel.textColor = [MAColorPalette blackColor];
    self.titleLabel.font = [MAFontSet deliveryTitleFont];
    self.descriptionLabel.textColor = [MAColorPalette grayColor];
    self.descriptionLabel.font = [MAFontSet deliveryDescriptionFont];
}

- (void)fillCellByPayMethod:(RestaurantPayMethod *)payMethod {
    NSUInteger position = [payMethod.position intValue] + 1;
    self.stepLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)position];
    self.titleLabel.text = payMethod.title;
    self.descriptionLabel.text = payMethod.desctiption;
}

@end
