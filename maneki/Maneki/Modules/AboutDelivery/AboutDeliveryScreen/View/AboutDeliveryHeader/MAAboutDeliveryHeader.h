//
//  MAAboutDeliveryHeader.h
//  Maneki
//
//  Created by Maximychev Evgeny on 15.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MAAboutDeliveryHeader : MABaseView

- (void)configureWithTitle:(NSString *)title;

@end
