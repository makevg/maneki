//
//  MAAboutDeliveryCellWithImage.m
//  Maneki
//
//  Created by Максимычев Е.О. on 14.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAAboutDeliveryCellWithImage.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MAAPI.h"
#import "UIImageView+AsyncLoad.h"
#import "ResraurantDeliveryInfo.h"

NSString *const cAboutDeliveryCellWithImage = @"AboutDeliveryCellWithImage";

@interface MAAboutDeliveryCellWithImage ()
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@end

@implementation MAAboutDeliveryCellWithImage

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cAboutDeliveryCellWithImage;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[ResraurantDeliveryInfo class]]) {
        [self fillCellByDeliveryInfo:model];
    }
}

#pragma mark - Private

- (void)configureCell {
    self.photoImageView.layer.cornerRadius = CGRectGetWidth(self.photoImageView.frame) / 2;
    self.photoImageView.clipsToBounds = YES;
    self.titleLabel.textColor = [MAColorPalette blackColor];
    self.titleLabel.font = [MAFontSet deliveryTitleFont];
    self.descriptionLabel.textColor = [MAColorPalette grayColor];
    self.descriptionLabel.font = [MAFontSet deliveryDescriptionFont];
}

- (void)fillCellByDeliveryInfo:(ResraurantDeliveryInfo *)deliveryInfo {
    self.titleLabel.text = deliveryInfo.title;
    self.descriptionLabel.text = deliveryInfo.body;
    [self.photoImageView loadAsyncFromUrl:[MANEKI_API urlWithPart:deliveryInfo.image_url]];
}

@end
