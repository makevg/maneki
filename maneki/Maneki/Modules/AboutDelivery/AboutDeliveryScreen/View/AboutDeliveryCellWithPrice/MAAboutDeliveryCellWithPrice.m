//
//  MAAboutDeliveryCellWithPrice.m
//  Maneki
//
//  Created by Максимычев Е.О. on 14.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAAboutDeliveryCellWithPrice.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "RestaurantDeliveryCost.h"

NSString *const cAboutDeliveryCellWithPrice = @"AboutDeliveryCellWithPrice";

@interface MAAboutDeliveryCellWithPrice ()
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@end

@implementation MAAboutDeliveryCellWithPrice

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cAboutDeliveryCellWithPrice;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[RestaurantDeliveryCost class]]) {
        [self fillCellByDeliveryRule:model];
    }
}

#pragma mark - Private

- (void)configureCell {
    self.priceLabel.textColor = [MAColorPalette orangeColor];
    self.priceLabel.font = [MAFontSet deliveryPriceFont];
    self.descriptionLabel.textColor = [MAColorPalette grayColor];
    self.descriptionLabel.font = [MAFontSet deliveryDescriptionFont];
}

- (void)fillCellByDeliveryRule:(RestaurantDeliveryCost *)deliveryRule {
    self.priceLabel.text = [NSString stringWithFormat:@"%@ р.", deliveryRule.cost];
    self.descriptionLabel.text = deliveryRule.body;
}

@end
