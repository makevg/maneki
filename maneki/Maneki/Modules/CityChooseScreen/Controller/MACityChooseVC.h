//
//  MACityChooseVC.h
//  Maneki
//
//  Created by Maximychev Evgeny on 18.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseTableVC.h"

@interface MACityChooseVC : MABaseTableVC

@property (nonatomic) BOOL showPrevScreen;

@end
