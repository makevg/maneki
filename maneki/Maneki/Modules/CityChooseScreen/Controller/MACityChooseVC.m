//
//  MACityChooseVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 18.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import "MACityChooseVC.h"
#import "MACityChooseView.h"
#import "MACityCell.h"
#import "City.h"
#import "MAAuthorizationVC.h"
#import "MACommonService.h"
#import "MAUserService.h"
#import "MARestaurantService.h"
#import "MACartService.h"

NSString *const cCityChooseStoryboardName = @"MACityChoose";
NSString *const cCityChooseVCIdentifier = @"CityChooseVC";

@interface MACityChooseVC () <ServiceResultDelegate>
@property(strong, nonatomic) IBOutlet MACityChooseView *contentView;
@end

@implementation MACityChooseVC

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    BOOL navBarHidden = self.showPrevScreen;
    [[self navigationController] setNavigationBarHidden:navBarHidden animated:animated];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cCityChooseStoryboardName;
}

+ (NSString *)identifier {
    return cCityChooseVCIdentifier;
}

- (UITableView *)getTableView {
    return self.contentView.tableView;
}

- (void)configureController {
    [super configureController];

    self.title = @"";
    [self cancelsTouches:NO];
    [self prepareTableView];
    [self requestData];
}

#pragma mark - Private

- (void)showAuthScreen {
    [self pushFeatureByName:[MAAuthorizationVC storyboardName] animated:YES];
}

- (void)showPreviousScreen {
    [UIView animateWithDuration:0.75
                     animations:^{
                         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                         [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                                                forView:self.navigationController.view
                                                  cache:NO];
                     }];
    [self.navigationController popViewControllerAnimated:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:MABaseVCUpdateContentNotification
                                                        object:nil];
}

- (void)saveCurrentCityAtIndexPath:(NSIndexPath *)indexPath {
    City *city = [self.fetchResult objectAtIndexPath:indexPath];
    [MACommonService saveCurrentCity:city];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [MACityCell cellIdentifier];
    MACityCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                       forIndexPath:indexPath];
    [cell setModel:[self.fetchResult objectAtIndexPath:indexPath]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self saveCurrentCityAtIndexPath:indexPath];

    [self showLoadingStateForce];
    [self.contentView.tableView setUserInteractionEnabled:NO];
    [MANEKI_SERVICE getRestaurantInfoWithDelegate:self];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (UIColor *)colorForErrorTitle {
    return [UIColor whiteColor];
}

- (void)requestData {
    [super requestData];

    self.fetchResult = [MANEKI_SERVICE getCitiesListWithDelegate:self];
}

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];

    [self.contentView.tableView setUserInteractionEnabled:YES];
    if ([service isEqual:[MARestaurantService class]]) {
        [self showLoadingState];
        [CART_SERVICE updateCartInfoWithSuccessHandler:^{
                    [self hideLoadingState];
                    self.showPrevScreen ? [self showPreviousScreen] : [self showAuthScreen];
                }
                                        failureHandler:^(NSError *error) {
                                            [self serverError:error
                                                   forService:service];
                                        }];
    }
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];

    [self.contentView.tableView setUserInteractionEnabled:YES];
    if ([service isEqual:[MARestaurantService class]]) {
        [self showMessage:[self requestErrorDescription]];
    }
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];

    [self.contentView.tableView setUserInteractionEnabled:YES];
    if ([service isEqual:[MARestaurantService class]]) {
        [self showMessage:[self requestErrorDescription]];
    }
}

@end
