//
//  MACityCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 18.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MACityCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "City.h"

NSString *const cCityCellIdentifier = @"MACityCell";

@interface MACityCell ()

@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@end

@implementation MACityCell

#pragma mark - Public

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
}

+ (NSString *)cellIdentifier {
    return cCityCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[City class]]) {
        [self fillCellByCity:model];
    }
}

- (void)configureCell {
    self.backgroundColor = [MAColorPalette cityCellBackgroundColor];
    self.cityLabel.textColor = [MAColorPalette whiteColor];
    self.cityLabel.font = [MAFontSet cityCellFont];
    self.selectedBackgroundView = [self prepareSelectedBackgroundView];
}

#pragma mark - Private

- (UIView *)prepareSelectedBackgroundView {
    UIView *selectedBackgroundView = [[UIView alloc] init];
    [selectedBackgroundView setBackgroundColor:[MAColorPalette backMenuCellSelectedStateColor]];
    return selectedBackgroundView;
}

- (void)fillCellByCity:(City *)city {
    self.cityLabel.text = city.name;
}

@end
