//
//  MACityChooseView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MACityChooseView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@interface MACityChooseView ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@end

@implementation MACityChooseView

#pragma mark - Setup

- (void)setup {
    [self setBackgroundColor:[MAColorPalette backgroundColor]];
    [self.infoLabel setTextColor:[MAColorPalette whiteColor]];
    [self.infoLabel setFont:[MAFontSet cityChooseLabelFont]];
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView setBackgroundColor:[MAColorPalette backgroundColor]];
    [self.tableView setSeparatorColor:[MAColorPalette backgroundColor]];
}

@end
