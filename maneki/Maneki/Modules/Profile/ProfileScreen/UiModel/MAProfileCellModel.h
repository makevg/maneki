//
//  MAProfileCellModel.h
//  Maneki
//
//  Created by Максимычев Е.О. on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAProfileCellModel : NSObject

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *placeholder;

- (instancetype)initWithTitle:(NSString *)title
                  placeholder:(NSString *)placeholder;

@end
