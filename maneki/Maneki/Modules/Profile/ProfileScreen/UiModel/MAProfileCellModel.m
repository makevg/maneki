//
//  MAProfileCellModel.m
//  Maneki
//
//  Created by Максимычев Е.О. on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProfileCellModel.h"

@implementation MAProfileCellModel

- (instancetype)initWithTitle:(NSString *)title
                  placeholder:(NSString *)placeholder {
    self = [super init];
    if (self) {
        _title = title;
        _placeholder = placeholder;
    }
    return self;
}

@end
