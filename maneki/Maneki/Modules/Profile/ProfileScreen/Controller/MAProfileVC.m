//
//  MAProfileVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProfileVC.h"
#import "MAProfileView.h"
#import "MAProfileCellWithField.h"
#import "MAProfileCellWithLabel.h"
#import "MAProfileCellWithLabels.h"
#import "MAProfileCellWithButton.h"
#import "MAProfileCellModel.h"
#import "MACityService.h"
#import "MAAddAddressVC.h"
#import "MACityChooseVC.h"
#import "MACommonService.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "VKSdk.h"
#import "City.h"
#import "User.h"
#import "MAUserService.h"
#import "MAAddressService.h"
#import "MAProfileAddressCell.h"
#import "SCLAlertView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "Address.h"

NSString *const cProfileStoryboardName = @"MAProfile";
NSString *const cProfileTitle = @"Профиль";

typedef NS_ENUM(NSUInteger, ProfileSections) {
    eProfileFieldsSection = 0,
    eProfileAddressSection,
    eProfileSocialSection,
    eProfileLogoutSection
};

typedef NS_ENUM(NSUInteger, SocialTypes) {
    eVkSocialType = 0,
    eFbSocialType
};

@interface MAProfileVC () <MGSwipeTableCellDelegate, VKSdkDelegate, VKSdkUIDelegate, NSFetchedResultsControllerDelegate, UITextFieldDelegate>
@property(strong, nonatomic) IBOutlet MAProfileView *contentView;
@end

@implementation MAProfileVC {
    MAUserService *userService;
    NSFetchedResultsController *addressFrc;
    NSMutableArray *secondSectionData;
    NSArray *sectionsData;
    NSArray *vkScope;
    BOOL tappedSaveButton;
    NSString *userName;
}

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self setTitle:cProfileTitle];
    [self requestData];
    [self.contentView.tableView reloadData];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cProfileStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (UITableView *)getTableView {
    return self.contentView.tableView;
}

- (void)configureController {
    [self prepareVkSdk];
    [self prepareTableView];
    [self cancelsTouches:NO];
    [self.contentView showSaveButton:NO];
}

- (void)requestData {
    [super requestData];
    userService = [MAUserService new];
    self.fetchResult = [userService getUserInfoWithDelegate:self];
    addressFrc = [MAAddressService getAddressListWithDelegate:self];
    addressFrc.delegate = self;
    sectionsData = [self prepareSectionsData];
}

#pragma mark - Private

- (NSArray *)prepareSectionsData {
    NSArray *firstSectionData = [self prepareFirstSectionData];
    secondSectionData = [[self prepareSecondSectionData] mutableCopy];
    NSArray *thirdSectionData = [self prepareThirdSectionData];
    NSArray *fourthSectionData = [self prepareFourthSectionData];
    return @[firstSectionData, secondSectionData, thirdSectionData, fourthSectionData];
}

- (NSArray *)prepareFirstSectionData {
    userName = [[self.fetchResult.fetchedObjects[0] name] length] > 0 ? [self.fetchResult.fetchedObjects[0] name] : @"";
    NSString *phone = [[self.fetchResult.fetchedObjects[0] phone] length] > 0 ? [self.fetchResult.fetchedObjects[0] phone] : @"";
    MAProfileCellModel *nameModel = [[MAProfileCellModel alloc] initWithTitle:userName
                                                                  placeholder:@"Ваше имя"];
    MAProfileCellModel *phoneModel = [[MAProfileCellModel alloc] initWithTitle:phone
                                                                   placeholder:@"Номер телефона"];
    return @[nameModel, phoneModel];
}

- (NSArray *)prepareSecondSectionData {
    NSMutableArray *array = [[addressFrc fetchedObjects] mutableCopy];
    [array addObject:@""];
    return array;
}

- (NSArray *)prepareThirdSectionData {
    NSString *vkString = [[self.fetchResult.fetchedObjects[0] vk] boolValue] ? @"Отвязать ВКонтакте" : @"Привязать ВКонтакте";
    NSString *fbString = [[self.fetchResult.fetchedObjects[0] fb] boolValue] ? @"Отвязать Facebook" : @"Привязать Facebook";
    MAProfileCellModel *vkModel = [[MAProfileCellModel alloc] initWithTitle:@""
                                                                placeholder:vkString];
    MAProfileCellModel *fbModel = [[MAProfileCellModel alloc] initWithTitle:@""
                                                                placeholder:fbString];
    return @[vkModel, fbModel];
}

- (NSArray *)prepareFourthSectionData {
    MAProfileCellModel *logoutModel = [[MAProfileCellModel alloc] initWithTitle:@""
                                                                    placeholder:@"Выйти"];
    return @[logoutModel];
}

- (void)prepareVkSdk {
    vkScope = @[VK_PER_EMAIL];
    NSString *vkAppId = [MACommonService getVkAppId];
    [[VKSdk initializeWithAppId:vkAppId] registerDelegate:self];
    [[VKSdk instance] setUiDelegate:self];
    [VKSdk wakeUpSession:vkScope completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationAuthorized) {
            NSLog(@"work started");
        } else if (error) {
            NSLog(@"%@", [error description]);
        }
    }];
}

- (NSString *)cellIdentifierAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier;
    switch (indexPath.section) {
        case eProfileFieldsSection:
            identifier = [MAProfileCellWithField cellIdentifier];
            break;
        case eProfileAddressSection:
            identifier = indexPath.row != [secondSectionData count] - 1 ? [MAProfileAddressCell cellIdentifier] : [MAProfileCellWithLabel cellIdentifier];
            break;
        case eProfileSocialSection:
            identifier = [MAProfileCellWithLabels cellIdentifier];
            break;
        case eProfileLogoutSection:
            identifier = [MAProfileCellWithButton cellIdentifier];
            break;
        default:
            break;
    }
    return identifier;
}

- (void)openSocialByIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case eVkSocialType:
            [self vkAction];
            break;
        case eFbSocialType:
            [self fbAction];
            break;
        default:
            break;
    }
}

- (void)vkAction {
    if ([[self.fetchResult.fetchedObjects[0] vk] boolValue]) {
        [VKSdk forceLogout];
        [self showLoadingStateForce];
        [userService unBindSocialType:eSRTVK
                             delegate:self];
    } else {
        [VKSdk authorize:vkScope];
    }
}

- (void)fbAction {
    if ([[self.fetchResult.fetchedObjects[0] fb] boolValue]) {
        [[FBSDKLoginManager new] logOut];
        [self showLoadingStateForce];
        [userService unBindSocialType:eSRTFB
                             delegate:self];
    } else {
        [self openFb];
    }
}

- (void)openFb {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    [self showMessage:error.localizedDescription];
                                    NSLog(@"Process error");
                                } else if (result.isCancelled) {
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in");
                                    [userService bindSocialType:eSRTFB
                                                          token:result.token.tokenString
                                                       delegate:self];
                                }
                            }];
}

- (void)logout {
    [MACommonService setLoggedIn:NO];
    [MAUserService deleteUserWithDelegate:self];
    [MAAddressService deleteAddressesWithDelegate:self];
    [self setRootFeatureByName:[MACityChooseVC storyboardName]];
}

- (void)showSuccessAlert {
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.customViewColor = [MAColorPalette orangeColor];
    alert.iconTintColor = [UIColor whiteColor];

    [alert setTitleFontFamily:cRegularFontName withSize:20.f];
    [alert setBodyTextFontFamily:cRegularFontName withSize:14.f];
    [alert setButtonsTextFontFamily:cBoldFontName withSize:14.f];

    [alert showInfo:@"Поздравляем"
           subTitle:[NSString stringWithFormat:@"Данные сохранены"]
   closeButtonTitle:@"Ок"
           duration:0.0f];
}

- (void)showAddresByIndexPath:(NSIndexPath *)indexPath {
    NSManagedObjectID *addressId = [secondSectionData[indexPath.row] objectID];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[MAAddAddressVC storyboardName] bundle:nil];
    MAAddAddressVC *vc = [sb instantiateInitialViewController];
    vc.addressId = addressId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Actions

- (IBAction)tappedSaveButton:(id)sender {
    tappedSaveButton = YES;
    userName = [userName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [userService changeUserName:userName delegate:self];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    if (![userName isEqualToString:textField.text]) {
        userName = textField.text;
        [self.contentView showSaveButton:YES];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (![userName isEqualToString:textField.text]) {
        userName = textField.text;
        [self.contentView showSaveButton:YES];
    }
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [sectionsData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *dataForSection = sectionsData[(NSUInteger) section];
    return [dataForSection count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [self cellIdentifierAtIndexPath:indexPath];
    if ([identifier isEqualToString:[MAProfileAddressCell cellIdentifier]]) {
        MAProfileAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                                     forIndexPath:indexPath];
        cell.delegate = self;
        [cell setModel:secondSectionData[(NSUInteger) indexPath.row]];
        return cell;
    } else {
        MABaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                                    forIndexPath:indexPath];
        NSArray *dataForSection = sectionsData[(NSUInteger) indexPath.section];
        [cell setModel:dataForSection[(NSUInteger) indexPath.row]];
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case eProfileAddressSection: {
            if (indexPath.row == [secondSectionData count] - 1) {
                [self pushFeatureByName:[MAAddAddressVC storyboardName] animated:YES];
            } else {
                [self showAddresByIndexPath:indexPath];
            }
            break;
        }
        case eProfileSocialSection:
            [self openSocialByIndexPath:indexPath];
            break;
        case eProfileLogoutSection:
            [self logout];
            break;
        default:
            break;
    }
}

#pragma mark - MGSwipeTableCellDelegate

- (BOOL)swipeTableCell:(MGSwipeTableCell *)cell
   tappedButtonAtIndex:(NSInteger)index
             direction:(MGSwipeDirection)direction
         fromExpansion:(BOOL)fromExpansion {
    NSIndexPath *indexPath = [self.contentView.tableView indexPathForCell:cell];
    switch (index) {
        case 0: {
            [MAAddressService deleteAddress:secondSectionData[(NSUInteger) indexPath.row] delegate:self];
            [secondSectionData removeObjectAtIndex:(NSUInteger) indexPath.row];
            [self.contentView.tableView deleteRowsAtIndexPaths:@[indexPath]
                                              withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case 1: {
            [MAAddressService setIsMainAddress:secondSectionData[(NSUInteger) indexPath.row]
                                      delegate:self];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
        }
        default:
            break;
    }
    return YES;
}

#pragma mark - NSFetchResultControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type {
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    sectionsData = [self prepareSectionsData];
    [self.contentView.tableView reloadData];
}

#pragma mark - VKSdkDelegate

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self.navigationController.topViewController];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    if (result.token) {
        [userService bindSocialType:eSRTVK
                              token:result.token.accessToken
                           delegate:self];
    } else if (result.error) {
        NSLog(@"%@", [NSString stringWithFormat:@"Access denied\n%@", result.error]);
        [self showMessage:result.error.localizedDescription];
    }
}

- (void)vkSdkUserAuthorizationFailed {
    NSLog(@"Access denied");
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self.navigationController.topViewController presentViewController:controller
                                                              animated:YES
                                                            completion:nil];
}

#pragma mark - ServiceResultDelegate

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];

    if (tappedSaveButton) {
        tappedSaveButton = NO;
        [self.contentView showSaveButton:NO];
        [self showSuccessAlert];
        self.fetchResult = [userService getUserInfoWithDelegate:self];
        sectionsData = [self prepareSectionsData];
        [self.contentView.tableView reloadData];
    }
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];

    [self showMessage:[self requestErrorDescription]];
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];

    [self showMessage:error.localizedDescription];
}

@end
