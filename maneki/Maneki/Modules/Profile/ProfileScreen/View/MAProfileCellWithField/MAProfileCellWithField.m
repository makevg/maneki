//
//  MAProfileCellWithField.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProfileCellWithField.h"
#import "MAProfileCellModel.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cProfileCellWithFieldIdentifier = @"ProfileCellWithField";

@implementation MAProfileCellWithField

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cProfileCellWithFieldIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[MAProfileCellModel class]]) {
        [self fillCellByModel:model];
    }
}

#pragma mark - Private

- (void)fillCellByModel:(MAProfileCellModel *)model {
    if (![model.placeholder isEqualToString:@"Ваше имя"]) {
        self.field.enabled = NO;
    }
    self.field.placeholder = model.placeholder;
    self.field.text = model.title;
}

@end
