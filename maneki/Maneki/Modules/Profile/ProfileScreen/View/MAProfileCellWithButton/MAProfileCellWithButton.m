//
//  MAProfileCellWithButton.m
//  Maneki
//
//  Created by Maximychev Evgeny on 05.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAProfileCellWithButton.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cProfileCellWithButtonIdentifier = @"ProfileCellWithButton";

@interface MAProfileCellWithButton ()
@property (weak, nonatomic) IBOutlet UILabel *logoutLabel;
@end

@implementation MAProfileCellWithButton

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cProfileCellWithButtonIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSString class]]) {
        self.logoutLabel.text = model;
    }
}

- (void)configureCell {
    self.backgroundColor = [MAColorPalette whiteColor];
    self.logoutLabel.textColor = [MAColorPalette contactFieldPlaceholderRequiredTextColor];
    self.logoutLabel.font = [MAFontSet profileLogoutButtonFont];
}

@end
