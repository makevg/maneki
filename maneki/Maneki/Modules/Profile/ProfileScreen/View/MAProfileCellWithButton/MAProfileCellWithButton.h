//
//  MAProfileCellWithButton.h
//  Maneki
//
//  Created by Maximychev Evgeny on 05.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseTableViewCell.h"

@interface MAProfileCellWithButton : MABaseTableViewCell

@end
