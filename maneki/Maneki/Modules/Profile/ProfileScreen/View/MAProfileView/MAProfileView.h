//
//  MAProfileView.h
//  Maneki
//
//  Created by Maximychev Evgeny on 21.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MAProfileView : MABaseView

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void)showSaveButton:(BOOL)show;

@end
