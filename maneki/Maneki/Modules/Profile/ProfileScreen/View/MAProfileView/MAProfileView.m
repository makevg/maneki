//
//  MAProfileView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProfileView.h"
#import "MAColorPalette.h"
#import "MABottomButton.h"

@interface MAProfileView ()
@property (weak, nonatomic) IBOutlet MABottomButton *saveButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saveButtonHeightConstraint;
@end

@implementation MAProfileView

#pragma mark - Setup

- (void)setup {
    self.tableView.backgroundColor = [MAColorPalette lightGrayColor];
    self.saveButton.style = MABottomButtonOrangeStyle;
}

#pragma mark - Public

- (void)showSaveButton:(BOOL)show {
    self.saveButtonHeightConstraint.constant = show ? 50.f : 0.f;
    self.saveButton.hidden = !show;
}

@end
