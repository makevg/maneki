//
//  MAProfileCellWithLabel.h
//  Maneki
//
//  Created by Maximychev Evgeny on 21.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseTableViewCell.h"

@interface MAProfileCellWithLabel : MABaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end
