//
//  MAProfileCellWithLabel.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProfileCellWithLabel.h"
#import "MAProfileCellModel.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cProfileCellWithLabel = @"ProfileCellWithLabel";

@implementation MAProfileCellWithLabel

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cProfileCellWithLabel;
}

- (void)configureCell {
    self.infoLabel.textColor = [MAColorPalette orangeColor];
    self.infoLabel.font = [MAFontSet profileLabelFont];
}

@end
