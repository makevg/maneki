//
//  MAProfileCellWithLabels.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProfileCellWithLabels.h"
#import "MAProfileCellModel.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cProfileCellWithLabels = @"ProfileCellWithLabels";

@implementation MAProfileCellWithLabels

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cProfileCellWithLabels;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[MAProfileCellModel class]]) {
        [self fillCellByModel:model];
    }
}

- (void)configureCell {
    self.leftLabel.textColor = [MAColorPalette blackColor];
    self.leftLabel.font = [MAFontSet profileLabelFont];
    self.rightLabel.textColor = [MAColorPalette grayColor];
    self.rightLabel.font = [MAFontSet profileLabelFont];
}

#pragma mark - Private

- (void)fillCellByModel:(MAProfileCellModel *)model {
    self.leftLabel.text = model.placeholder;
    self.rightLabel.text = model.title;
}

@end
