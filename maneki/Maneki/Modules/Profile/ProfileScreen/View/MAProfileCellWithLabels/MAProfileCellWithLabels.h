//
//  MAProfileCellWithLabels.h
//  Maneki
//
//  Created by Maximychev Evgeny on 21.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseTableViewCell.h"

@interface MAProfileCellWithLabels : MABaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@end
