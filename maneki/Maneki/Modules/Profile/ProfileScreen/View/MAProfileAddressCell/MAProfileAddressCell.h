//
//  MAProfileAddressCell.h
//  Maneki
//
//  Created by Maximychev Evgeny on 24.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"

@interface MAProfileAddressCell : MGSwipeTableCell

+ (NSString *)cellIdentifier;
- (void)setModel:(id)model;

@end
