//
//  MAProfileAddressCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 24.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAProfileAddressCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "Address.h"

NSString *const cProfileAddressCellIdentifier = @"ProfileAddressCellIdentifier";

@interface MAProfileAddressCell ()
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end

@implementation MAProfileAddressCell

- (void)awakeFromNib {
    [self configureCell];
}

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cProfileAddressCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[Address class]]) {
        [self fillCellByAddress:model];
    }
}

#pragma mark - Private

- (void)configureCell {
    self.tintColor = [MAColorPalette backgroundColor];
    self.addressLabel.font = [MAFontSet profileLabelFont];
    self.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Удалить" backgroundColor:[UIColor redColor]],
                          [MGSwipeButton buttonWithTitle:@"Как основной" backgroundColor:[MAColorPalette backgroundColor]]];
}

- (void)fillCellByAddress:(Address *)address {
    self.addressLabel.text = [NSString stringWithFormat:@"%@, %@", address.street, address.house];
    if ([address.is_main boolValue]) {
        self.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

@end
