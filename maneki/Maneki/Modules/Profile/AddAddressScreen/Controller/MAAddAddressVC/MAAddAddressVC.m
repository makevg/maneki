//
//  MAAddAddressVC.m
//  Maneki
//
//  Created by Максимычев Е.О. on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAAddAddressVC.h"
#import "MAAddAddressTableVC.h"
#import "MAAddAddressView.h"
#import "MABottomButton.h"
#import "MAProfileTextField.h"
#import "MAAddressService.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "SCLAlertView.h"
#import "Address.h"

NSString *const cAddAddressStoryboardName = @"MAAddAddress";
NSString *const cChangeAddressTitle = @"Редактировать адрес";

@interface MAAddAddressVC () <MAAddAddressTableVCDelegate>
@property (strong, nonatomic) IBOutlet MAAddAddressView *contentView;
@end

@implementation MAAddAddressVC {
    MAAddAddressTableVC *destinationVC;
    UIAlertController *clearFieldsAlert;
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cAddAddressStoryboardName;
}

- (void)configureController {
    if (self.addressId) {
        [self setTitle:cChangeAddressTitle];
        [self.contentView.addAdresButton setTitle:cChangeAddressTitle forState:UIControlStateNormal];
        Address *address = [MAAddressService addressById:self.addressId];
        [self fillFieldsByAddress:address];
    }
    [self prepareClearFieldsAlert];
}

#pragma mark - Private

- (void)fillFieldsByAddress:(Address *)address {
    destinationVC.streetField.text = address.street;
    destinationVC.houseField.text = address.house;
    destinationVC.apartmentField.text = [NSString stringWithFormat:@"%@", address.flat];
    destinationVC.porchField.text = [NSString stringWithFormat:@"%@", address.porch];
    destinationVC.floorField.text = [NSString stringWithFormat:@"%@", address.floor];
    destinationVC.codeField.text = address.porch_code;
}

- (BOOL)fieldsIsEmpty {
    return [destinationVC.streetField.text length] <= 0
            && [destinationVC.houseField.text length] <= 0
            && [destinationVC.apartmentField.text length] <= 0
            && [destinationVC.porchField.text length] <= 0
            && [destinationVC.floorField.text length] <= 0
            && [destinationVC.codeField.text length] <= 0;
}

- (void)clearFields {
    destinationVC.streetField.text = @"";
    destinationVC.houseField.text = @"";
    destinationVC.apartmentField.text = @"";
    destinationVC.porchField.text = @"";
    destinationVC.floorField.text = @"";
    destinationVC.codeField.text = @"";
}

- (void)prepareClearFieldsAlert {
    clearFieldsAlert = [UIAlertController alertControllerWithTitle:nil
                                                           message:@"Очистить поля?"
                                                    preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Нет"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {}];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Да"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         [self clearFields];
                                                     }];
    [clearFieldsAlert addAction:cancelAction];
    [clearFieldsAlert addAction:okAction];
}

- (void)addNewAddress {
    [MAAddressService addAddressesWithStreet:destinationVC.streetField.text
                                       house:destinationVC.houseField.text
                                        flat:destinationVC.apartmentField.text
                                       porch:destinationVC.porchField.text
                                       floor:destinationVC.floorField.text
                                  porch_code:destinationVC.codeField.text
                                    delegate:self];
}

- (void)changeAddress {
    [MAAddressService updateAddressesById:[[MAAddressService addressById:self.addressId] id]
                                   street:destinationVC.streetField.text
                                    house:destinationVC.houseField.text
                                     flat:destinationVC.apartmentField.text
                                    porch:destinationVC.porchField.text
                                    floor:destinationVC.floorField.text
                               porch_code:destinationVC.codeField.text
                                 delegate:self];
}

- (void)addAddressAction {
    if (![self fieldsIsEmpty]) {
        self.addressId ? [self changeAddress] : [self addNewAddress];
    } else {
        [self showMessage:@"Заполниете поля"];
    }
}

- (void)showSuccessAlert {
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.customViewColor = [MAColorPalette orangeColor];
    alert.iconTintColor = [UIColor whiteColor];
    
    [alert setTitleFontFamily:cRegularFontName withSize:20.f];
    [alert setBodyTextFontFamily:cRegularFontName withSize:14.f];
    [alert setButtonsTextFontFamily:cBoldFontName withSize:14.f];
    
    [alert alertIsDismissed:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [alert showInfo:@"Поздравляем"
           subTitle:[NSString stringWithFormat:@"Адрес сохранен"]
   closeButtonTitle:@"Ок"
           duration:0.0f];
}

#pragma mark - Actions

- (IBAction)tappedAddAddressButton:(id)sender {
    [self addAddressAction];
}

- (IBAction)tappedClearButton:(id)sender {
    [self presentViewController:clearFieldsAlert animated:YES completion:nil];
}

#pragma mark - MAAddAddressTableVCDelegate

- (void)tappedKeyboardDoneButton:(MAAddAddressTableVC *)controller {
    [self addAddressAction];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    destinationVC = segue.destinationViewController;
    destinationVC.delegate = self;
}

#pragma mark - ServiceResultDelegate

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];
    
    [self showSuccessAlert];
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];
    
    [self showMessage:[self requestErrorDescription]];
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];
    
    [self showMessage:error.localizedDescription];
}

@end
