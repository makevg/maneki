//
//  MAAddAddressVC.h
//  Maneki
//
//  Created by Максимычев Е.О. on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseVC.h"

@interface MAAddAddressVC : MABaseVC

@property (nonatomic) NSManagedObjectID *addressId;

@end
