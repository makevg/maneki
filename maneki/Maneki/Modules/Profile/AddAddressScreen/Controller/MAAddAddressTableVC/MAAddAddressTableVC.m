//
//  MAAddAddressTableVC.m
//  Maneki
//
//  Created by Максимычев Е.О. on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAAddAddressTableVC.h"
#import "MAProfileTextField.h"

@interface MAAddAddressTableVC () <UITextFieldDelegate>

@end

@implementation MAAddAddressTableVC

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger tag = textField.tag;
    switch (tag) {
        case 0:
            [self.houseField becomeFirstResponder];
            break;
        case 1:
            [self.apartmentField becomeFirstResponder];
            break;
        case 2:
            [self.porchField becomeFirstResponder];
            break;
        case 3:
            [self.floorField becomeFirstResponder];
            break;
        case 4:
            [self.codeField becomeFirstResponder];
            break;
        case 5: {
            [self.codeField resignFirstResponder];
            if (self.delegate) {
                [self.delegate tappedKeyboardDoneButton:self];
            }
            break;
        }
        default:
            break;
    }
    return NO;
}

@end
