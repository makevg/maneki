//
//  MAAddAddressTableVC.h
//  Maneki
//
//  Created by Максимычев Е.О. on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MAProfileTextField;
@protocol MAAddAddressTableVCDelegate;

@interface MAAddAddressTableVC : UITableViewController

@property (weak, nonatomic) IBOutlet MAProfileTextField *streetField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *houseField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *apartmentField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *porchField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *floorField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *codeField;
@property (weak, nonatomic) id<MAAddAddressTableVCDelegate> delegate;

@end

@protocol MAAddAddressTableVCDelegate <NSObject>

- (void)tappedKeyboardDoneButton:(MAAddAddressTableVC *)controller;

@end