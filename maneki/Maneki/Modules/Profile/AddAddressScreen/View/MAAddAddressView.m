//
//  MAAddAddressView.m
//  Maneki
//
//  Created by Максимычев Е.О. on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAAddAddressView.h"
#import "MABottomButton.h"
#import "MAColorPalette.h"

@implementation MAAddAddressView

#pragma mark - Setup

- (void)setup {
    self.addAdresButton.backgroundColor = [MAColorPalette orangeColor];
}

@end
