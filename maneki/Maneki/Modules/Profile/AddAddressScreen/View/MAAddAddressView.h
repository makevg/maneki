//
//  MAAddAddressView.h
//  Maneki
//
//  Created by Максимычев Е.О. on 23.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@class MABottomButton;

@interface MAAddAddressView : MABaseView

@property (weak, nonatomic) IBOutlet MABottomButton *addAdresButton;

@end
