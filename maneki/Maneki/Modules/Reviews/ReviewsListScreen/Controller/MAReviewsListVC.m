//
//  MAReviewsListVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import "MAServiceLayer.h"
#import "MAReviewsListVC.h"
#import "MAReviewsListView.h"
#import "MAReviewCell.h"
#import "MAReviewVC.h"
#import "MAFontSet.h"

NSString *const cReviewsListStoryboardName = @"MAReviewsList";
NSString *const cReviewsListTitle = @"Отзывы";

@interface MAReviewsListVC () <ServiceResultDelegate>
@property (strong, nonatomic) IBOutlet MAReviewsListView *contentView;
@end

@implementation MAReviewsListVC

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self setTitle:cReviewsListTitle];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cReviewsListStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (UITableView *)getTableView {
    return self.contentView.tableView;
}

- (void)configureController {
    [self requestData];
    [self prepareTableView];
    [self cancelsTouches:YES];
}

- (void)requestData {
    [super requestData];

    self.fetchResult = [MANEKI_SERVICE getReviewsListWithDelegate:self];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [MAReviewCell cellIdentifier];
    MAReviewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                         forIndexPath:indexPath];
    [cell setModel:[self.fetchResult objectAtIndexPath:indexPath]];
    return cell;
}

#pragma mark - Actions

- (IBAction)segmentSwitched:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        self.fetchResult = [MANEKI_SERVICE getReviewsListWithDelegate:self];
    } else {
        self.fetchResult = [MANEKI_SERVICE getMyReviewsListWithDelegate:self];
    }
    [self.contentView.tableView reloadData];
    [self scrollToTop];
}

- (IBAction)tappedWriteReviewButton:(id)sender {
    [self pushFeatureByName:[MAReviewVC storyboardName] animated:YES];
}

#pragma mark - DZNEmptyDataSetSource

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:cSadKittyImageName];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"Нет отзывов";
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName : [MAFontSet navBatTitleFont],
                                 NSForegroundColorAttributeName : [UIColor blackColor],
                                 NSParagraphStyleAttributeName : paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

@end
