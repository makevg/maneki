//
//  MAReviewsListView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAReviewsListView.h"
#import "MABottomButton.h"
#import "MAColorPalette.h"

@interface MAReviewsListView ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet MABottomButton *writeReviewButton;
@end

@implementation MAReviewsListView

#pragma mark - Setup

- (void)setup {
    self.backgroundColor = [MAColorPalette lightGrayColor];
    self.tableView.backgroundColor = [MAColorPalette lightGrayColor];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 120.f;
    self.segmentControl.tintColor = [MAColorPalette backgroundColor];
    self.writeReviewButton.backgroundColor = [MAColorPalette orangeColor];
}

@end
