//
//  MAReviewCell.m
//  Maneki
//
//  Created by Максимычев Е.О. on 28.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAReviewCell.h"
#import "MARateView.h"
#import "Review.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MADataFormatter.h"

NSString *const cReviewCellIdentifier = @"ReviewCell";

@interface MAReviewCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet MARateView *rateView;
@end

@implementation MAReviewCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cReviewCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[Review class]]) {
        [self fillCellByReview:model];
    }
}

- (void)configureCell {
    self.nameLabel.textColor = [MAColorPalette blackColor];
    self.nameLabel.font = [MAFontSet reviewUserNameFont];
    self.dateTimeLabel.textColor = [MAColorPalette lightGrayColor];
    self.dateTimeLabel.font = [MAFontSet reviewDateTimeFont];
    self.reviewLabel.textColor = [MAColorPalette grayColor];
    self.reviewLabel.font = [MAFontSet reviewDescriptionFont];
    self.rateView.backgroundColor = [MAColorPalette whiteColor];
    self.rateView.alignment = RateViewAlignmentLeft;
}

#pragma mark - Private

- (void)fillCellByReview:(Review *)review {
    self.nameLabel.text = review.user_name;
    self.dateTimeLabel.text = [MADataFormatter stringByUnixTimeStamp:review.published_at];
    self.reviewLabel.text = review.body;
    self.rateView.rate = [review.rate floatValue];
}

@end
