//
//  MAReviewVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAReviewVC.h"
#import "MAReviewView.h"
#import "MARateView.h"
#import "SCLAlertView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MAUserService.h"
#import "MAProfileVC.h"

NSString *const cReviewStoryboardName = @"MAReview";
NSString *const cReviewTitle = @"Новый отзыв";

@interface MAReviewVC () <MARateViewDelegate, UITextViewDelegate>
@property (strong, nonatomic) IBOutlet MAReviewView *contentView;
@property (strong, nonatomic) IBOutlet UIView *ratingView;
@property (weak, nonatomic) IBOutlet MARateView *rateView;
@property (weak, nonatomic) IBOutlet UIView *thinLineVIew;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButtonItem;
@end

@implementation MAReviewVC {
    NSNumber *curRate;
    NSFetchedResultsController *frc;
    BOOL tappedDoneButton;
}

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTitle:cReviewTitle];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cReviewStoryboardName;
}

- (void)configureController {
    MAUserService *userService = [MAUserService new];
    frc = [userService getUserInfoWithDelegate:self];
    [self prepareRatingView];
    self.contentView.reviewTextView.delegate = self;
    self.contentView.reviewTextView.inputAccessoryView = self.ratingView;
    [self.contentView.reviewTextView becomeFirstResponder];
}

#pragma mark - Private

- (void)prepareRatingView {
    curRate = @(0);
    self.thinLineVIew.backgroundColor = [MAColorPalette lightGrayColor];
    self.rateView.padding = 20;
    self.rateView.alignment = RateViewAlignmentCenter;
    self.rateView.editable = YES;
    self.rateView.delegate = self;
    [self.ratingView addSubview:self.rateView];
}

- (void)showSuccessAlert {
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.customViewColor = [MAColorPalette orangeColor];
    alert.iconTintColor = [UIColor whiteColor];

    [alert setTitleFontFamily:cRegularFontName withSize:20.f];
    [alert setBodyTextFontFamily:cRegularFontName withSize:14.f];
    [alert setButtonsTextFontFamily:cBoldFontName withSize:14.f];

    [alert addButton:@"Ок" actionBlock:^(void) {
        [self.navigationController popViewControllerAnimated:YES];
    }];

    [alert showInfo:@"Поздравляем"
           subTitle:[NSString stringWithFormat:@"Ваш отзыв успешно добавлен"]
   closeButtonTitle:nil
           duration:0.0f];
}

- (void)showErrorAlert {
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.customViewColor = [MAColorPalette orangeColor];
    alert.iconTintColor = [UIColor whiteColor];
    
    [alert setTitleFontFamily:cRegularFontName withSize:20.f];
    [alert setBodyTextFontFamily:cRegularFontName withSize:14.f];
    [alert setButtonsTextFontFamily:cBoldFontName withSize:14.f];
    
    [alert addButton:@"Представиться" actionBlock:^(void) {
        [self pushFeatureByName:[MAProfileVC storyboardName] animated:YES];
    }];
    
    [alert addButton:@"Отмена" actionBlock:^(void) {}];
    
    [alert showError:@"Внимание"
           subTitle:[NSString stringWithFormat:@"У вас не указано имя. Пожалуйста, представтесь"]
   closeButtonTitle:nil
           duration:0.0f];
}

#pragma mark - Actions

- (IBAction)tappedDoneButton:(id)sender {
    tappedDoneButton = YES;
    [self.contentView.reviewTextView resignFirstResponder];
    NSString *name = [[frc.fetchedObjects[0] name] length] > 0 ? [frc.fetchedObjects[0] name] : @"";
    if ([name length] == 0) {
        [self showErrorAlert];
    } else {
        [MANEKI_SERVICE addReviewWithText:self.contentView.reviewTextView.text
                                     rate:curRate
                                 delegate:self];
    }
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    self.doneButtonItem.enabled = [textView.text length] > 0;
}

#pragma mark - DYRateViewDelegate

- (void)rateView:(MARateView *)rateView changedToNewRate:(NSNumber *)rate {
    NSLog(@"Rate: %d", rate.intValue);
    curRate = rate;
}

#pragma mark - ServiceResultDelegate

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];

    if (tappedDoneButton) {
        [self showSuccessAlert];
    }
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];

    [self showMessage:error.localizedDescription];
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];

    [self showMessage:error.localizedDescription];
}

@end
