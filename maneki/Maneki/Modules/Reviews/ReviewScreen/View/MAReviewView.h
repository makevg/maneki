//
//  MAReviewView.h
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MAReviewView : MABaseView

@property (weak, nonatomic) IBOutlet UITextView *reviewTextView;

@end
