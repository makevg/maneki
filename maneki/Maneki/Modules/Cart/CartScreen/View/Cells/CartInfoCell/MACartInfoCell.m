//
//  MACartInfoCell.m
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MACartInfoCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MACartService.h"

NSString *const cCartInfoCellIdentifier = @"CartInfoCell";

@interface MACartInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@end

@implementation MACartInfoCell {
    NSInteger saleValue;
}

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cCartInfoCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSNumber class]]) {
        [self fillCellByCost:model];
    }
}

- (void)configureCell {
    self.infoLabel.textColor = [MAColorPalette grayColor];
    self.infoLabel.font = [MAFontSet orderInfoCellFont];
    self.costLabel.textColor = [MAColorPalette orangeColor];
    self.costLabel.font = [MAFontSet orderInfoCellPriceFont];
}

#pragma mark - Private

- (void)fillCellByCost:(NSNumber *)cost {
    self.costLabel.text = [NSString stringWithFormat:@"%li р.", (long)[cost integerValue]];
    NSString *infoString = saleValue > 0 ? [NSString stringWithFormat:@"Итого, с учетом скидки %li%%:", (long)saleValue] : @"Итого:";
    self.infoLabel.text = infoString;
}

- (void)setSaleValue:(NSInteger)saleValue1 {
    saleValue = saleValue1;
}


@end
