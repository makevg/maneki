//
//  MACartInfoCell.h
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseTableViewCell.h"

@interface MACartInfoCell : MABaseTableViewCell

- (void)setSaleValue:(NSInteger)saleValue;

@end
