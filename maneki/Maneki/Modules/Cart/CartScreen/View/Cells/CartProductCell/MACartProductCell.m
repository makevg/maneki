//
//  MACartProductCell.m
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MACartProductCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MACartItem.h"

NSString *const cCartProductCellIdentifier = @"CartProductCell";

@interface MACartProductCell ()
@property (weak, nonatomic) IBOutlet UILabel *productInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@end

@implementation MACartProductCell {
    NSUInteger productQuantity;
    NSUInteger itemId;
}

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cCartProductCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[MACartItem class]]) {
        [self fillCellByCartItem:model];
    }
}

- (void)configureCell {
    productQuantity = 0;
    [self updateCountLabelByCount:productQuantity];
    self.productInfoLabel.textColor = [MAColorPalette blackColor];
    self.productInfoLabel.font = [MAFontSet orderCellDefaultFont];
    self.costLabel.textColor = [MAColorPalette orangeColor];
    self.costLabel.font = [MAFontSet orderProductCellCountFont];
    self.descriptionLabel.textColor = [MAColorPalette grayColor];
    self.descriptionLabel.font = [MAFontSet orderInfoCellPropertiesFont];
    self.countLabel.font = [MAFontSet productCountLabelFont];
}

#pragma mark - Private

- (void)fillCellByCartItem:(MACartItem *)item {
    NSInteger numberOfLines = 2;
    if ([item.riceTitle length] > 0
            && [item.productTitle length] > 0
            && [item.sousTitle length] > 0) {
        numberOfLines = 0;
    }
    productQuantity = (NSUInteger) item.quantity;
    [self updateCountLabelByCount:productQuantity];
    self.productInfoLabel.text = item.productTitle;
    self.descriptionLabel.text = item.productDescr;
    self.descriptionLabel.numberOfLines = numberOfLines;
    self.costLabel.text = [NSString stringWithFormat:@"%@ р.", item.cost];
    itemId = item.itemId;
}

- (void)updateCountLabelByCount:(NSUInteger)count {
    if (count > 0) {
        self.countLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long) count];
    }
}

#pragma mark - Actions

- (IBAction)tappedMinusButton:(id)sender {
    productQuantity--;
    [self updateCountLabelByCount:productQuantity];
    if (self.delegate) {
        [self.delegate cell:self tappedMinusButtonWithItemId:itemId productQuantity:productQuantity];
    }
}

- (IBAction)tappedPlusButton:(id)sender {
    self.minusButton.enabled = YES;
    productQuantity++;
    [self updateCountLabelByCount:productQuantity];
    if (self.delegate) {
        [self.delegate cell:self tappedPlusButtonWithItemId:itemId productQuantity:productQuantity];
    }
}

@end
