//
//  MACartProductCell.h
//  Maneki
//
//  Created by Максимычев Е.О. on 30.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseTableViewCell.h"

@protocol MACartProductCellDelegate;

@interface MACartProductCell : MABaseTableViewCell

@property (weak, nonatomic) id<MACartProductCellDelegate> delegate;

@end

@protocol MACartProductCellDelegate

- (void)cell:(MACartProductCell *)cell tappedPlusButtonWithItemId:(NSUInteger)itemId productQuantity:(NSUInteger)quantity;

- (void)cell:(MACartProductCell *)cell tappedMinusButtonWithItemId:(NSUInteger)itemId productQuantity:(NSUInteger)quantity;

@end
