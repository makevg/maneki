//
//  MACartView.h
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@class MACartView;

typedef NS_OPTIONS(NSUInteger, MACartViewDeliveryMethod) {
    MACartViewDeliveryMethodDelivery = 0,
    MACartViewDeliveryMethodManual
};

@protocol MACartViewDelegate

@required
- (void)cartView:(MACartView *)cartView didChangeDeliveryMethod:(MACartViewDeliveryMethod)deliveryMethod;

@end

@interface MACartView : MABaseView

@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property(weak, nonatomic) id<MACartViewDelegate> delegate;
@property(weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

- (void)showCheckoutButton:(BOOL)show;

- (void)showControls:(BOOL)show;

- (void)setDeliveryEnabled:(BOOL)enabled;

- (void)setManualEnabled:(BOOL)enabled;

@end
