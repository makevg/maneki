//
//  MACartView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MACartView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MABottomButton.h"
#import "MACartService.h"

NSUInteger const cDeliverySegmentIdx = 0;
NSUInteger const cManualSegmentIdx = 1;
NSString *cInfoString = @"Вы не можете оформить заказ, поскольку минимальная сумма заказа состовляет";

@interface MACartView ()
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;
@property(weak, nonatomic) IBOutlet MABottomButton *checkoutButton;
@property(weak, nonatomic) IBOutlet UIView *infoView;
@property(weak, nonatomic) IBOutlet UILabel *checkoutLabel;
@property(weak, nonatomic) IBOutlet UILabel *infoLabel;
@end

@implementation MACartView

#pragma mark - Setup

- (void)setup {
    self.backgroundColor = [MAColorPalette lightGrayColor];
    self.tableView.backgroundColor = [MAColorPalette lightGrayColor];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 55.f;
    self.tableView.tableFooterView = [UIView new];
    self.segmentControl.tintColor = [MAColorPalette backgroundColor];
    self.checkoutButton.backgroundColor = [MAColorPalette orangeColor];
    self.infoView.backgroundColor = [MAColorPalette mediumGrayColor];
    self.checkoutLabel.textColor = [MAColorPalette whiteColor];
    self.checkoutLabel.font = [MAFontSet cartViewTitleLabelFont];
    self.infoLabel.textColor = [MAColorPalette whiteColor];
    self.infoLabel.font = [MAFontSet cartViewDescrLabelFont];
    [self updateInfoLabelBySelectedSegmentIndex:self.segmentControl.selectedSegmentIndex];
    [self.segmentControl addTarget:self
                            action:@selector(segmentControlAction:)
                  forControlEvents:UIControlEventValueChanged];
}

#pragma mark - Private

- (MACartViewDeliveryMethod)getDeliveryMethodFromSegmentedControl {
    switch ( self.segmentControl.selectedSegmentIndex ) {
        case cDeliverySegmentIdx:
        default:
            return MACartViewDeliveryMethodDelivery;
        case cManualSegmentIdx:
            return MACartViewDeliveryMethodManual;
    }
}

- (void)segmentControlAction:(id)sender {
    if( self.delegate ) {
        [self.delegate cartView:self
        didChangeDeliveryMethod:[self getDeliveryMethodFromSegmentedControl]];
        [self updateInfoLabelBySelectedSegmentIndex:self.segmentControl.selectedSegmentIndex];
    }
}

- (void)updateInfoLabelBySelectedSegmentIndex:(NSInteger)index {
    self.infoLabel.text = [NSString stringWithFormat:@"%@ %@ руб.", cInfoString, [CART_SERVICE getMinOrderCostByDeliveryType:(MADeliveryMethod) index]];
}

#pragma mark - Public

- (void)showCheckoutButton:(BOOL)show {
    self.infoView.hidden = show;
    self.tableViewBottomConstraint.constant = show ? -40.f : 0.f;
    show ? [self bringSubviewToFront:self.checkoutButton] : [self bringSubviewToFront:self.infoView];
}

- (void)showControls:(BOOL)show {
    self.segmentControl.hidden = !show;
    self.checkoutButton.hidden = !show;
    self.infoView.hidden = !show;
}

- (void)setDeliveryEnabled:(BOOL)enabled {
    [self.segmentControl setEnabled:enabled
                  forSegmentAtIndex:cDeliverySegmentIdx];
}

- (void)setManualEnabled:(BOOL)enabled {
    [self.segmentControl setEnabled:enabled
                  forSegmentAtIndex:cManualSegmentIdx];
}


@end
