//
//  MACartVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "MACartVC.h"
#import "MACartView.h"
#import "MACartProductCell.h"
#import "MACartInfoCell.h"
#import "MACheckoutVC.h"
#import "MACartItem.h"
#import "MAFontSet.h"

NSString *const cCartStoryboardName = @"MACart";
NSString *const cCartTitle = @"Корзина";
NSString *const cClearCartImageName = @"CloseWhite";
NSString *const cClearCartAlertMessage = @"Вы действительно хотите очистить корзину?";
NSString *const cRightButtonTitle = @"Удалить";

@interface MACartVC () <MACartProductCellDelegate, MACartViewDelegate>
@property(strong, nonatomic) IBOutlet MACartView *contentView;
@end

@implementation MACartVC {
    NSMutableArray *products;
    MACartViewDeliveryMethod currentCartDeliveryMethod;
}

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTitle:cCartTitle];

    [CART_SERVICE updateCartInfoWithSuccessHandler:^{
                [self reInitDataSource];
                [self.contentView.tableView reloadData];
            }
                                    failureHandler:^(NSError *error) {
                                        [self showMessage:error.localizedDescription];
                                    }];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cCartStoryboardName;
}

- (UITableView *)getTableView {
    return self.contentView.tableView;
}

- (void)configureController {
    currentCartDeliveryMethod = [self getCartDeliveryTypeByDeliveryType:CART_SERVICE.currentDeliveryMethod];

    self.contentView.delegate = self;
    [self reInitDataSource];
    [self prepareTableView];
    [self cancelsTouches:NO];
}

- (MACartViewDeliveryMethod)getCartDeliveryTypeByDeliveryType:(MADeliveryMethod)deliveryMethod {
    switch (deliveryMethod) {
        case MADeliveryMethodDelivery:
        default:
            return MACartViewDeliveryMethodDelivery;
        case MADeliveryMethodManual:
            return MACartViewDeliveryMethodManual;
    }
}

- (MADeliveryMethod)getDeliveryTypeByCartDeliveryType:(MACartViewDeliveryMethod)deliveryMethod {
    switch (deliveryMethod) {
        case MACartViewDeliveryMethodDelivery:
        default:
            return MADeliveryMethodDelivery;
        case MACartViewDeliveryMethodManual:
            return MADeliveryMethodManual;
    }
}

- (void)reInitDataSource {
    [self.contentView setDeliveryEnabled:[CART_SERVICE isMethodAvailable:MADeliveryMethodDelivery]];
    [self.contentView setManualEnabled:[CART_SERVICE isMethodAvailable:MADeliveryMethodManual]];
    [self.contentView.segmentControl setSelectedSegmentIndex:currentCartDeliveryMethod];

    products = [[CART_SERVICE getItems] mutableCopy];
    if ([CART_SERVICE currentDeliveryMethod] != MADeliveryMethodUnknown && ![CART_SERVICE cartIsEmpty]) {
        [self prepareClearCartButton];
        MADeliveryMethod deliveryMethod = [self getDeliveryTypeByCartDeliveryType:currentCartDeliveryMethod];
        [self.contentView showCheckoutButton:[CART_SERVICE checkOrderAbilityByDeliveryType:deliveryMethod]];
    } else {
        [self.contentView showControls:NO];
    }
}

#pragma mark - Private

- (void)prepareClearCartButton {
    UIBarButtonItem *clearCartButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:cClearCartImageName]
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self
                                                                       action:@selector(clearCartAction)];
    self.navigationItem.rightBarButtonItem = clearCartButton;
}

- (void)prepareClearCartAlert {
    UIAlertController *alertController = [UIAlertController
            alertControllerWithTitle:nil
                             message:cClearCartAlertMessage
                      preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction
            actionWithTitle:@"Нет"
                      style:UIAlertActionStyleCancel
                    handler:^(UIAlertAction *action) {
                    }];

    UIAlertAction *okAction = [UIAlertAction
            actionWithTitle:@"Да"
                      style:UIAlertActionStyleDefault
                    handler:^(UIAlertAction *action) {
                        [self clearCart];
                    }];

    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)clearCart {
    PCAngularActivityIndicatorView *indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                               forView:self.view];
    [indicatorView startAnimating];
    self.view.userInteractionEnabled = NO;

    __weak __typeof(&*self) weakSelf = self;

    [CART_SERVICE clearCartWithSuccessHandler:^{
        __strong __typeof(&*weakSelf) strongSelf = weakSelf;

        if (strongSelf) {
            strongSelf.navigationItem.rightBarButtonItem = nil;
            [strongSelf handleSuccessWithProgressHUD:indicatorView];
        }
    } failureHandler:^(NSError *error) {
        __strong __typeof(&*weakSelf) strongSelf = weakSelf;

        if (strongSelf) {
            [strongSelf handleError:error withProgressHUD:indicatorView];
        }
    }];
}

- (void)updateCartByRow:(NSInteger)row quantity:(NSUInteger)quantity {
    if (!quantity) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        [self tableView:self.contentView.tableView
     commitEditingStyle:UITableViewCellEditingStyleDelete
      forRowAtIndexPath:indexPath];

        return;
    }

    MACartItem *item = products[(NSUInteger) row];

    PCAngularActivityIndicatorView *indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                               forView:self.view];
    [indicatorView startAnimating];
    self.view.userInteractionEnabled = NO;

    __weak __typeof(&*self) weakSelf = self;

    [CART_SERVICE updateCartItem:item
                          amount:quantity
                  successHandler:^(NSUInteger productId, NSUInteger amount) {
                      __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                      if (strongSelf) {
                          [self handleSuccessWithProgressHUD:indicatorView];
                      }
                  }
                  failureHandler:^(NSUInteger productId, NSUInteger amount, NSError *error) {
                      __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                      if (strongSelf) {
                          [strongSelf handleError:error withProgressHUD:indicatorView];
                      }
                  }];
}

#pragma mark - Actions

- (void)clearCartAction {
    [self prepareClearCartAlert];
}

- (IBAction)tappedCheckoutButton:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[MACheckoutVC storyboardName] bundle:nil];
    MACheckoutVC *vc = [sb instantiateInitialViewController];
    vc.deliveryMethod = [self getDeliveryTypeByCartDeliveryType:currentCartDeliveryMethod];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([products count])
        return [products count] + 1;

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == [products count]) {
        MACartInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:[MACartInfoCell cellIdentifier]
                                                               forIndexPath:indexPath];
        MADeliveryMethod deliveryMethod = [self getDeliveryTypeByCartDeliveryType:currentCartDeliveryMethod];
        [cell setSaleValue:[CART_SERVICE getSaleValueByDeliveryType:deliveryMethod]];
        [cell setModel:@([CART_SERVICE totalAmountByDeliveryType:[self getDeliveryTypeByCartDeliveryType:currentCartDeliveryMethod]])];
        return cell;
    } else {
        MACartProductCell *cell = [tableView dequeueReusableCellWithIdentifier:[MACartProductCell cellIdentifier]
                                                                  forIndexPath:indexPath];
        [cell setModel:products[(NSUInteger) indexPath.row]];
        cell.delegate = self;
        return cell;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row != [products count];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        MACartItem *item = products[(NSUInteger) indexPath.row];

        PCAngularActivityIndicatorView *indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                                   forView:self.view];
        [indicatorView startAnimating];
        self.view.userInteractionEnabled = NO;

        __weak __typeof(&*self) weakSelf = self;
        [CART_SERVICE removeItemById:item.itemId
                           productId:[item.productId unsignedIntegerValue]
                      successHandler:^(NSUInteger productId, NSUInteger amount) {
                          __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                          if (strongSelf) {
                              [strongSelf handleSuccessWithProgressHUD:indicatorView];
                          }
                      }
                      failureHandler:^(NSUInteger productId, NSUInteger amount, NSError *error) {
                          __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                          if (strongSelf) {
                              [strongSelf handleError:error withProgressHUD:indicatorView];
                          }
                      }];
    }
}

#pragma mark - UITableViewDelegate

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return cRightButtonTitle;
}

#pragma mark - MACartProductCellDelegate

- (void)cell:(MACartProductCell *)cell tappedPlusButtonWithItemId:(NSUInteger)itemId productQuantity:(NSUInteger)quantity {
    NSInteger row = [CART_SERVICE itemPositionById:itemId];
    [self updateCartByRow:row quantity:quantity];
}

- (void)cell:(MACartProductCell *)cell tappedMinusButtonWithItemId:(NSUInteger)itemId productQuantity:(NSUInteger)quantity {
    NSInteger row = [CART_SERVICE itemPositionById:itemId];
    [self updateCartByRow:row quantity:quantity];
}

#pragma mark - DZNEmptyDataSetSource

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:cSadKittyImageName];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"Ваша корзина пуста";

    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;

    NSDictionary *attributes = @{NSFontAttributeName : [MAFontSet navBatTitleFont],
            NSForegroundColorAttributeName : [UIColor blackColor],
            NSParagraphStyleAttributeName : paragraph};

    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - MACartViewDelegate

- (void)cartView:(MACartView *)cartView didChangeDeliveryMethod:(MACartViewDeliveryMethod)deliveryMethod {
    currentCartDeliveryMethod = deliveryMethod;

    PCAngularActivityIndicatorView *indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                               forView:self.view];
    [indicatorView startAnimating];
    self.view.userInteractionEnabled = NO;
    __weak __typeof(&*self) weakSelf = self;
    [CART_SERVICE updateCartInfoWithSuccessHandler:^{
                __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                if (strongSelf) {
                    [strongSelf handleSuccessWithProgressHUD:indicatorView];
                }
            }
                                    failureHandler:^(NSError *error) {
                                        __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                                        if (strongSelf) {
                                            [strongSelf handleError:error withProgressHUD:indicatorView];
                                        }
                                    }];
}

- (void)handleSuccessWithProgressHUD:(PCAngularActivityIndicatorView *)progressHUD {
    if (progressHUD)
        [progressHUD stopAnimating];

    self.view.userInteractionEnabled = YES;
    [self reInitDataSource];
    [self.contentView.tableView reloadData];
}

- (void)handleError:(NSError *)error withProgressHUD:(PCAngularActivityIndicatorView *)progressHUD {
    if (progressHUD)
        [progressHUD stopAnimating];

    self.view.userInteractionEnabled = YES;
    [self showMessage:error.localizedDescription];
    [self reInitDataSource];
    [self.contentView.tableView reloadData];
}

@end
