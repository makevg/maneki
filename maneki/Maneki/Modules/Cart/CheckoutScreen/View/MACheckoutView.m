//
//  MACheckoutView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MACheckoutView.h"
#import "MABottomButton.h"
#import "MAColorPalette.h"

@interface MACheckoutView ()
@property (weak, nonatomic) IBOutlet MABottomButton *checkoutButton;
@end

@implementation MACheckoutView

#pragma mark - Setup

- (void)setup {
    self.checkoutButton.backgroundColor = [MAColorPalette orangeColor];
}

@end
