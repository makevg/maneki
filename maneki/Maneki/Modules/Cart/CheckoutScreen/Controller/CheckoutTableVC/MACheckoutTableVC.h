//
//  MACheckoutTableVC.h
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MACartService.h"

@class Address;
@class MAProfileTextField;
@class MACheckBoxView;
@protocol MACheckoutTableVCDelegate;
@class MABaseButton;

@interface MACheckoutTableVC : UITableViewController

@property (nonatomic) MADeliveryMethod deliveryMethod;
@property (weak, nonatomic) IBOutlet UIButton *chooseButton;
@property (weak, nonatomic) IBOutlet MAProfileTextField *nameField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *phoneField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *streetField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *houseField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *apartmentField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *porchField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *floorField;
@property (weak, nonatomic) IBOutlet MAProfileTextField *codeField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet MAProfileTextField *moneyField;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet MACheckBoxView *checkBoxView;
@property (weak, nonatomic) id<MACheckoutTableVCDelegate> delegate;

- (void)clearFields;
- (void)clearAddressFields;
- (BOOL)fieldsIsEmpty;
- (BOOL)manualFieldIsEmpty;
- (void)fillAddress:(Address *)address;

@end

@protocol MACheckoutTableVCDelegate

- (void)tappedChooseAddressButton:(MACheckoutTableVC *)controller;
- (void)controller:(MACheckoutTableVC *)controller switchedPayMethod:(MAPayMethod)payMethod;

@end