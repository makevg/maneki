//
//  MACheckoutTableVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MACheckoutTableVC.h"
#import "MAProfileTextField.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MAOrderHeader.h"
#import "Address.h"
#import "MABaseButton.h"
#import "MACommonService.h"

CGFloat const cMAOrderHeaderHeight = 60.f;
CGFloat const cDefaultRowHeight = 44.f;

@interface MACheckoutTableVC () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *footerInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *payInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *saveAddressLabel;
@end

@implementation MACheckoutTableVC {
    NSArray *sectionsHeaders;
    NSInteger selectedSegmentIndex;
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configure];
}

#pragma mark - Private

- (void)configure {
    selectedSegmentIndex = 0;
    sectionsHeaders = [self prepareSectionsHeaders];
    [self configureFields];
}

- (NSArray *)prepareSectionsHeaders {
    return @[@"Контактные данные", @"Способы оплаты", @"Комментарий к заказу"];
}

- (void)configureFields {
    self.nameField.type = eRequiredType;
    self.nameField.placeholder = @"Ваше имя";
    self.phoneField.type = eRequiredType;
    self.phoneField.placeholder = @"Номер телефона";
    self.streetField.type = eRequiredType;
    self.streetField.placeholder = @"Улица";
    self.houseField.type = eRequiredType;
    self.houseField.placeholder = @"Дом, корпус";
    self.apartmentField.type = eRequiredType;
    self.apartmentField.placeholder = @"Квартира";
    self.segmentControl.tintColor = [MAColorPalette backgroundColor];
    self.commentTextView.textColor = [MAColorPalette blackColor];
    self.commentTextView.font = [MAFontSet contactFieldFont];
    self.addressLabel.font = [MAFontSet deliveryHeaderFont];
    self.footerInfoLabel.font = [MAFontSet checkoutInfoLabelFont];
    self.payInfoLabel.font = [MAFontSet checkoutInfoLabelFont];
    self.saveAddressLabel.font = [MAFontSet checkoutSaveAddressLabelFont];
}

#pragma mark - Public

- (void)clearFields {
    self.nameField.text = @"";
    self.phoneField.text = @"";
    self.moneyField.text = @"";
    self.commentTextView.text = @"";
    [self clearAddressFields];
}

- (void)clearAddressFields {
    self.streetField.text = @"";
    self.houseField.text = @"";
    self.apartmentField.text = @"";
    self.porchField.text = @"";
    self.floorField.text = @"";
    self.codeField.text = @"";
}

- (BOOL)fieldsIsEmpty {
    return !([self.nameField.text length] > 0
            && [self.phoneField.text length] > 0
            && [self.streetField.text length] > 0
            && [self.houseField.text length] > 0
            && [self.apartmentField.text length] > 0);
}

- (BOOL)manualFieldIsEmpty {
    return !([self.nameField.text length] > 0
             && [self.phoneField.text length] > 0);
}

- (void)fillAddress:(Address *)address {
    self.streetField.text = address.street;
    self.houseField.text = address.house;
    self.apartmentField.text = [address.flat stringValue];
    self.porchField.text = [address.porch isEqualToNumber:@0] ? @"" : [address.porch stringValue];
    self.floorField.text = [address.floor isEqualToNumber:@0] ? @"" : [address.floor stringValue];
    self.codeField.text = address.porch_code;
}

#pragma mark - Actions

- (IBAction)tappedChooseAddressButton:(id)sender {
    if (self.delegate) {
        [self.delegate tappedChooseAddressButton:self];
    }
}

- (IBAction)switchedSegmentedControl:(UISegmentedControl *)sender {
    selectedSegmentIndex = sender.selectedSegmentIndex;
    MAPayMethod payMethod = MAPayMethodMoney;
    switch (selectedSegmentIndex) {
        case 0:
            payMethod = MAPayMethodMoney;
            break;
        case 1:
            payMethod = MAPayMethodOnlineCard;
            break;
        case 2:
            payMethod = MAPayMethodManualCard;
            break;
    }
    if (self.delegate) {
        [self.delegate controller:self switchedPayMethod:payMethod];
    }
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            if (self.deliveryMethod == MADeliveryMethodDelivery) {
                return [MACommonService isLoggedIn] ? 11 : 10;
            } else {
                return 2;
            }
        case 1:
            return 2;
        case 2:
            return 1;

        default:
            return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            if (indexPath.row == 2 || indexPath.row == 9) {
                return 60.f;
            } else {
                return cDefaultRowHeight;
            }
        case 1:
            switch (indexPath.row) {
                case 0:
                    return selectedSegmentIndex == 1 ? 80.f : cDefaultRowHeight;
                case 1:
                    return selectedSegmentIndex == 0 ? cDefaultRowHeight : 0.f;
            }
        case 2:
            return 100.f;
        default:
            return cDefaultRowHeight;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, cMAOrderHeaderHeight);
    MAOrderHeader *view = [[MAOrderHeader alloc] initWithFrame:frame];
    [view configureWithTitle:sectionsHeaders[(NSUInteger) section]];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return cMAOrderHeaderHeight;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger tag = textField.tag;
    switch (tag) {
        case 0:
            [self.phoneField becomeFirstResponder];
            break;
        case 1:
            [self.streetField becomeFirstResponder];
            break;
        case 2:
            [self.houseField becomeFirstResponder];
            break;
        case 3:
            [self.apartmentField becomeFirstResponder];
            break;
        case 4:
            [self.porchField becomeFirstResponder];
            break;
        case 5:
            [self.floorField becomeFirstResponder];
            break;
        case 6:
            [self.codeField becomeFirstResponder];
            break;
        case 7:
            [self.codeField resignFirstResponder];
            break;
        case 8:
            [self.moneyField resignFirstResponder];
            break;
        default:
            break;
    }
    return NO;
}

@end
