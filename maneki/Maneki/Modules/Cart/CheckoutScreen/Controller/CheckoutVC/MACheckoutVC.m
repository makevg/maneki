//
//  MACheckoutVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <Crashlytics/Crashlytics/CLSLogging.h>
#import "MACheckoutVC.h"
#import "MACheckoutTableVC.h"
#import "MAProfileTextField.h"
#import "MACheckBoxView.h"
#import "STKWebKitModalViewController.h"
#import "MAUserService.h"
#import "MAAddressService.h"
#import "Address.h"
#import "User.h"
#import "SCLAlertView.h"
#import "MAColorPalette.h"
#import "MACommonService.h"
#import "MAFontSet.h"

NSString *const cCheckoutStoryboardName = @"MACheckout";
NSString *const cClearFieldsAlertMessage = @"Очистить поля?";
NSString *const cOnlinePayUrlMessage = @"http://api.manekicafe.ru/cart/order/onlinepay/?order_id=";
NSString *const cFieldsEmptyMessage = @"Заполните обязательные поля";

@interface MACheckoutVC () <MACheckoutTableVCDelegate, NSFetchedResultsControllerDelegate, STKWebKitModalViewControllerDelegate>

@end

@implementation MACheckoutVC {
    MACheckoutTableVC *destinationVC;
    NSArray *addresses;
    MAPayMethod curPayMethod;
    NSNumber *createdOrderId;
    NSFetchedResultsController *addressFrc;
    NSFetchedResultsController *userFrc;
    BOOL mIsPayed;
    BOOL mNeedStopCheck;
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cCheckoutStoryboardName;
}

- (void)configureController {
    mNeedStopCheck = NO;
    mIsPayed = NO;
    curPayMethod = MAPayMethodMoney;
    destinationVC.deliveryMethod = self.deliveryMethod;
    destinationVC.chooseButton.hidden = YES;
    [self prepareSegmentedControl];
    [self prepareLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                   forView:self.view];
    [self requestData];
}

- (void)requestData {
    [super requestData];

    if ([MACommonService isLoggedIn]) {
        userFrc = [[MAUserService new] getUserInfoWithDelegate:self];
        userFrc.delegate = self;

        if ([[userFrc fetchedObjects] count]) {
            destinationVC.nameField.text = [userFrc.fetchedObjects[0] name];
            destinationVC.phoneField.text = [userFrc.fetchedObjects[0] phone];
        }

        addressFrc = [MAAddressService getAddressListWithDelegate:self];
        addressFrc.delegate = self;
        [self handleAddresses];
    }
}

- (void)handleAddresses {
    addresses = [addressFrc fetchedObjects];
    destinationVC.chooseButton.hidden = ![addresses count];

    BOOL isSet = NO;
    for (Address *address in addresses) {
        if ([address.is_main boolValue]) {
            isSet = YES;
            [destinationVC fillAddress:address];
            break;
        }
    }

    if( !isSet ) {
        [destinationVC clearAddressFields];
    }
}

- (void)showLoadingState {

}

#pragma mark - Private

- (void)prepareSegmentedControl {
    [destinationVC.segmentControl setEnabled:[CART_SERVICE canMoneyMethodByDeliveryType:self.deliveryMethod]
                           forSegmentAtIndex:0];
    [destinationVC.segmentControl setEnabled:[CART_SERVICE canOnlineCardMethodByDeliveryType:self.deliveryMethod]
                           forSegmentAtIndex:1];
}

- (void)prepareClearFieldsAlert {
    UIAlertController *alertController = [UIAlertController
            alertControllerWithTitle:nil
                             message:cClearFieldsAlertMessage
                      preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction
            actionWithTitle:@"Нет"
                      style:UIAlertActionStyleCancel
                    handler:^(UIAlertAction *action) {
                    }];

    UIAlertAction *okAction = [UIAlertAction
            actionWithTitle:@"Да"
                      style:UIAlertActionStyleDefault
                    handler:^(UIAlertAction *action) {
                        [destinationVC clearFields];
                    }];

    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)prepareAddressActionSheet {
    UIAlertController *alertController = [UIAlertController
            alertControllerWithTitle:nil
                             message:@"Выберите адрес"
                      preferredStyle:UIAlertControllerStyleActionSheet];

    for (Address *address in addresses) {
        UIAlertAction *addressAction = [UIAlertAction
                actionWithTitle:[NSString stringWithFormat:@"%@, %@", address.street, address.house]
                          style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction *action) {
                            [destinationVC fillAddress:address];
                        }];
        [alertController addAction:addressAction];
    }

    UIAlertAction *cancelAction = [UIAlertAction
            actionWithTitle:@"Отмена"
                      style:UIAlertActionStyleCancel
                    handler:^(UIAlertAction *action) {
                    }];
    [alertController addAction:cancelAction];

    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showMenuScreen {
    [UIView animateWithDuration:0.6f
                     animations:^{
                         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                         [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                                                forView:self.navigationController.view
                                                  cache:NO];
                     }];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)sendOrder {
    NSString *description = [NSString stringWithFormat:@"Нужна сдача с: %@, Комментарий: %@",
                             destinationVC.moneyField.text,
                             destinationVC.commentTextView.text
                             ];
    
    [self showLoadingStateForce];
    self.navigationController.view.userInteractionEnabled = NO;
    [CART_SERVICE sendOrderWithPayMethod:curPayMethod
                            deliveryType:self.deliveryMethod
                                  street:destinationVC.streetField.text
                                   house:destinationVC.houseField.text
                                    room:destinationVC.apartmentField.text
                                    name:destinationVC.nameField.text
                                   phone:destinationVC.phoneField.text
                                   porch:destinationVC.porchField.text
                                   floor:destinationVC.floorField.text
                               porchCode:destinationVC.codeField.text
                             description:description
                             saveAddress:[destinationVC.checkBoxView isChecked]
                            withDelegate:self];
}

- (void)deliveryMode {
    if (![destinationVC fieldsIsEmpty]) {
        [self sendOrder];
    } else {
        [self showMessage:cFieldsEmptyMessage];
    }
}

- (void)manualMode {
    if (![destinationVC manualFieldIsEmpty]) {
        [self sendOrder];
    } else {
        [self showMessage:cFieldsEmptyMessage];
    }
}

#pragma mark - Actions

- (IBAction)tappedCheckoutButton:(id)sender {
    if (createdOrderId) {
        [self runOnlinePay];
        return;
    }
    
    switch (self.deliveryMethod) {
        case MADeliveryMethodDelivery:
            [self deliveryMode];
            break;
        case MADeliveryMethodManual:
            [self manualMode];
            break;
        default:
            break;
    }
}

- (IBAction)tappedClearButton:(id)sender {
    [self prepareClearFieldsAlert];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    destinationVC = segue.destinationViewController;
    destinationVC.delegate = self;
}

#pragma mark - MACheckoutTableVCDelegate

- (void)tappedChooseAddressButton:(MACheckoutTableVC *)controller {
    if (addresses && [addresses count] > 0) {
        [self prepareAddressActionSheet];
    } else {
        [self showMessage:@"Нет сохраненных адресов"];
    }
}

- (void)controller:(MACheckoutTableVC *)controller switchedPayMethod:(MAPayMethod)payMethod {
    curPayMethod = payMethod;
    NSLog(@"%lu", (unsigned long) curPayMethod);
}

- (void)handleAfterOnlinePay {
    if (mIsPayed) {
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        alert.customViewColor = [MAColorPalette lightGreenColor];
        [alert setTitleFontFamily:cRegularFontName withSize:20.f];
        [alert setBodyTextFontFamily:cRegularFontName withSize:14.f];
        [alert setButtonsTextFontFamily:cBoldFontName withSize:14.f];

        [alert alertIsDismissed:^{
            [CART_SERVICE clearCartWithSuccessHandler:^{
                [self showMenuScreen];
            }                          failureHandler:^(NSError *error) {
                [self showMenuScreen];
            }];
        }];

        [alert showSuccess:@"Поздравляем"
                  subTitle:[NSString stringWithFormat:@"Ваш заказ %@ создан и оплачен.", createdOrderId]
          closeButtonTitle:@"Закрыть"
                  duration:0.0f];
    } else {
        [destinationVC.view setUserInteractionEnabled:NO];

        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        alert.customViewColor = [MAColorPalette orangeColor];
        alert.iconTintColor = [UIColor whiteColor];

        [alert setTitleFontFamily:cRegularFontName withSize:20.f];
        [alert setBodyTextFontFamily:cRegularFontName withSize:14.f];
        [alert setButtonsTextFontFamily:cBoldFontName withSize:14.f];

        [alert addButton:@"Оплатить" actionBlock:^(void) {
            [self runOnlinePay];
        }];

        [alert addButton:@"Закрыть" actionBlock:^(void) {
            [CART_SERVICE clearCartWithSuccessHandler:^{
                        [self showMenuScreen];
                    }
                                       failureHandler:^(NSError *error) {
                                           [self showMenuScreen];
                                       }];
        }];

        [alert showInfo:@"Внимание"
               subTitle:[NSString stringWithFormat:@"Вы не оплатили заказ. Хотите оплатить?"]
       closeButtonTitle:nil
               duration:0.0f];
    }
}

- (void)checkPay {
    [CART_SERVICE checkPayOrderId:createdOrderId
               withSuccessHandler:^(BOOL result) {
                   if (result || mNeedStopCheck) {
                       [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                                selector:@selector(checkPay)
                                                                  object:nil];

                       mNeedStopCheck = NO;
                       mIsPayed = result;

                       if ([self.navigationController.presentedViewController isKindOfClass:[STKWebKitModalViewController class]]) {
                           [self.navigationController.presentedViewController dismissViewControllerAnimated:YES
                                                                                                 completion:^{
                                                                                                     [self handleAfterOnlinePay];
                                                                                                 }];
                       } else {
                           [self handleAfterOnlinePay];
                       }
                   } else {
                       [self performSelector:@selector(checkPay) withObject:nil afterDelay:2.0];
                   }
               } failureHandler:^(NSError *error) {
                [self performSelector:@selector(checkPay) withObject:nil afterDelay:2.0];
            }];
}

- (void)runOnlinePay {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", cOnlinePayUrlMessage, createdOrderId]];
    CLS_LOG(@"online url %@", url);

    [self checkPay];
    STKWebKitModalViewController *controller = [[STKWebKitModalViewController alloc] initWithURL:url];
    controller.closeDelegate = self;
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];

    self.navigationController.view.userInteractionEnabled = YES;
    if ([service isEqual:[MACartService class]]) {
        createdOrderId = object;

        if (curPayMethod == MAPayMethodOnlineCard) {
            [self runOnlinePay];
        } else {
            SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
            alert.customViewColor = [MAColorPalette lightGreenColor];
            [alert setTitleFontFamily:cRegularFontName withSize:20.f];
            [alert setBodyTextFontFamily:cRegularFontName withSize:14.f];
            [alert setButtonsTextFontFamily:cBoldFontName withSize:14.f];

            [alert alertIsDismissed:^{
                [CART_SERVICE clearCartWithSuccessHandler:^{
                    [self showMenuScreen];
                }                          failureHandler:^(NSError *error) {
                    [self showMenuScreen];
                }];
            }];

            [alert showSuccess:@"Поздравляем"
                      subTitle:[NSString stringWithFormat:@"Ваш заказ %@ создан.", createdOrderId]
              closeButtonTitle:@"Закрыть"
                      duration:0.0f];
        }
    }
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];

    self.navigationController.view.userInteractionEnabled = YES;

    [self showMessage:[self requestErrorDescription]];
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];

    self.navigationController.view.userInteractionEnabled = YES;

    if (error.code == 500)
        [self showMessage:[self requestErrorDescription]];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if (controller == addressFrc) {
        [self handleAddresses];
    }

    if (controller == userFrc) {
        if ([[userFrc fetchedObjects] count]) {
            destinationVC.nameField.text = [userFrc.fetchedObjects[0] name];
            destinationVC.phoneField.text = [userFrc.fetchedObjects[0] phone];
        }
    }
}

- (void)didPressedCloseButton {
    mNeedStopCheck = YES;
}

@end
