//
//  MACheckoutVC.h
//  Maneki
//
//  Created by Maximychev Evgeny on 27.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseVC.h"
#import "MACartService.h"

@interface MACheckoutVC : MABaseVC

@property (nonatomic, assign) MADeliveryMethod deliveryMethod;

@end
