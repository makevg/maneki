//
//  MANewsListVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 09.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import "MAServiceLayer.h"
#import "News.h"
#import "MANewsListVC.h"
#import "MANewsListView.h"
#import "MANewsCell.h"
#import "MANewsVC.h"

NSString *const cNewsListStoryboardName = @"MANewsList";
NSString *const cNewsListTitle = @"Новости";

@interface MANewsListVC () <ServiceResultDelegate>
@property(strong, nonatomic) IBOutlet MANewsListView *contentView;
@end

@implementation MANewsListVC

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTitle:cNewsListTitle];
}

- (void)dealloc {
    [self removeObserver:self];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cNewsListStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (UITableView *)getTableView {
    return self.contentView.tableView;
}

- (void)configureController {
    [self addContentUpdateObserver:self
                          selector:@selector(updateNewsNotification)];
    [self prepareTableView];
    [self cancelsTouches:NO];
    [self requestData];
}

- (void)requestData {
    [super requestData];
    self.fetchResult = [MANEKI_SERVICE getNewsListWithDelegate:self];
}

#pragma mark - Private

- (void)showNewsScreenByNewsId:(NSManagedObjectID *)newsId title:(NSString *)title {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[MANewsVC storyboardName]
                                                 bundle:nil];
    MANewsVC *vc = [sb instantiateInitialViewController];
    vc.newsId = newsId;
    vc.navBarTitle = title;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [MANewsCell cellIdentifier];
    MANewsCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                       forIndexPath:indexPath];
    News *news = [self.fetchResult objectAtIndexPath:indexPath];
    [cell setModel:news];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    News *news = [self.fetchResult objectAtIndexPath:indexPath];
    [self showNewsScreenByNewsId:news.objectID title:news.title];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.contentView.tableView.rowHeight;
}

#pragma mark - Notifications

- (void)updateNewsNotification {
    [self requestData];
    [self.contentView.tableView reloadData];
}

@end
