//
//  MANewsListView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 09.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MANewsListView.h"
#import "MAColorPalette.h"

@interface MANewsListView ()
@end

@implementation MANewsListView

#pragma mark - Setup

- (void)setup {
    self.tableView.backgroundColor = [MAColorPalette lightGrayColor];
    self.tableView.tableFooterView = [UIView new];
}

@end
