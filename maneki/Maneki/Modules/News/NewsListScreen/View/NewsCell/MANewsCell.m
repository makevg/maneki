//
//  MANewsCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 09.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MANewsCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "News.h"
#import "MADataFormatter.h"
#import "MAAPI.h"
#import "UIImageView+AsyncLoad.h"

NSString *const cNewsCellIdentifier = @"NewsCell";

@interface MANewsCell ()

@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet UILabel *publishDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation MANewsCell

+ (NSString *)cellIdentifier {
    return cNewsCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[News class]]) {
        [self fillCellByNews:model];
    }
}

#pragma mark - Configure

- (void)configureCell {
    self.newsImageView.layer.cornerRadius = CGRectGetWidth(self.newsImageView.frame) / 2;
    self.newsImageView.clipsToBounds = YES;
    self.captionLabel.textColor = [MAColorPalette blackColor];
    self.captionLabel.font = [MAFontSet newsCellCaptionLabelFont];
    self.publishDateLabel.textColor = [MAColorPalette lightGrayColor];
    self.publishDateLabel.font = [MAFontSet newsCellPublishLabelFont];
    self.descriptionLabel.textColor = [MAColorPalette newsCellDescriptionLabelColor];
    self.descriptionLabel.font = [MAFontSet newsCellDescriptionLabelFont];
}

- (void)fillCellByNews:(News *)news {
    self.captionLabel.text = news.title;
    self.publishDateLabel.text = [MADataFormatter stringByUnixTimeStamp:news.published_at];
    self.descriptionLabel.text = [MADataFormatter stringByHTML:news.short_descr];
    [self.newsImageView loadAsyncFromUrl:[MANEKI_API urlWithPart:news.preview_image_url] placeHolder:@"PlaceholderMini"];
}

@end
