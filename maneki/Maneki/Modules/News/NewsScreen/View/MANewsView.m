//
//  MANewsView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 09.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MANewsView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "News.h"
#import "MADataFormatter.h"
#import "MAAPI.h"
#import "UIImageView+AsyncLoad.h"

@interface MANewsView () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet UILabel *publishDateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeightConstraint;
@end

@implementation MANewsView

- (void)setModel:(id)model {
    if ([model isKindOfClass:[News class]]) {
        [self configureByNews:model];
    }
}

#pragma mark - Setup

- (void)setup {
    self.imageView.layer.cornerRadius = CGRectGetWidth(self.imageView.frame) / 2;
    self.imageView.clipsToBounds = YES;
    self.captionLabel.textColor = [MAColorPalette blackColor];
    self.captionLabel.font = [MAFontSet newsViewCaptionLabelFont];
    self.publishDateLabel.textColor = [MAColorPalette lightGrayColor];
    self.publishDateLabel.font = [MAFontSet newsViewPublishLabelFont];
    self.webView.hidden = YES;
    self.webView.delegate = self;
    self.webView.scrollView.scrollEnabled = NO;
}

- (void)configureByNews:(News *)news {
    self.captionLabel.text = news.title;
    self.publishDateLabel.text = [MADataFormatter stringByUnixTimeStamp:news.published_at];
    [self.imageView loadAsyncFromUrl:[MANEKI_API urlWithPart:news.preview_image_url] placeHolder:@"PlaceholderMini"];
    NSString *description = [news.long_descr length] > 0 ? news.long_descr : news.short_descr;
    [self.webView loadHTMLString:description baseURL:nil];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webview {
    NSString *contentHeightStr = [webview stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
    CGFloat padding = 20.f;
    CGFloat contentHeight = (CGFloat)[contentHeightStr floatValue] + padding;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, contentHeight + self.webView.frame.origin.y);
    self.webViewHeightConstraint.constant = contentHeight;
    self.webView.hidden = NO;
    if (self.delegate) {
        [self.delegate webViewLoaded:self];
    }
}

@end
