//
//  MANewsView.h
//  Maneki
//
//  Created by Maximychev Evgeny on 09.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@protocol MANewsViewDelegate;

@interface MANewsView : MABaseView

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) id<MANewsViewDelegate> delegate;

@end

@protocol MANewsViewDelegate

- (void)webViewLoaded:(MANewsView *)view;

@end