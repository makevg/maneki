//
//  MANewsVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 09.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MANewsVC.h"
#import "MANewsView.h"
#import "News.h"
#import "MADB.h"

NSString *const cNewsStoryboardName = @"MANews";

@interface MANewsVC () <MANewsViewDelegate>
@property (strong, nonatomic) IBOutlet MANewsView *contentView;
@end

@implementation MANewsVC

#pragma mark - Public

+ (NSString *)storyboardName {
    return cNewsStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (void)configureController {
    [self setTitle:self.navBarTitle];
    News *news = [[MANEKI_DB produceContextForRead] objectRegisteredForID:self.newsId];
    [self.contentView setModel:news];
    self.contentView.delegate = self;
    [self prepareLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                   forView:self.contentView];
    [self showLoadingState];
}

#pragma mark - MANewsViewDelegate

- (void)webViewLoaded:(MANewsView *)view {
    [self hideLoadingState];
}

@end
