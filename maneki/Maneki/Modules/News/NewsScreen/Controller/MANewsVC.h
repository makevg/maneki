//
//  MANewsVC.h
//  Maneki
//
//  Created by Maximychev Evgeny on 09.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <UIKit/UIKit.h>
#import "MABaseVC.h"

@interface MANewsVC : MABaseVC

@property (nonatomic) NSManagedObjectID *newsId;
@property (nonatomic) NSString *navBarTitle;

@end
