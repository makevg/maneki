//
//  MASignUpCodeStepView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 24.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASignUpCodeStepView.h"
#import "MAColorPalette.h"

@interface MASignUpCodeStepView ()

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation MASignUpCodeStepView

- (void)setup {
    [self setBackgroundColor:[MAColorPalette lightGrayColor]];
    [self.infoLabel setTextColor:[MAColorPalette grayColor]];
}

@end
