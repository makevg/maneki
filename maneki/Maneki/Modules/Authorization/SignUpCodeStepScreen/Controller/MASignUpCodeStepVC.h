//
//  MASignUpCodeStepVC.h
//  Maneki
//
//  Created by Maximychev Evgeny on 24.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseVC.h"

@interface MASignUpCodeStepVC : MABaseVC

@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *hash_code;

@end
