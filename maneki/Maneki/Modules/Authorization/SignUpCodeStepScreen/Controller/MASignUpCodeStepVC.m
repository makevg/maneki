//
//  MASignUpCodeStepVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 24.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASignUpCodeStepVC.h"
#import "MABottomButton.h"
#import "MASignUpField.h"
#import "MAWelcomeVC.h"
#import "MAUserService.h"
#import "MACommonService.h"

NSString *const cSignUpCodeStepStoryboardName = @"MASignUpCodeStep";
NSString *const cSignUpCodeStepTitle = @"Подтверждение телефона";

@interface MASignUpCodeStepVC ()

@property(strong, nonatomic) IBOutlet MABottomButton *signUpButton;
@property (weak, nonatomic) IBOutlet MABottomButton *signUpBottomButton;
@property(weak, nonatomic) IBOutlet MASignUpField *codeField;

@end

@implementation MASignUpCodeStepVC {
    MAUserService *userService;
    PCAngularActivityIndicatorView *indicatorView;
}

#pragma mark - Configure

+ (NSString *)storyboardName {
    return cSignUpCodeStepStoryboardName;
}

- (void)configureController {
    userService = [[MAUserService alloc] init];

    self.title = cSignUpCodeStepTitle;
    [self.signUpButton setBackgroundColor:[UIColor orangeColor]];
    self.codeField.type = eCodeType;
    [self.codeField setInputAccessoryView:self.signUpButton];
    [self.codeField becomeFirstResponder];
    self.signUpButton.style = MABottomButtonOrangeStyle;
    self.signUpBottomButton.style = MABottomButtonOrangeStyle;
    indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleSmall
                                               forView:self.signUpButton];
}

#pragma mark - Private

- (void)showWelcomeScreen {
    [self presentModalFeatureByName:[MAWelcomeVC storyboardName] animated:NO];
}

#pragma mark - Actions

- (IBAction)tappedSignUpButton:(id)sender {
    self.signUpButton.enabled = NO;
    [indicatorView startAnimating];
    [userService validatePhoneSecondStepWithPhone:self.phone
                                             code:self.codeField.text
                                             hash:self.hash_code
                                         delegate:self];
}

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];

    [indicatorView stopAnimating];
    self.signUpButton.enabled = YES;
    [MACommonService setLoggedIn:YES];
    [self showWelcomeScreen];
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];
    [self showMessage:error.localizedDescription];
    [indicatorView stopAnimating];
    self.signUpButton.enabled = YES;
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];
    [self showMessage:error.localizedDescription];
    [indicatorView stopAnimating];
    self.signUpButton.enabled = YES;
}

@end
