//
//  MARegistrationView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 22.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MARegistrationView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MABaseButton.h"

@interface MARegistrationView ()

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *socialLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet MABaseButton *vkButton;
@property (weak, nonatomic) IBOutlet MABaseButton *fbButton;
@end

@implementation MARegistrationView

- (void)setup {
    [self setBackgroundColor:[MAColorPalette backgroundColor]];
    self.separatorView.backgroundColor = [MAColorPalette thinLineViewBackgroundColor];
    [self.infoLabel setTextColor:[MAColorPalette whiteColor]];
    [self.infoLabel setFont:[MAFontSet registrationViewLabelFont]];
    [self.socialLabel setTextColor:[MAColorPalette lightGreenColor]];
    [self.vkButton setTitleColor:[MAColorPalette thinLineViewBackgroundColor]
                        forState:UIControlStateHighlighted];
    [self.fbButton setTitleColor:[MAColorPalette thinLineViewBackgroundColor]
                        forState:UIControlStateHighlighted];
}

@end
