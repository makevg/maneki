//
//  MARegistrationTableVC.m
//  Maneki
//
//  Created by Максимычев Е.О. on 14.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MARegistrationTableVC.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MAContactTextField.h"

@interface MARegistrationTableVC () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@end

@implementation MARegistrationTableVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureView];
}

#pragma mark - Private

- (void)configureView {
    self.infoLabel.textColor = [MAColorPalette whiteColor];
    self.infoLabel.font = [MAFontSet registrationViewLabelFont];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger tag = textField.tag;
    switch (tag) {
        case 0:
            [self.passwordField becomeFirstResponder];
            break;
        case 1:
            [self.rePasswordField becomeFirstResponder];
            break;
        case 2: {
            [self.rePasswordField resignFirstResponder];
            if (self.delegate) {
                [self.delegate tappedKeyboardDoneButton:self];
            }
            break;
        }
        default:
            break;
    }
    return NO;
}

@end
