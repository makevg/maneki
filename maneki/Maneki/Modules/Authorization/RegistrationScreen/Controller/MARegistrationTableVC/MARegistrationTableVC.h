//
//  MARegistrationTableVC.h
//  Maneki
//
//  Created by Максимычев Е.О. on 14.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MAContactTextField;
@protocol MARegistrationTableVCDelegate;

@interface MARegistrationTableVC : UITableViewController

@property (weak, nonatomic) IBOutlet MAContactTextField *emailField;
@property (weak, nonatomic) IBOutlet MAContactTextField *passwordField;
@property (weak, nonatomic) IBOutlet MAContactTextField *rePasswordField;
@property (weak, nonatomic) id<MARegistrationTableVCDelegate> delegate;

@end

@protocol MARegistrationTableVCDelegate <NSObject>

- (void)tappedKeyboardDoneButton:(MARegistrationTableVC *)controller;

@end
