//
//  MARegistrationVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 22.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MARegistrationVC.h"
#import "MARegistrationTableVC.h"
#import "MASignUpPhoneStepVC.h"
#import "MAContactTextField.h"
#import "MABottomButton.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "VKSdk.h"
#import "MACommonService.h"
#import "MAUserService.h"
#import "MAWelcomeVC.h"

NSString *const cRegistrationStoryboardName = @"MARegistration";

@interface MARegistrationVC () <MARegistrationTableVCDelegate, VKSdkDelegate, VKSdkUIDelegate>
@property(weak, nonatomic) IBOutlet MABottomButton *signUpButton;
@end

@implementation MARegistrationVC {
    MARegistrationTableVC *destinationVC;
    NSArray *vkScope;
    MAUserService *userService;
    PCAngularActivityIndicatorView *indicatorView;
}

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cRegistrationStoryboardName;
}

- (void)configureController {
    userService = [[MAUserService alloc] init];
    [self prepareVkSdk];
    self.signUpButton.style = MABottomButtonGreenStyle;
    indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleSmall
                                               forView:self.signUpButton];
}

#pragma mark - Private

- (void)prepareVkSdk {
    vkScope = @[VK_PER_EMAIL];
    NSString *vkAppId = [MACommonService getVkAppId];
    [[VKSdk initializeWithAppId:vkAppId] registerDelegate:self];
    [[VKSdk instance] setUiDelegate:self];
    [VKSdk wakeUpSession:vkScope completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationAuthorized) {
            NSLog(@"work started");
        } else if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
}

- (BOOL)dataIsValid {
    return [destinationVC.emailField.text length] > 0 && [destinationVC.passwordField.text length] > 0
            && [destinationVC.rePasswordField.text length] > 0;
}

- (void)signUpAction {
    if ([self dataIsValid]) {
        self.signUpButton.enabled = NO;
        dispatch_safe_main_async(^{
            [indicatorView startAnimating];
        });
        [userService registerUserWithEmail:destinationVC.emailField.text
                                  password:destinationVC.passwordField.text
                                 password2:destinationVC.rePasswordField.text
                                  delegate:self];
    } else {
        [self showMessage:@"Заполните поля"];
    }
}

#pragma mark - Actions

- (IBAction)tappedVkButton:(id)sender {
    [indicatorView startAnimating];
    self.signUpButton.enabled = NO;
    if ([VKSdk accessToken]) {
        [userService loginUserWithSocialType:eSRTVK
                                       token:[VKSdk accessToken].accessToken
                                    delegate:self];
    } else {
        [VKSdk authorize:vkScope];
    }
}

- (IBAction)tappedFbButton:(id)sender {
    self.signUpButton.enabled = NO;
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error || result.isCancelled) {
                                    dispatch_safe_main_sync(^{
                                        [indicatorView stopAnimating];
                                        self.signUpButton.enabled = YES;

                                        if (error)
                                            [self showMessage:error.localizedDescription];
                                    });
                                } else {
                                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                        [indicatorView startAnimating];
                                        [userService loginUserWithSocialType:eSRTFB
                                                                       token:result.token.tokenString
                                                                    delegate:self];
                                    });
                                }
                            }];
}

- (IBAction)tappedSignUpButton:(id)sender {
    [self signUpAction];
}

#pragma mark - MARegistrationTableVCDelegate

- (void)tappedKeyboardDoneButton:(MARegistrationTableVC *)controller {
    [self signUpAction];
}

#pragma mark - VKSdkDelegate

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self.navigationController.topViewController];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
    [VKSdk authorize:vkScope];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    if (result.token) {
        dispatch_safe_main_sync(^{
            self.signUpButton.enabled = NO;
            [indicatorView startAnimating];
        });

        [userService loginUserWithSocialType:eSRTVK
                                       token:result.token.accessToken
                                    delegate:self];
    } else if (result.error) {
        NSLog(@"%@", [NSString stringWithFormat:@"Access denied\n%@", result.error]);
        [self showMessage:result.error.localizedDescription];
    }
}

- (void)vkSdkUserAuthorizationFailed {
    NSLog(@"Access denied");
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self.navigationController.topViewController presentViewController:controller
                                                              animated:YES
                                                            completion:nil];
}

- (void)showWelcomeScreen {
    [MACommonService setLoggedIn:YES];
    [self presentModalFeatureByName:[MAWelcomeVC storyboardName] animated:NO];
}

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];

    [indicatorView stopAnimating];
    self.signUpButton.enabled = YES;
    [self showWelcomeScreen];
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];

    [indicatorView stopAnimating];
    self.signUpButton.enabled = YES;
    [self showMessage:[self requestErrorDescription]];
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];

    [indicatorView stopAnimating];
    self.signUpButton.enabled = YES;
    if (error.code == 403) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:[MASignUpPhoneStepVC storyboardName]
                                                     bundle:nil];
        MASignUpPhoneStepVC *vc = [sb instantiateInitialViewController];
        vc.hash_code = error.userInfo[@"result"];

        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self showMessage:error.localizedDescription];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    destinationVC = segue.destinationViewController;
    destinationVC.delegate = self;
}

@end
