//
//  MASignInView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASignInView.h"
#import "MAColorPalette.h"
#import "MABaseButton.h"

@interface MASignInView ()

@property (weak, nonatomic) IBOutlet UIButton *questionButton;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet MABaseButton *vkButton;
@property (weak, nonatomic) IBOutlet MABaseButton *fbButton;
@end

@implementation MASignInView

#pragma mark - Setup

- (void)setup {
    [self setBackgroundColor:[MAColorPalette backgroundColor]];
    self.separatorView.backgroundColor = [MAColorPalette thinLineViewBackgroundColor];
    [self.infoLabel setTextColor:[MAColorPalette lightGreenColor]];
    [self.questionButton setTitleColor:[MAColorPalette whiteColor]
                              forState:UIControlStateNormal];
    [self.vkButton setTitleColor:[MAColorPalette thinLineViewBackgroundColor]
                        forState:UIControlStateHighlighted];
    [self.fbButton setTitleColor:[MAColorPalette thinLineViewBackgroundColor]
                        forState:UIControlStateHighlighted];
}

@end
