//
//  MASignInVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASignInVC.h"
#import "MAContactTextField.h"
#import "MABottomButton.h"
#import "MAForgotPasswordVC.h"
#import "MASignUpPhoneStepVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "VKSdk.h"
#import "MACommonService.h"
#import "MAUserService.h"
#import "MAWelcomeVC.h"

NSString *const cSignInStoryboardName = @"MASignIn";

@interface MASignInVC () <UITextFieldDelegate, VKSdkDelegate, VKSdkUIDelegate>
@property(weak, nonatomic) IBOutlet MAContactTextField *emailField;
@property(weak, nonatomic) IBOutlet MAContactTextField *passwordField;
@property(weak, nonatomic) IBOutlet MABottomButton *signInButton;
@end

@implementation MASignInVC {
    NSArray *vkScope;
    MAUserService *_userService;
    PCAngularActivityIndicatorView *indicatorView;
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cSignInStoryboardName;
}

- (void)configureController {
    _userService = [[MAUserService alloc] init];
    self.title = @"Вход";
    [self prepareVkSdk];
    self.signInButton.style = MABottomButtonGreenStyle;
    indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleSmall
                                               forView:self.signInButton];
}

#pragma mark - Private

- (void)prepareVkSdk {
    vkScope = @[VK_PER_EMAIL];
    NSString *vkAppId = [MACommonService getVkAppId];
    [[VKSdk initializeWithAppId:vkAppId] registerDelegate:self];
    [[VKSdk instance] setUiDelegate:self];
    [VKSdk wakeUpSession:vkScope completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationAuthorized) {
            NSLog(@"work started");
        } else if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
}

- (BOOL)dataIsValid {
    return [self.emailField.text length] == 0
            || [self.passwordField.text length] == 0;
}

- (void)signInAction {
    if ([self dataIsValid]) {
        [self showMessage:@"Заполните поля"];
    } else {
        self.signInButton.enabled = NO;
        [indicatorView startAnimating];
        [_userService loginUserWithEmail:self.emailField.text
                                password:self.passwordField.text
                                delegate:self];
    }
}

#pragma mark - Actions

- (IBAction)tappedVkButton:(id)sender {
    [indicatorView startAnimating];
    self.signInButton.enabled = NO;
    if ([VKSdk accessToken]) {
        [_userService loginUserWithSocialType:eSRTVK
                                        token:[VKSdk accessToken].accessToken
                                     delegate:self];
    } else {
        [VKSdk authorize:vkScope];
    }
}

- (IBAction)tappedFbButton:(id)sender {
    self.signInButton.enabled = NO;
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error || result.isCancelled) {
                                    dispatch_safe_main_sync(^{
                                        [indicatorView stopAnimating];
                                        self.signInButton.enabled = YES;

                                        if (error)
                                            [self showMessage:error.localizedDescription];
                                    });
                                } else {
                                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                        [indicatorView startAnimating];
                                        [_userService loginUserWithSocialType:eSRTFB
                                                                        token:result.token.tokenString
                                                                     delegate:self];
                                    });
                                }
                            }];
}

- (IBAction)tappedForgotPasswordButton:(id)sender {
    [self pushFeatureByName:[MAForgotPasswordVC storyboardName] animated:YES];
}

- (IBAction)tappedSignInButton:(id)sender {
    [self signInAction];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self signInAction];
    }
    return NO;
}

#pragma mark - VKSdkDelegate

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self.navigationController.topViewController];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
    [VKSdk authorize:vkScope];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    if (result.token) {
        dispatch_safe_main_sync(^{
            self.signInButton.enabled = NO;
            [indicatorView startAnimating];
        });
        [_userService loginUserWithSocialType:eSRTVK
                                        token:result.token.accessToken
                                     delegate:self];
    } else if (result.error) {
        NSLog(@"%@", [NSString stringWithFormat:@"Access denied\n%@", result.error]);
        [self showMessage:result.error.localizedDescription];
    }
}

- (void)vkSdkUserAuthorizationFailed {
    NSLog(@"Access denied");
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self.navigationController.topViewController presentViewController:controller
                                                              animated:YES
                                                            completion:nil];
}

#pragma mark - Private

- (void)showWelcomeScreen {
    [MACommonService setLoggedIn:YES];
    [self presentModalFeatureByName:[MAWelcomeVC storyboardName] animated:NO];
}

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];
    [indicatorView stopAnimating];
    self.signInButton.enabled = YES;
    [self showWelcomeScreen];
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];

    [self showMessage:[self requestErrorDescription]];
    [indicatorView stopAnimating];
    self.signInButton.enabled = YES;
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];

    [indicatorView stopAnimating];
    self.signInButton.enabled = YES;
    if (error.code == 403) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:[MASignUpPhoneStepVC storyboardName]
                                                     bundle:nil];
        MASignUpPhoneStepVC *vc = [sb instantiateInitialViewController];
        vc.hash_code = error.userInfo[@"result"];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self showMessage:error.localizedDescription];
    }
}

@end
