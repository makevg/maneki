//
//  MAAuthorizationView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 19.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAAuthorizationView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MASaleView.h"

@interface MAAuthorizationView ()

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet MASaleView *saleView;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@end

@implementation MAAuthorizationView

#pragma mark - Setup

- (void)setup {
    [self setBackgroundColor:[MAColorPalette backgroundColor]];
    self.separatorView.backgroundColor = [MAColorPalette thinLineViewBackgroundColor];
    [self.infoLabel setTextColor:[MAColorPalette whiteColor]];
    [self.infoLabel setFont:[MAFontSet authLabelFont]];
    [self.saleView prepareWithSale:15];
}

@end
