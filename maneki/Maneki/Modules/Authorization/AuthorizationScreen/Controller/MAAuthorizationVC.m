//
//  MAAuthorizationVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 19.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "MAAuthorizationVC.h"
#import "MASignInVC.h"
#import "MARegistrationVC.h"
#import "MAWelcomeVC.h"
#import "MACommonService.h"

NSString *const cAuthorizationVCStoryboardName = @"MAAuthorization";
NSString *const cTitle = @"Авторизация";

@interface MAAuthorizationVC ()
@end

@implementation MAAuthorizationVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:animated];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cAuthorizationVCStoryboardName;
}

- (void)configureController {
    [MACommonService setLoggedIn:NO];
    self.title = cTitle;
}

#pragma mark - Actions
- (IBAction)tappedSignInButton:(id)sender {
    [self pushFeatureByName:[MASignInVC storyboardName] animated:YES];
}

- (IBAction)tappedSignUpButton:(id)sender {
    [self pushFeatureByName:[MARegistrationVC storyboardName] animated:YES];
}

- (IBAction)tappedContinueButton:(id)sender {
    [self setRootFeatureByName:[MAWelcomeVC storyboardName]];
}

@end
