//
//  MASignUpPhoneStepView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 23.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASignUpPhoneStepView.h"
#import "MAColorPalette.h"

@interface MASignUpPhoneStepView ()

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation MASignUpPhoneStepView

#pragma mark - Setup

- (void)setup {
    [self setBackgroundColor:[MAColorPalette lightGrayColor]];
    [self.infoLabel setTextColor:[MAColorPalette grayColor]];
}

@end
