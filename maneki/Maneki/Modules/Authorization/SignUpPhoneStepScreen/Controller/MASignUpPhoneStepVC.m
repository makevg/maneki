//
//  MASignUpPhoneStepVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 23.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASignUpPhoneStepVC.h"
#import "MABottomButton.h"
#import "MASignUpField.h"
#import "MASignUpCodeStepVC.h"
#import "MAUserService.h"

NSString *const cSignUpPhoneStepStoryboardName = @"MASignUpPhoneStep";
NSString *const cSignUpPhoneStepTitle = @"Подтверждение телефона";

@interface MASignUpPhoneStepVC ()
@property(weak, nonatomic) IBOutlet MASignUpField *phoneField;
@property(strong, nonatomic) IBOutlet MABottomButton *getPasswordButton;
@property (weak, nonatomic) IBOutlet MABottomButton *getPasswordBottomButton;
@end

@implementation MASignUpPhoneStepVC {
    MAUserService *userService;
    PCAngularActivityIndicatorView *indicatorView;
}

#pragma mark - Configure

+ (NSString *)storyboardName {
    return cSignUpPhoneStepStoryboardName;
}

- (void)configureController {
    userService = [[MAUserService alloc] init];

    self.title = cSignUpPhoneStepTitle;
    [self.getPasswordButton setBackgroundColor:[UIColor orangeColor]];
    self.phoneField.type = ePhoneType;
    [self.phoneField setInputAccessoryView:self.getPasswordButton];
    self.getPasswordButton.style = MABottomButtonOrangeStyle;
    self.getPasswordBottomButton.style = MABottomButtonOrangeStyle;
    indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleSmall
                                               forView:self.getPasswordButton];
}

#pragma mark - Actions

- (IBAction)tappedGetPasswordButton:(id)sender {
    self.phoneField.enabled = NO;
    [indicatorView startAnimating];
    [userService validatePhoneFirstStepWithPhone:self.phoneField.text
                                            hash:self.hash_code
                                        delegate:self];
}

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];

    [indicatorView stopAnimating];
    self.getPasswordButton.enabled = YES;
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[MASignUpCodeStepVC storyboardName] bundle:nil];
    MASignUpCodeStepVC *vc = [sb instantiateInitialViewController];
    vc.phone = self.phoneField.text;
    vc.hash_code = self.hash_code;

    [self.navigationController pushViewController:vc animated:YES];
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];

    [self showMessage:error.localizedDescription];
    [indicatorView stopAnimating];
    self.getPasswordButton.enabled = YES;
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];

    [self showMessage:error.localizedDescription];
    [indicatorView stopAnimating];
    self.getPasswordButton.enabled = YES;
}

@end
