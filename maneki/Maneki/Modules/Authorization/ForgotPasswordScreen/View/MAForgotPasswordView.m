//
//  MAForgotPasswordView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAForgotPasswordView.h"
#import "MAColorPalette.h"

@implementation MAForgotPasswordView

#pragma mark - Setup

- (void)setup {
    [self setBackgroundColor:[MAColorPalette backgroundColor]];
}

@end
