//
//  MAForgotPasswordTableVC.m
//  Maneki
//
//  Created by Максимычев Е.О. on 14.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAForgotPasswordTableVC.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@interface MAForgotPasswordTableVC () <UITextFieldDelegate>

@end

@implementation MAForgotPasswordTableVC

#pragma mark - LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureView];
}

#pragma mark - Private

- (void)configureView {
    self.tableView.tableFooterView = [UIView new];
    self.infoLabel.textColor = [MAColorPalette whiteColor];
    self.infoLabel.font = [MAFontSet forgotPasswordLabel];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (self.delegate) {
        [self.delegate tappedKeyBoardDoneButton:self];
    }
    return NO;
}

@end
