//
//  MAForgotPasswordTableVC.h
//  Maneki
//
//  Created by Максимычев Е.О. on 14.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MAContactTextField;
@protocol MAForgotPasswordTableVCDelegate;

@interface MAForgotPasswordTableVC : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet MAContactTextField *emailField;
@property (weak, nonatomic) id<MAForgotPasswordTableVCDelegate> delegate;

@end

@protocol MAForgotPasswordTableVCDelegate <NSObject>

- (void)tappedKeyBoardDoneButton:(MAForgotPasswordTableVC *)controller;

@end
