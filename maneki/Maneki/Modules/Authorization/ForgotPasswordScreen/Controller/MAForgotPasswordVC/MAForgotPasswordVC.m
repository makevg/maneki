//
//  MAForgotPasswordVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 21.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAForgotPasswordVC.h"
#import "MAForgotPasswordTableVC.h"
#import "MAContactTextField.h"
#import "MABottomButton.h"
#import "MADataValidator.h"
#import "MAUserService.h"
#import "SCLAlertView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cForgotPasswordStoryboardName = @"MAForgotPassword";

@interface MAForgotPasswordVC () <MAForgotPasswordTableVCDelegate>
@property (weak, nonatomic) IBOutlet MABottomButton *sendButton;
@end

@implementation MAForgotPasswordVC {
    MAForgotPasswordTableVC *destinationVC;
    PCAngularActivityIndicatorView *indicatorView;
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cForgotPasswordStoryboardName;
}

- (void)configureController {
    self.sendButton.style = MABottomButtonGreenStyle;
    indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleSmall
                                               forView:self.sendButton];
}

#pragma mark - Private

- (void)sendAction {
    if ([MADataValidator validateEmail:destinationVC.emailField.text]) {
        NSLog(@"data is valid");
        self.sendButton.enabled = NO;
        [indicatorView startAnimating];
        MAUserService *userService = [MAUserService new];
        [userService getForgotPasswordWithEmail:destinationVC.emailField.text
                                       delegate:self];
    } else {
        [self showMessage:@"Неверный адрес электронной почты"];
    }
}

- (void)showSuccessAlert {
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.customViewColor = [MAColorPalette orangeColor];
    alert.iconTintColor = [UIColor whiteColor];
    
    [alert setTitleFontFamily:cRegularFontName withSize:20.f];
    [alert setBodyTextFontFamily:cRegularFontName withSize:14.f];
    [alert setButtonsTextFontFamily:cBoldFontName withSize:14.f];
    
    [alert addButton:@"Ок" actionBlock:^(void) {
        [self.navigationController popToViewController:self.navigationController.viewControllers[1]
                                              animated:YES];
    }];
    
    [alert showInfo:@"Внимание"
           subTitle:[NSString stringWithFormat:@"Инструкции по восстановлению высланы на ваш email"]
   closeButtonTitle:nil
           duration:0.0f];
}

#pragma mark - Actions

- (IBAction)tappedSendButton:(id)sender {
    [self sendAction];
}

#pragma mark - MAForgotPasswordTableVCDelegate

- (void)tappedKeyBoardDoneButton:(MAForgotPasswordTableVC *)controller {
    [self sendAction];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    destinationVC = segue.destinationViewController;
    destinationVC.delegate = self;
}

#pragma mark - ServiceResultDelegate

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];
    [indicatorView stopAnimating];
    self.sendButton.enabled = YES;
    [self showSuccessAlert];
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];
    
    [self showMessage:[self requestErrorDescription]];
    [indicatorView stopAnimating];
    self.sendButton.enabled = YES;
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];

    [self showMessage:[self requestErrorDescription]];
    [indicatorView stopAnimating];
    self.sendButton.enabled = YES;
}

@end
