//
//  MABackMenuSaleHeader.m
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABackMenuSaleHeader.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@interface MABackMenuSaleHeader ()

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIView *thinLineView;

@end

@implementation MABackMenuSaleHeader

#pragma mark - init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.mainView];
        [self configureView];
    }
    return self;
}

#pragma mark - Private

- (void)configureView {
    [self prepareConstraints];
    [self.mainView setBackgroundColor:[MAColorPalette backgroundColor]];
    [self.thinLineView setBackgroundColor:[MAColorPalette thinLineViewBackgroundColor]];
    [self.infoLabel setTextColor:[MAColorPalette whiteColor]];
    [self.infoLabel setFont:[MAFontSet saleLabelFont]];
}

- (void)prepareConstraints {
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
}

#pragma mark - Actions

- (IBAction)tappedView:(id)sender {
    if (self.delegate) {
        [self.delegate tappedBackMenuSaleHeader:self];
    }
}


@end
