//
//  MABackMenuSaleHeader.h
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@protocol MABackMenuSaleHeaderDelegate;

@interface MABackMenuSaleHeader : MABaseView

@property (weak, nonatomic) id<MABackMenuSaleHeaderDelegate> delegate;

@end

@protocol MABackMenuSaleHeaderDelegate <NSObject>

- (void)tappedBackMenuSaleHeader:(MABackMenuSaleHeader *)header;

@end
