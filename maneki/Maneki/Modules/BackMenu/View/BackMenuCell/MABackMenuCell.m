//
//  MABackMenuCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABackMenuCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cBackMenuCellIdentifier = @"MABackMenuCell";

@interface MABackMenuCell ()

@property (weak, nonatomic) IBOutlet UILabel *itemLabel;

@end

@implementation MABackMenuCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cBackMenuCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSString class]]) {
        self.itemLabel.text = model;
        [self configureCell];
    }
}

- (void)configureCell {
    [self setBackgroundColor:[MAColorPalette backgroundColor]];
    [self.itemLabel setTextColor:[MAColorPalette whiteColor]];
    [self.itemLabel setFont:[MAFontSet backMenuCellFont]];
    self.selectedBackgroundView = [self prepareSelectedBackgroundView];
}

#pragma mark - Private

- (UIView *)prepareSelectedBackgroundView {
    UIView *selectedBackgroundView = [[UIView alloc] init];
    [selectedBackgroundView setBackgroundColor:[MAColorPalette backMenuCellSelectedStateColor]];
    return selectedBackgroundView;
}

@end
