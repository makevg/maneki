//
//  MABackMenuHeader.h
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@protocol MABackMenuHeaderDelegate;

@interface MABackMenuHeader : MABaseView

@property (weak, nonatomic) id<MABackMenuHeaderDelegate> delegate;

@end

@protocol MABackMenuHeaderDelegate <NSObject>

- (void)backMenuHeader:(MABackMenuHeader *)header tappedFeedbackButton:(UIButton *)button;
- (void)backMenuHeader:(MABackMenuHeader *)header tappedPhoneButton:(UIButton *)button;
- (void)tappedLogoImageView:(MABackMenuHeader *)header;

@end
