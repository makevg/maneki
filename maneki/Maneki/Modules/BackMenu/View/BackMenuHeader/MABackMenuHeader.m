//
//  MABackMenuHeader.m
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABackMenuHeader.h"
#import "MAColorPalette.h"

@interface MABackMenuHeader ()

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *feedbackButton;
@property (weak, nonatomic) IBOutlet UIButton *phoneButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIView *thinLineView;

@end

@implementation MABackMenuHeader

#pragma mark - init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.mainView];
        [self configureView];
    }
    return self;
}

#pragma mark - Private 

- (void)configureView {
    [self prepareConstraints];
    [self configureSubviews];
}

- (void)prepareConstraints {
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
}

- (void)configureSubviews {
    [self.mainView setBackgroundColor:[MAColorPalette backgroundColor]];
    [self.thinLineView setBackgroundColor:[MAColorPalette thinLineViewBackgroundColor]];
}

#pragma mark - Actions

- (IBAction)tappedFeedbackButton:(id)sender {
    if (self.delegate) {
        [self.delegate backMenuHeader:self tappedFeedbackButton:sender];
    }
}

- (IBAction)tappedPhoneButton:(id)sender {
    if (self.delegate) {
        [self.delegate backMenuHeader:self tappedPhoneButton:sender];
    }
}

- (IBAction)tappedLogoImageView:(id)sender {
    if (self.delegate) {
        [self.delegate tappedLogoImageView:self];
    }
}

@end
