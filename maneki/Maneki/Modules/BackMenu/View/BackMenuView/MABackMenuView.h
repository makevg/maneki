//
//  MABackMenuView.h
//  Maneki
//
//  Created by Максимычев Е.О. on 03.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@class MABackMenuHeader;
@class MABackMenuSaleHeader;
@class City;

@interface MABackMenuView : MABaseView

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet MABackMenuHeader *profileHeaderView;
@property (weak, nonatomic) IBOutlet MABackMenuSaleHeader *saleHeaderView;
@property (weak, nonatomic) IBOutlet UIButton *cityButton;

- (void)setCity:(City *)city;

@end
