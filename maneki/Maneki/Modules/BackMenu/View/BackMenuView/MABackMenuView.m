//
//  MABackMenuView.m
//  Maneki
//
//  Created by Максимычев Е.О. on 03.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABackMenuView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MABackMenuHeader.h"
#import "MABackMenuSaleHeader.h"
#import "City.h"

@interface MABackMenuView ()

@end

@implementation MABackMenuView

#pragma mark - Setup

- (void)setup {
    [self bringSubviewToFront:self.saleHeaderView];
    [self setBackgroundColor:[MAColorPalette backgroundColor]];
    [self.tableView setBackgroundColor:[MAColorPalette backgroundColor]];
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.cityButton.titleLabel setFont:[MAFontSet backMenuCityButtonFont]];
    [self.cityButton setTitleColor:[MAColorPalette whiteColor] forState:UIControlStateNormal];
}

- (void)setCity:(City *)city {
    NSString *title = [NSString stringWithFormat:@"Ваш город: %@", city.name];
    [self.cityButton setTitle:title forState:UIControlStateNormal];
}

@end
