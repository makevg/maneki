//
//  MABackMenuRootVC.h
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RESideMenu/RESideMenu.h>

@interface MABackMenuRootVC : RESideMenu

+ (NSString *)storyboardName;

@end
