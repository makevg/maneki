//
//  MABackMenuRootVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABackMenuRootVC.h"
#import "MABackMenuVC.h"
#import "MAMenuVC.h"

NSString *const cLeftMenuRootStoryboardName = @"MABackMenuRoot";

@interface MABackMenuRootVC () <RESideMenuDelegate>

@end

@implementation MABackMenuRootVC

#pragma mark - Lifecycle

- (void)awakeFromNib {
    [self configureController];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cLeftMenuRootStoryboardName;
}

#pragma mark - Private

- (void)configureController {
    [self configureContentView];
    self.contentViewController = [self loadFeatureByName:[MAMenuVC storyboardName]];
    self.leftMenuViewController = [self loadFeatureByName:[MABackMenuVC storyboardName]];
    self.fadeMenuView = NO;
    self.panFromEdge = NO;
    self.bouncesHorizontally = NO;
    self.parallaxEnabled = NO;
    self.delegate = self;
}

- (void)configureContentView {
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.contentViewScaleValue = 0.9f;
    self.contentViewInPortraitOffsetCenterX = 80;
    self.contentViewShadowColor = [UIColor blackColor];
    self.contentViewShadowOffset = CGSizeMake(0, 0);
    self.contentViewShadowOpacity = 0.6;
    self.contentViewShadowRadius = 12;
    self.contentViewShadowEnabled = YES;
}

- (UIViewController *)loadFeatureByName:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    return [sb instantiateInitialViewController];
}

#pragma mark - RESideMenuDelegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController {
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController {
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController {
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController {
}

@end
