//
//  MABackMenuVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 26.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABackMenuVC.h"
#import "MABackMenuView.h"
#import "MABackMenuHeader.h"
#import "MABackMenuSaleHeader.h"
#import "MABackMenuCell.h"
#import "MAMenuVC.h"
#import "MANewsListVC.h"
#import "MASalesListVC.h"
#import "MAAboutDeliveryVC.h"
#import "MAAuthorizationVC.h"
#import "MAOrdersInfoVC.h"
#import "MAOrdersListVC.h"
#import "MAProfileVC.h"
#import "MAReviewsListVC.h"
#import "MACommonService.h"
#import "Restaurant+CoreDataProperties.h"
#import "MAAPI.h"
#import "MACityService.h"
#import "MACityChooseVC.h"

NSString *const cLeftMenuStoryboardName = @"MABackMenu";

@interface MABackMenuVC () <UITableViewDataSource, UITableViewDelegate,
        MABackMenuHeaderDelegate, MABackMenuSaleHeaderDelegate>
@property(strong, nonatomic) IBOutlet MABackMenuView *contentView;
@end

@implementation MABackMenuVC {
    NSArray *menuItems;
}

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.contentView setCity:[MACityService getCurrentCity]];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
};

#pragma mark - Public

+ (NSString *)storyboardName {
    return cLeftMenuStoryboardName;
}

- (void)configureController {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(unauthorizedHandler:)
                                                 name:kUnauthorizedNotification
                                               object:nil];

    menuItems = [self prepareMenuItems];
    [self setDelegatesAndDataSource];
    [self prepareHeader];
    [self cancelsTouches:NO];
}

- (void)unauthorizedHandler:(NSNotification *)notification {
    [self.contentView bringSubviewToFront:self.contentView.saleHeaderView];
}

- (void)setDelegatesAndDataSource {
    self.contentView.tableView.dataSource = self;
    self.contentView.tableView.delegate = self;
    self.contentView.profileHeaderView.delegate = self;
    self.contentView.saleHeaderView.delegate = self;
}

#pragma mark - Private

- (void)prepareHeader {
    [self.contentView bringSubviewToFront:[MACommonService isLoggedIn] ? self.contentView.profileHeaderView : self.contentView.saleHeaderView];
}

- (NSArray *)prepareMenuItems {
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"BackMenu"
                                                          ofType:@"plist"];
    return [NSArray arrayWithContentsOfFile:plistPath];
}

- (void)showMenuScreen {
    [self showFeatureByName:[MAMenuVC storyboardName]];
}

- (void)showSalesScreen {
    [self showFeatureByName:[MASalesListVC storyboardName]];
}

- (void)showOrdersHistoryScreen {
    if ([MACommonService isLoggedIn]) {
        [self showFeatureByName:[MAOrdersListVC storyboardName]];
    } else {
        [self showFeatureByName:[MAOrdersInfoVC storyboardName]];
    }
}

- (void)showAboutDeliveryScreen {
    [self showFeatureByName:[MAAboutDeliveryVC storyboardName]];
}

- (void)showNewsScreen {
    [self showFeatureByName:[MANewsListVC storyboardName]];
}

- (void)showCityChooseScreen {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[MACityChooseVC storyboardName] bundle:nil];
    MACityChooseVC *vc = [sb instantiateViewControllerWithIdentifier:[MACityChooseVC identifier]];
    vc.showPrevScreen = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Actions

- (IBAction)tappedCityButton:(id)sender {
    [self hideMenuViewController];
    [self showCityChooseScreen];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [MABackMenuCell cellIdentifier];
    MABackMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                           forIndexPath:indexPath];
    [cell setModel:menuItems[(NSUInteger) indexPath.row]];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            [self showMenuScreen];
            break;
        case 1:
            [self showSalesScreen];
            break;
        case 2:
            [self showOrdersHistoryScreen];
            break;
        case 3:
            [self showAboutDeliveryScreen];
            break;
        case 4:
            [self showNewsScreen];
            break;
        default:
            break;
    }
}

#pragma mark - MABackMenuHeaderDelegate

- (void)backMenuHeader:(MABackMenuHeader *)header tappedFeedbackButton:(UIButton *)button {
    [self pushFeatureByName:[MAReviewsListVC storyboardName] animated:YES];
}

- (void)backMenuHeader:(MABackMenuHeader *)header tappedPhoneButton:(UIButton *)button {
    Restaurant *restaurant = [MANEKI_SERVICE getOnlyRestaurantInfo];
    [MACommonService callToPhone:restaurant.phone];
}

- (void)tappedLogoImageView:(MABackMenuHeader *)header {
    [self pushFeatureByName:[MAProfileVC storyboardName] animated:YES];
}

#pragma mark - MABackMenuSaleHeaderDelegate

- (void)tappedBackMenuSaleHeader:(MABackMenuSaleHeader *)header {
    [self pushFeatureByName:[MAAuthorizationVC storyboardName] animated:YES];
}

@end
