//
//  MAWelcomeView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAWelcomeView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "PCAngularActivityIndicatorView.h"
#import "MABottomButton.h"

@interface MAWelcomeView ()

@property(weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property(weak, nonatomic) IBOutlet UIView *loadingContentView;
@property(strong, nonatomic) PCAngularActivityIndicatorView *indicatorView;
@property(weak, nonatomic) IBOutlet MABottomButton *retryButton;

@end

@implementation MAWelcomeView

#pragma mark - Setup

- (void)setup {
    [self setBackgroundColor:[MAColorPalette backgroundColor]];
    [self.welcomeLabel setTextColor:[MAColorPalette whiteColor]];
    [self.welcomeLabel setFont:[MAFontSet welcomeLabel]];
    self.retryButton.backgroundColor = [MAColorPalette orangeColor];
    self.retryButton.hidden = YES;

    self.indicatorView = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleDefault];
    self.indicatorView.color = [MAColorPalette orangeColor];
    self.indicatorView.translatesAutoresizingMaskIntoConstraints = NO;

    [_loadingContentView addSubview:self.indicatorView];
    [_loadingContentView addConstraints:@[
            [NSLayoutConstraint constraintWithItem:self.indicatorView
                                         attribute:NSLayoutAttributeCenterX
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:_loadingContentView
                                         attribute:NSLayoutAttributeCenterX
                                        multiplier:1
                                          constant:0],
            [NSLayoutConstraint constraintWithItem:self.indicatorView
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:_loadingContentView
                                         attribute:NSLayoutAttributeCenterY
                                        multiplier:1
                                          constant:0]
    ]];
}

- (void)showLoadingState {
    self.retryButton.hidden = YES;
    [self.indicatorView startAnimating];
}

- (void)hideLoadingState {
    [self.indicatorView stopAnimating];
}

- (void)showErrorState {
    self.retryButton.hidden = NO;
}


@end
