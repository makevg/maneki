//
//  MAWelcomeView.h
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MAWelcomeView : MABaseView

- (void)showLoadingState;
- (void)hideLoadingState;
- (void)showErrorState;

@end
