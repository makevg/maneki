//
//  MAWelcomeVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <RESideMenu/RESideMenu.h>
#import "MAWelcomeVC.h"
#import "MABackMenuRootVC.h"
#import "MAUserService.h"
#import "MACartService.h"
#import "MAWelcomeView.h"
#import "MARestaurantService.h"
#import "MACityChooseVC.h"

NSString *const cWelcomeVCStoryboardName = @"MAWelcome";

@interface MAWelcomeVC ()

@end

@implementation MAWelcomeVC {
    MAUserService *_userService;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if( self = [super initWithCoder:aDecoder]) {
        self.checkUser = NO;
    }

    return self;
}

+ (NSString *)storyboardName {
    return cWelcomeVCStoryboardName;
}

- (void)configureController {
    _userService = [[MAUserService alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self updateInfo];
}

#pragma mark - Private

- (IBAction)updateInfo {
    [(MAWelcomeView *) self.view showLoadingState];
    [MANEKI_SERVICE getRestaurantInfoWithDelegate:self];
}

- (void)showMenu {
    [(MAWelcomeView *) self.view hideLoadingState];
    [self setRootFeatureByName:[MABackMenuRootVC storyboardName]];
}

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];

    if ([service isEqual:[MARestaurantService class]]) {
        [CART_SERVICE updateCartInfoWithSuccessHandler:^{
            [_userService getUserInfoWithDelegate:self];
        }
                                        failureHandler:^(NSError *error) {
                                            [self serverError:error forService:[CART_SERVICE class]];
                                        }];
    }

    if ([service isEqual:[MAUserService class]]) {
        [self showMenu];
    }
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];

    [(MAWelcomeView *) self.view hideLoadingState];
    [(MAWelcomeView *) self.view showErrorState];
    [self showMessage:[self requestErrorDescription]];
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];

    if (error.code == 401) {
        if ([service isEqual:[MAUserService class]] && !self.checkUser) {
            [self showMenu];
        } else {
            [self setRootFeatureByName:[MACityChooseVC storyboardName]];
        }
        return;
    }

    [(MAWelcomeView *) self.view hideLoadingState];
    [(MAWelcomeView *) self.view showErrorState];
    [self showMessage:[self requestErrorDescription]];
}

- (void)networkStateDidChanged:(NSNotification *)notification {
    if ([notification.object boolValue]) {
        [self updateInfo];
    }
}

@end