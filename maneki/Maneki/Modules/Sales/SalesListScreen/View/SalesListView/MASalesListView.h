//
//  MASalesListView.h
//  Maneki
//
//  Created by Maximychev Evgeny on 13.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MASalesListView : MABaseView

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

