//
//  MASalesListView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 13.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASalesListView.h"
#import "MAColorPalette.h"

@implementation MASalesListView

#pragma mark - Setup

- (void)setup {
    self.tableView.backgroundColor = [MAColorPalette lightGrayColor];
    self.tableView.tableFooterView = [UIView new];
}

@end
