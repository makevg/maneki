//
//  MASaleCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 13.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASaleCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MADataFormatter.h"
#import "Sale.h"
#import "MAAPI.h"
#import "UIImageView+AsyncLoad.h"

NSString *const cSaleCellIdentifier = @"SaleCell";

@interface MASaleCell ()
@property (weak, nonatomic) IBOutlet UIImageView *saleImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@end

@implementation MASaleCell

#pragma mark - Configure

+ (NSString *)cellIdentifier {
    return cSaleCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[Sale class]]) {
        [self fillCellBySale:model];
    }
}

- (void)configureCell {
    self.saleImageView.layer.cornerRadius = CGRectGetWidth(self.saleImageView.frame) / 2;
    self.saleImageView.clipsToBounds = YES;
    self.titleLabel.textColor = [MAColorPalette blackColor];
    self.titleLabel.font = [MAFontSet newsCellCaptionLabelFont];
    self.descriptionLabel.textColor = [MAColorPalette newsCellDescriptionLabelColor];
    self.descriptionLabel.font = [MAFontSet newsCellDescriptionLabelFont];
}

- (void)fillCellBySale:(Sale *)sale {
    self.titleLabel.text = sale.title;
    self.descriptionLabel.text = [MADataFormatter stringByHTML:sale.long_descr];
    [self.saleImageView loadAsyncFromUrl:[MANEKI_API urlWithPart:sale.preview_image_url] placeHolder:@"PlaceholderMini"];
}

@end
