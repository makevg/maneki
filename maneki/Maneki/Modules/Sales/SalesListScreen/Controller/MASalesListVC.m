//
//  MASalesListVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 13.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import "MAServiceLayer.h"
#import "Sale.h"
#import "MASalesListVC.h"
#import "MASalesListView.h"
#import "MASaleCell.h"
#import "MASaleVC.h"

NSString *const cSalesListStoryboardName = @"MASalesList";
NSString *const cSalesListTitle          = @"Акции";

@interface MASalesListVC () <ServiceResultDelegate>
@property (strong, nonatomic) IBOutlet MASalesListView *contentView;
@end

@implementation MASalesListVC

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTitle:cSalesListTitle];
}

- (void)dealloc {
    [self removeObserver:self];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cSalesListStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (UITableView *)getTableView {
    return self.contentView.tableView;
}

- (void)configureController {
    [self addContentUpdateObserver:self
                          selector:@selector(updateSalesNotification)];
    [self prepareTableView];
    [self cancelsTouches:NO];
    [self requestData];
}

- (void)requestData {
    [super requestData];
    self.fetchResult = [MANEKI_SERVICE getSalesListWithDelegate:self];
}

#pragma mark - Private

- (void)showSaleScreenBySaleId:(NSManagedObjectID *)saleId title:(NSString *)title {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[MASaleVC storyboardName]
                                                 bundle:nil];
    MASaleVC *vc = [sb instantiateInitialViewController];
    vc.saleId = saleId;
    vc.navBarTitle = title;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [MASaleCell cellIdentifier];
    MASaleCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                       forIndexPath:indexPath];
    [cell setModel:[self.fetchResult objectAtIndexPath:indexPath]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Sale *sale = [self.fetchResult objectAtIndexPath:indexPath];
    [self showSaleScreenBySaleId:sale.objectID title:sale.title];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.contentView.tableView.rowHeight;
}

#pragma mark - Notifications

- (void)updateSalesNotification {
    [self requestData];
    [self.contentView.tableView reloadData];
}

@end
