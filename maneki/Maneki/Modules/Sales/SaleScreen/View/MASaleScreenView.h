//
//  MASaleScreenView.h
//  Maneki
//
//  Created by Maximychev Evgeny on 13.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@protocol MASaleScreenViewDelegate;

@interface MASaleScreenView : MABaseView

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) id<MASaleScreenViewDelegate> delegate;

@end

@protocol MASaleScreenViewDelegate

- (void)webViewLoaded:(MASaleScreenView *)view;

@end
