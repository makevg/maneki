//
//  MASaleScreenView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 13.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASaleScreenView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MADataFormatter.h"
#import "Sale.h"
#import "MAAPI.h"
#import "UIImageView+AsyncLoad.h"

@interface MASaleScreenView () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *saleImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeightConstraint;
@end

@implementation MASaleScreenView

- (void)setModel:(id)model {
    if ([model isKindOfClass:[Sale class]]) {
        [self configureBySale:model];
    }
}

#pragma mark - Setup

- (void)setup {
    self.saleImageView.layer.cornerRadius = CGRectGetWidth(self.saleImageView.frame) / 2;
    self.saleImageView.clipsToBounds = YES;
    self.titleLabel.textColor = [MAColorPalette blackColor];
    self.titleLabel.font = [MAFontSet newsCellCaptionLabelFont];
    self.webView.hidden = YES;
    self.webView.delegate = self;
    self.webView.scrollView.scrollEnabled = NO;
}

- (void)configureBySale:(Sale *)sale {
    [self.saleImageView loadAsyncFromUrl:[MANEKI_API urlWithPart:sale.image_url] placeHolder:@"PlaceholderMini"];
    self.titleLabel.text = sale.title;
    [self.webView loadHTMLString:sale.long_descr baseURL:nil];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webview {
    NSString *contentHeightStr = [webview stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
    CGFloat padding = 20.f;
    CGFloat contentHeight = (CGFloat)[contentHeightStr floatValue] + padding;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, contentHeight + self.webView.frame.origin.y);
    self.webViewHeightConstraint.constant = contentHeight;
    self.webView.hidden = NO;
    if (self.delegate) {
        [self.delegate webViewLoaded:self];
    }
}

@end
