//
//  MASaleVC.h
//  Maneki
//
//  Created by Maximychev Evgeny on 13.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import "MABaseVC.h"

@interface MASaleVC : MABaseVC

@property (nonatomic) NSManagedObjectID *saleId;
@property (nonatomic) NSString *navBarTitle;

@end
