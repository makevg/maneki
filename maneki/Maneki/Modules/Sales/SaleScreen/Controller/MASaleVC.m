//
//  MASaleVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 13.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MASaleVC.h"
#import "MASaleScreenView.h"
#import "MADB.h"
#import "Sale.h"

NSString *const cSaleStoryboardName = @"MASale";

@interface MASaleVC () <MASaleScreenViewDelegate>

@property (strong, nonatomic) IBOutlet MASaleScreenView *contentView;

@end

@implementation MASaleVC

#pragma mark - Public

+ (NSString *)storyboardName {
    return cSaleStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (void)configureController {
    [self setTitle:self.navBarTitle];
    Sale *sale = [[MANEKI_DB produceContextForRead] objectRegisteredForID:self.saleId];
    [self.contentView setModel:sale];
    self.contentView.delegate = self;
    [self prepareLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                   forView:self.contentView];
    [self showLoadingState];
}

#pragma mark - MASaleScreenViewDelegate

- (void)webViewLoaded:(MASaleScreenView *)view {
    [self hideLoadingState];
}

@end
