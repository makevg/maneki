//
//  MAProductCategoriesVC.m
//  Maneki
//
//  Created by Максимычев Е.О. on 04.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <SDWebImage/SDWebImageManager.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "MAProductCategoriesVC.h"
#import "MAProductCategoriesView.h"
#import "MAProductsVC.h"
#import "MAWokVC.h"
#import "MAProductCategoryCell.h"
#import "ProductCategory+CoreDataProperties.h"

NSString *const cProductCategoriesStoryboardName = @"MAProductCategories";
NSString *const cWokBuilderIdentifier = @"wok_constructor";

@interface MAProductCategoriesVC () <
        UICollectionViewDelegate,
        UICollectionViewDelegateFlowLayout,
        DZNEmptyDataSetSource,
        DZNEmptyDataSetDelegate, ServiceResultDelegate>

@property(strong, nonatomic) IBOutlet MAProductCategoriesView *contentView;

@end

@implementation MAProductCategoriesVC

#pragma mark - Public

+ (NSString *)storyboardName {
    return cProductCategoriesStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (void)configureController {
    [super configureController];

    self.title = self.navBarTitle;
    [self setDelegatesAndDataSource];
    [self cancelsTouches:NO];

    [self requestData];
}

- (void)setDelegatesAndDataSource {
    self.contentView.collectionView.dataSource = (id <UICollectionViewDataSource>) self;
    self.contentView.collectionView.delegate = self;
    self.contentView.collectionView.emptyDataSetSource = self;
    self.contentView.collectionView.emptyDataSetDelegate = self;
}

- (UICollectionView *)getCollectionView {
    return self.contentView.collectionView;
}

- (void)requestData {
    [super requestData];

    [MANEKI_SERVICE getCategoriesListByCategoryCode:self.code
                                           receiver:^(NSFetchedResultsController *controller) {
                                               self.fetchResult = controller;
                                               self.fetchResult.delegate = (id <NSFetchedResultsControllerDelegate>) self;
                                               [self.contentView.collectionView reloadData];
                                           }
                                           delegate:self];
}

#pragma mark - Private

- (UIViewController *)prepareVCByProductCategory:(ProductCategory *)category {
    if ([category.string_id isEqualToString:cWokBuilderIdentifier]) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:[MAWokVC storyboardName]
                                                     bundle:nil];
        MAWokVC *wokVC = [sb instantiateInitialViewController];
        wokVC.code = category.string_id;
        return wokVC;
    } else {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:[MAProductsVC storyboardName]
                                                     bundle:nil];
        MAProductsVC *productsVC = [sb instantiateInitialViewController];
        productsVC.code = category.string_id;
        productsVC.navBarTitle = category.title;
        return productsVC;
    }
}

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MAProductCategoryCell *cell = [cv dequeueReusableCellWithReuseIdentifier:[MAProductCategoryCell cellIdentifier]
                                                                forIndexPath:indexPath];
    [cell setModel:[self.fetchResult objectAtIndexPath:indexPath]];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ProductCategory *category = [self.fetchResult objectAtIndexPath:indexPath];
    UIViewController *vc = [self prepareVCByProductCategory:category];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    BOOL wide = (indexPath.row == [[self.fetchResult fetchedObjects] count] - 1) && (indexPath.row % 2 != 1);
    CGFloat screenWidth = CGRectGetWidth(self.contentView.collectionView.frame);
    CGFloat cellWidth = wide ? screenWidth : screenWidth / 2 - 0.5f;
    CGFloat cellHeight = 180.f;
    return CGSizeMake(cellWidth, cellHeight);
}

@end