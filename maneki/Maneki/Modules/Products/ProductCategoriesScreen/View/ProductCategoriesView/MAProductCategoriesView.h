//
//  MAProductCategoriesView.h
//  Maneki
//
//  Created by Максимычев Е.О. on 04.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MAProductCategoriesView : MABaseView

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
