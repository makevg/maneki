//
//  MAProductCategoriesView.m
//  Maneki
//
//  Created by Максимычев Е.О. on 04.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProductCategoriesView.h"
#import "MAColorPalette.h"

@interface MAProductCategoriesView ()

@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation MAProductCategoriesView

#pragma mark - Setup

- (void)setup {
    [self.collectionView setBackgroundColor:[MAColorPalette lightGrayColor]];
    self.flowLayout.minimumLineSpacing = 1;
    self.flowLayout.minimumInteritemSpacing = 1;
    self.activityIndicator.color = [MAColorPalette orangeColor];
}

#pragma mark - Public

- (void)startAnimation {
    [self.activityIndicator startAnimating];
}

- (void)stopAnimation {
    [self.activityIndicator stopAnimating];
}

@end
