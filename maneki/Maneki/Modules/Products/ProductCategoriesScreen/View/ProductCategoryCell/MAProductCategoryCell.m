//
//  MAProductCategoryCell.m
//  Maneki
//
//  Created by Максимычев Е.О. on 04.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MABaseCollectionViewCell.h"
#import "MAProductCategoryCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "Product.h"
#import "ProductCategory.h"
#import "MAAPI.h"
#import "UIImageView+AsyncLoad.h"

NSString *const cProductCategoryCellIdentifier = @"MAProductCategoryCell";

@interface MAProductCategoryCell ()

@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *categoryLogo;

@end

@implementation MAProductCategoryCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cProductCategoryCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[ProductCategory class]]) {
        [self fillCellByProduct:model];
    }
}

- (void)configureCell {
    [self setBackgroundColor:[MAColorPalette whiteColor]];
    self.categoryNameLabel.text = @"Вок-конструктор";
    self.categoryNameLabel.textColor = [UIColor blackColor];
    self.categoryNameLabel.font = [MAFontSet productCategoryCellFont];
}

#pragma mark - Private

- (void)fillCellByProduct:(ProductCategory *)category {
    self.categoryNameLabel.text = category.title;
    [self.categoryLogo loadAsyncFromUrl:[MANEKI_API urlWithPart:category.image_url]];
}

- (void)setHighlighted:(BOOL)highlighted {
//    [super setHighlighted:highlighted];
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    return layoutAttributes;
}

@end
