//
//  MACategoryItemCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MACategoryItemCell.h"
#import "MACategoryItem.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cCategoryItemCellIdentifier = @"MACategoryItemCell";

@interface MACategoryItemCell ()

@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;

@end

@implementation MACategoryItemCell {
    MACategoryItem *item;
}

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cCategoryItemCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[MACategoryItem class]]) {
        item = model;
        [self configureCell];
    }
}

- (void)configureCell {
    self.categoryNameLabel.text = item.categoryName;
    self.categoryNameLabel.textColor = [MAColorPalette whiteColor];
    self.categoryNameLabel.font = [MAFontSet categoryItemFont];
    self.categoryImageView.image = item.categoryImage;
}

@end
