//
//  MAMenuView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 06.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAMenuView.h"
#import "MAColorPalette.h"

@implementation MAMenuView

#pragma mark - Setup

- (void)setup {
    [self.tableView setBackgroundColor:[MAColorPalette blackColor]];
}

@end
