//
//  MAMenuVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAMenuVC.h"
#import "MAMenuView.h"
#import "MACategoryItem.h"
#import "MACategoryItemCell.h"
#import "MAProductCategoriesVC.h"
#import "MAProductsVC.h"

NSString *const cMenuStoryboardName = @"MAMenu";
NSString *const cMenuVCTitle = @"Меню";

@interface MAMenuVC () <UITableViewDataSource, UITableViewDelegate>

@property(strong, nonatomic) IBOutlet MAMenuView *contentView;

@end

@implementation MAMenuVC {
    NSArray *categoriesData;
}

+ (NSString *)storyboardName {
    return cMenuStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

#pragma mark - Private

- (void)configureController {
    [self cancelsTouches:NO];
    categoriesData = [self prepareCategoriesData];
    self.contentView.tableView.dataSource = self;
    self.contentView.tableView.delegate = self;
    self.title = cMenuVCTitle;
}

- (NSArray *)prepareCategoriesData {
    MACategoryItem *panAsianItem = [[MACategoryItem alloc] initWithIdName:@"Паназиатская кухня"
                                                                     code:@"panasian"
                                                                    image:[UIImage imageNamed:@"PanAsian"]];

    MACategoryItem *japanItem = [[MACategoryItem alloc] initWithIdName:@"Японская кухня"
                                                                  code:@"japan"
                                                                 image:[UIImage imageNamed:@"Japan"]];

    MACategoryItem *dessertsItem = [[MACategoryItem alloc] initWithIdName:@"Десерты"
                                                                     code:@"deserty"
                                                                    image:[UIImage imageNamed:@"Desserts"]];

    MACategoryItem *drinksItem = [[MACategoryItem alloc] initWithIdName:@"Напитки"
                                                                   code:@"drinks"
                                                                  image:[UIImage imageNamed:@"Drinks"]];
    return @[panAsianItem, japanItem, dessertsItem, drinksItem];
}

- (CGFloat)calculateCellHeight {
    CGFloat tableViewHeight = CGRectGetHeight(self.contentView.tableView.frame);
    return tableViewHeight / [categoriesData count];
}

- (void)showProductCategoriesByCode:(NSString *)code title:(NSString *)title {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[MAProductCategoriesVC storyboardName]
                                                 bundle:nil];
    MAProductCategoriesVC *vc = [sb instantiateInitialViewController];
    vc.code = code;
    vc.navBarTitle = title;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showProductsByCode:(NSString *)code title:(NSString *)title {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[MAProductsVC storyboardName]
                                                 bundle:nil];
    MAProductsVC *vc = [sb instantiateInitialViewController];
    vc.code = code;
    vc.navBarTitle = title;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [categoriesData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [MACategoryItemCell cellIdentifier];
    MACategoryItemCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                               forIndexPath:indexPath];
    MACategoryItem *categoryItem = categoriesData[(NSUInteger) indexPath.row];
    [cell setModel:categoryItem];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MACategoryItem *item = categoriesData[(NSUInteger) indexPath.row];
    if( indexPath.row == 2 ) { //Дисерты - сразу в товар
        [self showProductsByCode:item.categoryCode title:item.categoryName];
    } else {
        [self showProductCategoriesByCode:item.categoryCode title:item.categoryName];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self calculateCellHeight];
}

@end
