//
//  MACategoryItem.h
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MACategoryItem : NSObject

@property (nonatomic) NSString *categoryName;
@property (nonatomic) NSString *categoryCode;
@property (nonatomic) UIImage *categoryImage;

- (instancetype)initWithIdName:(NSString *)categoryName code:(NSString *)categoryCode image:(UIImage *)categoryImage;

@end
