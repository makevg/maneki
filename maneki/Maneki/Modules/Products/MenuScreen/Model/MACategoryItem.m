//
//  MACategoryItem.m
//  Maneki
//
//  Created by Maximychev Evgeny on 28.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MACategoryItem.h"

@implementation MACategoryItem

#pragma mark - Init

- (instancetype)initWithIdName:(NSString *)categoryName code:(NSString *)categoryCode image:(UIImage *)categoryImage {
    self = [super init];
    if (self) {
        _categoryName  = categoryName;
        _categoryCode  = categoryCode;
        _categoryImage = categoryImage;
    }
    return self;
}

@end
