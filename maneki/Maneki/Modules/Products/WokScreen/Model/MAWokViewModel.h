//
// Created by Buravlev Mikhail on 19.01.16.
// Copyright (c) 2016 Maneki. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MAWokViewModel : NSObject

@property (nonatomic, strong) NSNumber *cost;

@property (nonatomic, strong) NSNumber *productId;
@property (nonatomic, strong) NSString *productTitle;

@property (nonatomic, strong) NSNumber *riceId;
@property (nonatomic, strong) NSString *riceTitle;
@property (nonatomic, strong) NSString *riceParentCode;

@property (nonatomic, strong) NSNumber *sauceId;
@property (nonatomic, strong) NSString *sauceTitle;
@property (nonatomic, strong) NSString *sauceParentCode;

@property (nonatomic, assign) NSUInteger quantity;

@end