//
//  MAWokChoosesCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 04.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAWokChoosesCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MAWokViewModel.h"

NSString *const cWokChoosesCellIdentifier = @"WokChoosesCell";
CGFloat const cWokChoosesCellHeight = 115.f;

@interface MAWokChoosesCell ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *riceCounterLabel;
@property (weak, nonatomic) IBOutlet UILabel *riceLabel;
@property (weak, nonatomic) IBOutlet UILabel *productCounterLabel;
@property (weak, nonatomic) IBOutlet UILabel *productLabel;
@property (weak, nonatomic) IBOutlet UILabel *sousCounterLabel;
@property (weak, nonatomic) IBOutlet UILabel *sousLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceValueLabel;
@end

@implementation MAWokChoosesCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cWokChoosesCellIdentifier;
}

+ (CGFloat)cellHeight {
    return cWokChoosesCellHeight;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[MAWokViewModel class]]) {
        [self fillByDictionary:model];
    }
}

- (void)configureCell {
    self.infoLabel.textColor = [MAColorPalette blackColor];
    self.infoLabel.font = [MAFontSet wokChoosesCellInfoFont];
    self.riceCounterLabel.font = [MAFontSet wokChoosesCellCounterFont];
    self.riceLabel.textColor = [MAColorPalette grayColor];
    self.riceLabel.font = [MAFontSet wokChoosesCellDescrFont];
    self.productCounterLabel.font = [MAFontSet wokChoosesCellCounterFont];
    self.productLabel.textColor = [MAColorPalette grayColor];
    self.productLabel.font = [MAFontSet wokChoosesCellDescrFont];
    self.sousCounterLabel.font = [MAFontSet wokChoosesCellCounterFont];
    self.sousLabel.textColor = [MAColorPalette grayColor];
    self.sousLabel.font = [MAFontSet wokChoosesCellDescrFont];

    self.priceInfoLabel.textColor = [MAColorPalette grayColor];
    self.priceInfoLabel.font = [MAFontSet wokChoosesCellCostInfoFont];
    self.priceValueLabel.textColor = [MAColorPalette orangeColor];
    self.priceValueLabel.font = [MAFontSet wokChoosesCellCostValueFont];
}

#pragma mark - Private

- (void)fillByDictionary:(MAWokViewModel *)wokViewModel {
    NSString *rice = [wokViewModel.riceTitle length] > 0 ? wokViewModel.riceTitle : @"-";
    self.riceLabel.text = rice;
    NSString *product = [wokViewModel.productTitle length] > 0 ? wokViewModel.productTitle : @"-";
    self.productLabel.text = product;
    NSString *sous = [wokViewModel.sauceTitle length] > 0 ? wokViewModel.sauceTitle : @"-";
    self.sousLabel.text = sous;
    NSInteger cost = [wokViewModel.cost integerValue];
    self.priceValueLabel.text = [NSString stringWithFormat:@"%li р.", (long)cost];
}

@end