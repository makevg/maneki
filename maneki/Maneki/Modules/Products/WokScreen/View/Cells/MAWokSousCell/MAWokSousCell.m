//
//  MAWokSousCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 04.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAWokSousCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "WokPropertyValue.h"

NSString *const cWokSousCellIdentifier = @"WokSousCell";
CGFloat const cWokSousCellHeight = 44.f;

@interface MAWokSousCell ()
@property(weak, nonatomic) IBOutlet UILabel *infoLabel;
@property(weak, nonatomic) IBOutlet UIImageView *propertyImageView;
@end

@implementation MAWokSousCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cWokSousCellIdentifier;
}

+ (CGFloat)cellHeight {
    return cWokSousCellHeight;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[WokPropertyValue class]]) {
        [self fillCellByWokPropertyValue:model];
    }
}

- (void)configureCell {
    self.accessoryType = UITableViewCellAccessoryNone;
    self.tintColor = [MAColorPalette backgroundColor];
    self.infoLabel.textColor = [MAColorPalette blackColor];
    self.infoLabel.font = [MAFontSet wokSousCellFont];
}

#pragma mark - Private

- (void)fillCellByWokPropertyValue:(WokPropertyValue *)value {
    self.accessoryType = UITableViewCellAccessoryNone;
    self.infoLabel.text = value.title;
    self.propertyImageView.hidden = ![value.hot boolValue];
}

@end