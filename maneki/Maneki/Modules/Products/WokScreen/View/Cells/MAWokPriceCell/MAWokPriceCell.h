//
//  MAWokPriceCell.h
//  Maneki
//
//  Created by Maximychev Evgeny on 04.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseTableViewCell.h"

@protocol MAWokPriceCellDelegate;

@interface MAWokPriceCell : MABaseTableViewCell

@property (weak, nonatomic) id<MAWokPriceCellDelegate> delegate;

@end

@protocol MAWokPriceCellDelegate

- (void)cell:(MAWokPriceCell *)cell wokQuantityChanged:(NSUInteger)quantity;

@end
