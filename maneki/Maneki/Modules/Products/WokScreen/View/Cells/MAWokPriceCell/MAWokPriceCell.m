//
//  MAWokPriceCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 04.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAWokPriceCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

NSString *const cWokPriceCellIdentifier = @"WokPriceCell";
CGFloat const cWokPriceCellHeight = 75.f;

@interface MAWokPriceCell ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@end

@implementation MAWokPriceCell {
    NSUInteger wokCost;
    NSUInteger wokQuantity;
}

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cWokPriceCellIdentifier;
}

+ (CGFloat)cellHeight {
    return cWokPriceCellHeight;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSNumber class]]) {
        wokCost = [model unsignedIntegerValue];
        [self updateCostLabelByCost:wokCost quantity:wokQuantity];
    }
}

- (void)configureCell {
    wokQuantity = 1;
    wokCost = 0;
    [self updateCountLabelByCount:wokQuantity];
    self.infoLabel.textColor = [MAColorPalette grayColor];
    self.infoLabel.font = [MAFontSet wokChoosesCellCostInfoFont];
    self.priceLabel.textColor = [MAColorPalette orangeColor];
    self.priceLabel.font = [MAFontSet wokChoosesCellCostValueFont];
    self.countLabel.textColor = [MAColorPalette blackColor];
    self.countLabel.font = [MAFontSet productCountLabelFont];
}

#pragma mark - Private

- (void)updateCountLabelByCount:(NSUInteger)count {
    self.minusButton.enabled = count >= 2;
    self.countLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long) count];
    [self updateCostLabelByCost:wokCost quantity:count];
}

- (void)updateCostLabelByCost:(NSUInteger)cost quantity:(NSUInteger) quantity {
    self.priceLabel.text = [NSString stringWithFormat:@"%li р.", (long) (cost * quantity)];
    self.plusButton.enabled = wokCost > 0;
}

#pragma mark - Actions

- (IBAction)tappedMinusButton:(id)sender {
    wokQuantity--;
    [self updateCountLabelByCount:wokQuantity];
    if (self.delegate) {
        [self.delegate cell:self wokQuantityChanged:wokQuantity];
    }
}

- (IBAction)tappedPlusButton:(id)sender {
    wokQuantity++;
    [self updateCountLabelByCount:wokQuantity];
    if (self.delegate) {
        [self.delegate cell:self wokQuantityChanged:wokQuantity];
    }
}

@end