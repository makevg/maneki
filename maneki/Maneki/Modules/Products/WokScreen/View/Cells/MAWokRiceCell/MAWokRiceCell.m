//
//  MAWokRiceCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 06.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAWokRiceCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "WokPropertyValue.h"
#import "UIImageView+AsyncLoad.h"

NSString *const cWokProductsCellIdentifier = @"WokRiceCell";
CGFloat const cWokProductsCellHeight = 110.f;

@interface MAWokRiceCell ()
@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorWidthConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *firstProductImageView;
@property (weak, nonatomic) IBOutlet UILabel *firstInfoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *secondProductImageView;
@property (weak, nonatomic) IBOutlet UILabel *secondInfoLabel;
@property (weak, nonatomic) IBOutlet UIView *firstSelectorView;
@property (weak, nonatomic) IBOutlet UIView *secondSelectorView;
@end

@implementation MAWokRiceCell {
    NSString *firstRiceTitle;
    NSString *secondRiceTitle;
}

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cWokProductsCellIdentifier;
}

+ (CGFloat)cellHeight {
    return cWokProductsCellHeight;
}

- (void)setFirstModel:(id)model {
    if ([model isKindOfClass:[WokPropertyValue class]]) {
        [self fillByFirstValue:model];
    }
}

- (void)setSecondModel:(id)model {
    if ([model isKindOfClass:[WokPropertyValue class]]) {
        [self fillBySecondValue:model];
    }
}

- (void)configureCell {
    self.separatorWidthConstraint.constant = 0.5f;
    [self prepareFirstView];
    [self prepareSecondView];
}

- (void)hideSelections {
    self.firstSelectorView.hidden = YES;
    self.secondSelectorView.hidden = YES;
}

- (void)showLeftSelector:(BOOL)show {
    self.firstSelectorView.hidden = !show;
    self.secondSelectorView.hidden = show;
}

- (void)showRightSelector:(BOOL)show {
    self.firstSelectorView.hidden = show;
    self.secondSelectorView.hidden = !show;
}

#pragma mark - Private

- (void)prepareFirstView {
    self.firstProductImageView.layer.cornerRadius = self.firstProductImageView.frame.size.width / 2;
    self.firstProductImageView.clipsToBounds = YES;
    self.firstInfoLabel.font = [MAFontSet wokProductCellFont];
    [self prepareSelectorView:self.firstSelectorView];
}

- (void)prepareSecondView {
    self.secondProductImageView.layer.cornerRadius = self.secondProductImageView.frame.size.width / 2;
    self.secondProductImageView.clipsToBounds = YES;
    self.secondInfoLabel.font = [MAFontSet wokProductCellFont];
    [self prepareSelectorView:self.secondSelectorView];
}

- (void)prepareSelectorView:(UIView *)selectorView {
    selectorView.backgroundColor = [UIColor clearColor];
    selectorView.layer.cornerRadius = selectorView.frame.size.width / 2;
    selectorView.layer.borderColor = [MAColorPalette backgroundColor].CGColor;
    selectorView.layer.borderWidth = 3.f;
    selectorView.hidden = YES;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint firstLocation = [touch locationInView:self.firstView];
    CGPoint secondLocation = [touch locationInView:self.secondView];
    if ([self.firstView pointInside:firstLocation withEvent:event]) {
        if (self.delegate) {
            [self.delegate cell:self selectedLeftViewWithRiceTitle:firstRiceTitle];
        }
    } else if ([self.secondView pointInside:secondLocation withEvent:event]) {
        if (self.delegate) {
            [self.delegate cell:self selectedRightViewWithRiceTitle:secondRiceTitle];
        }
    }
}

- (void)fillByFirstValue:(WokPropertyValue *)value {
    self.firstInfoLabel.text = value.title;
    NSString *productUrl = [NSString stringWithFormat:@"http://manekicafe.ru%@", value.image];
    [self.firstProductImageView loadAsyncFromUrl:productUrl placeHolder:@"PlaceholderMini"];
    firstRiceTitle = value.title;
}

- (void)fillBySecondValue:(WokPropertyValue *)value {
    self.secondInfoLabel.text = value.title;
    NSString *productUrl = [NSString stringWithFormat:@"http://manekicafe.ru%@", value.image];
    [self.secondProductImageView loadAsyncFromUrl:productUrl placeHolder:@"PlaceholderMini"];
    secondRiceTitle = value.title;
}

@end
