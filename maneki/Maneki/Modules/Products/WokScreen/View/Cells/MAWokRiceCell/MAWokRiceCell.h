//
//  MAWokRiceCell.h
//  Maneki
//
//  Created by Maximychev Evgeny on 06.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseTableViewCell.h"

typedef NS_ENUM(NSUInteger, MAWokProductsCellViewsType) {
    eLeftViewType = 0,
    eRightViewType
};

@protocol MAWokRiceCellDelegate;

@interface MAWokRiceCell : MABaseTableViewCell

@property (weak , nonatomic) id<MAWokRiceCellDelegate> delegate;

- (void)setFirstModel:(id)model;
- (void)setSecondModel:(id)model;
- (void)hideSelections;
- (void)showLeftSelector:(BOOL)show;
- (void)showRightSelector:(BOOL)show;

@end

@protocol MAWokRiceCellDelegate <NSObject>

- (void)cell:(MAWokRiceCell *)cell selectedLeftViewWithRiceTitle:(NSString *)riceTitle;
- (void)cell:(MAWokRiceCell *)cell selectedRightViewWithRiceTitle:(NSString *)riceTitle;

@end