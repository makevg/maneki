//
//  MAWokBasisCell.h
//  Maneki
//
//  Created by Maximychev Evgeny on 04.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseTableViewCell.h"

@interface MAWokBasisCell : MABaseTableViewCell

- (void)showSelector:(BOOL)show;

@end
