//
//  MAWokBasisCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 04.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAWokBasisCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "Product.h"
#import "MADataFormatter.h"
#import "UIImageView+AsyncLoad.h"

NSString *const cWokBasisCellIdentifier = @"WokBasisCell";
CGFloat const cWokBasisCellHeight = 110.f;

@interface MAWokBasisCell ()
@property (weak, nonatomic) IBOutlet UIView *selectorView;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@end

@implementation MAWokBasisCell

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cWokBasisCellIdentifier;
}

+ (CGFloat)cellHeight {
    return cWokBasisCellHeight;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[Product class]]) {
        [self fillCellByProduct:model];
    }
}

- (void)configureCell {
    self.selectorView.layer.cornerRadius = self.selectorView.frame.size.width / 2;
    self.selectorView.layer.borderColor = [MAColorPalette backgroundColor].CGColor;
    self.selectorView.layer.borderWidth = 3.f;
    self.selectorView.backgroundColor = [UIColor clearColor];
    self.selectorView.hidden = YES;
    self.infoLabel.textColor = [MAColorPalette blackColor];
    self.infoLabel.font = [MAFontSet wokBasisCellInfoFont];
    self.descriptionLabel.textColor = [MAColorPalette grayColor];
    self.descriptionLabel.font = [MAFontSet wokBasisCellDescrFont];
    self.costLabel.textColor = [MAColorPalette orangeColor];
    self.costLabel.font = [MAFontSet wokBasisCellCostFont];
}

- (void)showSelector:(BOOL)show {
    self.selectorView.hidden = !show;
}

#pragma mark - Private

- (void)fillCellByProduct:(Product *)product {
    [self showSelector:NO];
    [self.productImageView loadAsyncFromUrl:[NSString stringWithFormat:@"http://manekicafe.ru%@", product.preview_image_url] placeHolder:@"PlaceholderMini"];
    self.infoLabel.text = product.title;
    self.descriptionLabel.text = [MADataFormatter stringByHTML:product.descr];
    self.costLabel.text = [NSString stringWithFormat:@"%@ р.", product.cost];
}

@end
