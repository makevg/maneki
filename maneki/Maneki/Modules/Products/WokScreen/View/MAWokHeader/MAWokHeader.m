//
//  MAWokHeader.m
//  Maneki
//
//  Created by Maximychev Evgeny on 05.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAWokHeader.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@interface MAWokHeader ()
@property (weak, nonatomic) IBOutlet UIView *positionView;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation MAWokHeader

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.mainView];
        [self configureView];
    }
    return self;
}

#pragma mark - Private

- (void)configureView {
    [self prepareConstraints];
    [self prepareSubviews];
}

- (void)prepareConstraints {
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
}

- (void)prepareSubviews {
    self.mainView.backgroundColor = [UIColor clearColor];
    self.positionView.backgroundColor = [MAColorPalette orangeColor];
    self.positionView.layer.cornerRadius = self.positionView.frame.size.width / 2;
    self.positionView.clipsToBounds = YES;
    self.positionLabel.textColor = [MAColorPalette whiteColor];
    self.positionLabel.font = [MAFontSet wokHeaderPositionFont];
    self.titleLabel.textColor = [MAColorPalette blackColor];
    self.titleLabel.font = [MAFontSet wokHeaderTitleFont];
}

#pragma mark - Public

- (void)configureWithTitle:(NSString *)title position:(NSInteger)position {
    self.titleLabel.text = title;
    self.positionLabel.text = [NSString stringWithFormat:@"%@", @(position + 1)];
}

@end
