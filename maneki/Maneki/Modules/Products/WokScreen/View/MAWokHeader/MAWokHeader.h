//
//  MAWokHeader.h
//  Maneki
//
//  Created by Maximychev Evgeny on 05.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MAWokHeader : MABaseView

@property (strong, nonatomic) IBOutlet UIView *mainView;

- (void)configureWithTitle:(NSString *)title position:(NSInteger)position;

@end
