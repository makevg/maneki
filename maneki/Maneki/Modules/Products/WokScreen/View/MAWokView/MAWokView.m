//
//  MAWokView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 04.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAWokView.h"
#import "MABottomButton.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"

@interface MAWokView ()
@property (weak, nonatomic) IBOutlet MABottomButton *addToCartButton;
@end

@implementation MAWokView

#pragma mark - Setup

- (void)setup {
    self.tableView.backgroundColor = [MAColorPalette lightGrayColor];
    self.addToCartButton.backgroundColor = [MAColorPalette orangeColor];
}

#pragma mark - Public

- (void)setAddToCartButtonTitle:(NSString *)title {
    [self.addToCartButton setTitle:title forState:UIControlStateNormal];
}

- (void)showAddToCartButton:(BOOL)show {
    self.addToCartButton.hidden = !show;
}

@end
