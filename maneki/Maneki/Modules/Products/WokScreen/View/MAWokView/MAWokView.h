//
//  MAWokView.h
//  Maneki
//
//  Created by Maximychev Evgeny on 04.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MAWokView : MABaseView

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void)setAddToCartButtonTitle:(NSString *)title;
- (void)showAddToCartButton:(BOOL)show;

@end
