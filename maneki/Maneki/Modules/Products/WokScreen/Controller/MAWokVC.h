//
//  MAWokVC.h
//  Maneki
//
//  Created by Maximychev Evgeny on 04.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseVC.h"

@interface MAWokVC : MABaseVC

@property (nonatomic) NSString *code;

@end
