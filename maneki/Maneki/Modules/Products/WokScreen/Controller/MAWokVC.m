//
//  MAWokVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 04.01.16.
//  Copyright © 2016 Maneki. All rights reserved.
//

#import "MAWokVC.h"
#import "MAWokView.h"
#import "MAWokRiceCell.h"
#import "MAWokBasisCell.h"
#import "MAWokSousCell.h"
#import "MAWokChoosesCell.h"
#import "MAWokPriceCell.h"
#import "MAWokHeader.h"
#import "Product.h"
#import "WokProperty.h"
#import "WokPropertyValue+CoreDataProperties.h"
#import "MACartService.h"
#import "MAWokViewModel.h"

NSString *const cWokStoryboardName = @"MAWok";
static const CGFloat cDefaultHeaderHeight = 20.f;

typedef NS_ENUM(NSUInteger, WokSectionsType) {
    eRiceType = 0,
    eBasisType,
    eSousType,
    eCostType
};

@interface MAWokVC () <
        UITableViewDataSource,
        UITableViewDelegate,
        MAWokRiceCellDelegate,
        MAWokPriceCellDelegate,
        ServiceResultDelegate,
        NSFetchedResultsControllerDelegate>
@property(strong, nonatomic) IBOutlet MAWokView *contentView;
@end

@implementation MAWokVC {
    NSFetchedResultsController *productsFrc;
    NSFetchedResultsController *propertiesFrc;

    NSArray <WokPropertyValue *> *riceValues;
    NSArray <WokPropertyValue *> *sauceValues;

    NSArray <NSString *> *sectionsHeaders;
    NSArray <NSString *> *buttonTitles;

    NSUInteger sectionToScroll;
    MAWokProductsCellViewsType viewType;

    NSIndexPath *selectedRiceCellIndexPath;
    NSIndexPath *selectedBasisCellIndexPath;
    NSIndexPath *selectedSousCellIndexPath;

    MAWokViewModel *wokViewModel;
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cWokStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (void)configureController {
    [self prepareLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault forView:self.contentView.tableView];
    [self initData];
    [self setDelegatesAndDataSource];
    [self cancelsTouches:NO];
    [self.contentView setAddToCartButtonTitle:buttonTitles[0]];
    [self.contentView showAddToCartButton:NO];
    self.contentView.tableView.estimatedRowHeight = 0;
}

- (void)setDelegatesAndDataSource {
    self.contentView.tableView.dataSource = self;
    self.contentView.tableView.delegate = self;
}

#pragma mark - Private

- (void)fillNoodleAndRise {
    if (![[propertiesFrc fetchedObjects] count])
        return;

    WokProperty *wokProperty = [propertiesFrc objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    WokProperty *rices;
    WokProperty *souses;

    if ([wokProperty.code isEqualToString:@"noodles_rice"]) {
        rices = wokProperty;
        souses = [propertiesFrc objectAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    } else {
        souses = wokProperty;
        rices = [propertiesFrc objectAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    }

    sauceValues = [souses.values allObjects];
    sauceValues = [sauceValues sortedArrayUsingComparator:^NSComparisonResult(WokPropertyValue *obj1, WokPropertyValue *obj2) {
        if ([obj1.position integerValue] < [obj2.position integerValue])
            return NSOrderedDescending;

        return NSOrderedAscending;
    }];

    riceValues = [rices.values allObjects];
    riceValues = [riceValues sortedArrayUsingComparator:^NSComparisonResult(WokPropertyValue *obj1, WokPropertyValue *obj2) {
        if ([obj1.position integerValue] < [obj2.position integerValue])
            return NSOrderedDescending;

        return NSOrderedAscending;
    }];
}

- (void)initData {
    sectionToScroll = eRiceType;
    sectionsHeaders = [self prepareSectionsHeaders];
    buttonTitles = [self prepareButtonTitles];
    wokViewModel.quantity = 1;
    [self prepareWokDictionary];
    [self requestData];
}

- (void)requestData {
    [super requestData];
    [MANEKI_SERVICE getWokProductsByCategoryCode:self.code
                                        receiver:^(NSArray *array) {
                                            productsFrc = array[0];
                                            propertiesFrc = array[1];
                                            productsFrc.delegate = self;
                                            propertiesFrc.delegate = self;
                                            [self fillNoodleAndRise];

                                            [self.contentView.tableView reloadData];
                                            [self.contentView showAddToCartButton:YES];
                                        } delegate:self];
};

- (void)prepareWokDictionary {
    wokViewModel = [MAWokViewModel new];
}

- (NSArray *)prepareSectionsHeaders {
    return @[@"Выберите лапшу или рис", @"Добавьте основу", @"Заправьте соусом", @""];
}

- (NSArray *)prepareButtonTitles {
    return @[@"Выберите лапшу или рис", @"Добавьте основу", @"Заправьте соусом", @"В корзину"];
}

- (NSString *)cellIdentifierByIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier;
    switch (indexPath.section) {
        case eRiceType:
            identifier = [MAWokRiceCell cellIdentifier];
            break;
        case eBasisType:
            identifier = [MAWokBasisCell cellIdentifier];
            break;
        case eSousType:
            identifier = [MAWokSousCell cellIdentifier];
            break;
        case eCostType:
            identifier = indexPath.row != 1 ? [MAWokChoosesCell cellIdentifier] : [MAWokPriceCell cellIdentifier];
            break;
        default:
            break;
    }
    return identifier;
}

- (MABaseTableViewCell *)cellByIdentifier:(NSString *)identifier
                                tableView:(UITableView *)tableView
                                indexPath:(NSIndexPath *)indexPath {
    if ([identifier isEqualToString:[MAWokRiceCell cellIdentifier]]) {
        MAWokRiceCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                              forIndexPath:indexPath];

        [cell hideSelections];
        cell.delegate = self;
        [cell setFirstModel:riceValues[(NSUInteger) (indexPath.row * 2)]];
        if ((indexPath.row * 2 + 1) < [riceValues count])
            [cell setSecondModel:riceValues[(NSUInteger) (indexPath.row * 2 + 1)]];

        if ([indexPath isEqual:selectedRiceCellIndexPath]) {
            switch (viewType) {
                case eLeftViewType:
                    [cell showLeftSelector:YES];
                    break;
                case eRightViewType:
                    [cell showRightSelector:YES];
                    break;
            }
        }

        return cell;
    } else if ([identifier isEqualToString:[MAWokPriceCell cellIdentifier]]) {
        MAWokPriceCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                               forIndexPath:indexPath];
        cell.delegate = self;
        [cell setModel:wokViewModel.cost];
        return cell;
    } else {
        MABaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                                    forIndexPath:indexPath];
        if ([identifier isEqualToString:[MAWokBasisCell cellIdentifier]]) {
            id <NSFetchedResultsSectionInfo> sectionInfo = [productsFrc sections][0];
            [cell setModel:[sectionInfo objects][(NSUInteger) indexPath.row]];
            if ([indexPath isEqual:selectedBasisCellIndexPath]) {
                [(MAWokBasisCell *) cell showSelector:YES];
            }
        } else if ([identifier isEqualToString:[MAWokSousCell cellIdentifier]]) {
            [cell setModel:sauceValues[(NSUInteger) indexPath.row]];
            if ([indexPath isEqual:selectedSousCellIndexPath]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        } else if ([identifier isEqualToString:[MAWokChoosesCell cellIdentifier]]) {
            [cell setModel:wokViewModel];
        }
        return cell;
    }
}

- (void)checkFull {
    if (selectedRiceCellIndexPath && selectedBasisCellIndexPath && selectedSousCellIndexPath) {
        sectionToScroll = eCostType;
    }
}

- (void)updateTableView:(UITableView *)tableView cellsByIndexPath:(NSIndexPath *)indexPath andOldIndexPath:(NSIndexPath *)oldIndexPath {
    NSMutableArray *arr = [@[indexPath] mutableCopy];
    if (oldIndexPath)
        [arr addObject:oldIndexPath];


    [UIView setAnimationsEnabled:NO];
    [tableView reloadRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationAutomatic];
    [UIView setAnimationsEnabled:YES];
}

- (void)selectRiceByTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    NSIndexPath *oldIndexPath = selectedRiceCellIndexPath;
    selectedRiceCellIndexPath = indexPath;

    if (!selectedSousCellIndexPath) {
        sectionToScroll = eSousType;
    }
    if (!selectedBasisCellIndexPath) {
        sectionToScroll = eBasisType;
    }

    [self checkFull];
    [self.contentView setAddToCartButtonTitle:buttonTitles[sectionToScroll]];
    [self updateTableView:tableView cellsByIndexPath:indexPath andOldIndexPath:oldIndexPath];
//    [self scrollToSection:sectionToScroll];
}

- (void)selectBasisByTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    NSIndexPath *oldIndexPath = selectedBasisCellIndexPath;
    selectedBasisCellIndexPath = indexPath;

    if (!selectedSousCellIndexPath) {
        sectionToScroll = eSousType;
    }
    if (!selectedRiceCellIndexPath) {
        sectionToScroll = eRiceType;
    }

    [self checkFull];
    [self.contentView setAddToCartButtonTitle:buttonTitles[sectionToScroll]];

    [self fillProductByIndexPath:indexPath];
    [self updateTableView:tableView cellsByIndexPath:indexPath andOldIndexPath:oldIndexPath];
//    [self scrollToSection:sectionToScroll];
}

- (void)selectSousByTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    NSIndexPath *oldIndexPath = selectedSousCellIndexPath;
    selectedSousCellIndexPath = indexPath;

    if (!selectedBasisCellIndexPath) {
        sectionToScroll = eBasisType;
    }
    if (!selectedRiceCellIndexPath) {
        sectionToScroll = eRiceType;
    }

    [self checkFull];
    [self.contentView setAddToCartButtonTitle:buttonTitles[sectionToScroll]];

    [self fillSousByIndexPath:indexPath];
    [self updateTableView:tableView cellsByIndexPath:indexPath andOldIndexPath:oldIndexPath];
//    [self scrollToSection:sectionToScroll];
}

- (void)fillRiceByTitle:(NSString *)title viewType:(MAWokProductsCellViewsType)type {
    for (WokPropertyValue *propertyValue in riceValues) {
        if ([propertyValue.title isEqualToString:title]) {
            wokViewModel.riceId = propertyValue.id;
            wokViewModel.riceTitle = propertyValue.title;
            wokViewModel.riceParentCode = propertyValue.wok_property.code;
            viewType = type;

            break;
        }
    }
}

- (void)fillProductByIndexPath:(NSIndexPath *)indexPath {
    id <NSFetchedResultsSectionInfo> sectionInfo = [productsFrc sections][0];
    Product *product = [sectionInfo objects][(NSUInteger) indexPath.row];
    wokViewModel.productId = product.id;
    wokViewModel.productTitle = product.title;
    wokViewModel.cost = product.cost;
}

- (void)fillSousByIndexPath:(NSIndexPath *)indexPath {
    WokPropertyValue *wokPropertyValue = sauceValues[(NSUInteger) indexPath.row];

    wokViewModel.sauceId = wokPropertyValue.id;
    wokViewModel.sauceTitle = wokPropertyValue.title;
    wokViewModel.sauceParentCode = wokPropertyValue.wok_property.code;
}

- (void)scrollToSection:(NSInteger)section {
    NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:0
                                                        inSection:section];
    [self.contentView.tableView scrollToRowAtIndexPath:indexPathToScroll
                                      atScrollPosition:UITableViewScrollPositionTop
                                              animated:YES];
}

- (void)updatePriceCells {
    [UIView setAnimationsEnabled:NO];
    [self.contentView.tableView reloadSections:[NSIndexSet indexSetWithIndex:eCostType] withRowAnimation:UITableViewRowAnimationNone];
    [UIView setAnimationsEnabled:YES];
}

- (BOOL)isDataFilled {
    return (productsFrc && propertiesFrc && [[propertiesFrc fetchedObjects] count] && [[productsFrc fetchedObjects] count]);
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self isDataFilled])
        return [sectionsHeaders count];

    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (![self isDataFilled]) {
        return 0;
    } else {
        switch (section) {
            case eRiceType:
                return (int) (([riceValues count] + 1) / 2);
            case eBasisType: {
                id <NSFetchedResultsSectionInfo> sectionInfo;
                sectionInfo = [productsFrc sections][0];
                return [sectionInfo numberOfObjects];
            }
            case eSousType:
                return [sauceValues count];
            case eCostType:
                return 2;
            default:
                return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [self cellIdentifierByIndexPath:indexPath];
    return [self cellByIdentifier:identifier tableView:tableView indexPath:indexPath];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case eRiceType:
            return [MAWokRiceCell cellHeight];
        case eBasisType:
            return [MAWokBasisCell cellHeight];
        case eSousType:
            return [MAWokSousCell cellHeight];
        case eCostType:
            return indexPath.row != 1 ? [MAWokChoosesCell cellHeight] : [MAWokPriceCell cellHeight];
        default:
            return [MABaseTableViewCell cellHeight];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }

    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case eRiceType:
            [self selectRiceByTableView:tableView indexPath:indexPath];
            break;
        case eBasisType:
            [self selectBasisByTableView:tableView indexPath:indexPath];
            break;
        case eSousType:
            [self selectSousByTableView:tableView indexPath:indexPath];
            break;
        default:
            break;
    }

    [self updatePriceCells];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section != [sectionsHeaders count] - 1) {
        MAWokHeader *view = [MAWokHeader new];
        [view configureWithTitle:sectionsHeaders[(NSUInteger) section] position:section];
        return view;
    } else {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat headerHeight = section != [sectionsHeaders count] - 1 ? self.contentView.tableView.sectionHeaderHeight : cDefaultHeaderHeight;
    return headerHeight;
}

#pragma mark - Actions

- (IBAction)tappedAddToCartButton:(id)sender {
    if (sectionToScroll == eCostType) {
        PCAngularActivityIndicatorView *indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                                   forView:self.view];
        [indicatorView startAnimating];
        self.view.userInteractionEnabled = NO;

        __weak __typeof(&*self) weakSelf = self;

        [CART_SERVICE updateRiceId:wokViewModel.riceId
                         riceTitle:wokViewModel.riceTitle
                          riceCode:wokViewModel.riceParentCode
                         productId:wokViewModel.productId
                           sauceId:wokViewModel.sauceId
                        sauceTitle:wokViewModel.sauceTitle
                         sauceCode:wokViewModel.sauceParentCode
                            amount:wokViewModel.quantity
                    successHandler:^(NSUInteger productId, NSUInteger amount) {
                        __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                        if (strongSelf) {
                            if (indicatorView)
                                [indicatorView stopAnimating];

                            strongSelf.view.userInteractionEnabled = YES;
                            [strongSelf.navigationController popViewControllerAnimated:YES];
                        }
                    }
                    failureHandler:^(NSUInteger productId, NSUInteger amount, NSError *error) {
                        __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                        if (strongSelf) {
                            if (indicatorView)
                                [indicatorView stopAnimating];

                            strongSelf.view.userInteractionEnabled = YES;
                            [strongSelf showMessage:error.localizedDescription];
                        }
                    }];
    } else {
        [self scrollToSection:sectionToScroll];
    }
}

#pragma mark - MAWokRiceCellDelegate

- (void)cell:(MAWokRiceCell *)cell selectedLeftViewWithRiceTitle:(NSString *)riceTitle {
    [self fillRiceByTitle:riceTitle viewType:eLeftViewType];
}

- (void)cell:(MAWokRiceCell *)cell selectedRightViewWithRiceTitle:(NSString *)riceTitle {
    [self fillRiceByTitle:riceTitle viewType:eRightViewType];
}

#pragma mark - MAWokPriceCellDelegate

- (void)cell:(MAWokPriceCell *)cell wokQuantityChanged:(NSUInteger)quantity {
    wokViewModel.quantity = quantity;
}

#pragma mark - NSFetchedResultController

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if (controller == propertiesFrc)
        [self fillNoodleAndRise];

    [self.contentView.tableView reloadData];
}

#pragma mark - ServiceResultDelegate

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];
}

- (void)networkError:(NSError *)error forService:(Class)service {
    [super networkError:error forService:service];

    [self.contentView.tableView reloadData];
}

- (void)serverError:(NSError *)error forService:(Class)service {
    [super serverError:error forService:service];

    [self.contentView.tableView reloadData];
}

@end
