//
//  MAProductView.h
//  Maneki
//
//  Created by Максимычев Е.О. on 16.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseView.h"

@interface MAProductView : MABaseView

- (void)updateCountLabelByCount:(NSUInteger)count;

@end
