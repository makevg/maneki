//
//  MAProductView.m
//  Maneki
//
//  Created by Максимычев Е.О. on 16.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProductView.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "MABaseButton.h"
#import "MAAPI.h"
#import "Product.h"
#import "UIImageView+AsyncLoad.h"

@interface MAProductView ()

@property(weak, nonatomic) IBOutlet UIView *contentView;

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property(weak, nonatomic) IBOutlet UILabel *priceLabel;
@property(weak, nonatomic) IBOutlet UILabel *countLabel;

@property(weak, nonatomic) IBOutlet MABaseButton *minusButton;
@property(weak, nonatomic) IBOutlet MABaseButton *plusButton;
@property(weak, nonatomic) IBOutlet MABaseButton *addToCartButton;

@property(weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property(weak, nonatomic) IBOutlet UIImageView *bigPropertyImageView;
@property(weak, nonatomic) IBOutlet UIImageView *propertyImageView;

@end

@implementation MAProductView

#pragma mark - Setup

- (void)setup {
    self.backgroundColor = [UIColor clearColor];
    self.contentView.layer.cornerRadius = 5.f;
    self.titleLabel.textColor = [MAColorPalette blackColor];
    self.titleLabel.font = [MAFontSet productScreenTitleFont];
    self.descriptionLabel.textColor = [MAColorPalette grayColor];
    self.descriptionLabel.font = [MAFontSet productScreenDescriptionFont];
    self.priceLabel.textColor = [MAColorPalette orangeColor];
    self.priceLabel.font = [MAFontSet productScreenPriceFont];
    self.countLabel.font = [MAFontSet productCountLabelFont];
    [self setNormalState];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[Product class]]) {
        [self fillViewByProduct:model];
    }
}

- (void)fillViewByProduct:(Product *)product {
    self.titleLabel.text = product.title;
    self.descriptionLabel.text = product.descr;
    self.priceLabel.text = [NSString stringWithFormat:@"%@ р.", product.cost];
    [self.photoImageView loadAsyncFromUrl:[MANEKI_API urlWithPart:product.image_url] placeHolder:@"PlaceholderBig"];

    [self fillPropsByProduct:product];
}

- (void)fillPropsByProduct:(Product *)product {
    self.propertyImageView.hidden = YES;
    self.bigPropertyImageView.hidden = YES;

    if ([product.prop_hot boolValue]) {
        self.propertyImageView.image = [UIImage imageNamed:@"Hot"];
        self.propertyImageView.hidden = NO;
    } else if ([product.prop_hot2 boolValue]) {
        self.propertyImageView.image = [UIImage imageNamed:@"VeryHot"];
        self.propertyImageView.hidden = NO;
    } else if ([product.prop_veg boolValue]) {
        self.propertyImageView.image = [UIImage imageNamed:@"Veg"];
        self.propertyImageView.hidden = NO;
    }

    if ([product.prop_new boolValue]) {
        self.bigPropertyImageView.image = [UIImage imageNamed:@"NewBig"];
        self.bigPropertyImageView.hidden = NO;
    } else if ([product.prop_hit boolValue]) {
        self.bigPropertyImageView.image = [UIImage imageNamed:@"HitBig"];
        self.bigPropertyImageView.hidden = NO;
    }
}

- (void)setNormalState {
    self.countLabel.hidden = YES;
    self.plusButton.hidden = YES;
    self.minusButton.hidden = YES;
    self.addToCartButton.hidden = NO;
}

- (void)setAddToCartState {
    self.countLabel.hidden = NO;
    self.plusButton.hidden = NO;
    self.minusButton.hidden = NO;
    self.addToCartButton.hidden = YES;
}

#pragma mark - Public

- (void)updateCountLabelByCount:(NSUInteger)count {
    count > 0 ? [self setAddToCartState] : [self setNormalState];
    self.countLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long) count];
}

@end
