//
//  MAProductVC.h
//  Maneki
//
//  Created by Максимычев Е.О. on 16.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseVC.h"

@protocol MAProductVCDelegate;

@interface MAProductVC : MABaseVC

@property (nonatomic, strong) NSNumber *productPk;
@property (weak, nonatomic) id<MAProductVCDelegate> delegate;

@end

@protocol MAProductVCDelegate

- (void)tappedChangeProductQuantityButton:(MAProductVC *)controller;

@end
