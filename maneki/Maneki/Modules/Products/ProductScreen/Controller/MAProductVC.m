//
//  MAProductVC.m
//  Maneki
//
//  Created by Максимычев Е.О. on 16.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProductVC.h"
#import "MAProductView.h"
#import "MACartService.h"
#import "MACartItem.h"
#import "Product+CoreDataProperties.h"

NSString *const cProductStoryboardName = @"MAProduct";

@interface MAProductVC ()

@property (strong, nonatomic) IBOutlet MAProductView *productView;

@end

@implementation MAProductVC {
    NSUInteger productCount;
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cProductStoryboardName;
}

- (void)configureController {
    [self cancelsTouches:NO];
    [self.productView setModel:[MANEKI_SERVICE getProductByPk:self.productPk]];
    MACartItem *item = [CART_SERVICE getItemByProductId:self.productPk];
    productCount = (NSUInteger) item.quantity;
    [self.productView updateCountLabelByCount:productCount];
}

#pragma mark - Private

- (void)updateCart {
    [self.productView updateCountLabelByCount:productCount];
    __weak __typeof(&*self) weakSelf = self;

    PCAngularActivityIndicatorView *indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                               forView:self.view];
    [indicatorView startAnimating];
    self.view.userInteractionEnabled = NO;
    [CART_SERVICE updateProductId:self.productPk
                           amount:productCount
                   successHandler:^(NSUInteger productId, NSUInteger amount) {
                       __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                       if (strongSelf) {
                           if (indicatorView)
                               [indicatorView stopAnimating];

                           strongSelf.view.userInteractionEnabled = YES;

                           [strongSelf updateCartButton];

                           if (strongSelf.delegate) {
                               [strongSelf.delegate tappedChangeProductQuantityButton:strongSelf];
                           }
                       }
                   }
                   failureHandler:^(NSUInteger productId, NSUInteger amount, NSError *error) {
                       __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                       if (strongSelf) {
                           if (indicatorView)
                               [indicatorView stopAnimating];

                           strongSelf.view.userInteractionEnabled = YES;

                           [strongSelf showMessage:error.localizedDescription];
                           [strongSelf.productView updateCountLabelByCount:amount];

                           if (strongSelf.delegate) {
                               [strongSelf.delegate tappedChangeProductQuantityButton:strongSelf];
                           }
                       }
                   }];
}

#pragma mark - Actions

- (IBAction)tappedCloseButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)tappedPlusButton:(id)sender {
    productCount++;
    [self updateCart];
}

- (IBAction)tappedMinusButton:(id)sender {
    if (productCount > 0) {
        productCount--;
        [self updateCart];
    }
}

- (IBAction)tappedAddToCartButton:(id)sender {
    productCount = 1;
    [self updateCart];
}

@end
