//
//  MAProductsVC.h
//  Maneki
//
//  Created by Maximychev Evgeny on 06.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseVC.h"
#import "MABaseCollectionVC.h"

@interface MAProductsVC : MABaseCollectionVC

@property (nonatomic) NSString *code;
@property (nonatomic) NSString *navBarTitle;

@end
