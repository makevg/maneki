//
//  MAProductsVC.m
//  Maneki
//
//  Created by Maximychev Evgeny on 06.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <SDWebImage/SDWebImageManager.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "MAProductsVC.h"
#import "MAProductsView.h"
#import "MAProductCell.h"
#import "MAProductVC.h"
#import "Product+CoreDataProperties.h"
#import "MAFontSet.h"
#import "MACartService.h"
#import "MACartItem.h"

NSString *const cProductsStoryboardName = @"MAProducts";
NSString *const cCoffeeCode = @"kofe";

@interface MAProductsVC () <
        UICollectionViewDelegate,
        MAProductCellDelegate, MAProductVCDelegate,
        DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, ServiceResultDelegate>
@property(strong, nonatomic) IBOutlet MAProductsView *contentView;
@end

@implementation MAProductsVC {
    NSIndexPath *selectedIndexPath;
    CGFloat m_screenWidth;
}

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.contentView.collectionView reloadData];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cProductsStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (void)configureController {
    [self setTitle:self.navBarTitle];

    m_screenWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]);

    [self setDelegatesAndDataSource];
    [self cancelsTouches:NO];
    [self requestData];
}

- (void)setDelegatesAndDataSource {
    self.contentView.collectionView.dataSource = (id <UICollectionViewDataSource>) self;
    self.contentView.collectionView.delegate = self;
    self.contentView.collectionView.emptyDataSetSource = self;
    self.contentView.collectionView.emptyDataSetDelegate = self;
}

- (UICollectionView *)getCollectionView {
    return self.contentView.collectionView;
}

- (void)requestData {
    [super requestData];

    [MANEKI_SERVICE getProductsByCategoryCode:self.code
                                     receiver:^(NSFetchedResultsController *controller) {
                                         self.fetchResult = controller;
                                         self.fetchResult.delegate = (id <NSFetchedResultsControllerDelegate>) self;

                                         [self.contentView.collectionView reloadData];
                                     }
                                     delegate:self];
}

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MAProductCell *cell = [cv dequeueReusableCellWithReuseIdentifier:[MAProductCell cellIdentifier]
                                                        forIndexPath:indexPath];
    cell.delegate = self;
    Product *product = [self.fetchResult objectAtIndexPath:indexPath];
    [cell setModel:product];
    [cell updateCountLabelByCount:(NSUInteger) [[CART_SERVICE getItemByProductId:product.id] quantity]];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndexPath = indexPath;
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[MAProductVC storyboardName] bundle:nil];
    MAProductVC *vc = [sb instantiateInitialViewController];
    Product *product = [self.fetchResult objectAtIndexPath:indexPath];
    vc.productPk = product.id;
    vc.delegate = self;
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellWidth = m_screenWidth / 2 - 0.5f;
    CGFloat cellHeight = 250.f;
    return CGSizeMake(cellWidth, cellHeight);
}

#pragma mark - MAProductCellDelegate

- (void)cell:(MAProductCell *)cell updateProductId:(NSNumber *)prodId quantity:(NSUInteger)quantity {
    __weak __typeof(&*self) weakSelf = self;
    __weak __typeof(&*cell) weakCell = cell;

    PCAngularActivityIndicatorView *indicatorView = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleSmall
                                                                               forView:cell];
    [indicatorView startAnimating];
    cell.userInteractionEnabled = NO;

    [CART_SERVICE updateProductId:prodId
                           amount:quantity
                   successHandler:^(NSUInteger productId, NSUInteger amount) {
                       __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                       if (strongSelf)
                           [strongSelf updateCartButton];

                       __strong __typeof(&*weakCell) strongCell = weakCell;
                       if (strongCell) {
                           if (indicatorView)
                               [indicatorView stopAnimating];

                           strongCell.userInteractionEnabled = YES;
                           [strongCell updateCountLabelByCount:amount];
                       }

                   }
                   failureHandler:^(NSUInteger productId, NSUInteger amount, NSError *error) {
                       __strong __typeof(&*weakSelf) strongSelf = weakSelf;

                       if (strongSelf) {
                           [strongSelf showMessage:error.localizedDescription];
                       }

                       __strong __typeof(&*weakCell) strongCell = weakCell;
                       if (strongCell) {
                           if (indicatorView)
                               [indicatorView stopAnimating];

                           strongCell.userInteractionEnabled = YES;
                           [strongCell updateCountLabelByCount:amount];
                       }
                   }];
}

- (void)cell:(MAProductCell *)cell tappedPlusButtonWithProductId:(NSNumber *)prodId quantity:(NSUInteger)quantity {
    [self cell:cell updateProductId:prodId quantity:quantity];
}

- (void)cell:(MAProductCell *)cell tappedMinusButtonWithProductId:(NSNumber *)prodId quantity:(NSUInteger)quantity {
    [self cell:cell updateProductId:prodId quantity:quantity];
}

- (void)cell:(MAProductCell *)cell tappedAddToCartButtonWithProductId:(NSNumber *)prodId {
    [self cell:cell updateProductId:prodId quantity:1];
}

#pragma mark - MAProductVCDelegate

- (void)tappedChangeProductQuantityButton:(MAProductVC *)controller {
    [self updateCartButton];
    [self.contentView.collectionView reloadItemsAtIndexPaths:@[selectedIndexPath]];
}


#pragma mark - DZNEmptyDataSetSource

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    if ([self isLoading])
        return nil;

    if (![self requestErrorDescription] && [self.code isEqualToString:cCoffeeCode]) {
        return [UIImage imageNamed:@"CofeLogo"];
    }
    return [super imageForEmptyDataSet:scrollView];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    if ([self isLoading])
        return nil;

    if (![self requestErrorDescription] && [self.code isEqualToString:cCoffeeCode]) {
        NSString *text = @"Кофе можно взять с собой (не действует при доставке с курьером)";

        NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
        paragraph.lineBreakMode = NSLineBreakByWordWrapping;
        paragraph.alignment = NSTextAlignmentCenter;

        NSDictionary *attributes = @{NSFontAttributeName : [MAFontSet navBatTitleFont],
                NSForegroundColorAttributeName : [UIColor blackColor],
                NSParagraphStyleAttributeName : paragraph};

        return [[NSAttributedString alloc] initWithString:text attributes:attributes];
    }

    return [super descriptionForEmptyDataSet:scrollView];
}

- (void)successResult:(id)object forService:(Class)service {
    [super successResult:object forService:service];

    if( [self.code isEqualToString:cCoffeeCode] ) {
        [[self getCollectionView] reloadData];
    }
}

@end