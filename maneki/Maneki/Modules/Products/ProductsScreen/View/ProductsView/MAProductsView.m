//
//  MAProductsView.m
//  Maneki
//
//  Created by Maximychev Evgeny on 06.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProductsView.h"
#import "MAColorPalette.h"

@interface MAProductsView ()

@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@end

@implementation MAProductsView

#pragma mark - Setup

- (void)setup {
    [self.collectionView setBackgroundColor:[MAColorPalette lightGrayColor]];
    self.flowLayout.minimumLineSpacing = 1;
    self.flowLayout.minimumInteritemSpacing = 1;
}

@end
