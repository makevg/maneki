//
//  MAProductCell.m
//  Maneki
//
//  Created by Maximychev Evgeny on 06.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import "MAProductCell.h"
#import "MAColorPalette.h"
#import "MAFontSet.h"
#import "Product.h"
#import "MAAPI.h"
#import "UIImageView+AsyncLoad.h"
#import "MABaseButton.h"

NSString *const cProductCellIdentifier = @"MAProductCell";

@interface MAProductCell ()

@property(weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property(weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property(weak, nonatomic) IBOutlet UILabel *productCountLabel;

@property(weak, nonatomic) IBOutlet MABaseButton *minusButton;
@property(weak, nonatomic) IBOutlet MABaseButton *plusButton;
@property(weak, nonatomic) IBOutlet MABaseButton *addToCartButton;

@property(weak, nonatomic) IBOutlet UIImageView *productImageView;
@property(weak, nonatomic) IBOutlet UIImageView *topPropertyImageView;
@property(weak, nonatomic) IBOutlet UIImageView *bottomPropertyImageView;

@end

@implementation MAProductCell {
    NSUInteger productQuantity;
    NSNumber *productId;
}

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return cProductCellIdentifier;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[Product class]]) {
        [self fillCellByProduct:model];
    }
}

- (void)configureCell {
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    productQuantity = 0;
    [self updateCountLabelByCount:productQuantity];
    [self setBackgroundColor:[MAColorPalette whiteColor]];
    self.productNameLabel.font = [MAFontSet productNameLabelFont];
    self.productPriceLabel.font = [MAFontSet productPriceLabelFont];
    self.productCountLabel.font = [MAFontSet productCountLabelFont];
}

- (void)updateCountLabelByCount:(NSUInteger)count {
    count > 0 ? [self setAddToCartState] : [self setNormalState];
    self.productCountLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long) count];
    productQuantity = count;
}

#pragma mark - Private

- (void)setNormalState {
    self.productCountLabel.hidden = YES;
    self.plusButton.hidden = YES;
    self.minusButton.hidden = YES;
    self.addToCartButton.hidden = NO;
}

- (void)setAddToCartState {
    self.productCountLabel.hidden = NO;
    self.plusButton.hidden = NO;
    self.minusButton.hidden = NO;
    self.addToCartButton.hidden = YES;
}

- (void)fillCellByProduct:(Product *)product {
    self.productNameLabel.text = product.title;
    self.productPriceLabel.text = [NSString stringWithFormat:@"%@ р.", product.cost];
    [self.productImageView loadAsyncFromUrl:[MANEKI_API urlWithPart:product.preview_image_url]];
    [self fillPropsByProduct:product];
    productId = product.id;
}

- (void)fillPropsByProduct:(Product *)product {
    self.bottomPropertyImageView.hidden = YES;
    self.topPropertyImageView.hidden = YES;

    if ([product.prop_hot boolValue]) {
        self.bottomPropertyImageView.image = [UIImage imageNamed:@"Hot"];
        self.bottomPropertyImageView.hidden = NO;
    } else if ([product.prop_hot2 boolValue]) {
        self.bottomPropertyImageView.image = [UIImage imageNamed:@"VeryHot"];
        self.bottomPropertyImageView.hidden = NO;
    } else if ([product.prop_veg boolValue]) {
        self.bottomPropertyImageView.image = [UIImage imageNamed:@"Veg"];
        self.bottomPropertyImageView.hidden = NO;
    }

    if ([product.prop_new boolValue]) {
        self.topPropertyImageView.image = [UIImage imageNamed:@"New"];
        self.topPropertyImageView.hidden = NO;
    } else if ([product.prop_hit boolValue]) {
        self.topPropertyImageView.image = [UIImage imageNamed:@"Hit"];
        self.topPropertyImageView.hidden = NO;
    }
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    return layoutAttributes;
}

#pragma mark - Actions

- (IBAction)tappedPlusButton:(id)sender {
    productQuantity++;
    [self updateCountLabelByCount:productQuantity];
    if (self.delegate) {
        [self.delegate cell:self tappedPlusButtonWithProductId:productId quantity:productQuantity];
    }
}

- (IBAction)tappedMinusButton:(id)sender {
    if (productQuantity > 0) {
        productQuantity--;
        [self updateCountLabelByCount:productQuantity];
        if (self.delegate) {
            [self.delegate cell:self tappedMinusButtonWithProductId:productId quantity:productQuantity];
        }
    }
}

- (IBAction)tappedAddToCartButton:(id)sender {
    productQuantity = 1;
    [self updateCountLabelByCount:productQuantity];
    if (self.delegate) {
        [self.delegate cell:self tappedAddToCartButtonWithProductId:productId];
    }
}

@end
