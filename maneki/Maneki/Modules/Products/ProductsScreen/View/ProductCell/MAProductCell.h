//
//  MAProductCell.h
//  Maneki
//
//  Created by Maximychev Evgeny on 06.12.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MABaseCollectionViewCell.h"

@protocol MAProductCellDelegate;

@interface MAProductCell : MABaseCollectionViewCell

@property (weak, nonatomic) id<MAProductCellDelegate> delegate;

- (void)updateCountLabelByCount:(NSUInteger)count;

@end

@protocol MAProductCellDelegate

- (void)cell:(MAProductCell *)cell tappedPlusButtonWithProductId:(NSNumber *)prodId quantity:(NSUInteger)quantity;
- (void)cell:(MAProductCell *)cell tappedMinusButtonWithProductId:(NSNumber *)prodId quantity:(NSUInteger)quantity;
- (void)cell:(MAProductCell *)cell tappedAddToCartButtonWithProductId:(NSNumber *)prodId;

@end
