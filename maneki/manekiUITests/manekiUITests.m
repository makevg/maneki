//
//  ManekiUITests.m
//  ManekiUITests
//
//  Created by Buravlev Mikhail on 15.11.15.
//  Copyright © 2015 Maneki. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ManekiUITests-Swift.h"

@interface ManekiUITests : XCTestCase

@end

@implementation ManekiUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [Snapshot setLanguage:app];
    [app launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testExample {
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    XCUIElement *masterNavigationBar = app.navigationBars[@"Master"];
    [masterNavigationBar.buttons[@"Add"] tap];
    
//    XCUIElementQuery *tablesQuery = app.tables;
//    XCUIElement* table = [tablesQuery.tables elementBoundByIndex:0];
//    [[[table.cells elementBoundByIndex:0].staticTexts elementBoundByIndex:0] tap];
//    
//    [[app.navigationBars matchingIdentifier:@"Detail"].buttons[@"Master"] tap];
    [masterNavigationBar.buttons[@"Edit"] tap];
    
//    [tablesQuery.buttons[@"Delete 2015-11-14 22:31:34 +0000"] tap];
//    
//    XCUIElement *deleteButton = tablesQuery.buttons[@"Delete"];
//    [deleteButton tap];
//    [tablesQuery.buttons[@"Delete 2015-11-14 22:32:28 +0000"] tap];
//    [deleteButton tap];
    [masterNavigationBar.buttons[@"Done"] tap];
    // Use recording to get started writing UI tests.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    [Snapshot snapshot:@"01LoginScreen" waitForLoadingIndicator:YES];
}

@end
